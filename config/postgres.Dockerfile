FROM postgres:15.4

COPY config/postgres_init.sql /docker-entrypoint-initdb.d/postgres_init.sql