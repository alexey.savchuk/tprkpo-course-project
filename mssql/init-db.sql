EXEC sp_configure 'clr enable', 1;
RECONFIGURE;
EXEC sp_configure 'advanced options', 1;
RECONFIGURE;
EXEC sp_configure 'clr strict security', 0;
RECONFIGURE;

CREATE DATABASE course_research;