﻿using Microsoft.SqlServer.Server;
using System.Text.RegularExpressions;

namespace matchers;

public static partial class Matchers
{
    [SqlFunction(IsDeterministic = true, IsPrecise = true)]
    public static bool RegExMatch(string pattern, string matchString)
    {
        var re = new Regex(pattern.TrimEnd(null));
        return re.Match(matchString.TrimEnd(null)).Success;
    }
}