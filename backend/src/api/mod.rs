use axum::{extract::FromRef, routing, Router};

use crate::{
    auth::extract::{OryApiStateProvider},
    db::{extract::DatabaseStateProvider},
    snowflake::SnowflakeManager,
};

pub mod accounts;
pub mod admin;
pub mod dto;
mod error;
pub mod global_search;
pub mod organisations;
pub mod participants;
pub mod publications;
pub mod roles;

pub use error::{ApiError, ErrorMessage, Result};

use self::global_search::global_search;

#[cfg(not(tarpaulin_include))]
pub fn routes<S>() -> Router<S>
where
    SnowflakeManager: FromRef<S>,
    S: DatabaseStateProvider + OryApiStateProvider + Send + Sync + Clone + 'static,
{
    Router::new()
        .nest("/admin", admin::routes())
        .nest("/accounts", accounts::routes())
        .nest("/organisations", organisations::routes())
        .nest("/publications", publications::routes())
        .nest("/participants", participants::routes())
        .nest("/roles", roles::routes())
        .route("/search", routing::get(global_search))
}
