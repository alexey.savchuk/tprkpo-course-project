use axum::{debug_handler, extract::Query, Json};
use tracing::instrument;

use super::{dto, Result};
#[cfg(debug_assertions)]
use crate::server::AppState;
use crate::{db::extract::Database, snowflake::extract::SnowflakeState};

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn global_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::GlobalSearch>>> {
    let res = db
        .global_search()
        .global_search(&query.search, query.n as i64)
        .await?;
    let res = res
        .into_iter()
        .map(|gs| Ok(dto::GlobalSearch::from_db(&snowflake, gs)?))
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use axum::{extract::Query, Json};
    use mockall::predicate;
    use spectral::prelude::*;
    use tracing_test::traced_test;

    use crate::{
        api::dto,
        db::{self, extract::Database},
        snowflake::{self, extract::SnowflakeState, SnowflakeInternal},
    };

    #[tokio::test]
    #[traced_test]
    async fn global_search() {
        let snowflake = snowflake::test::snowflake_manager();

        let result = vec![
            db::GlobalSearch::Publication {
                item_id: SnowflakeInternal::from_i32(1),
                item_value: "something 1".to_string(),
            },
            db::GlobalSearch::Organisation {
                item_id: SnowflakeInternal::from_i32(1),
                item_value: "something 2".to_string(),
            },
            db::GlobalSearch::Participant {
                item_id: SnowflakeInternal::from_i32(1),
                item_value: "something 3".to_string(),
            },
            db::GlobalSearch::SubjectCode {
                item_id: SnowflakeInternal::from_i32(1),
                item_value: "something 4".to_string(),
            },
        ];
        let result_to_return = result.clone();

        let mut db = db::MockDatabase::new();
        db.global_search
            .expect_global_search()
            .times(1)
            .with(predicate::eq("something"), predicate::eq(10))
            .returning(move |_, _| Ok(result_to_return.clone()));

        let Json(got_result) = super::global_search(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Query(dto::FuzzySearchQuery {
                search: "something".to_string(),
                n: 10,
            }),
        )
        .await
        .unwrap();

        let result_to_compare = result
            .into_iter()
            .map(|gs| dto::GlobalSearch::from_db(&snowflake, gs).unwrap())
            .collect::<Vec<_>>();

        assert_that!(got_result).contains_all_of(&&result_to_compare);
    }
}
