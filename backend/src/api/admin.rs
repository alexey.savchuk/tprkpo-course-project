use axum::{debug_handler, extract::FromRef, routing, Json, Router};
use itertools::Itertools;
use serde::Deserialize;
use tracing::instrument;

use super::Result;
#[cfg(debug_assertions)]
use crate::server::AppState;
use crate::{
    auth::{
        self,
        extract::{ApplicationAdmin, OryApi, OryApiStateProvider, PermissionState},
    },
    db::extract::{Database, DatabaseStateProvider},
    snowflake::{extract::SnowflakeState, SnowflakeConverter, SnowflakeManager},
};

#[instrument(skip(ory, db))]
#[debug_handler(state = AppState)]
pub async fn get_admins(
    OryApi(ory): OryApi,
    Database(db): Database,
    _app_admin: ApplicationAdmin,
) -> Result<Json<Vec<auth::OryIdentity>>> {
    let app_admins = db
        .application_participation()
        .get_application_admins()
        .await?;
    let app_admins = ory
        .list_identities_for_ids(
            app_admins
                .into_iter()
                .map(|part| part.user_id)
                .collect_vec(),
        )
        .await?;
    Ok(Json(app_admins))
}

#[derive(Debug, Deserialize)]
pub struct AddAdminParams {
    user_id: auth::OryIdentity,
}

#[instrument(skip(snowflake, db, ory))]
#[debug_handler(state = AppState)]
pub async fn add_admin(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    OryApi(ory): OryApi,
    PermissionState {
        application_admin_role,
        ..
    }: PermissionState,
    Json(AddAdminParams { user_id }): Json<AddAdminParams>,
) -> Result<http::StatusCode> {
    let _check_identity_exists = ory.get_identity_by_id(&user_id.id).await?;
    let _insert_participation = db
        .application_participation()
        .insert_new_participation(
            &user_id,
            &snowflake.from_internal(application_admin_role.id)?,
        )
        .await?;
    Ok(http::StatusCode::NO_CONTENT)
}

#[instrument(skip(snowflake, db, ory))]
#[debug_handler(state = AppState)]
pub async fn remove_admin(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    OryApi(ory): OryApi,
    PermissionState {
        application_admin_role,
        ..
    }: PermissionState,
    Json(AddAdminParams { user_id }): Json<AddAdminParams>,
) -> Result<http::StatusCode> {
    let _check_identity_exists = ory.get_identity_by_id(&user_id.id).await?;
    let _insert_participation = db
        .application_participation()
        .stop_participation(
            &user_id,
            &snowflake.from_internal(application_admin_role.id)?,
        )
        .await?;
    Ok(http::StatusCode::NO_CONTENT)
}

#[cfg(not(tarpaulin_include))]
pub fn routes<S>() -> Router<S>
where
    SnowflakeManager: FromRef<S>,
    S: DatabaseStateProvider + OryApiStateProvider + Send + Sync + Clone + 'static,
{
    Router::new()
        .route("/add", routing::post(add_admin))
        .route("/remove", routing::post(remove_admin))
        .route("/list", routing::get(get_admins))
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use axum::Json;
    use chrono::Local;

    use mockall::predicate;
    use spectral::prelude::*;
    use tracing_test::traced_test;

    use crate::{
        auth::{
            self,
            extract::{ApplicationAdmin, OryApi, PermissionState},
            test::generate_identity_with_id,
            MockOryApi,
        },
        db::{self, extract::Database},
        snowflake::{self, extract::SnowflakeState, SnowflakeConverter, SnowflakeInternal},
    };

    #[tokio::test]
    #[traced_test]
    async fn list_successful() {
        let user_id = "1";
        let role_id = SnowflakeInternal::from_i32(2);

        let application_participants = db::ApplicationParticipation {
            user_id: user_id.to_string(),
            role_id: role_id.clone(),
        };
        let application_participants_to_return = application_participants.clone();

        let identity = generate_identity_with_id("id".to_string());
        let identity_for_mock = identity.clone();

        let mut db = db::MockDatabase::new();
        let mut ory_api = MockOryApi::new();

        db.application_participation
            .expect_get_application_admins()
            .times(1)
            .return_once(move || Ok(vec![application_participants_to_return]));

        ory_api
            .expect_list_identities_for_ids()
            .with(predicate::eq(vec![user_id.to_string()]))
            .times(1)
            .return_once(move |_| Ok(vec![identity_for_mock]));

        let Json(actual_ids) = super::get_admins(
            OryApi(Arc::new(ory_api)),
            Database(Arc::new(db)),
            ApplicationAdmin(()),
        )
        .await
        .unwrap();

        assert_that!(actual_ids).contains(identity);
    }

    #[tokio::test]
    #[traced_test]
    async fn add_successful() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();
        let mut ory_api = MockOryApi::new();

        let admin_role_id = SnowflakeInternal::from_i32(66);
        let user_id = "1";

        let ory_identity = auth::OryIdentity {
            created_at: Some("12:00".to_string()),
            credentials: None,
            id: user_id.to_string(),
            metadata_admin: None,
            metadata_public: None,
            organization_id: None,
            recovery_addresses: None,
            schema_id: "schema".to_string(),
            schema_url: "schema_url".to_string(),
            state: None,
            state_changed_at: None,
            traits: None,
            updated_at: None,
            verifiable_addresses: None,
        };
        let ory_identity_to_return = ory_identity.clone();

        let application_participant = db::ApplicationParticipation {
            user_id: user_id.to_string(),
            role_id: admin_role_id.clone(),
        };

        let applicatoin_role = db::ApplicationRole {
            id: admin_role_id.clone(),
            created_at: Local::now().naive_local(),
            name: "admin".to_string(),
        };
        let application_role_to_return = applicatoin_role.clone();

        let org_role_id = SnowflakeInternal::from_i32(1);
        let organisation_role = db::OrganisationRole {
            id: org_role_id.clone(),
            created_at: Local::now().naive_local(),
            name: "admin".to_string(),
        };

        let publication_role_id = SnowflakeInternal::from_i32(1);
        let publication_role = db::PublicationRole {
            id: publication_role_id.clone(),
            created_at: Local::now().naive_local(),
            name: "admin".to_string(),
        };

        ory_api
            .expect_get_identity_by_id()
            .times(1)
            .with(predicate::eq(user_id.to_string()))
            .returning(move |_| Ok(ory_identity_to_return.clone()));

        let admin_role_id_with = snowflake.from_internal(admin_role_id).unwrap();
        db.application_participation
            .expect_insert_new_participation()
            .times(1)
            .with(
                predicate::eq(ory_identity.clone()),
                predicate::eq(admin_role_id_with),
            )
            .returning(move |_, _| Ok(application_participant.clone()));

        let result = super::add_admin(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            OryApi(Arc::new(ory_api)),
            PermissionState {
                application_admin_role: application_role_to_return.clone(),
                organisation_admin_role: organisation_role.clone(),
                publication_admin_role: publication_role,
            },
            Json(super::AddAdminParams {
                user_id: ory_identity.clone(),
            }),
        )
        .await
        .unwrap();

        assert_eq!(result, http::StatusCode::NO_CONTENT);
    }

    #[tokio::test]
    #[traced_test]
    async fn remove_admin_successful() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();
        let mut ory_api = MockOryApi::new();

        let ory_identity = auth::test::generate_identity_with_id(uuid::Uuid::new_v4().to_string());
        let ory_dentity_to_return = ory_identity.clone();

        let admin_id = SnowflakeInternal::from_i32(1);
        let application_admin_role = db::ApplicationRole {
            id: admin_id.clone(),
            created_at: Local::now().naive_local(),
            name: "admin".to_string(),
        };

        let application_participation = db::ApplicationParticipation {
            user_id: ory_identity.id.clone(),
            role_id: admin_id.clone(),
        };
        let application_participation_to_return = application_participation.clone();

        let org_role_id = SnowflakeInternal::from_i32(1);
        let organisation_role = db::OrganisationRole {
            id: org_role_id.clone(),
            created_at: Local::now().naive_local(),
            name: "admin".to_string(),
        };

        let publication_role_id = SnowflakeInternal::from_i32(1);
        let publication_role = db::PublicationRole {
            id: publication_role_id.clone(),
            created_at: Local::now().naive_local(),
            name: "admin".to_string(),
        };

        ory_api
            .expect_get_identity_by_id()
            .times(1)
            .with(predicate::eq(ory_identity.id.clone()))
            .returning(move |_| Ok(ory_dentity_to_return.clone()));

        db.application_participation
            .expect_stop_participation()
            .times(1)
            .with(
                predicate::eq(ory_identity.clone()),
                predicate::eq(snowflake.from_internal(admin_id).unwrap()),
            )
            .return_once(move |_, _| Ok(application_participation_to_return));

        let result = super::remove_admin(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            OryApi(Arc::new(ory_api)),
            PermissionState {
                application_admin_role: application_admin_role.clone(),
                organisation_admin_role: organisation_role,
                publication_admin_role: publication_role,
            },
            Json(super::AddAdminParams {
                user_id: ory_identity.clone(),
            }),
        )
        .await
        .unwrap();

        assert_eq!(result, http::StatusCode::NO_CONTENT);
    }
}
