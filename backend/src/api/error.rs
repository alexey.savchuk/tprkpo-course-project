use axum::{http::StatusCode, response::IntoResponse, Json};
use serde::Serialize;
use thiserror::Error;

use crate::db::DbError;

#[derive(Debug, Error)]
pub enum ApiError {
    #[error(transparent)]
    TokioRecvError(#[from] tokio::sync::oneshot::error::RecvError),
    #[error(transparent)]
    AxumExtensionError(#[from] axum::extract::rejection::ExtensionRejection),
    #[error(transparent)]
    AxumPathError(#[from] axum::extract::rejection::PathRejection),
    #[error(transparent)]
    SnowflakeError(#[from] crate::snowflake::SnowflakeError),
    #[error(transparent)]
    DtoError(#[from] super::dto::DtoError),
    #[error(transparent)]
    DbError(#[from] crate::db::DbError),
    #[error(transparent)]
    AuthError(#[from] crate::auth::AuthError),
    #[error(transparent)]
    TraitError(#[from] crate::auth::TraitError),
}

impl ApiError {
    fn status_code(&self) -> StatusCode {
        match self {
            Self::DbError(DbError::NoEntity { .. }) => StatusCode::NOT_FOUND,
            Self::SnowflakeError(e) => e.status_code(),
            Self::DtoError(e) => e.status_code(),
            Self::AuthError(err) => err.status_code(),
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[derive(Debug, Serialize)]
pub struct ErrorMessage {
    message: String,
}

impl IntoResponse for ErrorMessage {
    fn into_response(self) -> axum::response::Response {
        Json(self).into_response()
    }
}

impl From<ApiError> for ErrorMessage {
    fn from(err: ApiError) -> Self {
        Self {
            message: err.to_string(),
        }
    }
}

impl IntoResponse for ApiError {
    fn into_response(self) -> axum::response::Response {
        (self.status_code(), Json(ErrorMessage::from(self))).into_response()
    }
}

pub type Result<T> = std::result::Result<T, ApiError>;


#[cfg(test)]
mod tests {
    use crate::{auth, db};

    #[test]
    fn get_status_code() {
        let err = super::ApiError::DbError(
            db::DbError::NoEntity { entity: "", id: "".to_string() },
        );
        assert_eq!(err.status_code(), http::StatusCode::NOT_FOUND);

        let err = super::ApiError::TraitError(
            auth::TraitError::NoTraitsError
        );
        assert_eq!(err.status_code(), http::StatusCode::INTERNAL_SERVER_ERROR);
    }

}
