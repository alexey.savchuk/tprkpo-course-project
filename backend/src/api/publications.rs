use std::collections::HashMap;

use axum::{
    debug_handler,
    extract::{FromRef, Path, Query},
    routing::{get, post},
    Json, Router,
};
use serde::{Deserialize, Serialize};
use serde_with::serde_as;
use tracing::instrument;

use super::{dto, Result};
#[cfg(debug_assertions)]
use crate::server::AppState;
use crate::{
    auth::extract::{ConnectedAcademicParticipant, OryApiStateProvider, PublicationAdmin},
    db::{
        self,
        extract::{Database, DatabaseStateProvider},
    },
    snowflake::{
        extract::SnowflakeState, Snowflake, SnowflakeConverter, SnowflakeExternal, SnowflakeManager,
    },
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PubId {
    pub pub_id: SnowflakeExternal<db::Publication>,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn publications_list_all(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
) -> Result<Json<Vec<dto::Publication>>> {
    let publications = db.publication().list_all().await?;
    let res = publications
        .into_iter()
        .map(|(publication, code)| Ok(dto::Publication::from_db(&snowflake, publication, code)?))
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn publications_fuzzy_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::Publication>>> {
    let publications = db
        .publication()
        .fuzzy_search_on_title(&query.search, query.n as i64)
        .await?;
    let res = publications
        .into_iter()
        .map(|(publication, code)| Ok(dto::Publication::from_db(&snowflake, publication, code)?))
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn subject_codes_fuzzy_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::SubjectCode>>> {
    let codes = db
        .subject_code()
        .fuzzy_search(&query.search, query.n as i64)
        .await?;
    let res = codes
        .into_iter()
        .map(|code| Ok(dto::SubjectCode::from_db(&snowflake, code)?))
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn publication_get_by_id(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(PubId { pub_id }): Path<PubId>,
) -> Result<Json<dto::Publication>> {
    let (publication, code) = db
        .publication()
        .ensure(&snowflake.from_external(pub_id)?)
        .await?;
    let res = dto::Publication::from_db(&snowflake, publication, code)?;
    Ok(Json(res))
}

#[instrument(skip())]
#[debug_handler(state = AppState)]
pub async fn publication_get_meta(
    maybe_publication_admin: Option<PublicationAdmin>,
) -> Result<Json<dto::PublicationMeta>> {
    let editable = maybe_publication_admin.is_some();
    let res = dto::PublicationMeta { editable };
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn publication_participants(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(PubId { pub_id }): Path<PubId>,
) -> Result<Json<Vec<dto::PublicationParticipant>>> {
    let participants = db
        .publication_participation()
        .get_participants_for_publication(&snowflake.from_external(pub_id)?)
        .await?;
    let res = participants
        .into_iter()
        .map(|(participant, role)| {
            Ok(dto::PublicationParticipant::from_db(
                &snowflake,
                participant,
                role,
            )?)
        })
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn publication_organisations(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(PubId { pub_id }): Path<PubId>,
) -> Result<Json<Vec<dto::Organisation>>> {
    let organisations = db
        .publication_participation()
        .get_organisations_for_publication(&snowflake.from_external(pub_id)?)
        .await?;
    let res = organisations
        .into_iter()
        .map(|org| Ok(dto::Organisation::from_db(&snowflake, org)?))
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn publication_references(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(PubId { pub_id }): Path<PubId>,
) -> Result<Json<dto::PublicationReference>> {
    let pub_id = snowflake.from_external(pub_id)?;
    let to_references = db
        .publication_reference()
        .get_publications_to_id(&pub_id)
        .await?;
    let from_references = db
        .publication_reference()
        .get_publications_from_id(&pub_id)
        .await?;
    let to_references = to_references
        .into_iter()
        .map(|(publication, code)| Ok(dto::Publication::from_db(&snowflake, publication, code)?))
        .collect::<Result<_>>()?;
    let from_references = from_references
        .into_iter()
        .map(|(publication, code)| Ok(dto::Publication::from_db(&snowflake, publication, code)?))
        .collect::<Result<_>>()?;
    let res = dto::PublicationReference {
        to: to_references,
        from: from_references,
    };
    Ok(Json(res))
}

#[derive(Debug, Deserialize)]
pub struct ParticipantWithRole {
    id: SnowflakeExternal<db::AcademicParticipant>,
    role: dto::ParticipationRole,
}

#[derive(Debug, Deserialize)]
pub struct NewParticipantWithRole {
    name: String,
    role: dto::ParticipationRole,
}

#[serde_with::serde_as]
#[derive(Debug, Deserialize)]
pub struct CreatePublicationRequest {
    title: String,
    #[serde_as(as = "serde_with::TimestampMilliSeconds<i64>")]
    published_at: chrono::DateTime<chrono::Utc>,
    subject_code: dto::SubjectCode,
    known_participants: Vec<ParticipantWithRole>,
    new_participants: Vec<NewParticipantWithRole>,
    references: Vec<dto::Publication>,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn publication_create_new(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    ConnectedAcademicParticipant(referrer): ConnectedAcademicParticipant,
    Json(req): Json<CreatePublicationRequest>,
) -> Result<Json<dto::Publication>> {
    let author_role = db.publication_role().get_author().await?;
    let referrer = snowflake.from_internal(referrer.id)?;

    let CreatePublicationRequest {
        title,
        published_at,
        subject_code,
        known_participants,
        new_participants,
        references,
    } = req;
    let published_at = published_at.date_naive();
    let publication = db
        .publication()
        .insert_new(
            &title,
            &published_at,
            &snowflake.from_external(subject_code.id)?,
        )
        .await?;
    let participants: HashMap<Snowflake<db::AcademicParticipant>, Snowflake<db::PublicationRole>> = {
        let mut pars = HashMap::new();
        pars.insert(referrer.clone(), snowflake.from_internal(author_role.id)?);
        for ParticipantWithRole { id, role } in known_participants.into_iter() {
            pars.insert(
                snowflake.from_external(id)?,
                snowflake.from_external(role.id)?,
            );
        }
        for NewParticipantWithRole { name, role } in new_participants.into_iter() {
            let par = db.academic_participants().insert_new(&name).await?;
            pars.insert(
                snowflake.from_internal(par.id)?,
                snowflake.from_external(role.id)?,
            );
        }
        pars
    };
    for (subject, role) in participants.into_iter() {
        let state = if subject == referrer {
            db::ParticipationState::Active
        } else {
            db::ParticipationState::Pending
        };
        db.publication_participation()
            .insert_new_participation(
                &snowflake.from_internal(publication.id)?,
                &role,
                &subject,
                &referrer,
                &state,
            )
            .await?;
    }
    for reference in references.into_iter() {
        let from_id = snowflake.from_internal(publication.id)?;
        let to_id = snowflake.from_external(reference.id)?;
        let _ = db
            .publication_reference()
            .insert_new(&from_id, &to_id)
            .await?;
    }
    let (publication, code) = db
        .publication()
        .ensure(&snowflake.from_internal(publication.id)?)
        .await?;
    let publication = dto::Publication::from_db(&snowflake, publication, code)?;
    Ok(Json(publication))
}

#[cfg(not(tarpaulin_include))]
pub fn routes<S>() -> Router<S>
where
    SnowflakeManager: FromRef<S>,
    S: DatabaseStateProvider + OryApiStateProvider + Send + Sync + Clone + 'static,
{
    Router::new()
        .route("/create", post(publication_create_new))
        .route("/all", get(publications_list_all))
        .route("/search", get(publications_fuzzy_search))
        .route("/subject_codes/search", get(subject_codes_fuzzy_search))
        .route("/:pub_id", get(publication_get_by_id))
        .route("/:pub_id/meta", get(publication_get_meta))
        .route("/:pub_id/participants", get(publication_participants))
        .route("/:pub_id/organisations", get(publication_organisations))
        .route("/:pub_id/references", get(publication_references))
}

#[cfg(test)]
mod publication_tests {
    use std::sync::Arc;

    use super::*;
    use axum::{extract::Path, Json};
    use chrono::prelude::*;
    use spectral::prelude::*;

    use mockall::predicate;
    use tracing_test::traced_test;

    use crate::{
        api::{dto, ApiError},
        db::{self, extract::Database, Entity},
        snowflake::{
            self, extract::SnowflakeState, test::eq_internal, SnowflakeConverter, SnowflakeInternal,
        },
    };

    #[tokio::test]
    async fn publication_references_success() {
        let snowflake_manager = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let publication_id = SnowflakeInternal::from_i32(1);

        let publications_to = std::collections::HashMap::from([
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                db::SubjectCode {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Subject 1".to_string(),
                    code: "SUBJ_1".to_string(),
                },
            ),
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                db::SubjectCode {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Subject 1".to_string(),
                    code: "SUBJ_1".to_string(),
                },
            ),
        ]);
        let publications_to_return = publications_to.clone();

        let publications_from = std::collections::HashMap::from([
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                db::SubjectCode {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Subject 1".to_string(),
                    code: "SUBJ_1".to_string(),
                },
            ),
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                db::SubjectCode {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Subject 1".to_string(),
                    code: "SUBJ_1".to_string(),
                },
            ),
        ]);
        let publications_from_return = publications_from.clone();

        db.publication_reference
            .expect_get_publications_to_id()
            .with(predicate::eq(
                snowflake_manager.from_internal(publication_id).unwrap(),
            ))
            .returning(move |_| Ok(publications_to_return.clone()));

        db.publication_reference
            .expect_get_publications_from_id()
            .with(predicate::eq(
                snowflake_manager.from_internal(publication_id).unwrap(),
            ))
            .returning(move |_| Ok(publications_from_return.clone()));

        let Json(got_references) = super::publication_references(
            SnowflakeState(snowflake_manager.clone()),
            Database(Arc::new(db)),
            Path(PubId {
                pub_id: snowflake_manager
                    .from_internal(publication_id)
                    .unwrap()
                    .external,
            }),
        )
        .await
        .unwrap();

        let references = dto::PublicationReference {
            to: publications_to
                .into_iter()
                .map(|(p, c)| dto::Publication::from_db(&snowflake_manager, p, c).unwrap())
                .collect::<Vec<_>>(),
            from: publications_from
                .into_iter()
                .map(|(p, c)| dto::Publication::from_db(&snowflake_manager, p, c).unwrap())
                .collect::<Vec<_>>(),
        };

        assert_that!(got_references).is_equal_to(references);
    }

    #[tokio::test]
    #[traced_test]
    async fn publication_get_by_id_success() {
        let snowflake_manager = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let test_pub_id = SnowflakeInternal::from_i32(1);
        let expected_publication = db::Publication {
            id: test_pub_id.clone(),
            created_at: Local::now().naive_local(),
            code_id: SnowflakeInternal::from_i32(1),
            title: "Test Publication".to_string(),
            published_at: Local::now().date_naive(),
        };
        let expected_code = db::SubjectCode {
            id: SnowflakeInternal::from_i32(2),
            created_at: Local::now().naive_local(),
            name: "Test Code".to_string(),
            code: "123".to_string(),
        };

        db.publication
            .expect_ensure()
            .with(eq_internal(test_pub_id))
            .return_once(move |_| Ok((expected_publication, expected_code)));

        let result = publication_get_by_id(
            SnowflakeState(snowflake_manager.clone()),
            Database(Arc::new(db)),
            Path(PubId {
                pub_id: snowflake_manager
                    .from_internal(test_pub_id)
                    .unwrap()
                    .external,
            }),
        )
        .await
        .unwrap();

        let Json(publication) = result;

        assert_eq!(publication.title, "Test Publication");
        assert_eq!(publication.subject_code.code, "123");
    }

    #[tokio::test]
    #[traced_test]
    async fn publications_list_all_test() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let test_publications = vec![
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Test Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                db::SubjectCode {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Test Code 1".to_string(),
                    code: "123".to_string(),
                },
            ),
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(2),
                    title: "Test Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                db::SubjectCode {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Test Code 2".to_string(),
                    code: "456".to_string(),
                },
            ),
        ];

        db.publication
            .expect_list_all()
            .return_once(move || Ok(test_publications));

        let result = publications_list_all(SnowflakeState(snowflake), Database(Arc::new(db)))
            .await
            .unwrap();

        let Json(publications) = result;

        assert_eq!(publications.len(), 2);
        assert_eq!(publications[0].title, "Test Publication 1");
        assert_eq!(publications[1].title, "Test Publication 2");
    }

    #[tokio::test]
    #[traced_test]
    async fn get_publication_participants() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let publication_id = SnowflakeInternal::from_i32(1);

        let participants = std::collections::HashMap::from([
            (
                db::AcademicParticipant {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    full_name: "Participant 1".to_string(),
                },
                db::PublicationRole {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Role 1".to_string(),
                },
            ),
            (
                db::AcademicParticipant {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    full_name: "Participant 2".to_string(),
                },
                db::PublicationRole {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Role 2".to_string(),
                },
            ),
        ]);
        let participant_to_return = participants.clone();

        db.publication_participation
            .expect_get_participants_for_publication()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(publication_id).unwrap(),
            ))
            .returning(move |_| Ok(participant_to_return.clone()));

        let Json(got_participants) = super::publication_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(PubId {
                pub_id: snowflake.from_internal(publication_id).unwrap().external,
            }),
        )
        .await
        .unwrap();

        let participants_to_compare = participants
            .into_iter()
            .map(|(participant, role)| dto::PublicationParticipant {
                id: snowflake.from_internal(participant.id).unwrap().external,
                name: participant.full_name,
                role: dto::ParticipationRole {
                    id: snowflake.from_internal(role.id).unwrap().external,
                    name: role.name,
                },
            })
            .collect::<Vec<_>>();

        assert_that!(got_participants).contains_all_of(&&participants_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn get_publication_organisations() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let publication_id = SnowflakeInternal::from_i32(1);

        let organisations = std::collections::HashSet::from([
            db::Organisation {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                name: "Organisation 1".to_string(),
            },
            db::Organisation {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                name: "Organisation 2".to_string(),
            },
        ]);
        let organisation_to_return = organisations.clone();

        db.publication_participation
            .expect_get_organisations_for_publication()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(publication_id).unwrap(),
            ))
            .returning(move |_| Ok(organisation_to_return.clone()));

        let Json(got_organisations) = super::publication_organisations(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(PubId {
                pub_id: snowflake.from_internal(publication_id).unwrap().external,
            }),
        )
        .await
        .unwrap();

        let organisation_to_compare = organisations
            .into_iter()
            .map(|o| dto::Organisation {
                id: snowflake.from_internal(o.id).unwrap().external,
                name: o.name,
            })
            .collect::<Vec<_>>();

        assert_that!(got_organisations).contains_all_of(&&organisation_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn get_publication_participants_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let publication_id = SnowflakeInternal::from_i32(1);
        let publication_id_hex = snowflake
            .from_internal(publication_id)
            .unwrap()
            .external
            .to_hex();

        db.publication_participation
            .expect_get_organisations_for_publication()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(publication_id).unwrap(),
            ))
            .returning(move |_| {
                Err(db::DbError::NoEntity {
                    entity: db::Publication::ENTITY,
                    id: publication_id_hex.clone(),
                })
            });

        let result = super::publication_organisations(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(PubId {
                pub_id: snowflake.from_internal(publication_id).unwrap().external,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(db::DbError::NoEntity {
                entity: db::Publication::ENTITY,
                id: _
            })
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn get_publication_organisations_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let publication_id = SnowflakeInternal::from_i32(1);
        let publication_id_hex = snowflake
            .from_internal(publication_id)
            .unwrap()
            .external
            .to_hex();

        db.publication_participation
            .expect_get_organisations_for_publication()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(publication_id).unwrap(),
            ))
            .returning(move |_| {
                Err(db::DbError::NoEntity {
                    entity: db::Publication::ENTITY,
                    id: publication_id_hex.clone(),
                })
            });

        let result = super::publication_organisations(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(PubId {
                pub_id: snowflake.from_internal(publication_id).unwrap().external,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(db::DbError::NoEntity {
                entity: db::Publication::ENTITY,
                id: _
            })
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn test_publication_organisations() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let publication_id = SnowflakeInternal::from_i32(1);

        let organisations = std::collections::HashSet::from([db::Organisation {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Organisation 1".to_string(),
        }]);
        let vec_organisations_to_return = organisations.clone();

        db.publication_participation
            .expect_get_organisations_for_publication()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(publication_id).unwrap(),
            ))
            .return_once(move |_| Ok(vec_organisations_to_return.clone()));

        let Json(got_organisations) = super::publication_organisations(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(PubId {
                pub_id: snowflake.from_internal(publication_id).unwrap().external,
            }),
        )
        .await
        .unwrap();

        let organisation_to_compare = organisations
            .into_iter()
            .map(|o| dto::Organisation {
                id: snowflake.from_internal(o.id).unwrap().external,
                name: o.name,
            })
            .collect::<Vec<_>>();

        assert_that!(got_organisations).contains_all_of(&&organisation_to_compare)
    }

    #[tokio::test]
    #[traced_test]
    async fn publications_fuzzy_search_test() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let search_query = "Test Publication";
        let n_results = 2;
        let test_publications = vec![
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Test Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                db::SubjectCode {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Test Code 1".to_string(),
                    code: "123".to_string(),
                },
            ),
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(2),
                    title: "Test Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                db::SubjectCode {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Test Code 2".to_string(),
                    code: "456".to_string(),
                },
            ),
        ];

        db.publication
            .expect_fuzzy_search_on_title()
            .withf(move |query, n| query == search_query && *n == n_results)
            .return_once(move |_, _| Ok(test_publications));

        let result = publications_fuzzy_search(
            SnowflakeState(snowflake),
            Database(Arc::new(db)),
            Query(dto::FuzzySearchQuery {
                search: search_query.to_string(),
                n: n_results as usize,
            }),
        )
        .await
        .unwrap();

        let Json(publications) = result;
        assert_eq!(publications.len(), 2);
        assert_eq!(publications[0].title, "Test Publication 1");
        assert_eq!(publications[1].title, "Test Publication 2");
    }

    #[tokio::test]
    #[traced_test]
    async fn subject_codes_fuzzy_search_test() {
        let mut db = db::MockDatabase::new();

        let test_query = "Math";
        let n_results = 2;
        let test_subject_codes = vec![
            db::SubjectCode {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                name: "Mathematics".to_string(),
                code: "MATH101".to_string(),
            },
            db::SubjectCode {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                name: "Applied Mathematics".to_string(),
                code: "MATH201".to_string(),
            },
        ];

        db.subject_code
            .expect_fuzzy_search()
            .withf(move |query, n| query == test_query && *n == n_results)
            .return_once(move |_, _| Ok(test_subject_codes));

        let snowflake = snowflake::test::snowflake_manager();

        let result = subject_codes_fuzzy_search(
            SnowflakeState(snowflake),
            Database(Arc::new(db)),
            Query(dto::FuzzySearchQuery {
                search: test_query.to_string(),
                n: n_results as usize,
            }),
        )
        .await
        .unwrap();

        let Json(subject_codes) = result;

        assert_eq!(subject_codes.len(), 2);
        assert_eq!(subject_codes[0].code, "MATH101");
        assert_eq!(subject_codes[1].code, "MATH201");
    }

    #[tokio::test]
    #[traced_test]
    async fn publication_get_meta_non_admin() {
        let maybe_publication_admin: Option<PublicationAdmin> = None;

        let result = publication_get_meta(maybe_publication_admin).await.unwrap();
        let Json(meta) = result;
        assert!(meta.editable == false);
    }
}
