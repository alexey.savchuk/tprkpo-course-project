use std::collections::HashSet;

use axum::{
    debug_handler,
    extract::{FromRef, Path, Query},
    routing::{delete, get, post, put},
    Json, Router,
};
use chrono::Local;
use serde::{Deserialize, Serialize};
use tracing::instrument;

use super::{dto, Result};
#[cfg(debug_assertions)]
use crate::server::AppState;
use crate::{
    auth::extract::{ApplicationAdmin, OrganisationAdmin, OryApiStateProvider},
    db::{
        self,
        extract::{Database, DatabaseStateProvider},
    },
    snowflake::{extract::SnowflakeState, SnowflakeConverter, SnowflakeExternal, SnowflakeManager},
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct OrgId {
    pub org_id: SnowflakeExternal<db::Organisation>,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisations_list_all(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
) -> Result<Json<Vec<dto::Organisation>>> {
    let orgs = db.organisation().list_all().await?;
    let orgs = orgs
        .into_iter()
        .map(|org| Ok(dto::Organisation::from_db(&snowflake, org)?))
        .collect::<Result<_>>()?;
    Ok(Json(orgs))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisations_fuzzy_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::Organisation>>> {
    let orgs = db
        .organisation()
        .get_by_fuzzy_name(&query.search, query.n as i64)
        .await?;
    let orgs = orgs
        .into_iter()
        .map(|org| Ok(dto::Organisation::from_db(&snowflake, org)?))
        .collect::<Result<_>>()?;
    Ok(Json(orgs))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisation_get_by_id(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(OrgId { org_id }): Path<OrgId>,
) -> Result<Json<dto::Organisation>> {
    let org = db
        .organisation()
        .ensure(&snowflake.from_external(org_id)?)
        .await?;
    let org = dto::Organisation::from_db(&snowflake, org)?;
    Ok(Json(org))
}

#[derive(Debug, Deserialize)]
pub struct ParticipantsQuery {
    include_historical: Option<bool>,
    include_pending: Option<bool>,
    include_declined: Option<bool>,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisation_participants(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(OrgId { org_id }): Path<OrgId>,
    Query(ParticipantsQuery {
        include_historical,
        include_pending,
        include_declined,
    }): Query<ParticipantsQuery>,
    maybe_org_admin: Option<OrganisationAdmin>,
) -> Result<Json<Vec<dto::OrganisationParticipant>>> {
    let mut participants = Vec::new();

    // 1. active participants
    let active_participants = db
        .organisation_participation()
        .get_active_participants_for_organisation(&snowflake.from_external(org_id)?)
        .await?
        .into_iter()
        .map(|(participant, roles)| {
            Ok(dto::OrganisationParticipant::from_db(
                &snowflake,
                participant,
                roles,
                false,
                false,
                false,
            )?)
        })
        .collect::<Result<Vec<_>>>()?;
    participants.extend(active_participants.into_iter());

    // 2. Historic participants
    if include_historical.unwrap_or(false) && maybe_org_admin.is_some() {
        let active_participants = db
            .organisation_participation()
            .get_historic_participants_for_organisation(&snowflake.from_external(org_id)?)
            .await?
            .into_iter()
            .map(|(participant, roles)| {
                Ok(dto::OrganisationParticipant::from_db(
                    &snowflake,
                    participant,
                    roles,
                    true,
                    false,
                    false,
                )?)
            })
            .collect::<Result<Vec<_>>>()?;
        participants.extend(active_participants.into_iter());
    };

    // 2. Pending participants
    if include_pending.unwrap_or(false) && maybe_org_admin.is_some() {
        let active_participants = db
            .organisation_participation()
            .get_pending_participants_for_organisation(&snowflake.from_external(org_id)?)
            .await?
            .into_iter()
            .map(|(participant, roles)| {
                Ok(dto::OrganisationParticipant::from_db(
                    &snowflake,
                    participant,
                    roles,
                    false,
                    true,
                    false,
                )?)
            })
            .collect::<Result<Vec<_>>>()?;
        participants.extend(active_participants.into_iter());
    };

    // 2. Declined participants
    if include_declined.unwrap_or(false) && maybe_org_admin.is_some() {
        let active_participants = db
            .organisation_participation()
            .get_declined_participants_for_organisation(&snowflake.from_external(org_id)?)
            .await?
            .into_iter()
            .map(|(participant, roles)| {
                Ok(dto::OrganisationParticipant::from_db(
                    &snowflake,
                    participant,
                    roles,
                    false,
                    false,
                    true,
                )?)
            })
            .collect::<Result<Vec<_>>>()?;
        participants.extend(active_participants.into_iter());
    };

    Ok(Json(participants))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisations_participants_fuzzy_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(OrgId { org_id }): Path<OrgId>,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::Participant>>> {
    let participants = db
        .organisation_participation()
        .search_participants_by_fuzzy_name(
            &snowflake.from_external(org_id)?,
            &query.search,
            query.n as i64,
        )
        .await?;
    let participants = participants
        .into_iter()
        .map(|org| Ok(dto::Participant::from_db(&snowflake, org)?))
        .collect::<Result<_>>()?;
    Ok(Json(participants))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisation_publications(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(OrgId { org_id }): Path<OrgId>,
) -> Result<Json<Vec<dto::Publication>>> {
    let publications = db
        .publication_participation()
        .get_publications_for_organisation(&snowflake.from_external(org_id)?)
        .await?;
    let resp = publications
        .into_iter()
        .map(|(publication, code)| Ok(dto::Publication::from_db(&snowflake, publication, code)?))
        .collect::<Result<_>>()?;
    Ok(Json(resp))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisation_meta(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(OrgId { org_id }): Path<OrgId>,
    maybe_org_admin: Option<OrganisationAdmin>,
) -> Result<Json<dto::OrganisationMeta>> {
    let publications = db
        .publication_participation()
        .get_publications_for_organisation(&snowflake.from_external(org_id)?)
        .await?;
    let participants = db
        .organisation_participation()
        .get_active_participants_for_organisation(&snowflake.from_external(org_id)?)
        .await?;
    let citations = db
        .publication_reference()
        .get_citation_count_for_organisation(&snowflake.from_external(org_id)?)
        .await?;
    let editable = maybe_org_admin.is_some();
    let resp = dto::OrganisationMeta {
        participants: participants.len(),
        publications: publications.len(),
        citations: citations.count as usize,
        editable,
    };
    Ok(Json(resp))
}

#[derive(Debug, Deserialize)]
pub struct NewOrganisationParams {
    pub name: String,
    pub admin_id: SnowflakeExternal<db::AcademicParticipant>,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisations_new(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    _app_admin: ApplicationAdmin,
    Json(NewOrganisationParams { name, admin_id }): Json<NewOrganisationParams>,
) -> Result<Json<dto::Organisation>> {
    let org = db.organisation().insert_new(&name).await?;
    let admin_role = db.organisation_role().get_admin().await?;
    let _participation = db
        .organisation_participation()
        .insert_new_participation(
            &snowflake.from_internal(org.id)?,
            &snowflake.from_internal(admin_role.id)?,
            &snowflake.from_external(admin_id)?,
            &snowflake.from_external(admin_id)?,
            &chrono::Local::now().date_naive(),
            &db::ParticipationState::Active,
        )
        .await?;
    let res = dto::Organisation::from_db(&snowflake, org)?;
    Ok(Json(res))
}

#[derive(Debug, Deserialize)]
pub struct UpdateOrganisationParams {
    pub name: String,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisation_update(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(OrgId { org_id }): Path<OrgId>,
    _org_admin: OrganisationAdmin,
    Json(UpdateOrganisationParams { name }): Json<UpdateOrganisationParams>,
) -> Result<Json<dto::Organisation>> {
    let org = db
        .organisation()
        .update_name(&snowflake.from_external(org_id)?, &name)
        .await?;
    let res = dto::Organisation::from_db(&snowflake, org)?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisation_modify_participant(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(OrgWithPar { org_id, par_id }): Path<OrgWithPar>,
    OrganisationAdmin(org_admin): OrganisationAdmin,
    Json(req_roles): Json<Vec<dto::OrganisationRole>>,
) -> Result<Json<Vec<dto::OrganisationRole>>> {
    let org_id = snowflake.from_external(org_id)?;
    let subject_id = snowflake.from_external(par_id)?;
    let referrer_id = snowflake.from_internal(org_admin.id)?;
    let req_roles: HashSet<_> = req_roles
        .into_iter()
        .map(|role| Ok(snowflake.from_external(role.id)?))
        .collect::<Result<_>>()?;
    let roles: HashSet<_> = db
        .organisation_participation()
        .get_roles_for(&org_id, &subject_id)
        .await?
        .into_iter()
        .map(|role| Ok(snowflake.from_internal(role.id)?))
        .collect::<Result<_>>()?;
    let extra_roles = roles.difference(&req_roles);
    for role_id in extra_roles {
        let participation = db
            .organisation_participation()
            .get_participation_by(&org_id, &subject_id, &role_id)
            .await?;
        db.organisation_participation()
            .stop_participation(&snowflake.from_internal(participation.id)?)
            .await?;
    }
    let new_roles = req_roles.difference(&roles);
    for role_id in new_roles {
        db.organisation_participation()
            .insert_new_participation(
                &org_id,
                role_id,
                &subject_id,
                &referrer_id,
                &Local::now().date_naive(),
                &db::ParticipationState::Pending,
            )
            .await?;
    }
    let res_roles = db
        .organisation_participation()
        .get_roles_for(&org_id, &subject_id)
        .await?
        .into_iter()
        .map(|role| Ok(dto::OrganisationRole::from_db(&snowflake, role)?))
        .collect::<Result<_>>()?;
    Ok(Json(res_roles))
}

#[derive(Debug, Deserialize)]
pub struct OrgWithPar {
    pub org_id: SnowflakeExternal<db::Organisation>,
    pub par_id: SnowflakeExternal<db::AcademicParticipant>,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn organisation_remove_participant(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(OrgWithPar { org_id, par_id }): Path<OrgWithPar>,
    _org_admin: OrganisationAdmin,
) -> Result<()> {
    let org_id = snowflake.from_external(org_id)?;
    let par_id = snowflake.from_external(par_id)?;
    let roles = db
        .organisation_participation()
        .get_roles_for(&org_id, &par_id)
        .await?;
    for role in roles {
        let role_id = snowflake.from_internal(role.id)?;
        let participation = db
            .organisation_participation()
            .get_participation_by(&org_id, &par_id, &role_id)
            .await?;
        db.organisation_participation()
            .stop_participation(&snowflake.from_internal(participation.id)?)
            .await?;
    }
    Ok(())
}

#[cfg(not(tarpaulin_include))]
pub fn routes<S>() -> Router<S>
where
    SnowflakeManager: FromRef<S>,
    S: DatabaseStateProvider + OryApiStateProvider + Send + Sync + Clone + 'static,
{
    Router::new()
        .route("/all", get(organisations_list_all))
        .route("/search", get(organisations_fuzzy_search))
        .route("/create", post(organisations_new))
        .route("/:org_id", get(organisation_get_by_id))
        .route("/:org_id", put(organisation_update))
        .route("/:org_id/meta", get(organisation_meta))
        .route("/:org_id/participants", get(organisation_participants))
        .route(
            "/:org_id/participants/search",
            get(organisations_participants_fuzzy_search),
        )
        .route(
            "/:org_id/participants/:par_id",
            put(organisation_modify_participant),
        )
        .route(
            "/:org_id/participants/:par_id",
            delete(organisation_remove_participant),
        )
        .route("/:org_id/publications", get(organisation_publications))
}

#[cfg(test)]
mod tests {
    use std::{
        collections::{HashMap, HashSet},
        sync::Arc,
    };

    use axum::{
        extract::{Path, Query},
        Json,
    };
    use chrono::prelude::*;
    use mockall::predicate::{self};

    use spectral::prelude::*;
    use tracing_test::traced_test;

    use crate::{
        api::{
            dto,
            organisations::{NewOrganisationParams, OrgId, OrgWithPar, UpdateOrganisationParams},
            ApiError,
        },
        auth::extract::{ApplicationAdmin, OrganisationAdmin},
        db::{self, extract::Database, DbError, Entity, Organisation},
        snowflake::{self, extract::SnowflakeState, SnowflakeConverter, SnowflakeInternal},
    };

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_active_participants() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = SnowflakeInternal::from_i32(1);

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Academic".into(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(99),
            created_at: Local::now().naive_local(),
            full_name: "Ivan Ivanov".into(),
        };

        let active_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Merry Christmas".into(),
            },
        ];

        let historic_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                full_name: "George Smith".into(),
            },
        ];

        let pending_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(5),
                created_at: Local::now().naive_local(),
                full_name: "Mark Anderson".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(6),
                created_at: Local::now().naive_local(),
                full_name: "William Wright".into(),
            },
        ];

        let declined_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(7),
                created_at: Local::now().naive_local(),
                full_name: "Paul Smith".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(8),
                created_at: Local::now().naive_local(),
                full_name: "George Anderson".into(),
            },
        ];

        let active_participations = active_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let active_participation_to_return = active_participations.clone();

        let historic_participations = historic_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let historic_participation_to_return = historic_participations.clone();

        let pending_participations = pending_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let pending_participation_to_return = pending_participations.clone();

        let declined_participations = declined_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let declined_participation_to_return = declined_participations.clone();

        let active_participants_to_compare = active_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, false)
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let _historic_participants_to_compare = historic_participation_to_return
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, true, false, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _pending_participants_to_compare = pending_participation_to_return
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, true, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _declined_participants_to_compare = declined_participation_to_return
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, true).unwrap()
            })
            .collect::<Vec<_>>();

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(active_participation_to_return.clone()));

        let Json(got_participants) = super::organisation_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(org_id).unwrap().external,
            }),
            Query(super::ParticipantsQuery {
                include_historical: None,
                include_pending: None,
                include_declined: None,
            }),
            Some(OrganisationAdmin(connected_participant.clone())),
        )
        .await
        .unwrap();

        assert_that!(got_participants).contains_all_of(&&active_participants_to_compare);
        assert_that!(got_participants);
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_historic_participants() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = SnowflakeInternal::from_i32(1);

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Academic".into(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(99),
            created_at: Local::now().naive_local(),
            full_name: "Ivan Ivanov".into(),
        };

        let active_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Merry Christmas".into(),
            },
        ];

        let historic_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                full_name: "George Smith".into(),
            },
        ];

        let pending_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(5),
                created_at: Local::now().naive_local(),
                full_name: "Mark Anderson".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(6),
                created_at: Local::now().naive_local(),
                full_name: "William Wright".into(),
            },
        ];

        let declined_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(7),
                created_at: Local::now().naive_local(),
                full_name: "Paul Smith".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(8),
                created_at: Local::now().naive_local(),
                full_name: "George Anderson".into(),
            },
        ];

        let active_participations = active_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let active_participation_to_return = active_participations.clone();

        let historic_participations = historic_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let historic_participation_to_return = historic_participations.clone();

        let pending_participations = pending_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _pending_participation_to_return = pending_participations.clone();

        let declined_participations = declined_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _declined_participation_to_return = declined_participations.clone();

        let active_participants_to_compare = active_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, false)
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let historic_participants_to_compare = historic_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, true, false, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _pending_participants_to_compare = pending_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, true, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _declined_participants_to_compare = declined_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, true).unwrap()
            })
            .collect::<Vec<_>>();

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(active_participation_to_return.clone()));

        db.organisation_participation
            .expect_get_historic_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(historic_participation_to_return.clone()));

        let Json(got_participants) = super::organisation_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(org_id).unwrap().external,
            }),
            Query(super::ParticipantsQuery {
                include_historical: Some(true),
                include_pending: None,
                include_declined: None,
            }),
            Some(OrganisationAdmin(connected_participant.clone())),
        )
        .await
        .unwrap();

        assert_that!(got_participants).contains_all_of(&&active_participants_to_compare);
        assert_that!(got_participants).contains_all_of(&&historic_participants_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_pending_participants() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = SnowflakeInternal::from_i32(1);

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Academic".into(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(99),
            created_at: Local::now().naive_local(),
            full_name: "Ivan Ivanov".into(),
        };

        let active_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Merry Christmas".into(),
            },
        ];

        let historic_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                full_name: "George Smith".into(),
            },
        ];

        let pending_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(5),
                created_at: Local::now().naive_local(),
                full_name: "Mark Anderson".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(6),
                created_at: Local::now().naive_local(),
                full_name: "William Wright".into(),
            },
        ];

        let declined_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(7),
                created_at: Local::now().naive_local(),
                full_name: "Paul Smith".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(8),
                created_at: Local::now().naive_local(),
                full_name: "George Anderson".into(),
            },
        ];

        let active_participations = active_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let active_participation_to_return = active_participations.clone();

        let historic_participations = historic_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _historic_participation_to_return = historic_participations.clone();

        let pending_participations = pending_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let pending_participation_to_return = pending_participations.clone();

        let declined_participations = declined_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _declined_participation_to_return = declined_participations.clone();

        let active_participants_to_compare = active_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, false)
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let _historic_participants_to_compare = historic_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, true, false, false).unwrap()
            })
            .collect::<Vec<_>>();

        let pending_participants_to_compare = pending_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, true, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _declined_participants_to_compare = declined_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, true).unwrap()
            })
            .collect::<Vec<_>>();

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(active_participation_to_return.clone()));

        db.organisation_participation
            .expect_get_pending_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(pending_participation_to_return.clone()));

        let Json(got_participants) = super::organisation_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(org_id).unwrap().external,
            }),
            Query(super::ParticipantsQuery {
                include_historical: None,
                include_pending: Some(true),
                include_declined: None,
            }),
            Some(OrganisationAdmin(connected_participant.clone())),
        )
        .await
        .unwrap();

        assert_that!(got_participants).contains_all_of(&&active_participants_to_compare);
        assert_that!(got_participants).contains_all_of(&&pending_participants_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_declined_participants() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = SnowflakeInternal::from_i32(1);

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Academic".into(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(99),
            created_at: Local::now().naive_local(),
            full_name: "Ivan Ivanov".into(),
        };

        let active_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Merry Christmas".into(),
            },
        ];

        let historic_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                full_name: "George Smith".into(),
            },
        ];

        let pending_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(5),
                created_at: Local::now().naive_local(),
                full_name: "Mark Anderson".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(6),
                created_at: Local::now().naive_local(),
                full_name: "William Wright".into(),
            },
        ];

        let declined_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(7),
                created_at: Local::now().naive_local(),
                full_name: "Paul Smith".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(8),
                created_at: Local::now().naive_local(),
                full_name: "George Anderson".into(),
            },
        ];

        let active_participations = active_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let active_participation_to_return = active_participations.clone();

        let historic_participations = historic_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _historic_participation_to_return = historic_participations.clone();

        let pending_participations = pending_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _pending_participation_to_return = pending_participations.clone();

        let declined_participations = declined_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let declined_participation_to_return = declined_participations.clone();

        let active_participants_to_compare = active_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, false)
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let _historic_participants_to_compare = historic_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, true, false, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _pending_participants_to_compare = pending_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, true, false).unwrap()
            })
            .collect::<Vec<_>>();

        let declined_participants_to_compare = declined_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, true).unwrap()
            })
            .collect::<Vec<_>>();

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(active_participation_to_return.clone()));

        db.organisation_participation
            .expect_get_declined_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(declined_participation_to_return.clone()));

        let Json(got_participants) = super::organisation_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(org_id).unwrap().external,
            }),
            Query(super::ParticipantsQuery {
                include_historical: None,
                include_pending: None,
                include_declined: Some(true),
            }),
            Some(OrganisationAdmin(connected_participant.clone())),
        )
        .await
        .unwrap();

        assert_that!(got_participants).contains_all_of(&&active_participants_to_compare);
        assert_that!(got_participants).contains_all_of(&&declined_participants_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_historic_participants_without_admin() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = SnowflakeInternal::from_i32(1);

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Academic".into(),
        };

        let _connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(99),
            created_at: Local::now().naive_local(),
            full_name: "Ivan Ivanov".into(),
        };

        let active_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Merry Christmas".into(),
            },
        ];

        let historic_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                full_name: "George Smith".into(),
            },
        ];

        let pending_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(5),
                created_at: Local::now().naive_local(),
                full_name: "Mark Anderson".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(6),
                created_at: Local::now().naive_local(),
                full_name: "William Wright".into(),
            },
        ];

        let declined_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(7),
                created_at: Local::now().naive_local(),
                full_name: "Paul Smith".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(8),
                created_at: Local::now().naive_local(),
                full_name: "George Anderson".into(),
            },
        ];

        let active_participations = active_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let active_participation_to_return = active_participations.clone();

        let historic_participations = historic_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _historic_participation_to_return = historic_participations.clone();

        let pending_participations = pending_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _pending_participation_to_return = pending_participations.clone();

        let declined_participations = declined_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _declined_participation_to_return = declined_participations.clone();

        let active_participants_to_compare = active_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, false)
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let _historic_participants_to_compare = historic_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, true, false, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _pending_participants_to_compare = pending_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, true, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _declined_participants_to_compare = declined_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, true).unwrap()
            })
            .collect::<Vec<_>>();

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(active_participation_to_return.clone()));

        let Json(got_participants) = super::organisation_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(org_id).unwrap().external,
            }),
            Query(super::ParticipantsQuery {
                include_historical: Some(true),
                include_pending: None,
                include_declined: None,
            }),
            None,
        )
        .await
        .unwrap();

        assert_that!(got_participants).contains_all_of(&&active_participants_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_pending_participants_without_admin() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = SnowflakeInternal::from_i32(1);

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Academic".into(),
        };

        let _connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(99),
            created_at: Local::now().naive_local(),
            full_name: "Ivan Ivanov".into(),
        };

        let active_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Merry Christmas".into(),
            },
        ];

        let historic_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                full_name: "George Smith".into(),
            },
        ];

        let pending_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(5),
                created_at: Local::now().naive_local(),
                full_name: "Mark Anderson".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(6),
                created_at: Local::now().naive_local(),
                full_name: "William Wright".into(),
            },
        ];

        let declined_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(7),
                created_at: Local::now().naive_local(),
                full_name: "Paul Smith".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(8),
                created_at: Local::now().naive_local(),
                full_name: "George Anderson".into(),
            },
        ];

        let active_participations = active_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let active_participation_to_return = active_participations.clone();

        let historic_participations = historic_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _historic_participation_to_return = historic_participations.clone();

        let pending_participations = pending_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _pending_participation_to_return = pending_participations.clone();

        let declined_participations = declined_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _declined_participation_to_return = declined_participations.clone();

        let active_participants_to_compare = active_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, false)
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let _historic_participants_to_compare = historic_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, true, false, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _pending_participants_to_compare = pending_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, true, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _declined_participants_to_compare = declined_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, true).unwrap()
            })
            .collect::<Vec<_>>();

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(active_participation_to_return.clone()));

        let Json(got_participants) = super::organisation_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(org_id).unwrap().external,
            }),
            Query(super::ParticipantsQuery {
                include_historical: None,
                include_pending: Some(true),
                include_declined: None,
            }),
            None,
        )
        .await
        .unwrap();

        assert_that!(got_participants).contains_all_of(&&active_participants_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_declined_participants_without_admin() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = SnowflakeInternal::from_i32(1);

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Academic".into(),
        };

        let _connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(99),
            created_at: Local::now().naive_local(),
            full_name: "Ivan Ivanov".into(),
        };

        let active_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Merry Christmas".into(),
            },
        ];

        let historic_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                full_name: "George Smith".into(),
            },
        ];

        let pending_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(5),
                created_at: Local::now().naive_local(),
                full_name: "Mark Anderson".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(6),
                created_at: Local::now().naive_local(),
                full_name: "William Wright".into(),
            },
        ];

        let declined_participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(7),
                created_at: Local::now().naive_local(),
                full_name: "Paul Smith".into(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(8),
                created_at: Local::now().naive_local(),
                full_name: "George Anderson".into(),
            },
        ];

        let active_participations = active_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let active_participation_to_return = active_participations.clone();

        let historic_participations = historic_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _historic_participation_to_return = historic_participations.clone();

        let pending_participations = pending_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _pending_participation_to_return = pending_participations.clone();

        let declined_participations = declined_participants
            .into_iter()
            .map(|p| (p, vec![org_role.clone()]))
            .collect::<HashMap<_, _>>();
        let _declined_participation_to_return = declined_participations.clone();

        let active_participants_to_compare = active_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, false)
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let _historic_participants_to_compare = historic_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, true, false, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _pending_participants_to_compare = pending_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, true, false).unwrap()
            })
            .collect::<Vec<_>>();

        let _declined_participants_to_compare = declined_participations
            .into_iter()
            .map(|(p, r)| {
                dto::OrganisationParticipant::from_db(&snowflake, p, r, false, false, true).unwrap()
            })
            .collect::<Vec<_>>();

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| Ok(active_participation_to_return.clone()));

        let Json(got_participants) = super::organisation_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(org_id).unwrap().external,
            }),
            Query(super::ParticipantsQuery {
                include_historical: None,
                include_pending: None,
                include_declined: Some(true),
            }),
            None,
        )
        .await
        .unwrap();

        assert_that!(got_participants).contains_all_of(&&active_participants_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_participants() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = snowflake
            .from_internal(SnowflakeInternal::from_i32(66))
            .unwrap();

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(43),
            created_at: Local::now().naive_local(),
            name: "Organisation role".to_string(),
        };
        let active_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(10),
            created_at: Local::now().naive_local(),
            full_name: "Active participant".to_string(),
        };
        let historic_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(11),
            created_at: Local::now().naive_local(),
            full_name: "Historical participant".to_string(),
        };
        let pending_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(12),
            created_at: Local::now().naive_local(),
            full_name: "Pending participant".to_string(),
        };
        let declined_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(13),
            created_at: Local::now().naive_local(),
            full_name: "Declined participant".to_string(),
        };

        let active_participation =
            HashMap::from([(active_participant.clone(), vec![org_role.clone()])]);
        let historic_participation =
            HashMap::from([(historic_participant.clone(), vec![org_role.clone()])]);
        let pending_participation =
            HashMap::from([(pending_participant.clone(), vec![org_role.clone()])]);
        let declined_participation =
            HashMap::from([(declined_participant.clone(), vec![org_role.clone()])]);

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(active_participation.clone()));
        db.organisation_participation
            .expect_get_historic_participants_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(historic_participation.clone()));
        db.organisation_participation
            .expect_get_pending_participants_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(pending_participation.clone()));
        db.organisation_participation
            .expect_get_declined_participants_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(declined_participation.clone()));

        let Json(org_participants) = super::organisation_participants(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: org_id.external,
            }),
            Query(super::ParticipantsQuery {
                include_historical: Some(true),
                include_pending: Some(true),
                include_declined: Some(true),
            }),
            Some(OrganisationAdmin(active_participant.clone())),
        )
        .await
        .unwrap();

        assert_that!(org_participants)
            .matching_contains(|org_par| org_par.name == active_participant.full_name);
        assert_that!(org_participants).matching_contains(|org_par| {
            org_par.name == historic_participant.full_name && org_par.is_historical
        });
        assert_that!(org_participants).matching_contains(|org_par| {
            org_par.name == pending_participant.full_name && org_par.is_pending
        });
        assert_that!(org_participants).matching_contains(|org_par| {
            org_par.name == declined_participant.full_name && org_par.is_declined
        });
    }

    #[tokio::test]
    #[traced_test]
    async fn list_all() {
        let snowflake = snowflake::test::snowflake_manager();

        let organisations = vec![
            db::Organisation {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                name: "Academic".to_string(),
            },
            db::Organisation {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                name: "Research".to_string(),
            },
            db::Organisation {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                name: "Academic".to_string(),
            },
            db::Organisation {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                name: "Research".to_string(),
            },
        ];
        let organisation_to_return = organisations.clone();

        let mut db = db::MockDatabase::new();
        db.organisation
            .expect_list_all()
            .times(1)
            .returning(move || Ok(organisation_to_return.clone()));

        let Json(got_organisations) = super::organisations_list_all(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
        )
        .await
        .unwrap();

        assert_eq!(
            got_organisations,
            organisations
                .into_iter()
                .map(|org| dto::Organisation {
                    id: snowflake.from_internal(org.id).unwrap().external,
                    name: org.name.to_string(),
                })
                .collect::<Vec<_>>()
        );
    }

    #[tokio::test]
    #[traced_test]
    async fn publications_success() {
        let snowflake = snowflake::test::snowflake_manager();

        let org_id = snowflake
            .from_internal(SnowflakeInternal::from_i32(66))
            .unwrap();

        let publication = db::Publication {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            title: "Lorem Ipsum".to_string(),
            code_id: SnowflakeInternal::from_i32(1),
            published_at: Local::now().date_naive(),
        };

        let subj_code = db::SubjectCode {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Maths".to_string(),
            code: "MAT".to_string(),
        };

        let publications = HashMap::from([(publication.clone(), subj_code.clone())]);

        let mut db = db::MockDatabase::new();

        db.publication_participation
            .expect_get_publications_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(publications.clone()));

        let Json(publications) = super::organisation_publications(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: org_id.external,
            }),
        )
        .await
        .unwrap();

        assert_that!(publications).matching_contains(|publi| publi.title == publication.title);
    }

    #[tokio::test]
    #[traced_test]
    async fn publications_not_found() {
        let snowflake = snowflake::test::snowflake_manager();

        let org_id = snowflake
            .from_internal(SnowflakeInternal::from_i32(66))
            .unwrap();
        let org_id_hex = org_id.external.to_hex();

        let mut db = db::MockDatabase::new();
        db.publication_participation
            .expect_get_publications_for_organisation()
            .times(1)
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| {
                Err(DbError::NoEntity {
                    entity: Organisation::ENTITY,
                    id: org_id_hex.clone(),
                })
            });

        let rez = super::organisation_publications(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: org_id.external,
            }),
        )
        .await;

        assert_that!(rez).is_err();
        assert!(matches!(
            rez.unwrap_err(),
            ApiError::DbError(DbError::NoEntity {
                entity: Organisation::ENTITY,
                id: _org_id_hex
            })
        ));
    }

    #[tokio::test]
    #[traced_test]
    async fn fuzzy_search() {
        let snowflake = snowflake::test::snowflake_manager();

        let test_query = "Acad";
        let n_results = 2;
        let organisations = vec![
            db::Organisation {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                name: "Academic".to_string(),
            },
            db::Organisation {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                name: "Research".to_string(),
            },
        ];
        let org_to_return = organisations.clone();
        let mut db = db::MockDatabase::new();

        db.organisation
            .expect_get_by_fuzzy_name()
            .withf(move |query, n| query == test_query && *n == n_results)
            .return_once(move |_, _| Ok(org_to_return.clone()));

        let Json(got_organisations) = super::organisations_fuzzy_search(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Query(dto::FuzzySearchQuery {
                search: test_query.to_string(),
                n: n_results as usize,
            }),
        )
        .await
        .unwrap();

        assert_eq!(
            got_organisations,
            organisations
                .into_iter()
                .map(|org| dto::Organisation {
                    id: snowflake.from_internal(org.id).unwrap().external,
                    name: org.name.to_string(),
                })
                .collect::<Vec<_>>()
        );
    }

    #[tokio::test]
    #[traced_test]
    async fn meta_success() {
        let snowflake = snowflake::test::snowflake_manager();

        let mut db = db::MockDatabase::new();

        let org_id = snowflake
            .from_internal(SnowflakeInternal::from_i32(1))
            .unwrap();

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(43),
            created_at: Local::now().naive_local(),
            name: "Organisation role".to_string(),
        };
        let active_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(10),
            created_at: Local::now().naive_local(),
            full_name: "Active participant".to_string(),
        };
        let publication = db::Publication {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            title: "Lorem Ipsum".to_string(),
            code_id: SnowflakeInternal::from_i32(1),
            published_at: Local::now().date_naive(),
        };

        let subj_code = db::SubjectCode {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Maths".to_string(),
            code: "MAT".to_string(),
        };

        let citations = db::Count { count: 1 };

        let publications = HashMap::from([(publication.clone(), subj_code.clone())]);

        let active_participation =
            HashMap::from([(active_participant.clone(), vec![org_role.clone()])]);

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(active_participation.clone()));
        db.publication_participation
            .expect_get_publications_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(publications.clone()));
        db.publication_reference
            .expect_get_citation_count_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(citations.clone()));

        let Json(org_meta) = super::organisation_meta(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: org_id.external,
            }),
            Some(OrganisationAdmin(active_participant.clone())),
        )
        .await
        .unwrap();

        assert_that!(org_meta).is_equal_to(&dto::OrganisationMeta {
            participants: 1,
            publications: 1,
            citations: 1,
            editable: true,
        })
    }

    #[tokio::test]
    #[traced_test]
    async fn meta_not_editable() {
        let snowflake = snowflake::test::snowflake_manager();

        let mut db = db::MockDatabase::new();

        let org_id = snowflake
            .from_internal(SnowflakeInternal::from_i32(1))
            .unwrap();

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(43),
            created_at: Local::now().naive_local(),
            name: "Organisation role".to_string(),
        };
        let active_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(10),
            created_at: Local::now().naive_local(),
            full_name: "Active participant".to_string(),
        };
        let publication = db::Publication {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            title: "Lorem Ipsum".to_string(),
            code_id: SnowflakeInternal::from_i32(1),
            published_at: Local::now().date_naive(),
        };

        let subj_code = db::SubjectCode {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Maths".to_string(),
            code: "MAT".to_string(),
        };

        let citations = db::Count { count: 1 };

        let publications = HashMap::from([(publication.clone(), subj_code.clone())]);

        let active_participation =
            HashMap::from([(active_participant.clone(), vec![org_role.clone()])]);

        db.organisation_participation
            .expect_get_active_participants_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(active_participation.clone()));
        db.publication_participation
            .expect_get_publications_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(publications.clone()));
        db.publication_reference
            .expect_get_citation_count_for_organisation()
            .with(predicate::eq(org_id.clone()))
            .returning(move |_| Ok(citations.clone()));

        let Json(org_meta) = super::organisation_meta(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: org_id.external,
            }),
            None,
        )
        .await
        .unwrap();

        assert_that!(org_meta).is_equal_to(&dto::OrganisationMeta {
            participants: 1,
            publications: 1,
            citations: 1,
            editable: false,
        })
    }

    #[tokio::test]
    #[traced_test]
    async fn get_by_id_success() {
        let snowflake = snowflake::test::snowflake_manager();

        let organisation = db::Organisation {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Test Organisation".to_string(),
        };
        let org_to_return = organisation.clone();

        let mut db = db::MockDatabase::new();
        db.organisation
            .expect_ensure()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(organisation.id).unwrap(),
            ))
            .returning(move |_| Ok(organisation.clone()));

        let Json(got_organisation) = super::organisation_get_by_id(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake
                    .from_internal(SnowflakeInternal::from_i32(1))
                    .unwrap()
                    .external,
            }),
        )
        .await
        .unwrap();

        assert_eq!(
            got_organisation,
            dto::Organisation {
                id: snowflake.from_internal(org_to_return.id).unwrap().external,
                name: org_to_return.name.to_string(),
            }
        );
    }

    #[tokio::test]
    #[traced_test]
    async fn get_by_id_not_found() {
        let snowflake = snowflake::test::snowflake_manager();

        let org_id = SnowflakeInternal::from_i32(66);
        let org_id_hex = snowflake.from_internal(org_id).unwrap().external.to_hex();

        let mut db = db::MockDatabase::new();
        db.organisation
            .expect_ensure()
            .times(1)
            .with(predicate::eq(snowflake.from_internal(org_id).unwrap()))
            .returning(move |_| {
                Err(DbError::NoEntity {
                    entity: Organisation::ENTITY,
                    id: org_id_hex.clone(),
                })
            });

        let rez = super::organisation_get_by_id(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(org_id).unwrap().external,
            }),
        )
        .await;

        assert_that!(rez).is_err();
        assert!(matches!(
            rez.unwrap_err(),
            ApiError::DbError(DbError::NoEntity {
                entity: Organisation::ENTITY,
                id: _org_id_hex
            })
        ));
    }

    #[tokio::test]
    #[traced_test]
    async fn participants_fuzzy_search() {
        let snowflake = snowflake::test::snowflake_manager();

        let test_query = "Merry";
        let n_results = 2;
        let org_id = snowflake
            .from_internal(SnowflakeInternal::from_i32(1))
            .unwrap();

        let participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".to_string(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Merry Christmas".to_string(),
            },
        ];
        let participant_to_return = participants.clone();

        let mut db = db::MockDatabase::new();
        db.organisation_participation
            .expect_search_participants_by_fuzzy_name()
            .withf(move |org_id, query, n| {
                org_id == org_id && query == test_query && *n == n_results
            })
            .return_once(move |_, _, _| Ok(participant_to_return.clone()));

        let Json(got_participants) = super::organisations_participants_fuzzy_search(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: org_id.external,
            }),
            Query(dto::FuzzySearchQuery {
                search: test_query.to_string(),
                n: n_results as usize,
            }),
        )
        .await
        .unwrap();

        assert_eq!(
            got_participants,
            participants
                .into_iter()
                .map(|p| dto::Participant {
                    id: snowflake.from_internal(p.id).unwrap().external,
                    name: p.full_name.to_string(),
                })
                .collect::<Vec<_>>()
        );
    }

    #[tokio::test]
    #[traced_test]
    async fn new_success() {
        let snowflake = snowflake::test::snowflake_manager();

        let admin_id = SnowflakeInternal::from_i32(1);

        let mut db = db::MockDatabase::new();

        let org_name = "Test Organisation";
        let organisation = db::Organisation {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: org_name.to_string(),
        };
        let organisation_to_return = organisation.clone();

        db.organisation
            .expect_insert_new()
            .times(1)
            .with(predicate::eq(org_name))
            .returning(move |_| Ok(organisation_to_return.clone()));

        let org_role = db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Admin".to_string(),
        };
        let org_role_to_return = org_role.clone();

        db.organisation_role
            .expect_get_admin()
            .times(1)
            .returning(move || Ok(org_role_to_return.clone()));

        let org_part = db::OrganisationParticipation {
            id: SnowflakeInternal::from_i32(1),
            started_at: Local::now().date_naive(),
            stopped_at: None,
            organisation_id: organisation.id,
            role_id: org_role.id,
            subject_id: admin_id,
            referrer_id: admin_id,
            participation_state: db::ParticipationState::Active,
        };
        let org_part_to_return = org_part.clone();

        let org_id_with = snowflake.from_internal(organisation.id).unwrap();
        let role_id_with = snowflake.from_internal(org_role.id).unwrap();
        let subject_id_with = snowflake.from_internal(admin_id).unwrap();
        let referrer_id_with = snowflake.from_internal(admin_id).unwrap();

        db.organisation_participation
            .expect_insert_new_participation()
            .times(1)
            .withf(
                move |org_id, role_id, subject_id, referrer_id, created_at, status| {
                    *org_id == org_id_with.clone()
                        && *role_id == role_id_with.clone()
                        && *subject_id == subject_id_with.clone()
                        && *referrer_id == referrer_id_with.clone()
                        && *created_at == Local::now().date_naive()
                        && *status == db::ParticipationState::Active
                },
            )
            .returning(move |_, _, _, _, _, _| Ok(org_part_to_return.clone()));

        let Json(res) = super::organisations_new(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            ApplicationAdmin(()),
            Json(NewOrganisationParams {
                name: org_name.to_string(),
                admin_id: snowflake.from_internal(admin_id).unwrap().external,
            }),
        )
        .await
        .unwrap();

        assert_that!(res).is_equal_to(&dto::Organisation {
            id: snowflake.from_internal(organisation.id).unwrap().external,
            name: org_name.to_string(),
        })
    }

    #[tokio::test]
    #[traced_test]
    async fn update_success() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let organisation_id = SnowflakeInternal::from_i32(1);
        let organisation_name = "Test Organisation";

        let organisation = db::Organisation {
            id: organisation_id.clone(),
            created_at: Local::now().naive_local(),
            name: organisation_name.to_string(),
        };
        let organisation_to_return = organisation.clone();

        let admin_id = SnowflakeInternal::from_i32(1);
        let academic_participant = db::AcademicParticipant {
            id: admin_id.clone(),
            created_at: Local::now().naive_local(),
            full_name: "Admin".to_string(),
        };

        let org_id_with = snowflake.from_internal(organisation_id).unwrap();
        db.organisation
            .expect_update_name()
            .times(1)
            .withf(move |org_id, name| *org_id == org_id_with.clone() && name == organisation_name)
            .returning(move |_, _| Ok(organisation_to_return.clone()));

        let Json(res) = super::organisation_update(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgId {
                org_id: snowflake.from_internal(organisation_id).unwrap().external,
            }),
            OrganisationAdmin(academic_participant.clone()),
            Json(UpdateOrganisationParams {
                name: organisation_name.to_string(),
            }),
        )
        .await
        .unwrap();

        assert_that!(res).is_equal_to(&dto::Organisation {
            id: snowflake.from_internal(organisation.id).unwrap().external,
            name: organisation_name.to_string(),
        })
    }

    // #[tokio::test]
    // #[traced_test]
    // async fn modify_participant_success() {
    //     let snowflake = snowflake::test::snowflake_manager();

    //     let org_id = SnowflakeInternal::from_i32(1);
    //     let org_role_id = SnowflakeInternal::from_i32(1);
    //     let subject_id = SnowflakeInternal::from_i32(1);
    //     let referrer_id = SnowflakeInternal::from_i32(1);
    //     let org_participation_id = SnowflakeInternal::from_i32(1);

    //     let organisation_role = db::OrganisationRole {
    //         id: org_role_id.clone(),
    //         created_at: Local::now().naive_local(),
    //         name: "Admin".to_string()
    //     };
    //     let organisation_role_to_return = HashSet::from([organisation_role.clone()]);

    //     let new_organisation_role = db::OrganisationRole {
    //         id: org_role_id.clone(),
    //         created_at: Local::now().naive_local(),
    //         name: "Participant".to_string()
    //     };
    //     let new_organisation_role_to_return = HashSet::from([new_organisation_role.clone()]);

    //     let organisation_participation = db::OrganisationParticipation {
    //         id: org_participation_id.clone(),
    //         started_at: Local::now().date_naive(),
    //         stopped_at: None,
    //         organisation_id: org_id.clone(),
    //         role_id: org_role_id.clone(),
    //         subject_id: subject_id.clone(),
    //         referrer_id: referrer_id.clone(),
    //         participation_state: db::ParticipationState::Active
    //     };
    //     let organisation_participation_to_return = organisation_participation.clone();
    //     let organisation_participation_to_remove = organisation_participation.clone();

    //     let new_org_participation_id = SnowflakeInternal::from_i32(2);
    //     let started_at_with = Local::now().date_naive();
    //     let new_organisation_participation = db::OrganisationParticipation {
    //         id: new_org_participation_id.clone(),
    //         started_at: started_at_with.clone(),
    //         stopped_at: None,
    //         organisation_id: org_id.clone(),
    //         role_id: org_role_id.clone(),
    //         subject_id: subject_id.clone(),
    //         referrer_id: referrer_id.clone(),
    //         participation_state: db::ParticipationState::Pending
    //     };
    //     let new_organisation_participation_to_return = new_organisation_participation.clone();

    //     let admin_id = SnowflakeInternal::from_i32(1);
    //     let academic_participant = db::AcademicParticipant {
    //         id: admin_id.clone(),
    //         created_at: Local::now().naive_local(),
    //         full_name: "Admin".to_string()
    //     };

    //     let mut organisation_participants_db = db::mocks::MockOrganisationParticipationMethods::new();

    //     let org_id_with = snowflake.from_internal(org_id).unwrap();
    //     let subject_id_with = snowflake.from_internal(subject_id).unwrap();
    //     organisation_participants_db
    //         .expect_get_roles_for()
    //         .times(1)
    //         .withf(move |org_id, subject_id|
    //             *org_id == org_id_with.clone() &&
    //             *subject_id == subject_id_with.clone()
    //         )
    //         .returning(move |_, _| Ok(organisation_role_to_return.clone()));

    //     let mut organisation_participants_db = db::mocks::MockOrganisationParticipationMethods::new();

    //     let org_id_with = snowflake.from_internal(org_id).unwrap();
    //     let subject_id_with = snowflake.from_internal(subject_id).unwrap();
    //     let org_role_id_with = snowflake.from_internal(org_role_id).unwrap();
    //     organisation_participants_db
    //         .expect_get_participation_by()
    //         .times(1)
    //         .with(
    //             predicate::eq(org_id_with),
    //             predicate::eq(subject_id_with),
    //             predicate::eq(org_role_id_with)
    //         )
    //         .returning(move |_, _, _| Ok(organisation_participation_to_return.clone()));

    //     let org_participation_id_with = snowflake.from_internal(org_participation_id).unwrap();
    //     organisation_participants_db
    //         .expect_stop_participation()
    //         .times(1)
    //         .with(predicate::eq(org_participation_id_with))
    //         .returning(move |_| Ok(organisation_participation_to_remove.clone()));

    //     let new_org_id_with = snowflake.from_internal(org_id).unwrap();
    //     let org_role_id_with = snowflake.from_internal(org_role_id).unwrap();
    //     let subject_id_with = snowflake.from_internal(subject_id).unwrap();
    //     let referrer_id_with = snowflake.from_internal(referrer_id).unwrap();
    //     organisation_participants_db
    //         .expect_insert_new_participation()
    //         .times(1)
    //         .with(
    //             predicate::eq(new_org_id_with),
    //             predicate::eq(org_role_id_with),
    //             predicate::eq(subject_id_with),
    //             predicate::eq(referrer_id_with),
    //             predicate::eq(started_at_with.clone()),
    //             predicate::eq(db::ParticipationState::Pending)
    //         )
    //         .returning(move |_, _, _, _, _, _| Ok(new_organisation_participation.clone()));

    //     organisation_participants_db
    //         .expect_get_roles_for()
    //         .times(1)
    //         .with(
    //             predicate::eq(snowflake.from_internal(org_id).unwrap()),
    //             predicate::eq(snowflake.from_internal(subject_id).unwrap())
    //         )
    //         .returning(move |_, _| Ok(new_organisation_role_to_return.clone()));

    //     let Json(rez) = super::organisation_modify_participant(
    //         SnowflakeState(snowflake.clone()),
    //         OrganisationParticipationMethods(Box::new(organisation_participants_db)),
    //         Path(
    //             OrgWithPar {
    //                 org_id: snowflake.from_internal(org_id).unwrap().external,
    //                 par_id: snowflake.from_internal(subject_id).unwrap().external,
    //             }
    //         ),
    //         OrganisationAdmin(academic_participant.clone()),
    //         Json(vec![dto::OrganisationRole{
    //             id: snowflake.from_internal(org_role_id).unwrap().external,
    //             name: "Participant".to_string(),
    //         }]),
    //     );

    // }

    #[tokio::test]
    #[traced_test]
    async fn remove_participant_success() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let org_id = SnowflakeInternal::from_i32(1);
        let participant_id = SnowflakeInternal::from_i32(1);
        let org_participation_id = SnowflakeInternal::from_i32(1);
        let admin_id = SnowflakeInternal::from_i32(1);

        let org_role_id = SnowflakeInternal::from_i32(1);
        let organisation_role = db::OrganisationRole {
            id: org_role_id.clone(),
            created_at: Local::now().naive_local(),
            name: "Admin".to_string(),
        };
        let organisation_role_to_return = HashSet::from([organisation_role.clone()]);

        let organisation_participation = db::OrganisationParticipation {
            id: org_participation_id.clone(),
            started_at: Local::now().date_naive(),
            stopped_at: None,
            organisation_id: org_id.clone(),
            role_id: org_role_id.clone(),
            subject_id: participant_id.clone(),
            referrer_id: participant_id.clone(),
            participation_state: db::ParticipationState::Active,
        };
        let organisation_participation_to_return = organisation_participation.clone();
        let organisation_participation_to_remove = organisation_participation.clone();

        let academic_participant = db::AcademicParticipant {
            id: admin_id.clone(),
            created_at: Local::now().naive_local(),
            full_name: "Admin".to_string(),
        };

        let org_id_with = snowflake.from_internal(org_id).unwrap();
        let par_id_with = snowflake.from_internal(participant_id).unwrap();

        db.organisation_participation
            .expect_get_roles_for()
            .times(1)
            .withf(move |org_id, par_id| {
                *org_id == org_id_with.clone() && *par_id == par_id_with.clone()
            })
            .returning(move |_, _| Ok(organisation_role_to_return.clone()));

        let org_id_with = snowflake.from_internal(org_id).unwrap();
        let par_id_with = snowflake.from_internal(participant_id).unwrap();
        let org_role_id_with = snowflake.from_internal(org_role_id).unwrap();
        db.organisation_participation
            .expect_get_participation_by()
            .times(1)
            .withf(move |org_id, par_id, role_id| {
                *org_id == org_id_with.clone()
                    && *par_id == par_id_with.clone()
                    && *role_id == org_role_id_with.clone()
            })
            .returning(move |_, _, _| Ok(organisation_participation_to_return.clone()));

        let org_participation_id_with = snowflake.from_internal(org_participation_id).unwrap();
        db.organisation_participation
            .expect_stop_participation()
            .times(1)
            .withf(move |participation_id| *participation_id == org_participation_id_with.clone())
            .returning(move |_| Ok(organisation_participation_to_remove.clone()));

        let res = super::organisation_remove_participant(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(OrgWithPar {
                org_id: snowflake.from_internal(org_id).unwrap().external,
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            OrganisationAdmin(academic_participant.clone()),
        )
        .await
        .unwrap();

        assert_that!(res).is_equal_to(())
    }
}
