use axum::{
    debug_handler,
    extract::{FromRef, Path, Query},
    http, routing, Json, Router,
};
use serde::{Deserialize, Serialize};
use tracing::instrument;

use super::{dto, ApiError, Result};
#[cfg(debug_assertions)]
use crate::server::AppState;
use crate::{
    auth::{
        self,
        extract::{ConnectedAcademicParticipant, OryApiStateProvider},
    },
    db::{
        self,
        extract::{Database, DatabaseStateProvider},
        Entity, OrganisationParticipation, PublicationParticipation,
    },
    snowflake::{extract::SnowflakeState, SnowflakeConverter, SnowflakeExternal, SnowflakeManager},
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ParId {
    pub par_id: SnowflakeExternal<db::AcademicParticipant>,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participants_list_all(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
) -> Result<Json<Vec<dto::Participant>>> {
    let participants = db.academic_participants().list_all().await?;
    let res = participants
        .into_iter()
        .map(|par| Ok(dto::Participant::from_db(&snowflake, par)?))
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participants_fuzzy_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::Participant>>> {
    let participants = db
        .academic_participants()
        .get_by_fuzzy_name(&query.search, query.n as i64)
        .await?;
    let res = participants
        .into_iter()
        .map(|par| Ok(dto::Participant::from_db(&snowflake, par)?))
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_get_by_id(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id }): Path<ParId>,
) -> Result<Json<dto::Participant>> {
    let participant = db
        .academic_participants()
        .ensure(&snowflake.from_external(par_id)?)
        .await?;
    let res = dto::Participant::from_db(&snowflake, participant)?;
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_get_meta(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id }): Path<ParId>,
    maybe_academic_participant: Option<ConnectedAcademicParticipant>,
) -> Result<Json<dto::ParticipantMeta>> {
    let par_id = snowflake.from_external(par_id)?;
    let citations = db
        .publication_reference()
        .get_citation_count_for_participant(&par_id)
        .await?;
    let connected = maybe_academic_participant.is_some();
    let editable = maybe_academic_participant.is_some_and(
        |ConnectedAcademicParticipant(academic_participant)| {
            academic_participant.id == par_id.internal
        },
    );
    let res = dto::ParticipantMeta {
        citations: citations.count as usize,
        editable,
        connected,
    };
    Ok(Json(res))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_publications(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id }): Path<ParId>,
) -> Result<Json<Vec<dto::ParticipantPublication>>> {
    let participants = db
        .publication_participation()
        .get_active_participation_for_participant(&snowflake.from_external(par_id)?)
        .await?;
    let res = participants
        .into_iter()
        .map(|(publication, (role, code))| {
            Ok(dto::ParticipantPublication::from_db(
                &snowflake,
                publication,
                code,
                role,
            )?)
        })
        .collect::<Result<_>>()?;
    Ok(Json(res))
}

#[derive(Debug, Deserialize)]
pub struct OrganisationsQuery {
    active: Option<bool>,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_organisations(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id }): Path<ParId>,
    Query(query): Query<OrganisationsQuery>,
) -> Result<Json<Vec<dto::ParticipantOrganisation>>> {
    let active = query.active.unwrap_or(true);
    let par_id = snowflake.from_external(par_id)?;
    let organisations = match active {
        true => {
            db.organisation_participation()
                .get_active_participation_for_user(&par_id)
                .await?
        }
        false => {
            db.organisation_participation()
                .get_historic_participation_for_user(&par_id)
                .await?
        }
    };
    let resp = organisations
        .into_iter()
        .map(|(organisation, roles)| {
            Ok(dto::ParticipantOrganisation::from_db(
                &snowflake,
                organisation,
                roles,
            )?)
        })
        .collect::<Result<_>>()?;
    Ok(Json(resp))
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ReferenceTypes {
    CoParticipants,
    ReferencedTo,
    ReferencedFrom,
}

#[derive(Debug, Deserialize)]
pub struct ReferenceQuery {
    t: ReferenceTypes,
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_references(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id }): Path<ParId>,
    Query(query): Query<ReferenceQuery>,
) -> Result<Json<Vec<dto::ParticipantReference>>> {
    let par_id = snowflake.from_external(par_id)?;
    let organisations = match query.t {
        ReferenceTypes::CoParticipants => {
            db.publication_participation()
                .get_participants_connected_via_publications(&par_id)
                .await?
        }
        ReferenceTypes::ReferencedFrom => {
            db.publication_participation()
                .get_participants_connected_via_from_refs(&par_id)
                .await?
        }
        ReferenceTypes::ReferencedTo => {
            db.publication_participation()
                .get_participants_connected_via_to_refs(&par_id)
                .await?
        }
    };
    let resp = organisations
        .into_iter()
        .map(|(participant, score)| {
            Ok(dto::ParticipantReference::from_db(
                &snowflake,
                participant,
                score,
            )?)
        })
        .collect::<Result<_>>()?;
    Ok(Json(resp))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_list_publication_invites(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id: subject_id }): Path<ParId>,
    ConnectedAcademicParticipant(connected_participant): ConnectedAcademicParticipant,
) -> Result<Json<Vec<dto::ParticipantPublicationInvitation>>> {
    if snowflake.from_internal(connected_participant.id)? != snowflake.from_external(subject_id)? {
        Err(ApiError::AuthError(
            auth::AuthError::InsufficientPermissions,
        ))?;
    }
    let invitations = db
        .publication_participation()
        .get_pending_participation_for_participant(&snowflake.from_external(subject_id)?)
        .await?;
    let invitations = invitations
        .into_iter()
        .map(|(participation, publication, referrer)| {
            Ok(dto::ParticipantPublicationInvitation {
                id: snowflake
                    .from_internal(participation.participation_id)?
                    .external,
                publication_title: publication.title,
                inviter_name: referrer.full_name,
            })
        })
        .collect::<Result<_>>()?;
    Ok(Json(invitations))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_resolve_publication_invite(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id: subject_id }): Path<ParId>,
    ConnectedAcademicParticipant(connected_participant): ConnectedAcademicParticipant,
    Json(invitation_response): Json<dto::PublicationInvitationResponse>,
) -> Result<http::StatusCode> {
    if snowflake.from_internal(connected_participant.id)? != snowflake.from_external(subject_id)? {
        Err(ApiError::AuthError(
            auth::AuthError::InsufficientPermissions,
        ))?;
    }
    let invitations = db
        .publication_participation()
        .get_pending_participation_for_participant(&snowflake.from_external(subject_id)?)
        .await?;
    let participation_id = snowflake.from_external(invitation_response.id)?;
    if let None = invitations
        .into_iter()
        .find(|(participation, _, _)| participation.participation_id == participation_id.internal)
    {
        Err(ApiError::DbError(db::DbError::NoEntity {
            entity: PublicationParticipation::ENTITY,
            id: participation_id.external.to_hex(),
        }))?;
    }
    db.publication_participation()
        .update_participation_state(&participation_id, &invitation_response.status.into())
        .await?;
    Ok(http::StatusCode::NO_CONTENT)
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_list_organisation_invites(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id: subject_id }): Path<ParId>,
    ConnectedAcademicParticipant(connected_participant): ConnectedAcademicParticipant,
) -> Result<Json<Vec<dto::ParticipantOrganisationInvitation>>> {
    if snowflake.from_internal(connected_participant.id)? != snowflake.from_external(subject_id)? {
        Err(ApiError::AuthError(
            auth::AuthError::InsufficientPermissions,
        ))?;
    }
    let invitations = db
        .organisation_participation()
        .get_pending_participation_for_user(&snowflake.from_external(subject_id)?)
        .await?;
    let invitations = invitations
        .into_iter()
        .map(|(participation, organisation, referrer)| {
            Ok(dto::ParticipantOrganisationInvitation {
                id: snowflake.from_internal(participation.id)?.external,
                organisation_name: organisation.name,
                inviter_name: referrer.full_name,
            })
        })
        .collect::<Result<_>>()?;
    Ok(Json(invitations))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn participant_resolve_organisation_invite(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Path(ParId { par_id: subject_id }): Path<ParId>,
    ConnectedAcademicParticipant(connected_participant): ConnectedAcademicParticipant,
    Json(invitation_response): Json<dto::OrganisationInvitationResponse>,
) -> Result<http::StatusCode> {
    if snowflake.from_internal(connected_participant.id)? != snowflake.from_external(subject_id)? {
        Err(ApiError::AuthError(
            auth::AuthError::InsufficientPermissions,
        ))?;
    }
    let invitations = db
        .organisation_participation()
        .get_pending_participation_for_user(&snowflake.from_external(subject_id)?)
        .await?;
    let participation_id = snowflake.from_external(invitation_response.id)?;
    if let None = invitations
        .into_iter()
        .find(|(participation, _, _)| participation.id == participation_id.internal)
    {
        Err(ApiError::DbError(db::DbError::NoEntity {
            entity: OrganisationParticipation::ENTITY,
            id: participation_id.external.to_hex(),
        }))?;
    }
    db.organisation_participation()
        .update_participation_state(&participation_id, &invitation_response.status.into())
        .await?;
    Ok(http::StatusCode::NO_CONTENT)
}

#[cfg(not(tarpaulin_include))]
pub fn routes<S>() -> Router<S>
where
    SnowflakeManager: FromRef<S>,
    S: DatabaseStateProvider + OryApiStateProvider + Send + Sync + Clone + 'static,
{
    Router::new()
        .route("/all", routing::get(participants_list_all))
        .route("/search", routing::get(participants_fuzzy_search))
        .route("/:par_id", routing::get(participant_get_by_id))
        .route("/:par_id/meta", routing::get(participant_get_meta))
        .route(
            "/:par_id/publications",
            routing::get(participant_publications),
        )
        .route(
            "/:par_id/organisations",
            routing::get(participant_organisations),
        )
        .route("/:par_id/references", routing::get(participant_references))
        .route(
            "/:par_id/publications/invitations",
            routing::get(participant_list_publication_invites),
        )
        .route(
            "/:par_id/publications/invitations",
            routing::post(participant_resolve_publication_invite),
        )
        .route(
            "/:par_id/organisations/invitations",
            routing::get(participant_list_organisation_invites),
        )
        .route(
            "/:par_id/organisations/invitations",
            routing::post(participant_resolve_organisation_invite),
        )
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use axum::{
        extract::{Path, Query},
        Json,
    };
    use chrono::prelude::*;

    use mockall::predicate;
    use spectral::prelude::*;
    use tracing_test::traced_test;

    use crate::{
        api::{
            dto,
            participants::{OrganisationsQuery, ParId, ReferenceQuery, ReferenceTypes},
            ApiError,
        },
        auth::{extract::ConnectedAcademicParticipant, AuthError},
        db::{self, extract::Database, DbError, Entity},
        snowflake::{self, extract::SnowflakeState, SnowflakeConverter, SnowflakeInternal},
    };

    #[tokio::test]
    #[traced_test]
    async fn list_all() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".to_string(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".to_string(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(3),
                created_at: Local::now().naive_local(),
                full_name: "John Smith".to_string(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(4),
                created_at: Local::now().naive_local(),
                full_name: "Jane Smith".to_string(),
            },
        ];
        let participant_to_return = participants.clone();

        db.academic_participants
            .expect_list_all()
            .times(1)
            .returning(move || Ok(participant_to_return.clone()));

        let Json(got_participants) =
            super::participants_list_all(SnowflakeState(snowflake.clone()), Database(Arc::new(db)))
                .await
                .unwrap();

        assert_eq!(
            got_participants,
            participants
                .into_iter()
                .map(|p| dto::Participant {
                    id: snowflake.from_internal(p.id).unwrap().external,
                    name: p.full_name.to_string(),
                })
                .collect::<Vec<_>>()
        );
    }

    #[tokio::test]
    #[traced_test]
    async fn get_by_id() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let participant_to_return = participant.clone();

        db.academic_participants
            .expect_ensure()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant.id).unwrap(),
            ))
            .returning(move |_| Ok(participant_to_return.clone()));

        let Json(got_participant) = super::participant_get_by_id(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant.id).unwrap().external,
            }),
        )
        .await
        .unwrap();

        assert_eq!(
            got_participant,
            dto::Participant {
                id: snowflake.from_internal(participant.id).unwrap().external,
                name: participant.full_name.to_string(),
            }
        );
    }

    #[tokio::test]
    #[traced_test]
    async fn get_by_id_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant_id_hex = snowflake
            .from_internal(participant_id)
            .unwrap()
            .external
            .to_hex();
        let participant_hex_mock = participant_id_hex.clone();

        db.academic_participants
            .expect_ensure()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .return_once(move |_| {
                Err(DbError::NoEntity {
                    entity: db::AcademicParticipant::ENTITY,
                    id: participant_hex_mock.clone(),
                })
            });

        let got_error = super::participant_get_by_id(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
        )
        .await
        .unwrap_err();

        assert!(matches!(
            got_error,
            ApiError::DbError(
                DbError::NoEntity{
                    entity: db::AcademicParticipant::ENTITY,
                    id: got_id
                }
            ) if &got_id == &participant_id_hex
        ));
    }

    #[tokio::test]
    #[traced_test]
    async fn fuzzy_search() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participants = vec![
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                full_name: "John Doe".to_string(),
            },
            db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                full_name: "Jane Doe".to_string(),
            },
        ];
        let participants_to_return = participants.clone();

        db.academic_participants
            .expect_get_by_fuzzy_name()
            .times(1)
            .returning(move |_, _| Ok(participants_to_return.clone()));

        let Json(got_participants) = super::participants_fuzzy_search(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Query(dto::FuzzySearchQuery {
                search: "Doe".to_string(),
                n: 10,
            }),
        )
        .await
        .unwrap();

        let participant_to_compare = participants
            .into_iter()
            .map(|p| dto::Participant {
                id: snowflake.from_internal(p.id).unwrap().external,
                name: p.full_name.to_string(),
            })
            .collect::<Vec<_>>();

        assert_that!(got_participants).contains_all_of(&&participant_to_compare);
    }

    // #[tokio::test]
    // #[traced_test]
    // async fn fuzzy_search_error() {
    //     let snowflake = snowflake::test::snowflake_manager();

    //     let mut academic_participant = db::mocks::MockAcademicParticipantMethods::new();
    //     academic_participant
    //         .expect_get_by_fuzzy_name()
    //         .times(1)
    //         .returning(move |_, _| Err(DbError::IoError(
    //             tokio::io::Error::new(
    //                 tokio::io::ErrorKind::Other,
    //                 "Error",
    //             )
    //         )));

    //     let got_error = super::participants_fuzzy_search(
    //         SnowflakeState(snowflake.clone()),
    //         AcademicParticipantMethods(Box::new(academic_participant)),
    //         Query(dto::FuzzySearchQuery{
    //             search: "Doe".to_string(),
    //             n: 10,
    //         }),
    //     )
    //     .await
    //     .unwrap_err();

    //     let error_to_compare = ApiError::DbError
    //         (DbError::IoError(std::io::Error::new(
    //             std::io::ErrorKind::Other,
    //             "Error",
    //         ))
    //     );
    //     assert!(matches!(
    //         got_error, ApiError::DbError(
    //             DbError::IoError(
    //                 tokio::io::Error::new(
    //                     tokio::io::ErrorKind::Other,
    //                     "Error",
    //                 )
    //             )
    //         )
    //     ));
    // }

    #[tokio::test]
    #[traced_test]
    async fn get_meta_editable() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let _academic_participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        db.publication_reference
            .expect_get_citation_count_for_participant()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(db::Count { count: 42 }));

        let Json(got_meta) = super::participant_get_meta(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Option::from(ConnectedAcademicParticipant(connected_participant)),
        )
        .await
        .unwrap();

        assert_that!(got_meta).is_equal_to(&dto::ParticipantMeta {
            citations: 42,
            editable: true,
            connected: true,
        });
    }

    #[tokio::test]
    #[traced_test]
    async fn get_meta_not_editable() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let _academic_participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(2),
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        };

        db.publication_reference
            .expect_get_citation_count_for_participant()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(db::Count { count: 42 }));

        let Json(got_meta) = super::participant_get_meta(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Option::from(ConnectedAcademicParticipant(connected_participant)),
        )
        .await
        .unwrap();

        assert_that!(got_meta).is_equal_to(&dto::ParticipantMeta {
            citations: 42,
            editable: false,
            connected: true,
        });
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_publications() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let publications = std::collections::HashMap::from([
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                (
                    db::PublicationRole {
                        id: SnowflakeInternal::from_i32(1),
                        created_at: Local::now().naive_local(),
                        name: "Author".to_string(),
                    },
                    db::SubjectCode {
                        id: SnowflakeInternal::from_i32(1),
                        created_at: Local::now().naive_local(),
                        name: "Subject 1".to_string(),
                        code: "SUBJ_1".to_string(),
                    },
                ),
            ),
            (
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(2),
                    title: "Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                (
                    db::PublicationRole {
                        id: SnowflakeInternal::from_i32(2),
                        created_at: Local::now().naive_local(),
                        name: "Editor".to_string(),
                    },
                    db::SubjectCode {
                        id: SnowflakeInternal::from_i32(2),
                        created_at: Local::now().naive_local(),
                        name: "Subject 2".to_string(),
                        code: "SUBJ_2".to_string(),
                    },
                ),
            ),
        ]);
        let publications_to_return = publications.clone();

        db.publication_participation
            .expect_get_active_participation_for_participant()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(publications_to_return.clone()));

        let Json(got_publications) = super::participant_publications(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
        )
        .await
        .unwrap();

        let publications_to_compare = publications
            .into_iter()
            .map(|(publication, (role, code))| dto::ParticipantPublication {
                id: snowflake.from_internal(publication.id).unwrap().external,
                title: publication.title,
                published_at: publication.published_at,
                subject_code: dto::SubjectCode {
                    id: snowflake.from_internal(code.id).unwrap().external,
                    code: code.code,
                    description: code.name,
                },
                role: dto::ParticipationRole {
                    id: snowflake.from_internal(role.id).unwrap().external,
                    name: role.name,
                },
            })
            .collect::<Vec<_>>();

        assert_that!(got_publications).contains_all_of(&&publications_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_publications_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant_id_hex = snowflake
            .from_internal(participant_id)
            .unwrap()
            .external
            .to_hex();

        db.publication_participation
            .expect_get_active_participation_for_participant()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| {
                Err(DbError::NoEntity {
                    entity: db::AcademicParticipant::ENTITY,
                    id: participant_id_hex.clone(),
                })
            });

        let result = super::participant_publications(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(DbError::NoEntity {
                entity: db::AcademicParticipant::ENTITY,
                id: _participant_id_hex
            })
        ));
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_active_organisations() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let organisations = std::collections::HashMap::from([
            (
                db::Organisation {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 1".to_string(),
                },
                vec![
                    db::OrganisationRole {
                        id: SnowflakeInternal::from_i32(1),
                        created_at: Local::now().naive_local(),
                        name: "Organisation 1 Role".to_string(),
                    },
                    db::OrganisationRole {
                        id: SnowflakeInternal::from_i32(2),
                        created_at: Local::now().naive_local(),
                        name: "Organisation 1 Role 2".to_string(),
                    },
                ],
            ),
            (
                db::Organisation {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2".to_string(),
                },
                vec![db::OrganisationRole {
                    id: SnowflakeInternal::from_i32(3),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2 Role".to_string(),
                }],
            ),
        ]);
        let organisations_to_return = organisations.clone();

        db.organisation_participation
            .expect_get_active_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(organisations_to_return.clone()));

        let Json(got_organisations) = super::participant_organisations(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(OrganisationsQuery { active: Some(true) }),
        )
        .await
        .unwrap();

        let organisations_to_compare = organisations
            .into_iter()
            .map(|(organisation, roles)| dto::ParticipantOrganisation {
                id: snowflake.from_internal(organisation.id).unwrap().external,
                name: organisation.name,
                roles: roles
                    .into_iter()
                    .map(|role| dto::OrganisationRole {
                        id: snowflake.from_internal(role.id).unwrap().external,
                        name: role.name,
                    })
                    .collect::<Vec<_>>(),
            })
            .collect::<Vec<_>>();

        assert_that!(got_organisations).contains_all_of(&&organisations_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_historic_organisations() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let organisations = std::collections::HashMap::from([
            (
                db::Organisation {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 1".to_string(),
                },
                vec![
                    db::OrganisationRole {
                        id: SnowflakeInternal::from_i32(1),
                        created_at: Local::now().naive_local(),
                        name: "Organisation 1 Role".to_string(),
                    },
                    db::OrganisationRole {
                        id: SnowflakeInternal::from_i32(2),
                        created_at: Local::now().naive_local(),
                        name: "Organisation 1 Role 2".to_string(),
                    },
                ],
            ),
            (
                db::Organisation {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2".to_string(),
                },
                vec![db::OrganisationRole {
                    id: SnowflakeInternal::from_i32(3),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2 Role".to_string(),
                }],
            ),
        ]);
        let organisations_to_return = organisations.clone();

        // let mut db = db::mocks::MockOrganisationParticipationMethods::new();
        db.organisation_participation
            .expect_get_historic_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(organisations_to_return.clone()));

        let Json(got_organisations) = super::participant_organisations(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(OrganisationsQuery {
                active: Some(false),
            }),
        )
        .await
        .unwrap();

        let organisations_to_compare = organisations
            .into_iter()
            .map(|(organisation, roles)| dto::ParticipantOrganisation {
                id: snowflake.from_internal(organisation.id).unwrap().external,
                name: organisation.name,
                roles: roles
                    .into_iter()
                    .map(|role| dto::OrganisationRole {
                        id: snowflake.from_internal(role.id).unwrap().external,
                        name: role.name,
                    })
                    .collect::<Vec<_>>(),
            })
            .collect::<Vec<_>>();

        assert_that!(got_organisations).contains_all_of(&&organisations_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_active_organisations_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant_id_hex = snowflake
            .from_internal(participant_id)
            .unwrap()
            .external
            .to_hex();

        db.organisation_participation
            .expect_get_active_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| {
                Err(DbError::NoEntity {
                    entity: db::AcademicParticipant::ENTITY,
                    id: participant_id_hex.clone(),
                })
            });

        let result = super::participant_organisations(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(OrganisationsQuery { active: Some(true) }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(DbError::NoEntity {
                entity: db::AcademicParticipant::ENTITY,
                id: _participant_id_hex
            })
        ));
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_historic_organisations_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant_id_hex = snowflake
            .from_internal(participant_id)
            .unwrap()
            .external
            .to_hex();

        db.organisation_participation
            .expect_get_historic_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| {
                Err(DbError::NoEntity {
                    entity: db::AcademicParticipant::ENTITY,
                    id: participant_id_hex.clone(),
                })
            });

        let result = super::participant_organisations(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(OrganisationsQuery {
                active: Some(false),
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(DbError::NoEntity {
                entity: db::AcademicParticipant::ENTITY,
                id: _participant_id_hex
            })
        ));
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_co_participants_references() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let references = std::collections::HashMap::from([
            (
                db::AcademicParticipant {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    full_name: "John Doe".to_string(),
                },
                db::Score { score: 1 },
            ),
            (
                db::AcademicParticipant {
                    id: SnowflakeInternal::from_i32(3),
                    created_at: Local::now().naive_local(),
                    full_name: "Jane Doe".to_string(),
                },
                db::Score { score: 1 },
            ),
        ]);
        let references_to_return = references.clone();

        db.publication_participation
            .expect_get_participants_connected_via_publications()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(references_to_return.clone()));

        let Json(got_references) = super::participant_references(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(ReferenceQuery {
                t: ReferenceTypes::CoParticipants,
            }),
        )
        .await
        .unwrap();

        let references_to_compare = references
            .into_iter()
            .map(|(participant, score)| dto::ParticipantReference {
                id: snowflake.from_internal(participant.id).unwrap().external,
                name: participant.full_name,
                score: score.score as usize,
            })
            .collect::<Vec<_>>();

        assert_that!(got_references).contains_all_of(&&references_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_referenced_to_references() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let references = std::collections::HashMap::from([
            (
                db::AcademicParticipant {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    full_name: "John Doe".to_string(),
                },
                db::Score { score: 1 },
            ),
            (
                db::AcademicParticipant {
                    id: SnowflakeInternal::from_i32(3),
                    created_at: Local::now().naive_local(),
                    full_name: "Jane Doe".to_string(),
                },
                db::Score { score: 1 },
            ),
        ]);
        let references_to_return = references.clone();

        db.publication_participation
            .expect_get_participants_connected_via_to_refs()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(references_to_return.clone()));

        let Json(got_references) = super::participant_references(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(ReferenceQuery {
                t: ReferenceTypes::ReferencedTo,
            }),
        )
        .await
        .unwrap();

        let references_to_compare = references
            .into_iter()
            .map(|(participant, score)| dto::ParticipantReference {
                id: snowflake.from_internal(participant.id).unwrap().external,
                name: participant.full_name,
                score: score.score as usize,
            })
            .collect::<Vec<_>>();

        assert_that!(got_references).contains_all_of(&&references_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_referenced_from_references() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let references = std::collections::HashMap::from([
            (
                db::AcademicParticipant {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    full_name: "John Doe".to_string(),
                },
                db::Score { score: 1 },
            ),
            (
                db::AcademicParticipant {
                    id: SnowflakeInternal::from_i32(3),
                    created_at: Local::now().naive_local(),
                    full_name: "Jane Doe".to_string(),
                },
                db::Score { score: 1 },
            ),
        ]);
        let references_to_return = references.clone();

        db.publication_participation
            .expect_get_participants_connected_via_from_refs()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(references_to_return.clone()));

        let Json(got_references) = super::participant_references(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(ReferenceQuery {
                t: ReferenceTypes::ReferencedFrom,
            }),
        )
        .await
        .unwrap();

        let references_to_compare = references
            .into_iter()
            .map(|(participant, score)| dto::ParticipantReference {
                id: snowflake.from_internal(participant.id).unwrap().external,
                name: participant.full_name,
                score: score.score as usize,
            })
            .collect::<Vec<_>>();

        assert_that!(got_references).contains_all_of(&&references_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_co_participants_references_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant_id_hex = snowflake
            .from_internal(participant_id)
            .unwrap()
            .external
            .to_hex();

        db.publication_participation
            .expect_get_participants_connected_via_publications()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| {
                Err(DbError::NoEntity {
                    entity: db::AcademicParticipant::ENTITY,
                    id: participant_id_hex.clone(),
                })
            });

        let result = super::participant_references(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(ReferenceQuery {
                t: ReferenceTypes::CoParticipants,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(DbError::NoEntity {
                entity: db::AcademicParticipant::ENTITY,
                id: _participant_id_hex
            })
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_referenced_to_references_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant_id_hex = snowflake
            .from_internal(participant_id)
            .unwrap()
            .external
            .to_hex();

        db.publication_participation
            .expect_get_participants_connected_via_to_refs()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| {
                Err(DbError::NoEntity {
                    entity: db::AcademicParticipant::ENTITY,
                    id: participant_id_hex.clone(),
                })
            });

        let result = super::participant_references(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(ReferenceQuery {
                t: ReferenceTypes::ReferencedTo,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(DbError::NoEntity {
                entity: db::AcademicParticipant::ENTITY,
                id: _participant_id_hex
            })
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn get_participant_referenced_from_references_not_found() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant_id_hex = snowflake
            .from_internal(participant_id)
            .unwrap()
            .external
            .to_hex();

        db.publication_participation
            .expect_get_participants_connected_via_from_refs()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| {
                Err(DbError::NoEntity {
                    entity: db::AcademicParticipant::ENTITY,
                    id: participant_id_hex.clone(),
                })
            });

        let result = super::participant_references(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            Query(ReferenceQuery {
                t: ReferenceTypes::ReferencedFrom,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(DbError::NoEntity {
                entity: db::AcademicParticipant::ENTITY,
                id: _participant_id_hex
            })
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn list_publication_invites() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participation_id = SnowflakeInternal::from_i32(12);

        let subject_id = SnowflakeInternal::from_i32(66);
        let subject = db::AcademicParticipant {
            id: subject_id,
            created_at: Local::now().naive_local(),
            full_name: "Subject name".to_string(),
        };

        let referrer_id = SnowflakeInternal::from_i32(67);
        let referrer_name = "Referrer name";

        let publication_id = SnowflakeInternal::from_i32(81);
        let publication_title = "Publication title";

        let pending_participation = vec![(
            db::PublicationParticipation {
                participation_id,
                publication_id,
                role_id: SnowflakeInternal::from_i32(5),
                subject_id,
                referrer_id,
                participation_state: db::ParticipationState::Pending,
            },
            db::Publication {
                id: publication_id,
                created_at: Local::now().naive_local(),
                code_id: SnowflakeInternal::from_i32(1),
                title: publication_title.to_string(),
                published_at: Local::now().date_naive(),
            },
            db::AcademicParticipant {
                id: referrer_id,
                created_at: Local::now().naive_local(),
                full_name: referrer_name.to_string(),
            },
        )];

        db.publication_participation
            .expect_get_pending_participation_for_participant()
            .with(snowflake::test::eq_internal(subject_id))
            .returning(move |_| Ok(pending_participation.clone()));

        let Json(notifications) = super::participant_list_publication_invites(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(subject_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(subject),
        )
        .await
        .unwrap();

        assert_that!(notifications).contains(&dto::ParticipantPublicationInvitation {
            id: snowflake.from_internal(participation_id).unwrap().external,
            publication_title: publication_title.to_string(),
            inviter_name: referrer_name.to_string(),
        })
    }

    #[tokio::test]
    #[traced_test]
    async fn list_publication_invites_without_permissions() {
        let snowflake = snowflake::test::snowflake_manager();
        let db = db::MockDatabase::new();

        let subject_id = SnowflakeInternal::from_i32(1);
        let _subject = db::AcademicParticipant {
            id: subject_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant_id = SnowflakeInternal::from_i32(2);
        let connected_participant = db::AcademicParticipant {
            id: connected_participant_id,
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        };

        let result = super::participant_list_publication_invites(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(subject_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
        )
        .await;

        assert!(result.is_err());
        assert!(matches!(
            result.unwrap_err(),
            ApiError::AuthError(AuthError::InsufficientPermissions)
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_publication_invite_accept() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);

        let pending_invitations = vec![
            (
                db::PublicationParticipation {
                    participation_id: invitation_id,
                    publication_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                participant.clone(),
            ),
            (
                db::PublicationParticipation {
                    participation_id: SnowflakeInternal::from_i32(2),
                    publication_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(2),
                    title: "Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                participant.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        let resolved_invitation = db::PublicationParticipation {
            participation_id: invitation_id,
            publication_id: SnowflakeInternal::from_i32(1),
            role_id: SnowflakeInternal::from_i32(1),
            subject_id: participant_id,
            referrer_id: SnowflakeInternal::from_i32(1),
            participation_state: db::ParticipationState::Active,
        };
        let resolved_invitation_to_return = resolved_invitation.clone();

        db.publication_participation
            .expect_get_pending_participation_for_participant()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        db.publication_participation
            .expect_update_participation_state()
            .times(1)
            .with(
                predicate::eq(snowflake.from_internal(invitation_id).unwrap()),
                predicate::eq(db::ParticipationState::Active),
            )
            .returning(move |_, _| Ok(resolved_invitation_to_return.clone()));

        let result = super::participant_resolve_publication_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::PublicationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Accept,
            }),
        )
        .await
        .unwrap();

        assert_that!(result).is_equal_to(http::StatusCode::NO_CONTENT);
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_publication_invite_decline() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);

        let pending_invitations = vec![
            (
                db::PublicationParticipation {
                    participation_id: invitation_id,
                    publication_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                participant.clone(),
            ),
            (
                db::PublicationParticipation {
                    participation_id: SnowflakeInternal::from_i32(2),
                    publication_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(2),
                    title: "Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                participant.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        let resolved_invitation = db::PublicationParticipation {
            participation_id: invitation_id,
            publication_id: SnowflakeInternal::from_i32(1),
            role_id: SnowflakeInternal::from_i32(1),
            subject_id: participant_id,
            referrer_id: SnowflakeInternal::from_i32(1),
            participation_state: db::ParticipationState::Declined,
        };
        let resolved_invitation_to_return = resolved_invitation.clone();

        db.publication_participation
            .expect_get_pending_participation_for_participant()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        db.publication_participation
            .expect_update_participation_state()
            .times(1)
            .with(
                predicate::eq(snowflake.from_internal(invitation_id).unwrap()),
                predicate::eq(db::ParticipationState::Declined),
            )
            .returning(move |_, _| Ok(resolved_invitation_to_return.clone()));

        let result = super::participant_resolve_publication_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::PublicationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Decline,
            }),
        )
        .await
        .unwrap();

        assert_that!(result).is_equal_to(http::StatusCode::NO_CONTENT);
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_publication_invite_accept_without_permissions() {
        let snowflake = snowflake::test::snowflake_manager();
        let db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(2),
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);

        let result = super::participant_resolve_publication_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::PublicationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Accept,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::AuthError(AuthError::InsufficientPermissions)
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_publication_invite_decline_without_permissions() {
        let snowflake = snowflake::test::snowflake_manager();
        let db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(2),
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);

        let result = super::participant_resolve_publication_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::PublicationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Decline,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::AuthError(AuthError::InsufficientPermissions)
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_publication_invite_accept_not_found_invitation() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);
        let _invitation_id_hex = snowflake
            .from_internal(invitation_id)
            .unwrap()
            .external
            .to_hex();

        let pending_invitations = vec![
            (
                db::PublicationParticipation {
                    participation_id: SnowflakeInternal::from_i32(2),
                    publication_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                participant.clone(),
            ),
            (
                db::PublicationParticipation {
                    participation_id: SnowflakeInternal::from_i32(3),
                    publication_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(2),
                    title: "Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                participant.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        db.publication_participation
            .expect_get_pending_participation_for_participant()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        let result = super::participant_resolve_publication_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::PublicationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Accept,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(db::DbError::NoEntity {
                entity: db::PublicationParticipation::ENTITY,
                id: _invitation_id_hex,
            })
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_publication_invite_decline_not_found_invitation() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);
        let _invitation_id_hex = snowflake
            .from_internal(invitation_id)
            .unwrap()
            .external
            .to_hex();

        let pending_invitations = vec![
            (
                db::PublicationParticipation {
                    participation_id: SnowflakeInternal::from_i32(2),
                    publication_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Publication {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(1),
                    title: "Publication 1".to_string(),
                    published_at: Local::now().date_naive(),
                },
                participant.clone(),
            ),
            (
                db::PublicationParticipation {
                    participation_id: SnowflakeInternal::from_i32(3),
                    publication_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Publication {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    code_id: SnowflakeInternal::from_i32(2),
                    title: "Publication 2".to_string(),
                    published_at: Local::now().date_naive(),
                },
                participant.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        db.publication_participation
            .expect_get_pending_participation_for_participant()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        let result = super::participant_resolve_publication_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::PublicationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Decline,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(db::DbError::NoEntity {
                entity: db::PublicationParticipation::ENTITY,
                id: _invitation_id_hex,
            })
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_invites() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let _participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let referrer = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(2),
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        };

        let pending_invitations = vec![
            (
                db::OrganisationParticipation {
                    id: SnowflakeInternal::from_i32(1),
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 1".to_string(),
                },
                referrer.clone(),
            ),
            (
                db::OrganisationParticipation {
                    id: SnowflakeInternal::from_i32(2),
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2".to_string(),
                },
                referrer.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        db.organisation_participation
            .expect_get_pending_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        let Json(got_invitations) = super::participant_list_organisation_invites(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
        )
        .await
        .unwrap();

        let invitations_to_compare = pending_invitations
            .into_iter()
            .map(|(participation, organisation, participant)| {
                dto::ParticipantOrganisationInvitation {
                    id: snowflake.from_internal(participation.id).unwrap().external,
                    organisation_name: organisation.name,
                    inviter_name: participant.full_name,
                }
            })
            .collect::<Vec<_>>();

        assert_that!(got_invitations).contains_all_of(&&invitations_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn list_organisation_invites_without_permissions() {
        let snowflake = snowflake::test::snowflake_manager();
        let db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(2),
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        };

        let result = super::participant_list_organisation_invites(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::AuthError(AuthError::InsufficientPermissions)
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_organisation_invite_accept() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);

        let pending_invitations = vec![
            (
                db::OrganisationParticipation {
                    id: invitation_id,
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 1".to_string(),
                },
                participant.clone(),
            ),
            (
                db::OrganisationParticipation {
                    id: SnowflakeInternal::from_i32(2),
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2".to_string(),
                },
                participant.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        let resolved_invitation = db::OrganisationParticipation {
            id: invitation_id,
            started_at: Local::now().date_naive(),
            stopped_at: None,
            organisation_id: SnowflakeInternal::from_i32(1),
            role_id: SnowflakeInternal::from_i32(1),
            subject_id: participant_id,
            referrer_id: SnowflakeInternal::from_i32(1),
            participation_state: db::ParticipationState::Active,
        };
        let resolved_invitation_to_return = resolved_invitation.clone();

        db.organisation_participation
            .expect_get_pending_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        db.organisation_participation
            .expect_update_participation_state()
            .times(1)
            .with(
                predicate::eq(snowflake.from_internal(invitation_id).unwrap()),
                predicate::eq(db::ParticipationState::Active),
            )
            .returning(move |_, _| Ok(resolved_invitation_to_return.clone()));

        let result = super::participant_resolve_organisation_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::OrganisationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Accept,
            }),
        )
        .await
        .unwrap();

        assert_that!(result).is_equal_to(http::StatusCode::NO_CONTENT);
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_organisation_invite_decline() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);

        let pending_invitations = vec![
            (
                db::OrganisationParticipation {
                    id: invitation_id,
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 1".to_string(),
                },
                participant.clone(),
            ),
            (
                db::OrganisationParticipation {
                    id: SnowflakeInternal::from_i32(2),
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2".to_string(),
                },
                participant.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        let resolved_invitation = db::OrganisationParticipation {
            id: invitation_id,
            started_at: Local::now().date_naive(),
            stopped_at: None,
            organisation_id: SnowflakeInternal::from_i32(1),
            role_id: SnowflakeInternal::from_i32(1),
            subject_id: participant_id,
            referrer_id: SnowflakeInternal::from_i32(1),
            participation_state: db::ParticipationState::Declined,
        };
        let resolved_invitation_to_return = resolved_invitation.clone();

        db.organisation_participation
            .expect_get_pending_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        db.organisation_participation
            .expect_update_participation_state()
            .times(1)
            .with(
                predicate::eq(snowflake.from_internal(invitation_id).unwrap()),
                predicate::eq(db::ParticipationState::Declined),
            )
            .returning(move |_, _| Ok(resolved_invitation_to_return.clone()));

        let result = super::participant_resolve_organisation_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::OrganisationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Decline,
            }),
        )
        .await
        .unwrap();

        assert_that!(result).is_equal_to(http::StatusCode::NO_CONTENT);
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_organisation_invite_accept_without_permissions() {
        let snowflake = snowflake::test::snowflake_manager();
        let db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(2),
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);

        let result = super::participant_resolve_organisation_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::OrganisationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Accept,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::AuthError(AuthError::InsufficientPermissions),
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_organisation_invite_decline_without_permissions() {
        let snowflake = snowflake::test::snowflake_manager();
        let db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(2),
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);

        let result = super::participant_resolve_organisation_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::OrganisationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Decline,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::AuthError(AuthError::InsufficientPermissions),
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_organisation_invite_accept_not_found_invitation() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);
        let _invitation_id_hex = snowflake
            .from_internal(invitation_id)
            .unwrap()
            .external
            .to_hex();

        let pending_invitations = vec![
            (
                db::OrganisationParticipation {
                    id: SnowflakeInternal::from_i32(2),
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 1".to_string(),
                },
                participant.clone(),
            ),
            (
                db::OrganisationParticipation {
                    id: SnowflakeInternal::from_i32(3),
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2".to_string(),
                },
                participant.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        db.organisation_participation
            .expect_get_pending_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        let result = super::participant_resolve_organisation_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::OrganisationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Accept,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(db::DbError::NoEntity {
                entity: db::OrganisationParticipation::ENTITY,
                id: _invitation_id_hex
            })
        ));
    }

    #[tokio::test]
    #[traced_test]
    async fn resolve_organisation_invite_decline_not_found_invitation() {
        let snowflake = snowflake::test::snowflake_manager();
        let mut db = db::MockDatabase::new();

        let participant_id = SnowflakeInternal::from_i32(1);
        let participant = db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };

        let connected_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            full_name: "John Doe".to_string(),
        };
        let invitation_id = SnowflakeInternal::from_i32(1);
        let _invitation_id_hex = snowflake
            .from_internal(invitation_id)
            .unwrap()
            .external
            .to_hex();

        let pending_invitations = vec![
            (
                db::OrganisationParticipation {
                    id: SnowflakeInternal::from_i32(2),
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(1),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(1),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 1".to_string(),
                },
                participant.clone(),
            ),
            (
                db::OrganisationParticipation {
                    id: SnowflakeInternal::from_i32(3),
                    started_at: Local::now().date_naive(),
                    stopped_at: None,
                    organisation_id: SnowflakeInternal::from_i32(2),
                    role_id: SnowflakeInternal::from_i32(1),
                    subject_id: participant_id,
                    referrer_id: SnowflakeInternal::from_i32(1),
                    participation_state: db::ParticipationState::Pending,
                },
                db::Organisation {
                    id: SnowflakeInternal::from_i32(2),
                    created_at: Local::now().naive_local(),
                    name: "Organisation 2".to_string(),
                },
                participant.clone(),
            ),
        ];
        let pending_invitations_to_return = pending_invitations.clone();

        db.organisation_participation
            .expect_get_pending_participation_for_user()
            .times(1)
            .with(predicate::eq(
                snowflake.from_internal(participant_id).unwrap(),
            ))
            .returning(move |_| Ok(pending_invitations_to_return.clone()));

        let result = super::participant_resolve_organisation_invite(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Path(ParId {
                par_id: snowflake.from_internal(participant_id).unwrap().external,
            }),
            ConnectedAcademicParticipant(connected_participant),
            Json(dto::OrganisationInvitationResponse {
                id: snowflake.from_internal(invitation_id).unwrap().external,
                status: dto::InvitationResponse::Decline,
            }),
        )
        .await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::DbError(db::DbError::NoEntity {
                entity: db::OrganisationParticipation::ENTITY,
                id: _invitation_id_hex
            })
        ));
    }
}
