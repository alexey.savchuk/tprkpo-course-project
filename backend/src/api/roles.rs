use axum::{
    debug_handler,
    extract::{FromRef, Query},
    routing, Json, Router,
};
use tracing::instrument;

use super::{dto, Result};
#[cfg(debug_assertions)]
use crate::server::AppState;
use crate::{
    db::extract::{Database, DatabaseStateProvider},
    snowflake::{extract::SnowflakeState, SnowflakeManager},
};

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn roles_publication_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::ParticipationRole>>> {
    let roles = db
        .publication_role()
        .fuzzy_search(&query.search, query.n as i64)
        .await?;
    let roles = roles
        .into_iter()
        .map(|role| Ok(dto::ParticipationRole::from_db(&snowflake, role)?))
        .collect::<Result<_>>()?;
    Ok(Json(roles))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn roles_organisation_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::OrganisationRole>>> {
    let roles = db
        .organisation_role()
        .fuzzy_search(&query.search, query.n as i64)
        .await?;
    let roles = roles
        .into_iter()
        .map(|role| Ok(dto::OrganisationRole::from_db(&snowflake, role)?))
        .collect::<Result<_>>()?;
    Ok(Json(roles))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn roles_application_search(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<dto::ApplicationRole>>> {
    let roles = db
        .application_role()
        .fuzzy_search(&query.search, query.n as i64)
        .await?;
    let roles = roles
        .into_iter()
        .map(|role| Ok(dto::ApplicationRole::from_db(&snowflake, role)?))
        .collect::<Result<_>>()?;
    Ok(Json(roles))
}

#[cfg(not(tarpaulin_include))]
pub fn routes<S>() -> Router<S>
where
    SnowflakeManager: FromRef<S>,
    S: DatabaseStateProvider + Send + Sync + Clone + 'static,
{
    Router::new()
        .route(
            "/publication/search",
            routing::get(roles_publication_search),
        )
        .route(
            "/organisation/search",
            routing::get(roles_organisation_search),
        )
        .route(
            "/application/search",
            routing::get(roles_application_search),
        )
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use axum::{extract::Query, Json};
    use chrono::Local;
    use mockall::predicate;
    use spectral::{assert_that, iter::ContainingIntoIterAssertions};
    use tracing_test::traced_test;

    use crate::{
        api::dto,
        db::{self, extract::Database},
        snowflake::{self, extract::SnowflakeState, SnowflakeConverter, SnowflakeInternal},
    };

    #[tokio::test]
    #[traced_test]
    async fn roles_publication_search() {
        let snowflake = snowflake::test::snowflake_manager();

        let roles = vec![
            db::PublicationRole {
                id: SnowflakeInternal::from_i32(1),
                created_at: Local::now().naive_local(),
                name: "Author".to_string(),
            },
            db::PublicationRole {
                id: SnowflakeInternal::from_i32(2),
                created_at: Local::now().naive_local(),
                name: "Co-author".to_string(),
            },
        ];
        let roles_to_return = roles.clone();

        let mut db = db::MockDatabase::new();
        db.publication_role
            .expect_fuzzy_search()
            .times(1)
            .with(predicate::eq("author"), predicate::eq(10))
            .returning(move |_, _| Ok(roles_to_return.clone()));

        let Json(got_roles) = super::roles_publication_search(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Query(dto::FuzzySearchQuery {
                search: "author".to_string(),
                n: 10,
            }),
        )
        .await
        .unwrap();

        let roles_to_compare = roles
            .into_iter()
            .map(|role| dto::ParticipationRole {
                id: snowflake.from_internal(role.id).unwrap().external,
                name: role.name,
            })
            .collect::<Vec<_>>();

        assert_that!(got_roles).contains_all_of(&&roles_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn roles_organisation_search() {
        let snowflake = snowflake::test::snowflake_manager();

        let roles = vec![db::OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Admin".to_string(),
        }];
        let roles_to_return = roles.clone();

        let mut db = db::MockDatabase::new();
        db.organisation_role
            .expect_fuzzy_search()
            .times(1)
            .with(predicate::eq("admin"), predicate::eq(10))
            .returning(move |_, _| Ok(roles_to_return.clone()));

        let Json(got_roles) = super::roles_organisation_search(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Query(dto::FuzzySearchQuery {
                search: "admin".to_string(),
                n: 10,
            }),
        )
        .await
        .unwrap();

        let roles_to_compare = roles
            .into_iter()
            .map(|role| dto::OrganisationRole {
                id: snowflake.from_internal(role.id).unwrap().external,
                name: role.name,
            })
            .collect::<Vec<_>>();

        assert_that!(got_roles).contains_all_of(&&roles_to_compare);
    }

    #[tokio::test]
    #[traced_test]
    async fn roles_application_search() {
        let snowflake = snowflake::test::snowflake_manager();

        let roles = vec![db::ApplicationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: Local::now().naive_local(),
            name: "Admin".to_string(),
        }];
        let roles_to_return = roles.clone();

        let mut db = db::MockDatabase::new();
        db.application_role
            .expect_fuzzy_search()
            .times(1)
            .with(predicate::eq("admin"), predicate::eq(10))
            .returning(move |_, _| Ok(roles_to_return.clone()));

        let Json(got_roles) = super::roles_application_search(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Query(dto::FuzzySearchQuery {
                search: "admin".to_string(),
                n: 10,
            }),
        )
        .await
        .unwrap();

        let roles_to_compare = roles
            .into_iter()
            .map(|role| dto::ApplicationRole {
                id: snowflake.from_internal(role.id).unwrap().external,
                name: role.name,
            })
            .collect::<Vec<_>>();

        assert_that!(got_roles).contains_all_of(&&roles_to_compare);
    }
}
