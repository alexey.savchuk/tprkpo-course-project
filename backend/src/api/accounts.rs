use axum::{
    debug_handler,
    extract::{FromRef, Query},
    routing, Json, Router,
};
use fuzzy_matcher::{skim::SkimMatcherV2, FuzzyMatcher};
use itertools::Itertools;
use tracing::instrument;

use super::{dto, ApiError, Result};
#[cfg(debug_assertions)]
use crate::server::AppState;
use crate::{
    auth::{
        self,
        extract::{
            ApplicationAdmin, ConnectedAcademicParticipant, Identity, OryApi, OryApiStateProvider,
        },
        UserTraits,
    },
    db::extract::{Database, DatabaseStateProvider},
    snowflake::{extract::SnowflakeState, SnowflakeConverter, SnowflakeManager},
};

#[instrument(skip(snowflake))]
#[debug_handler(state = AppState)]
pub async fn get_account_meta(
    SnowflakeState(snowflake): SnowflakeState,
    maybe_connected_participant: Option<ConnectedAcademicParticipant>,
    maybe_app_admin: Option<ApplicationAdmin>,
) -> Result<Json<dto::AccountMetaResponse>> {
    let connected_participant = maybe_connected_participant
        .map(|ConnectedAcademicParticipant(par)| {
            Ok::<_, ApiError>(snowflake.from_internal(par.id)?.external)
        })
        .transpose()?;
    let is_app_admin = maybe_app_admin.is_some();
    Ok(Json(dto::AccountMetaResponse {
        connected_participant,
        is_app_admin,
    }))
}

#[instrument(skip(snowflake, db))]
#[debug_handler(state = AppState)]
pub async fn create_new_attached_participant(
    SnowflakeState(snowflake): SnowflakeState,
    Database(db): Database,
    Identity(user_id): Identity,
    maybe_connected_participant: Option<ConnectedAcademicParticipant>,
    Json(req): Json<dto::CreateNewAttachedParticipantRequest>,
) -> Result<Json<dto::Participant>> {
    if let Some(ConnectedAcademicParticipant(connected_participant)) = maybe_connected_participant {
        let connected_participant = dto::Participant::from_db(&snowflake, connected_participant)?;
        return Ok(Json(connected_participant));
    }
    let new_connected_participant = db
        .academic_participants()
        .insert_new(&req.full_name)
        .await?;
    let _ = db
        .academic_users()
        .insert_new(
            &user_id,
            &snowflake.from_internal(new_connected_participant.id)?,
        )
        .await?;
    let connected_participant = dto::Participant::from_db(&snowflake, new_connected_participant)?;
    Ok(Json(connected_participant))
}

#[instrument(skip(ory))]
#[debug_handler(state = AppState)]
pub async fn list_accounts(
    OryApi(ory): OryApi,
    _app_admin: ApplicationAdmin,
) -> Result<Json<Vec<auth::OryIdentity>>> {
    let identities = ory.list_identities().await?;
    Ok(Json(identities))
}

#[instrument(skip(ory))]
#[debug_handler(state = AppState)]
pub async fn search_accounts(
    OryApi(ory): OryApi,
    Query(query): Query<dto::FuzzySearchQuery>,
) -> Result<Json<Vec<auth::OryIdentity>>> {
    let matcher = SkimMatcherV2::default();
    let identities = ory.list_identities().await?;
    let identities = identities
        .into_iter()
        .map(|identity| {
            UserTraits::try_from(identity.traits.clone()).map(|traits| (identity, traits))
        })
        .collect::<std::result::Result<Vec<_>, auth::TraitError>>()?;
    let identities = identities
        .into_iter()
        .flat_map(|(identity, traits)| {
            let email_match = matcher.fuzzy_match(&traits.email, &query.search);
            email_match.map(|score| (score, identity))
        })
        .sorted_by_key(|(score, _)| score.clone())
        .map(|(_, id)| id)
        .take(query.n)
        .collect_vec();
    Ok(Json(identities))
}

#[cfg(not(tarpaulin_include))]
pub fn routes<S>() -> Router<S>
where
    SnowflakeManager: FromRef<S>,
    S: DatabaseStateProvider + OryApiStateProvider + Send + Sync + Clone + 'static,
{
    Router::new()
        .route(
            "/my/attach_participant",
            routing::post(create_new_attached_participant),
        )
        .route("/my/meta", routing::get(get_account_meta))
        .route("/list", routing::get(list_accounts))
        .route("/search", routing::get(search_accounts))
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use crate::{
        api::{dto, ApiError},
        auth::{
            self,
            extract::{ApplicationAdmin, ConnectedAcademicParticipant, Identity, OryApi},
            MockOryApi, OryIdentity, TraitError,
        },
        db::{self, extract::Database},
        snowflake::{self, extract::SnowflakeState, SnowflakeInternal},
    };
    use axum::{extract::Query, Json};
    use chrono::prelude::*;
    use mockall::predicate::eq;
    use serde_json::json;
    use spectral::prelude::*;
    use tracing_test::traced_test;

    #[tokio::test]
    async fn test_get_account_meta() {
        let snowflake = snowflake::test::snowflake_manager();

        let participant_id = SnowflakeInternal::from_i32(42);
        let connected_participant = Some(ConnectedAcademicParticipant(db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        }));

        let app_admin = Some(ApplicationAdmin { 0: () });

        let Json(response) =
            super::get_account_meta(SnowflakeState(snowflake), connected_participant, app_admin)
                .await
                .unwrap();

        assert!(response.connected_participant.is_some());
        assert_eq!(response.is_app_admin, true);
    }

    #[tokio::test]
    async fn test_get_account_meta_non_admin() {
        let snowflake = snowflake::test::snowflake_manager();

        let participant_id = SnowflakeInternal::from_i32(42);
        let connected_participant = Some(ConnectedAcademicParticipant(db::AcademicParticipant {
            id: participant_id,
            created_at: Local::now().naive_local(),
            full_name: "Jane Doe".to_string(),
        }));

        let app_admin = None;

        let Json(response) =
            super::get_account_meta(SnowflakeState(snowflake), connected_participant, app_admin)
                .await
                .unwrap();

        assert!(response.connected_participant.is_some());
        assert_eq!(response.is_app_admin, false);
    }

    #[tokio::test]
    async fn test_create_new_attached_participant() {
        let snowflake = snowflake::test::snowflake_manager();

        let mut db = db::MockDatabase::new();

        let new_participant_id = SnowflakeInternal::from_i32(72);
        let user_identity = auth::test::generate_identity_with_id(uuid::Uuid::new_v4().to_string());
        let participant_name = "Test Participant";

        db.academic_participants
            .expect_insert_new()
            .with(eq(participant_name))
            .returning(move |_| {
                Ok(db::AcademicParticipant {
                    id: new_participant_id.clone(),
                    created_at: Local::now().naive_local(),
                    full_name: participant_name.to_string(),
                })
            });

        let user_identity1 = user_identity.clone();

        db.academic_users
            .expect_insert_new()
            .with(
                eq(user_identity.clone()),
                snowflake::test::eq_internal(new_participant_id),
            )
            .returning(move |_, _| {
                Ok(db::AcademicUsers {
                    user_id: user_identity.clone().id.clone(),
                    participant_id: new_participant_id,
                })
            });

        let Json(connected_participant) = super::create_new_attached_participant(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Identity(Box::new(user_identity1)),
            None,
            Json(dto::CreateNewAttachedParticipantRequest {
                full_name: participant_name.to_string(),
            }),
        )
        .await
        .unwrap();

        assert_eq!(connected_participant.name, participant_name);
    }

    #[tokio::test]
    #[traced_test]
    async fn test_create_new_attached_participant_already_exists() {
        let snowflake = snowflake::test::snowflake_manager();
        let db = db::MockDatabase::new();

        let academic_participant = db::AcademicParticipant {
            id: SnowflakeInternal::from_i32(72),
            created_at: Local::now().naive_local(),
            full_name: "Test Participant".to_string(),
        };

        let Json(connected_participant) = super::create_new_attached_participant(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Identity(Box::new(auth::test::generate_identity_with_id(
                uuid::Uuid::new_v4().to_string(),
            ))),
            Some(ConnectedAcademicParticipant(academic_participant.clone())),
            Json(dto::CreateNewAttachedParticipantRequest {
                full_name: "Test Participant".to_string(),
            }),
        )
        .await
        .unwrap();

        assert_eq!(connected_participant.name, academic_participant.full_name);
    }

    #[tokio::test]
    #[traced_test]
    async fn test_list_accounts() {
        let ory_identities = vec![
            auth::test::generate_identity_with_id(uuid::Uuid::new_v4().to_string()),
            auth::test::generate_identity_with_id(uuid::Uuid::new_v4().to_string()),
        ];
        let ory_identities_to_return = ory_identities.clone();

        let mut ory_api = MockOryApi::new();
        ory_api
            .expect_list_identities()
            .times(1)
            .returning(move || Ok(ory_identities_to_return.clone()));

        let Json(result) = super::list_accounts(OryApi(Arc::new(ory_api)), ApplicationAdmin(()))
            .await
            .unwrap();

        assert_that!(result).contains_all_of(&&ory_identities);
    }

    #[tokio::test]
    #[traced_test]
    async fn test_search_accounts() {
        let mut ory_api = MockOryApi::new();

        let ory_identity = OryIdentity {
            created_at: None,
            credentials: None,
            id: uuid::Uuid::new_v4().to_string(),
            metadata_admin: None,
            metadata_public: None,
            organization_id: None,
            recovery_addresses: None,
            schema_id: "schema".to_string(),
            schema_url: "schema".to_string(),
            state: None,
            state_changed_at: None,
            traits: Some(json!({ "email": "moe@mylo.com" })),
            updated_at: None,
            verifiable_addresses: None,
        };

        let ory_identities = vec![ory_identity.clone()];
        let ory_identities_to_return = ory_identities.clone();
        let query_search = dto::FuzzySearchQuery {
            search: "moe@mylo.com".to_string(),
            n: 1,
        };

        ory_api
            .expect_list_identities()
            .times(1)
            .returning(move || Ok(ory_identities_to_return.clone()));

        let Json(result) = super::search_accounts(OryApi(Arc::new(ory_api)), Query(query_search))
            .await
            .unwrap();

        assert_that!(result).contains_all_of(&&ory_identities);
    }

    #[tokio::test]
    #[traced_test]
    async fn test_search_accounts_no_traits() {
        let mut ory_api = MockOryApi::new();

        let ory_identities = vec![
            auth::test::generate_identity_with_id(uuid::Uuid::new_v4().to_string()),
            auth::test::generate_identity_with_id(uuid::Uuid::new_v4().to_string()),
        ];
        let ory_identities_to_return = ory_identities.clone();

        let query_search = dto::FuzzySearchQuery {
            search: "".to_string(),
            n: 2,
        };

        ory_api
            .expect_list_identities()
            .times(1)
            .returning(move || Ok(ory_identities_to_return.clone()));

        let result = super::search_accounts(OryApi(Arc::new(ory_api)), Query(query_search)).await;

        assert_that!(result).is_err();
        assert!(matches!(
            result.unwrap_err(),
            ApiError::TraitError(TraitError::NoTraitsError)
        ))
    }

    #[tokio::test]
    #[traced_test]
    async fn create_and_connect_academic_user() {
        let snowflake = snowflake::test::snowflake_manager();

        let mut db = db::MockDatabase::new();

        let new_participant_id = SnowflakeInternal::from_i32(72);
        let user_identity = auth::test::generate_identity_with_id(uuid::Uuid::new_v4().to_string());
        let participant_name = "Test participant";

        db.academic_participants
            .expect_insert_new()
            .with(eq(participant_name))
            .returning(move |x| {
                Ok(db::AcademicParticipant {
                    id: new_participant_id.clone(),
                    created_at: Local::now().naive_local(),
                    full_name: x.to_string(),
                })
            });
        db.academic_users
            .expect_insert_new()
            .with(
                eq(user_identity.clone()),
                snowflake::test::eq_internal(new_participant_id),
            )
            .returning(move |ory_id, participant_id| {
                Ok(db::AcademicUsers {
                    user_id: ory_id.id.clone(),
                    participant_id: participant_id.internal,
                })
            });

        let Json(connected_participant) = super::create_new_attached_participant(
            SnowflakeState(snowflake.clone()),
            Database(Arc::new(db)),
            Identity(Box::new(user_identity)),
            None,
            Json(dto::CreateNewAttachedParticipantRequest {
                full_name: participant_name.to_string(),
            }),
        )
        .await
        .unwrap();

        assert_that!(connected_participant.name.as_str()).is_equal_to(participant_name)
    }
}
