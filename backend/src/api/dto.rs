use axum::http;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};
use thiserror::Error;

use crate::{
    db,
    snowflake::{SnowflakeConverter, SnowflakeError, SnowflakeExternal, SnowflakeManager},
};

pub type UserId = String;

#[derive(Debug, Error)]
pub enum DtoError {
    #[error(transparent)]
    SnowflakeError(#[from] SnowflakeError),
}

impl DtoError {
    pub fn status_code(&self) -> http::StatusCode {
        match self {
            DtoError::SnowflakeError(e) => e.status_code(),
        }
    }
}

pub type Result<T> = std::result::Result<T, DtoError>;

#[derive(Debug, Serialize, PartialEq)]
pub struct Organisation {
    pub id: SnowflakeExternal<db::Organisation>,
    pub name: String,
}

impl Organisation {
    pub fn from_db(snowflake: &SnowflakeManager, organisation: db::Organisation) -> Result<Self> {
        let db::Organisation { id, name, .. } = organisation;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name,
        })
    }
}

#[derive(Debug, Serialize, PartialEq)]
pub struct OrganisationMeta {
    pub participants: usize,
    pub publications: usize,
    pub citations: usize,
    pub editable: bool,
}

#[derive(Debug, Serialize, Clone, PartialEq)]
pub struct OrganisationParticipant {
    pub id: SnowflakeExternal<db::AcademicParticipant>,
    pub name: String,
    pub roles: Vec<OrganisationRole>,
    pub performance: f64,
    pub is_historical: bool,
    pub is_pending: bool,
    pub is_declined: bool,
}

impl OrganisationParticipant {
    pub fn from_db(
        snowflake: &SnowflakeManager,
        participant: db::AcademicParticipant,
        roles: Vec<db::OrganisationRole>,
        is_historical: bool,
        is_pending: bool,
        is_declined: bool,
    ) -> Result<Self> {
        let roles = roles
            .into_iter()
            .map(|org| OrganisationRole::from_db(snowflake, org))
            .collect::<Result<_>>()?;
        let db::AcademicParticipant { id, full_name, .. } = participant;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name: full_name,
            roles,
            performance: 1.0,
            is_historical,
            is_pending,
            is_declined,
        })
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct SubjectCode {
    pub id: SnowflakeExternal<db::SubjectCode>,
    pub code: String,
    pub description: String,
}

impl SubjectCode {
    pub fn from_db(snowflake: &SnowflakeManager, code: db::SubjectCode) -> Result<Self> {
        let db::SubjectCode { id, name, code, .. } = code;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            code,
            description: name,
        })
    }
}

#[derive(Debug, Serialize, PartialEq, Clone)]
pub struct Participant {
    pub id: SnowflakeExternal<db::AcademicParticipant>,
    pub name: String,
}

impl Participant {
    pub fn from_db(
        snowflake: &SnowflakeManager,
        participant: db::AcademicParticipant,
    ) -> Result<Self> {
        let db::AcademicParticipant { id, full_name, .. } = participant;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name: full_name,
        })
    }
}

#[derive(Debug, Serialize, PartialEq)]
pub struct ParticipantMeta {
    pub citations: usize,
    pub editable: bool,
    pub connected: bool,
}

#[derive(Debug, Serialize, PartialEq)]
pub struct ParticipantPublication {
    pub id: SnowflakeExternal<db::Publication>,
    pub title: String,
    pub published_at: NaiveDate,
    pub subject_code: SubjectCode,
    pub role: ParticipationRole,
}

impl ParticipantPublication {
    pub fn from_db(
        snowflake: &SnowflakeManager,
        publication: db::Publication,
        code: db::SubjectCode,
        role: db::PublicationRole,
    ) -> Result<Self> {
        let subject_code = SubjectCode::from_db(snowflake, code)?;
        let role = ParticipationRole::from_db(snowflake, role)?;
        let db::Publication {
            id,
            title,
            published_at,
            ..
        } = publication;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            title,
            published_at,
            subject_code,
            role,
        })
    }
}

#[derive(Debug, Serialize, PartialEq)]
pub struct ParticipantOrganisation {
    pub id: SnowflakeExternal<db::Organisation>,
    pub name: String,
    pub roles: Vec<OrganisationRole>,
}

impl ParticipantOrganisation {
    pub fn from_db(
        snowflake: &SnowflakeManager,
        organisation: db::Organisation,
        roles: Vec<db::OrganisationRole>,
    ) -> Result<Self> {
        let roles = roles
            .into_iter()
            .map(|role| Ok(OrganisationRole::from_db(snowflake, role)?))
            .collect::<Result<_>>()?;
        let db::Organisation { id, name, .. } = organisation;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name,
            roles,
        })
    }
}

#[derive(Debug, Serialize, PartialEq)]
pub struct ParticipantReference {
    pub id: SnowflakeExternal<db::AcademicParticipant>,
    pub name: String,
    pub score: usize,
}

impl ParticipantReference {
    pub fn from_db(
        snowflake: &SnowflakeManager,
        participant: db::AcademicParticipant,
        score: db::Score,
    ) -> Result<Self> {
        let db::AcademicParticipant { id, full_name, .. } = participant;
        let score = score.score as usize;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name: full_name,
            score,
        })
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Publication {
    pub id: SnowflakeExternal<db::Publication>,
    pub title: String,
    pub published_at: NaiveDate,
    pub subject_code: SubjectCode,
}

#[derive(Debug, Serialize, PartialEq)]
pub struct PublicationMeta {
    pub editable: bool,
}

impl Publication {
    pub fn from_db(
        snowflake: &SnowflakeManager,
        publication: db::Publication,
        code: db::SubjectCode,
    ) -> Result<Self> {
        let subject_code = SubjectCode::from_db(snowflake, code)?;
        let db::Publication {
            id,
            title,
            published_at,
            ..
        } = publication;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            title,
            published_at,
            subject_code,
        })
    }
}

#[derive(Debug, Serialize, PartialEq)]
pub struct PublicationParticipant {
    pub id: SnowflakeExternal<db::AcademicParticipant>,
    pub name: String,
    pub role: ParticipationRole,
}

impl PublicationParticipant {
    pub fn from_db(
        snowflake: &SnowflakeManager,
        participant: db::AcademicParticipant,
        role: db::PublicationRole,
    ) -> Result<Self> {
        let role = ParticipationRole::from_db(snowflake, role)?;
        let db::AcademicParticipant { id, full_name, .. } = participant;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name: full_name,
            role,
        })
    }
}

#[derive(Debug, Serialize, PartialEq)]
pub struct PublicationReference {
    pub to: Vec<Publication>,
    pub from: Vec<Publication>,
}

#[derive(Debug, Serialize, PartialEq)]
pub struct ApplicationRole {
    pub id: SnowflakeExternal<db::ApplicationRole>,
    pub name: String,
}

impl ApplicationRole {
    pub fn from_db(snowflake: &SnowflakeManager, role: db::ApplicationRole) -> Result<Self> {
        let db::ApplicationRole { id, name, .. } = role;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name,
        })
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct OrganisationRole {
    pub id: SnowflakeExternal<db::OrganisationRole>,
    pub name: String,
}

impl OrganisationRole {
    pub fn from_db(snowflake: &SnowflakeManager, role: db::OrganisationRole) -> Result<Self> {
        let db::OrganisationRole { id, name, .. } = role;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name,
        })
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct ParticipationRole {
    pub id: SnowflakeExternal<db::PublicationRole>,
    pub name: String,
}

impl ParticipationRole {
    pub fn from_db(snowflake: &SnowflakeManager, role: db::PublicationRole) -> Result<Self> {
        let db::PublicationRole { id, name, .. } = role;
        Ok(Self {
            id: snowflake.from_internal(id)?.external,
            name,
        })
    }
}

#[derive(Debug, Deserialize)]
pub struct FuzzySearchQuery {
    pub search: String,
    pub n: usize,
}

#[derive(Debug, Serialize, PartialEq)]
pub struct GlobalSearch {
    pub item_id: String,
    pub item_type: String,
    pub value: String,
}

impl GlobalSearch {
    pub fn from_db(snowflake: &SnowflakeManager, search: db::GlobalSearch) -> Result<Self> {
        let (item_type, item_id, value) = match search {
            db::GlobalSearch::Publication {
                item_id,
                item_value,
            } => (
                "publication",
                snowflake.from_internal(item_id)?.external.to_hex(),
                item_value,
            ),
            db::GlobalSearch::Participant {
                item_id,
                item_value,
            } => (
                "participant",
                snowflake.from_internal(item_id)?.external.to_hex(),
                item_value,
            ),
            db::GlobalSearch::Organisation {
                item_id,
                item_value,
            } => (
                "organisation",
                snowflake.from_internal(item_id)?.external.to_hex(),
                item_value,
            ),
            db::GlobalSearch::SubjectCode {
                item_id,
                item_value,
            } => (
                "subject_code",
                snowflake.from_internal(item_id)?.external.to_hex(),
                item_value,
            ),
        };
        Ok(GlobalSearch {
            item_id,
            item_type: item_type.to_string(),
            value,
        })
    }
}

#[derive(Debug, Serialize, PartialEq, Eq)]
pub struct ParticipantPublicationInvitation {
    pub id: SnowflakeExternal<db::PublicationParticipation>,
    pub publication_title: String,
    pub inviter_name: String,
}

#[derive(Debug, Serialize, PartialEq, Eq)]
pub struct ParticipantOrganisationInvitation {
    pub id: SnowflakeExternal<db::OrganisationParticipation>,
    pub organisation_name: String,
    pub inviter_name: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename = "lowercase")]
pub enum InvitationResponse {
    Accept,
    Decline,
}

impl Into<db::ParticipationState> for InvitationResponse {
    fn into(self) -> db::ParticipationState {
        match self {
            InvitationResponse::Accept => db::ParticipationState::Active,
            InvitationResponse::Decline => db::ParticipationState::Declined,
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct PublicationInvitationResponse {
    pub id: SnowflakeExternal<db::PublicationParticipation>,
    pub status: InvitationResponse,
}

#[derive(Debug, Deserialize)]
pub struct OrganisationInvitationResponse {
    pub id: SnowflakeExternal<db::OrganisationParticipation>,
    pub status: InvitationResponse,
}

#[derive(Debug, Deserialize)]
pub struct CreateNewAttachedParticipantRequest {
    pub full_name: String,
}

#[derive(Debug, Serialize)]
pub struct AccountMetaResponse {
    pub connected_participant: Option<SnowflakeExternal<db::AcademicParticipant>>,
    pub is_app_admin: bool,
}

#[cfg(test)]
mod tests {
    use tracing_test::traced_test;
    use crate::{api::dto::{self, DtoError}, db, snowflake::{self, SnowflakeConverter, SnowflakeError, SnowflakeInternal}};
    use spectral::prelude::*;

    #[tokio::test]
    #[traced_test]
    async fn global_search_publication_from_db() {
        let snowflake = snowflake::test::snowflake_manager();

        let entity_id = SnowflakeInternal::from_i32(1);
        let entity_id_hex = snowflake.from_internal(entity_id).unwrap().external.to_hex();

        let item_value = "Some publication".to_string();

        let entity = db::GlobalSearch::Publication {
            item_id: entity_id,
            item_value: item_value.clone(),
        };

        let result = super::GlobalSearch::from_db(&snowflake, entity.clone()).unwrap();

        assert_that!(result).is_equal_to(dto::GlobalSearch {
            item_id: entity_id_hex,
            item_type: "publication".to_string(),
            value: item_value,
        })
    }

    #[tokio::test]
    #[traced_test]
    async fn global_search_participant_from_db() {
        let snowflake = snowflake::test::snowflake_manager();

        let entity_id = SnowflakeInternal::from_i32(1);
        let entity_id_hex = snowflake.from_internal(entity_id).unwrap().external.to_hex();

        let item_value = "Some participant".to_string();

        let entity = db::GlobalSearch::Participant {
            item_id: entity_id,
            item_value: item_value.clone(),
        };

        let result = super::GlobalSearch::from_db(&snowflake, entity.clone()).unwrap();

        assert_that!(result).is_equal_to(dto::GlobalSearch {
            item_id: entity_id_hex,
            item_type: "participant".to_string(),
            value: item_value,
        })
    }

    #[tokio::test]
    #[traced_test]
    async fn global_search_organisation_from_db() {
        let snowflake = snowflake::test::snowflake_manager();

        let entity_id = SnowflakeInternal::from_i32(1);
        let entity_id_hex = snowflake.from_internal(entity_id).unwrap().external.to_hex();

        let item_value = "Some organisation".to_string();

        let entity = db::GlobalSearch::Organisation {
            item_id: entity_id,
            item_value: item_value.clone(),
        };

        let result = super::GlobalSearch::from_db(&snowflake, entity.clone()).unwrap();

        assert_that!(result).is_equal_to(dto::GlobalSearch {
            item_id: entity_id_hex,
            item_type: "organisation".to_string(),
            value: item_value,
        })
    }

    #[tokio::test]
    #[traced_test]
    async fn global_search_subject_code_from_db() {
        let snowflake = snowflake::test::snowflake_manager();

        let entity_id = SnowflakeInternal::from_i32(1);
        let entity_id_hex = snowflake.from_internal(entity_id).unwrap().external.to_hex();

        let item_value = "Some subject code".to_string();

        let entity = db::GlobalSearch::SubjectCode {
            item_id: entity_id,
            item_value: item_value.clone(),
        };

        let result = super::GlobalSearch::from_db(&snowflake, entity.clone()).unwrap();

        assert_that!(result).is_equal_to(dto::GlobalSearch {
            item_id: entity_id_hex,
            item_type: "subject_code".to_string(),
            value: item_value,
        })
    }

    #[tokio::test]
    #[traced_test]
    async fn status_code_bad_request() {
        let error = DtoError::SnowflakeError(SnowflakeError::HexError(hex::FromHexError::OddLength));

        assert_eq!(error.status_code(), http::StatusCode::BAD_REQUEST);
    }

    // #[tokio::test]
    // #[traced_test]
    // async fn status_code_internal_server_error() {
    //     let error = DtoError::SnowflakeError(SnowflakeError::SliceError(std::array::TryFromSliceError(())));

    //     assert_eq!(error.status_code(), http::StatusCode::INTERNAL_SERVER_ERROR);
    // }

    #[tokio::test]
    #[traced_test]
    async fn status_code_not_found() {
        let error = DtoError::SnowflakeError(SnowflakeError::AsconError(ascon_aead::Error));

        assert_eq!(error.status_code(), http::StatusCode::NOT_FOUND);
    }
}