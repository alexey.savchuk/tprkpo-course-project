use std::sync::Arc;

use super::{
    academic_participants::{
        AcademicParticipantMethods, AcademicParticipantProviderDb, MockAcademicParticipantMethods,
    },
    academic_users::{AcademicUsersMethods, AcademicUsersProviderDb, MockAcademicUsersMethods},
    application_participation::{
        ApplicationParticipationMethods, ApplicationParticipationProviderDb,
        MockApplicationParticipationMethods,
    },
    application_role::{
        ApplicationRoleMethods, ApplicationRoleProviderDb, MockApplicationRoleMethods,
    },
    global_search::{GlobalSearchMethods, GlobalSearchProviderDb, MockGlobalSearchMethods},
    organisation::{MockOrganisationMethods, OrganisationMethods, OrganisationProviderDb},
    organisation_participation::{
        MockOrganisationParticipationMethods, OrganisationParticipationMethods,
        OrganisationParticipationProviderDb,
    },
    organisation_role::{
        MockOrganisationRoleMethods, OrganisationRoleMethods, OrganisationRoleProviderDb,
    },
    publication::{MockPublicationMethods, PublicationMethods, PublicationProviderDb},
    publication_participation::{
        MockPublicationParticipationMethods, PublicationParticipationMethods,
        PublicationParticipationProviderDb,
    },
    publication_reference::{
        MockPublicationReferenceMethods, PublicationReferenceMethods,
        PublicationReferenceProviderDb,
    },
    publication_role::{
        MockPublicationRoleMethods, PublicationRoleMethods, PublicationRoleProviderDb,
    },
    subject_code::{MockSubjectCodeMethods, SubjectCodeMethods, SubjectCodeProviderDb},
    DbPool,
};

pub trait DatabaseMethods {
    fn academic_participants(&self) -> &(dyn AcademicParticipantMethods + Sync + Send);
    fn academic_users(&self) -> &(dyn AcademicUsersMethods + Sync + Send);
    fn application_participation(&self) -> &(dyn ApplicationParticipationMethods + Sync + Send);
    fn application_role(&self) -> &(dyn ApplicationRoleMethods + Sync + Send);
    fn global_search(&self) -> &(dyn GlobalSearchMethods + Sync + Send);
    fn organisation(&self) -> &(dyn OrganisationMethods + Sync + Send);
    fn organisation_participation(&self) -> &(dyn OrganisationParticipationMethods + Sync + Send);
    fn organisation_role(&self) -> &(dyn OrganisationRoleMethods + Sync + Send);
    fn publication(&self) -> &(dyn PublicationMethods + Sync + Send);
    fn publication_participation(&self) -> &(dyn PublicationParticipationMethods + Sync + Send);
    fn publication_reference(&self) -> &(dyn PublicationReferenceMethods + Sync + Send);
    fn publication_role(&self) -> &(dyn PublicationRoleMethods + Sync + Send);
    fn subject_code(&self) -> &(dyn SubjectCodeMethods + Sync + Send);
}

pub type DatabaseMethodsDyn = Arc<dyn DatabaseMethods + Send + Sync>;

#[derive(Debug, Clone)]
pub struct DatabaseProvider {
    pub academic_participants: AcademicParticipantProviderDb,
    pub academic_users: AcademicUsersProviderDb,
    pub application_participation: ApplicationParticipationProviderDb,
    pub application_role: ApplicationRoleProviderDb,
    pub global_search: GlobalSearchProviderDb,
    pub organisation: OrganisationProviderDb,
    pub organisation_participation: OrganisationParticipationProviderDb,
    pub organisation_role: OrganisationRoleProviderDb,
    pub publication: PublicationProviderDb,
    pub publication_participation: PublicationParticipationProviderDb,
    pub publication_reference: PublicationReferenceProviderDb,
    pub publication_role: PublicationRoleProviderDb,
    pub subject_code: SubjectCodeProviderDb,
}

impl DatabaseProvider {
    pub fn from_db_pool(db_pool: DbPool) -> Self {
        Self {
            academic_participants: AcademicParticipantProviderDb::from_db_pool(&db_pool),
            academic_users: AcademicUsersProviderDb::from_db_pool(&db_pool),
            application_participation: ApplicationParticipationProviderDb::from_db_pool(&db_pool),
            application_role: ApplicationRoleProviderDb::from_db_pool(&db_pool),
            global_search: GlobalSearchProviderDb::from_db_pool(&db_pool),
            organisation: OrganisationProviderDb::from_db_pool(&db_pool),
            organisation_participation: OrganisationParticipationProviderDb::from_db_pool(&db_pool),
            organisation_role: OrganisationRoleProviderDb::from_db_pool(&db_pool),
            publication: PublicationProviderDb::from_db_pool(&db_pool),
            publication_participation: PublicationParticipationProviderDb::from_db_pool(&db_pool),
            publication_reference: PublicationReferenceProviderDb::from_db_pool(&db_pool),
            publication_role: PublicationRoleProviderDb::from_db_pool(&db_pool),
            subject_code: SubjectCodeProviderDb::from_db_pool(&db_pool),
        }
    }
}

impl DatabaseMethods for DatabaseProvider {
    fn academic_participants(&self) -> &(dyn AcademicParticipantMethods + Sync + Send) {
        &self.academic_participants
    }
    fn academic_users(&self) -> &(dyn AcademicUsersMethods + Sync + Send) {
        &self.academic_users
    }
    fn application_participation(&self) -> &(dyn ApplicationParticipationMethods + Sync + Send) {
        &self.application_participation
    }
    fn application_role(&self) -> &(dyn ApplicationRoleMethods + Sync + Send) {
        &self.application_role
    }
    fn global_search(&self) -> &(dyn GlobalSearchMethods + Sync + Send) {
        &self.global_search
    }
    fn organisation(&self) -> &(dyn OrganisationMethods + Sync + Send) {
        &self.organisation
    }
    fn organisation_participation(&self) -> &(dyn OrganisationParticipationMethods + Sync + Send) {
        &self.organisation_participation
    }
    fn organisation_role(&self) -> &(dyn OrganisationRoleMethods + Sync + Send) {
        &self.organisation_role
    }
    fn publication(&self) -> &(dyn PublicationMethods + Sync + Send) {
        &self.publication
    }
    fn publication_participation(&self) -> &(dyn PublicationParticipationMethods + Sync + Send) {
        &self.publication_participation
    }
    fn publication_reference(&self) -> &(dyn PublicationReferenceMethods + Sync + Send) {
        &self.publication_reference
    }
    fn publication_role(&self) -> &(dyn PublicationRoleMethods + Sync + Send) {
        &self.publication_role
    }
    fn subject_code(&self) -> &(dyn SubjectCodeMethods + Sync + Send) {
        &self.subject_code
    }
}

#[derive(Debug)]
pub struct MockDatabase {
    pub academic_participants: MockAcademicParticipantMethods,
    pub academic_users: MockAcademicUsersMethods,
    pub application_participation: MockApplicationParticipationMethods,
    pub application_role: MockApplicationRoleMethods,
    pub global_search: MockGlobalSearchMethods,
    pub organisation: MockOrganisationMethods,
    pub organisation_participation: MockOrganisationParticipationMethods,
    pub organisation_role: MockOrganisationRoleMethods,
    pub publication: MockPublicationMethods,
    pub publication_participation: MockPublicationParticipationMethods,
    pub publication_reference: MockPublicationReferenceMethods,
    pub publication_role: MockPublicationRoleMethods,
    pub subject_code: MockSubjectCodeMethods,
}

impl DatabaseMethods for MockDatabase {
    fn academic_participants(&self) -> &(dyn AcademicParticipantMethods + Sync + Send) {
        &self.academic_participants
    }
    fn academic_users(&self) -> &(dyn AcademicUsersMethods + Sync + Send) {
        &self.academic_users
    }
    fn application_participation(&self) -> &(dyn ApplicationParticipationMethods + Sync + Send) {
        &self.application_participation
    }
    fn application_role(&self) -> &(dyn ApplicationRoleMethods + Sync + Send) {
        &self.application_role
    }
    fn global_search(&self) -> &(dyn GlobalSearchMethods + Sync + Send) {
        &self.global_search
    }
    fn organisation(&self) -> &(dyn OrganisationMethods + Sync + Send) {
        &self.organisation
    }
    fn organisation_participation(&self) -> &(dyn OrganisationParticipationMethods + Sync + Send) {
        &self.organisation_participation
    }
    fn organisation_role(&self) -> &(dyn OrganisationRoleMethods + Sync + Send) {
        &self.organisation_role
    }
    fn publication(&self) -> &(dyn PublicationMethods + Sync + Send) {
        &self.publication
    }
    fn publication_participation(&self) -> &(dyn PublicationParticipationMethods + Sync + Send) {
        &self.publication_participation
    }
    fn publication_reference(&self) -> &(dyn PublicationReferenceMethods + Sync + Send) {
        &self.publication_reference
    }
    fn publication_role(&self) -> &(dyn PublicationRoleMethods + Sync + Send) {
        &self.publication_role
    }
    fn subject_code(&self) -> &(dyn SubjectCodeMethods + Sync + Send) {
        &self.subject_code
    }
}

impl MockDatabase {
    pub fn new() -> Self {
        Self {
            academic_participants: MockAcademicParticipantMethods::new(),
            academic_users: MockAcademicUsersMethods::new(),
            application_participation: MockApplicationParticipationMethods::new(),
            application_role: MockApplicationRoleMethods::new(),
            global_search: MockGlobalSearchMethods::new(),
            organisation: MockOrganisationMethods::new(),
            organisation_participation: MockOrganisationParticipationMethods::new(),
            organisation_role: MockOrganisationRoleMethods::new(),
            publication: MockPublicationMethods::new(),
            publication_participation: MockPublicationParticipationMethods::new(),
            publication_reference: MockPublicationReferenceMethods::new(),
            publication_role: MockPublicationRoleMethods::new(),
            subject_code: MockSubjectCodeMethods::new(),
        }
    }
}

pub mod extract {
    use std::{convert::Infallible, sync::Arc};

    use axum::{
        async_trait,
        extract::{FromRef, FromRequestParts},
    };
    use http::request::Parts;
    use tracing::instrument;

    use super::{DatabaseMethodsDyn, DatabaseProvider};

    pub trait DatabaseStateProvider {
        fn provide_database(&self) -> DatabaseMethodsDyn;
    }

    pub struct Database(pub DatabaseMethodsDyn);

    #[async_trait]
    impl<S> FromRequestParts<S> for Database
    where
        S: DatabaseStateProvider + Send + Sync,
    {
        type Rejection = Infallible;

        #[instrument(name = "extract_database", level = "trace", skip_all)]
        async fn from_request_parts(
            _parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let db = state.provide_database();
            Ok(Self(db))
        }
    }

    impl<S> DatabaseStateProvider for S
    where
        Arc<DatabaseProvider>: FromRef<S>,
    {
        fn provide_database(&self) -> DatabaseMethodsDyn {
            let provider_ref = <Arc<DatabaseProvider>>::from_ref(self);
            provider_ref
        }
    }
}
