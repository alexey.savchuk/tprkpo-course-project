use axum::async_trait;
use mockall::automock;
use serde::Deserialize;
use std::collections::{HashMap, HashSet};

use chrono::NaiveDate;
use itertools::Itertools;
use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    AcademicParticipant, DbError, Entity, Organisation, OrganisationRole, ParticipationState,
    Result,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct OrganisationParticipation {
    #[serde(rename = "participation_id")]
    pub id: SnowflakeInternal<Self>,
    #[serde(rename = "participation_started_at")]
    pub started_at: NaiveDate,
    #[serde(rename = "participation_stopped_at")]
    pub stopped_at: Option<NaiveDate>,
    pub organisation_id: SnowflakeInternal<Organisation>,
    pub role_id: SnowflakeInternal<OrganisationRole>,
    pub subject_id: SnowflakeInternal<AcademicParticipant>,
    pub referrer_id: SnowflakeInternal<AcademicParticipant>,
    pub participation_state: ParticipationState,
}

impl Entity for OrganisationParticipation {
    const ENTITY: &'static str = "OrganisationParticipation";
}

#[automock]
#[async_trait]
pub trait OrganisationParticipationMethods {
    async fn get_participation_by(
        &self,
        organisation_id: &Snowflake<Organisation>,
        subject_id: &Snowflake<AcademicParticipant>,
        role_id: &Snowflake<OrganisationRole>,
    ) -> Result<OrganisationParticipation>;
    async fn get_roles_for(
        &self,
        organisation_id: &Snowflake<Organisation>,
        subject_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashSet<OrganisationRole>>;
    async fn search_participants_by_fuzzy_name(
        &self,
        organisation_id: &Snowflake<Organisation>,
        fuzzy_name: &str,
        top_n: i64,
    ) -> Result<Vec<AcademicParticipant>>;
    async fn get_active_participation_for_user(
        &self,
        subject_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<Organisation, Vec<OrganisationRole>>>;
    async fn get_historic_participation_for_user(
        &self,
        subject_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<Organisation, Vec<OrganisationRole>>>;
    async fn get_pending_participation_for_user(
        &self,
        subject_id: &Snowflake<AcademicParticipant>,
    ) -> Result<Vec<(OrganisationParticipation, Organisation, AcademicParticipant)>>;
    async fn get_active_participants_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<AcademicParticipant, Vec<OrganisationRole>>>;
    async fn get_historic_participants_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<AcademicParticipant, Vec<OrganisationRole>>>;
    async fn get_pending_participants_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<AcademicParticipant, Vec<OrganisationRole>>>;
    async fn get_declined_participants_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<AcademicParticipant, Vec<OrganisationRole>>>;
    async fn insert_new_participation(
        &self,
        organisation_id: &Snowflake<Organisation>,
        role_id: &Snowflake<OrganisationRole>,
        subject_id: &Snowflake<AcademicParticipant>,
        referrer_id: &Snowflake<AcademicParticipant>,
        participation_started_at: &NaiveDate,
        participation_state: &ParticipationState,
    ) -> Result<OrganisationParticipation>;
    async fn update_participation_state(
        &self,
        participation_id: &Snowflake<OrganisationParticipation>,
        new_state: &ParticipationState,
    ) -> Result<OrganisationParticipation>;
    async fn stop_participation(
        &self,
        participation_id: &Snowflake<OrganisationParticipation>,
    ) -> Result<OrganisationParticipation>;
}

#[derive(Debug, Clone)]
pub struct OrganisationParticipationProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl OrganisationParticipationMethods for OrganisationParticipationProviderDb {
    #[instrument(skip(self))]
    async fn get_participation_by(
        &self,
        organisation_id: &Snowflake<Organisation>,
        subject_id: &Snowflake<AcademicParticipant>,
        role_id: &Snowflake<OrganisationRole>,
    ) -> Result<OrganisationParticipation> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let subject_id = subject_id.internal.to_i32();
        let role_id = role_id.internal.to_i32();
        let row = conn
            .query(
                "
SELECT OP.*
FROM Research.OrganisationParticipation OP
WHERE OP.subject_id = @p1
  AND OP.organisation_id = @p2
  AND OP.role_id = @p3
;",
                &[&subject_id, &organisation_id, &role_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<OrganisationParticipation>(row)?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_roles_for(
        &self,
        organisation_id: &Snowflake<Organisation>,
        subject_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashSet<OrganisationRole>> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let subject_id = subject_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT O.*
FROM Research.ActiveOrganisationParticipation AOP
JOIN Research.OrganisationRoles O on AOP.role_id = O.role_id
WHERE AOP.subject_id = @p1
  AND AOP.organisation_id = @p2
;",
                &[&subject_id, &organisation_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<OrganisationRole>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn search_participants_by_fuzzy_name(
        &self,
        organisation_id: &Snowflake<Organisation>,
        fuzzy_name: &str,
        top_n: i64,
    ) -> Result<Vec<AcademicParticipant>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT TOP (10) AP.*,
    Research.FuzzyScore('par', AP.full_name) as score
FROM Research.ActiveOrganisationParticipation OP
JOIN Research.AcademicParticipants AP
  ON OP.subject_id = AP.participant_id
WHERE OP.organisation_id = 11
ORDER BY score DESC
;",
                &[&top_n, &fuzzy_name],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<AcademicParticipant>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn get_active_participation_for_user(
        &self,
        subject_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<Organisation, Vec<OrganisationRole>>> {
        let mut conn = self.get_connection().await?;
        let subject_id = subject_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT O.*, O2.*
FROM Research.ActiveOrganisationParticipation OP
JOIN Research.OrganisationRoles O on O.role_id = OP.role_id
JOIN Research.Organisations O2 on O2.organisation_id = OP.organisation_id
WHERE OP.subject_id = @p1
;",
                &[&subject_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(Organisation, OrganisationRole)>(row)?))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .into_group_map();
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_historic_participation_for_user(
        &self,
        subject_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<Organisation, Vec<OrganisationRole>>> {
        let mut conn = self.get_connection().await?;
        let subject_id = subject_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT O.*, O2.*
FROM Research.OrganisationParticipation OP
JOIN Research.OrganisationRoles O on O.role_id = OP.role_id
JOIN Research.Organisations O2 on O2.organisation_id = OP.organisation_id
WHERE OP.subject_id = @p1
;",
                &[&subject_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(Organisation, OrganisationRole)>(row)?))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .into_group_map();
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_pending_participation_for_user(
        &self,
        subject_id: &Snowflake<AcademicParticipant>,
    ) -> Result<Vec<(OrganisationParticipation, Organisation, AcademicParticipant)>> {
        let mut conn = self.get_connection().await?;
        let subject_id = subject_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT OP.*, O.organisation_created_at, O.organisation_name, Ref.*
FROM Research.OrganisationParticipation OP
JOIN Research.Organisations O on O.organisation_id = OP.organisation_id
JOIN Research.AcademicParticipants Ref on Ref.participant_id = OP.referrer_id
WHERE OP.subject_id = @p1
  AND OP.participation_state = 'pending'
;",
                &[&subject_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| {
                Ok(from_row::<(
                    OrganisationParticipation,
                    Organisation,
                    AcademicParticipant,
                )>(row)?)
            })
            .collect::<Result<Vec<_>>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_active_participants_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<AcademicParticipant, Vec<OrganisationRole>>> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT O.*, AP.*
FROM Research.ActiveOrganisationParticipation OP
JOIN Research.AcademicParticipants AP on AP.participant_id = OP.subject_id
JOIN Research.OrganisationRoles O on O.role_id = OP.role_id
WHERE OP.organisation_id = @p1
;",
                &[&organisation_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(AcademicParticipant, OrganisationRole)>(row)?))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .into_group_map();
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_historic_participants_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<AcademicParticipant, Vec<OrganisationRole>>> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT O.*, AP.*
FROM Research.OrganisationParticipation OP
JOIN Research.AcademicParticipants AP on AP.participant_id = OP.subject_id
JOIN Research.OrganisationRoles O on O.role_id = OP.role_id
WHERE OP.organisation_id = @p1
  AND OP.participation_stopped_at IS NOT NULL
;",
                &[&organisation_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(AcademicParticipant, OrganisationRole)>(row)?))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .into_group_map();
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_pending_participants_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<AcademicParticipant, Vec<OrganisationRole>>> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT O.*, AP.*
FROM Research.OrganisationParticipation OP
JOIN Research.AcademicParticipants AP on AP.participant_id = OP.subject_id
JOIN Research.OrganisationRoles O on O.role_id = OP.role_id
WHERE OP.organisation_id = @p1
  AND OP.participation_state = 'pending'
;",
                &[&organisation_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(AcademicParticipant, OrganisationRole)>(row)?))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .into_group_map();
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_declined_participants_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<AcademicParticipant, Vec<OrganisationRole>>> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT O.*, AP.*
FROM Research.OrganisationParticipation OP
JOIN Research.AcademicParticipants AP on AP.participant_id = OP.subject_id
JOIN Research.OrganisationRoles O on O.role_id = OP.role_id
WHERE OP.organisation_id = @p1
  AND OP.participation_state = 'declined'
;",
                &[&organisation_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(AcademicParticipant, OrganisationRole)>(row)?))
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .into_group_map();
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn insert_new_participation(
        &self,
        organisation_id: &Snowflake<Organisation>,
        role_id: &Snowflake<OrganisationRole>,
        subject_id: &Snowflake<AcademicParticipant>,
        referrer_id: &Snowflake<AcademicParticipant>,
        participation_started_at: &NaiveDate,
        participation_state: &ParticipationState,
    ) -> Result<OrganisationParticipation> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let subject_id = subject_id.internal.to_i32();
        let referrer_id = referrer_id.internal.to_i32();
        let role_id = role_id.internal.to_i32();
        let row = conn
            .query(
                "
INSERT INTO Research.OrganisationParticipation (organisation_id, role_id, subject_id, referrer_id, participation_started_at, participation_state)
OUTPUT inserted.*
VALUES (@p1, @p2, @p3, @p4, @p5, @p6)
;",
                &[&organisation_id, &role_id, &subject_id, &referrer_id, participation_started_at, participation_state],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<OrganisationParticipation>(row)?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn update_participation_state(
        &self,
        participation_id: &Snowflake<OrganisationParticipation>,
        new_state: &ParticipationState,
    ) -> Result<OrganisationParticipation> {
        let mut conn = self.get_connection().await?;
        let participation_id = participation_id.internal.to_i32();
        let row = conn
            .query(
                "
UPDATE Research.OrganisationParticipation
SET participation_state = @p2
OUTPUT inserted.*
WHERE participation_id = @p1
;",
                &[&participation_id, new_state],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<OrganisationParticipation>(row)?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn stop_participation(
        &self,
        participation_id: &Snowflake<OrganisationParticipation>,
    ) -> Result<OrganisationParticipation> {
        let mut conn = self.get_connection().await?;
        let participation_id = participation_id.internal.to_i32();
        let row = conn
            .query(
                "
UPDATE Research.OrganisationParticipation
SET participation_stopped_at = GETDATE()
OUTPUT inserted.*
WHERE participation_id = @p1
;",
                &[&participation_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<OrganisationParticipation>(row)?;
        Ok(res)
    }
}

impl OrganisationParticipationProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}

#[cfg(test)]
mod tests {
    use crate::db::from_row;

    use super::*;
    use std::borrow::Cow;

    use serde::Deserialize;
    use spectral::prelude::*;

    #[test]
    fn deserialize_row() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("participation_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(4))),
            ),
            (
                from_row::ColumnName("organisation_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(5))),
            ),
            (
                from_row::ColumnName("participation_started_at".into()),
                from_row::ColumnData(tiberius::ColumnData::Date(Some(tiberius::time::Date::new(
                    1000,
                )))),
            ),
            (
                from_row::ColumnName("participation_stopped_at".into()),
                from_row::ColumnData(tiberius::ColumnData::Date(None)),
            ),
            (
                from_row::ColumnName("role_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(6))),
            ),
            (
                from_row::ColumnName("subject_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(7))),
            ),
            (
                from_row::ColumnName("referrer_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(8))),
            ),
            (
                from_row::ColumnName("participation_state".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("active")))),
            ),
        ]);

        let data = OrganisationParticipation::deserialize(&row);
        assert_that!(&data).is_ok_containing(&OrganisationParticipation {
            id: SnowflakeInternal::from_i32(4),
            started_at: chrono::NaiveDate::from_ymd_opt(1, 1, 1).unwrap()
                + chrono::Duration::days(1000),
            stopped_at: None,
            organisation_id: SnowflakeInternal::from_i32(5),
            role_id: SnowflakeInternal::from_i32(6),
            subject_id: SnowflakeInternal::from_i32(7),
            referrer_id: SnowflakeInternal::from_i32(8),
            participation_state: ParticipationState::Active,
        });
    }
}
