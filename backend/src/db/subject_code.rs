use axum::async_trait;
use mockall::automock;
use serde::Deserialize;
use std::collections::{HashMap, HashSet};

use chrono::NaiveDateTime;
use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    DbError, Entity, Result,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct SubjectCode {
    #[serde(rename = "code_id")]
    pub id: SnowflakeInternal<Self>,
    #[serde(rename = "code_created_at")]
    pub created_at: NaiveDateTime,
    #[serde(rename = "code_name")]
    pub name: String,
    pub code: String,
}

impl Entity for SubjectCode {
    const ENTITY: &'static str = "SubjectCode";
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct Publications {
    pub publications: i32,
}

#[automock]
#[async_trait]
pub trait SubjectCodeMethods {
    async fn get_all(&self) -> Result<HashSet<SubjectCode>>;
    async fn get_by_id(&self, id: &Snowflake<SubjectCode>) -> Result<Option<SubjectCode>>;
    async fn ensure(&self, id: &Snowflake<SubjectCode>) -> Result<SubjectCode>;
    async fn get_all_with_score(&self) -> Result<HashMap<SubjectCode, Publications>>;
    async fn fuzzy_search(&self, fuzzy_search: &str, top_n: i64) -> Result<Vec<SubjectCode>>;
    async fn insert_new(&self, name: &str, code: &str) -> Result<SubjectCode>;
}

#[derive(Debug, Clone)]
pub struct SubjectCodeProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl SubjectCodeMethods for SubjectCodeProviderDb {
    #[instrument(skip(self))]
    async fn get_all(&self) -> Result<HashSet<SubjectCode>> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query("SELECT SC.* FROM Research.SubjectCodes SC;", &[])
            .await?
            .into_first_result()
            .await?;
        row.into_iter()
            .map(|row| Ok(from_row::<SubjectCode>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn get_by_id(&self, id: &Snowflake<SubjectCode>) -> Result<Option<SubjectCode>> {
        let mut conn = self.get_connection().await?;
        let id = id.internal.to_i32();
        let row = conn
            .query(
                "SELECT SC.* FROM Research.SubjectCodes SC WHERE SC.code_id = @p1;",
                &[&id],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<SubjectCode>(row)?)).transpose()
    }

    #[instrument(skip(self))]
    async fn ensure(&self, id: &Snowflake<SubjectCode>) -> Result<SubjectCode> {
        self.get_by_id(id).await?.ok_or(DbError::NoEntity {
            entity: SubjectCode::ENTITY,
            id: id.external.to_hex(),
        })
    }

    #[instrument(skip(self))]
    async fn get_all_with_score(&self) -> Result<HashMap<SubjectCode, Publications>> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "
SELECT SC.*, SCR.publications
FROM Research.SubjectCodesRanked SCR
JOIN Research.SubjectCodes SC on SCR.code_id = SC.code_id
ORDER BY SCR.publications
;",
                &[],
            )
            .await?
            .into_first_result()
            .await?;
        row.into_iter()
            .map(|row| Ok(from_row::<(SubjectCode, Publications)>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn fuzzy_search(&self, fuzzy_search: &str, top_n: i64) -> Result<Vec<SubjectCode>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT TOP (@p1) SC.*,
    Research.InlineMax(
        Research.FuzzyScore(@p2, SC.code),
        Research.FuzzyScore(@p2, SC.code_name)
    ) as score
FROM Research.SubjectCodes SC
ORDER BY score DESC
;",
                &[&top_n, &fuzzy_search],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<SubjectCode>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn insert_new(&self, name: &str, code: &str) -> Result<SubjectCode> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "
INSERT INTO Research.SubjectCodes (code, code_name)
OUTPUT INSERTED.*
VALUES (@p1, @p2)
;",
                &[&code, &name],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let code = from_row::<SubjectCode>(row)?;
        Ok(code)
    }
}

impl SubjectCodeProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}

#[cfg(test)]
mod tests {
    use crate::db::from_row;

    use super::*;
    use std::borrow::Cow;
    use tiberius::FromSql;

    use serde::Deserialize;
    use spectral::prelude::*;

    #[test]
    fn deserialize_subject_code() {
        let datetime = tiberius::time::DateTime::new(1, 1);
        let normal_datetime =
            chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::DateTime(Some(datetime)))
                .unwrap()
                .unwrap();

        let row = from_row::Row(vec![
            (
                from_row::ColumnName("code_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("code_created_at".into()),
                from_row::ColumnData(tiberius::ColumnData::DateTime(Some(datetime))),
            ),
            (
                from_row::ColumnName("code_name".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Name")))),
            ),
            (
                from_row::ColumnName("code".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Code")))),
            ),
        ]);

        let data = SubjectCode::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&SubjectCode {
            id: SnowflakeInternal::from_i32(1),
            created_at: normal_datetime.clone(),
            name: "Name".to_string(),
            code: "Code".to_string(),
        });
    }
}
