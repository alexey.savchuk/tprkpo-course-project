use axum::async_trait;
use chrono::NaiveDateTime;
use mockall::automock;
use serde::Deserialize;
use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    DbError, Entity, Result,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct AcademicParticipant {
    #[serde(rename = "participant_id")]
    pub id: SnowflakeInternal<Self>,
    #[serde(rename = "participant_created_at")]
    pub created_at: NaiveDateTime,
    pub full_name: String,
}

impl Entity for AcademicParticipant {
    const ENTITY: &'static str = "AcademicParticipant";
}

#[automock]
#[async_trait]
pub trait AcademicParticipantMethods {
    async fn list_all(&self) -> Result<Vec<AcademicParticipant>>;
    async fn get_by_id(
        &self,
        id: &Snowflake<AcademicParticipant>,
    ) -> Result<Option<AcademicParticipant>>;
    async fn ensure(&self, id: &Snowflake<AcademicParticipant>) -> Result<AcademicParticipant>;
    async fn get_by_fuzzy_name(
        &self,
        fuzzy_name: &str,
        top_n: i64,
    ) -> Result<Vec<AcademicParticipant>>;
    async fn insert_new(&self, full_name: &str) -> Result<AcademicParticipant>;
}

#[derive(Debug, Clone)]
pub struct AcademicParticipantProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl AcademicParticipantMethods for AcademicParticipantProviderDb {
    #[instrument(skip(self))]
    async fn list_all(&self) -> Result<Vec<AcademicParticipant>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "SELECT AP.* FROM Research.AcademicParticipants AP ORDER BY AP.participant_created_at;",
                &[],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<AcademicParticipant>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn get_by_id(
        &self,
        id: &Snowflake<AcademicParticipant>,
    ) -> Result<Option<AcademicParticipant>> {
        let mut conn = self.get_connection().await?;
        let id = id.internal.to_i32();
        let row = conn
            .query(
                "SELECT AP.* FROM Research.AcademicParticipants AP WHERE AP.participant_id = @p1;",
                &[&id],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<AcademicParticipant>(row)?))
            .transpose()
    }

    #[instrument(skip(self))]
    async fn ensure(&self, id: &Snowflake<AcademicParticipant>) -> Result<AcademicParticipant> {
        self.get_by_id(id).await?.ok_or(DbError::NoEntity {
            entity: AcademicParticipant::ENTITY,
            id: id.external.to_hex(),
        })
    }

    #[instrument(skip(self))]
    async fn get_by_fuzzy_name(
        &self,
        fuzzy_name: &str,
        top_n: i64,
    ) -> Result<Vec<AcademicParticipant>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT TOP (@P1) AP.*,
    Research.FuzzyScore(@P2, AP.full_name) as score
FROM Research.AcademicParticipants AP
ORDER BY score DESC
;",
                &[&top_n, &fuzzy_name],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<AcademicParticipant>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn insert_new(&self, full_name: &str) -> Result<AcademicParticipant> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "
INSERT INTO Research.AcademicParticipants (full_name)
OUTPUT INSERTED.*
VALUES (@p1)
;",
                &[&full_name],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let user = from_row::<AcademicParticipant>(row)?;
        Ok(user)
    }
}

impl AcademicParticipantProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}

#[cfg(test)]
mod tests {
    use std::borrow::Cow;

    use crate::{db::from_row, snowflake::SnowflakeInternal};
    use serde::Deserialize;
    use spectral::assert_that;
    use tiberius::FromSql;

    use super::AcademicParticipant;

    #[test]
    fn deserialize_academic_participant() {
        let datetime = tiberius::time::DateTime::new(1, 1);
        let normal_datetime = chrono::NaiveDateTime::from_sql(
            &tiberius::ColumnData::DateTime(Some(datetime))
        ).unwrap().unwrap();

        let row = from_row::Row(vec![
            (
                from_row::ColumnName("participant_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("participant_created_at".into()),
                from_row::ColumnData(tiberius::ColumnData::DateTime(Some(datetime))),
            ),
            (
                from_row::ColumnName("full_name".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Name")))),
            ),
        ]);

        let data = AcademicParticipant::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&AcademicParticipant {
            id: SnowflakeInternal::from_i32(1),
            created_at: normal_datetime.clone(),
            full_name: "Name".to_string(),
        });
    }
}
