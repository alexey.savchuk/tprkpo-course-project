use axum::async_trait;
use mockall::automock;
use serde::Deserialize;
use tracing::instrument;

use crate::snowflake::SnowflakeInternal;

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    AcademicParticipant, DbError, Entity, Organisation, Publication, Result, SubjectCode,
};

#[derive(Debug, Hash, PartialEq, Eq, Deserialize, Clone)]
#[serde(tag = "item_type")]
pub enum GlobalSearch {
    #[serde(rename = "publication")]
    Publication {
        item_id: SnowflakeInternal<Publication>,
        item_value: String,
    },
    #[serde(rename = "participant")]
    Participant {
        item_id: SnowflakeInternal<AcademicParticipant>,
        item_value: String,
    },
    #[serde(rename = "organisation")]
    Organisation {
        item_id: SnowflakeInternal<Organisation>,
        item_value: String,
    },
    #[serde(rename = "subject_code")]
    SubjectCode {
        item_id: SnowflakeInternal<SubjectCode>,
        item_value: String,
    },
}

impl Entity for GlobalSearch {
    const ENTITY: &'static str = "GlobalSearch";
}

#[automock]
#[async_trait]
pub trait GlobalSearchMethods {
    async fn global_search(&self, fuzzy_search: &str, top_n: i64) -> Result<Vec<GlobalSearch>>;
}

#[derive(Debug, Clone)]
pub struct GlobalSearchProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl GlobalSearchMethods for GlobalSearchProviderDb {
    #[instrument]
    async fn global_search(&self, fuzzy_search: &str, top_n: i64) -> Result<Vec<GlobalSearch>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT TOP (@p1) S.*
FROM Research.GlobalSearch(@p2) S
ORDER BY S.item_score DESC
;",
                &[&top_n, &fuzzy_search],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<GlobalSearch>(row)?))
            .collect::<Result<_>>()
    }
}

impl GlobalSearchProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}

#[cfg(test)]
mod tests {
    use crate::db::from_row;

    use super::*;
    use std::borrow::Cow;

    use serde::Deserialize;
    use spectral::prelude::*;

    #[test]
    fn deserialize_participant() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("item_type".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "participant",
                )))),
            ),
            (
                from_row::ColumnName("item_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("item_value".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "participant_value",
                )))),
            ),
        ]);

        let data = GlobalSearch::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&GlobalSearch::Participant {
            item_id: SnowflakeInternal::from_i32(1),
            item_value: "participant_value".to_string(),
        });
    }

    #[test]
    fn deserialize_publication() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("item_type".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "publication",
                )))),
            ),
            (
                from_row::ColumnName("item_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("item_value".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "publication_value",
                )))),
            ),
        ]);

        let data = GlobalSearch::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&GlobalSearch::Publication {
            item_id: SnowflakeInternal::from_i32(1),
            item_value: "publication_value".to_string(),
        });
    }

    #[test]
    fn deserialize_organisation() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("item_type".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "organisation",
                )))),
            ),
            (
                from_row::ColumnName("item_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("item_value".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "organisation_value",
                )))),
            ),
        ]);

        let data = GlobalSearch::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&GlobalSearch::Organisation {
            item_id: SnowflakeInternal::from_i32(1),
            item_value: "organisation_value".to_string(),
        });
    }

    #[test]
    fn deserialize_subject_code() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("item_type".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "subject_code",
                )))),
            ),
            (
                from_row::ColumnName("item_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("item_value".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "subject_code_value",
                )))),
            ),
        ]);

        let data = GlobalSearch::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&GlobalSearch::SubjectCode {
            item_id: SnowflakeInternal::from_i32(1),
            item_value: "subject_code_value".to_string(),
        });
    }
}
