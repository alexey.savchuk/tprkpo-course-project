use axum::async_trait;
use mockall::automock;
use serde::Deserialize;
use std::collections::HashMap;

use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    AcademicParticipant, DbError, Entity, Organisation, Publication, Result, SubjectCode,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct PublicationReference {
    pub from_id: SnowflakeInternal<Publication>,
    pub to_id: SnowflakeInternal<Publication>,
}

impl Entity for PublicationReference {
    const ENTITY: &'static str = "PublicationReference";
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct Count {
    pub count: i32,
}

#[automock]
#[async_trait]
pub trait PublicationReferenceMethods {
    async fn get_publications_from_id(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashMap<Publication, SubjectCode>>;
    async fn get_publications_to_id(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashMap<Publication, SubjectCode>>;
    async fn get_citation_count_for_id(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<Count>;
    async fn insert_new(
        &self,
        from_id: &Snowflake<Publication>,
        to_id: &Snowflake<Publication>,
    ) -> Result<PublicationReference>;
    async fn get_citation_count_for_participant(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<Count>;
    async fn get_citation_count_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<Count>;
}

#[derive(Debug, Clone)]
pub struct PublicationReferenceProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl PublicationReferenceMethods for PublicationReferenceProviderDb {
    #[instrument(skip(self))]
    async fn get_publications_from_id(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashMap<Publication, SubjectCode>> {
        let mut conn = self.get_connection().await?;
        let publication_id = publication_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT P.*, SC.code_created_at, SC.code, SC.code_name
FROM Research.PublicationReferences PR
JOIN Research.Publications P on P.publication_id = PR.from_id
JOIN Research.SubjectCodes SC on SC.code_id = P.code_id
WHERE PR.to_id = @p1
;",
                &[&publication_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(Publication, SubjectCode)>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_publications_to_id(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashMap<Publication, SubjectCode>> {
        let mut conn = self.get_connection().await?;
        let publication_id = publication_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT P.*, SC.code_created_at, SC.code, SC.code_name
FROM Research.PublicationReferences PR
JOIN Research.Publications P on P.publication_id = PR.to_id
JOIN Research.SubjectCodes SC on SC.code_id = P.code_id
WHERE PR.from_id = @p1
;",
                &[&publication_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(Publication, SubjectCode)>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_citation_count_for_id(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<Count> {
        let mut conn = self.get_connection().await?;
        let publication_id = publication_id.internal.to_i32();
        let row = conn
            .query(
                "SELECT count FROM Research.CitationCountPerPublication WHERE publication_id = @p1;",
                &[&publication_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let count = from_row::<Count>(row)?;
        Ok(count)
    }

    #[instrument(skip(self))]
    async fn insert_new(
        &self,
        from_id: &Snowflake<Publication>,
        to_id: &Snowflake<Publication>,
    ) -> Result<PublicationReference> {
        let mut conn = self.get_connection().await?;
        let from_id = from_id.internal.to_i32();
        let to_id = to_id.internal.to_i32();
        let row = conn
            .query(
                "
INSERT INTO Research.PublicationReferences (from_id, to_id)
OUTPUT inserted.*
VALUES (@p1, @p2)
;",
                &[&from_id, &to_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<PublicationReference>(row)?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_citation_count_for_participant(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<Count> {
        let mut conn = self.get_connection().await?;
        let participant_id = participant_id.internal.to_i32();
        let row = conn
            .query(
                "SELECT count FROM Research.CitationCountPerParticipant WHERE participant_id = @p1;",
                &[&participant_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let count = from_row::<Count>(row)?;
        Ok(count)
    }

    #[instrument(skip(self))]
    async fn get_citation_count_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<Count> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let row = conn
            .query(
                "SELECT count FROM Research.CitationCountPerOrganisation WHERE organisation_id = @p1;",
                &[&organisation_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let count = from_row::<Count>(row)?;
        Ok(count)
    }
}

impl PublicationReferenceProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}
#[cfg(test)]
mod tests {
    use crate::db::from_row;

    use super::*;

    use serde::Deserialize;
    use spectral::prelude::*;

    #[test]
    fn deserialize_publication_reference() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("from_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("to_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(2))),
            ),
        ]);

        let data = PublicationReference::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&PublicationReference {
            from_id: SnowflakeInternal::from_i32(1),
            to_id: SnowflakeInternal::from_i32(2),
        });
    }
}
