use tokio::net::TcpStream;
use tokio_util::compat::Compat;
pub type Client = tiberius::Client<Compat<TcpStream>>;

pub type PoolConnection<'a> = bb8::PooledConnection<'a, bb8_tiberius::ConnectionManager>;
pub type DbPool = bb8::Pool<bb8_tiberius::ConnectionManager>;

pub mod extract {
    use axum::async_trait;
    use axum::extract::FromRef;
    use axum::extract::FromRequestParts;
    use axum::http::request::Parts;

    use crate::api;

    use crate::db::DbError;

    use super::DbPool;

    pub struct DatabaseConnection(
        pub bb8::PooledConnection<'static, bb8_tiberius::ConnectionManager>,
    );

    #[async_trait]
    impl<S> FromRequestParts<S> for DatabaseConnection
    where
        DbPool: FromRef<S>,
        S: Send + Sync,
    {
        type Rejection = api::ApiError;

        async fn from_request_parts(
            _parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let pool = DbPool::from_ref(state);
            let conn = pool.get_owned().await.map_err(DbError::from)?;
            Ok(DatabaseConnection(conn))
        }
    }
}
