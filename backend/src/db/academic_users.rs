use axum::async_trait;
use mockall::automock;
use serde::Deserialize;
use tracing::instrument;

use crate::{
    auth::OryIdentity,
    snowflake::{Snowflake, SnowflakeInternal},
};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    AcademicParticipant, DbError, Entity, Result,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct AcademicUsers {
    pub user_id: String,
    pub participant_id: SnowflakeInternal<AcademicParticipant>,
}

impl Entity for AcademicUsers {
    const ENTITY: &'static str = "AcademicUsers";
}

#[automock]
#[async_trait]
pub trait AcademicUsersMethods {
    async fn get_academic_participant_for_user(
        &self,
        user_id: &OryIdentity,
    ) -> Result<Option<AcademicParticipant>>;
    async fn insert_new(
        &self,
        user_id: &OryIdentity,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<AcademicUsers>;
}

#[derive(Debug, Clone)]
pub struct AcademicUsersProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl AcademicUsersMethods for AcademicUsersProviderDb {
    #[instrument(skip(self))]
    async fn get_academic_participant_for_user(
        &self,
        user_id: &OryIdentity,
    ) -> Result<Option<AcademicParticipant>> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "
SELECT AP.*
FROM Research.AcademicUsers AU
JOIN Research.AcademicParticipants AP on AP.participant_id = AU.participant_id
WHERE AU.user_id = @p1
;",
                &[&user_id.id],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<AcademicParticipant>(row)?))
            .transpose()
    }

    #[instrument(skip(self))]
    async fn insert_new(
        &self,
        user_id: &OryIdentity,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<AcademicUsers> {
        let mut conn = self.get_connection().await?;
        let participant_id = participant_id.internal.to_i32();
        let row = conn
            .query(
                "
INSERT INTO Research.AcademicUsers(user_id, participant_id)
OUTPUT inserted.*
VALUES (@p1, @p2)
;",
                &[&user_id.id, &participant_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<AcademicUsers>(row)?;
        Ok(res)
    }
}

impl AcademicUsersProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}

#[cfg(test)]
mod tests {
    use std::borrow::Cow;

    use crate::{db::from_row, snowflake::SnowflakeInternal};
    use serde::Deserialize;
    use spectral::assert_that;

    use super::AcademicUsers;

    #[test]
    fn deserialize_academic_users() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("user_id".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("id")))),
            ),
            (
                from_row::ColumnName("participant_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(2))),
            ),
        ]);

        let data = AcademicUsers::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&AcademicUsers {
            user_id: "id".to_string(),
            participant_id: SnowflakeInternal::from_i32(2),
        });
    }
}
