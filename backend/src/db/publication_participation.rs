use axum::async_trait;
use mockall::automock;
use serde::Deserialize;
use std::collections::{HashMap, HashSet};

use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    AcademicParticipant, DbError, Entity, Organisation, ParticipationState, Publication,
    PublicationRole, Result, SubjectCode,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct PublicationParticipation {
    pub participation_id: SnowflakeInternal<Self>,
    pub publication_id: SnowflakeInternal<Publication>,
    pub role_id: SnowflakeInternal<PublicationRole>,
    pub subject_id: SnowflakeInternal<AcademicParticipant>,
    pub referrer_id: SnowflakeInternal<AcademicParticipant>,
    pub participation_state: ParticipationState,
}

impl Entity for PublicationParticipation {
    const ENTITY: &'static str = "PublicationParticipation";
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct Score {
    pub score: i32,
}

#[automock]
#[async_trait]
pub trait PublicationParticipationMethods {
    async fn get_active_participation_for_participant(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<Publication, (PublicationRole, SubjectCode)>>;
    async fn get_pending_participation_for_participant(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<Vec<(PublicationParticipation, Publication, AcademicParticipant)>>;
    async fn get_publication_roles_for_participant(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashSet<PublicationRole>>;
    async fn get_publications_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<Publication, SubjectCode>>;
    async fn get_participants_for_publication(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashMap<AcademicParticipant, PublicationRole>>;
    async fn get_organisations_for_publication(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashSet<Organisation>>;
    async fn get_participants_connected_via_publications(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<AcademicParticipant, Score>>;
    async fn get_participants_connected_via_from_refs(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<AcademicParticipant, Score>>;
    async fn get_participants_connected_via_to_refs(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<AcademicParticipant, Score>>;
    async fn insert_new_participation(
        &self,
        publication_id: &Snowflake<Publication>,
        role_id: &Snowflake<PublicationRole>,
        subject_id: &Snowflake<AcademicParticipant>,
        referrer_id: &Snowflake<AcademicParticipant>,
        participation_state: &ParticipationState,
    ) -> Result<PublicationParticipation>;
    async fn update_participation_state(
        &self,
        participation_id: &Snowflake<PublicationParticipation>,
        new_state: &ParticipationState,
    ) -> Result<PublicationParticipation>;
}

#[derive(Debug, Clone)]
pub struct PublicationParticipationProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl PublicationParticipationMethods for PublicationParticipationProviderDb {
    #[instrument(skip(self))]
    async fn get_active_participation_for_participant(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<Publication, (PublicationRole, SubjectCode)>> {
        let mut conn = self.get_connection().await?;
        let participant_id = participant_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT P.*, code_created_at, code, code_name, PR.*
FROM Research.ActivePublicationParticipation PP
JOIN Research.Publications P on P.publication_id = PP.publication_id
JOIN Research.SubjectCodes SC on SC.code_id = P.code_id
JOIN Research.PublicationRoles PR on PR.role_id = PP.role_id
WHERE PP.subject_id = @p1
;",
                &[&participant_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| {
                Ok(from_row::<(Publication, (PublicationRole, SubjectCode))>(
                    row,
                )?)
            })
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_pending_participation_for_participant(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<Vec<(PublicationParticipation, Publication, AcademicParticipant)>> {
        let mut conn = self.get_connection().await?;
        let participant_id = participant_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT PP.*, P.publication_created_at, P.code_id, P.publication_title, P.published_at, Ref.*
FROM Research.PublicationParticipation PP
JOIN Research.Publications P on P.publication_id = PP.publication_id
JOIN Research.AcademicParticipants Ref on Ref.participant_id = PP.referrer_id
WHERE PP.subject_id = @p1
  AND PP.participation_state = 'pending'
;",
                &[&participant_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| {
                Ok(from_row::<(
                    PublicationParticipation,
                    Publication,
                    AcademicParticipant,
                )>(row)?)
            })
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_publication_roles_for_participant(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashSet<PublicationRole>> {
        let mut conn = self.get_connection().await?;
        let participant_id = participant_id.internal.to_i32();
        let publication_id = publication_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT R.*
FROM Research.UserPublicationParticipation UPP
JOIN Research.PublicationRoles R on UPP.role_id = R.role_id
WHERE UPP.participant_id = @p1
  AND UPP.publication_id = @p2
;",
                &[&participant_id, &publication_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<PublicationRole>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_publications_for_organisation(
        &self,
        organisation_id: &Snowflake<Organisation>,
    ) -> Result<HashMap<Publication, SubjectCode>> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT P.*, code_created_at, code, code_name
FROM Research.PublicationsOfOrganisations PO
JOIN Research.Publications P on P.publication_id = PO.publication_id
JOIN Research.SubjectCodes SC on SC.code_id = P.code_id
WHERE PO.organisation_id = @p1
;",
                &[&organisation_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(Publication, SubjectCode)>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_participants_for_publication(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashMap<AcademicParticipant, PublicationRole>> {
        let mut conn = self.get_connection().await?;
        let publication_id = publication_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT AP.*, PR.*
FROM Research.ActivePublicationParticipation PP
JOIN Research.AcademicParticipants AP on AP.participant_id = PP.subject_id
JOIN Research.PublicationRoles PR on PR.role_id = PP.role_id
WHERE PP.publication_id = @p1
;",
                &[&publication_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(AcademicParticipant, PublicationRole)>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_organisations_for_publication(
        &self,
        publication_id: &Snowflake<Publication>,
    ) -> Result<HashSet<Organisation>> {
        let mut conn = self.get_connection().await?;
        let publication_id = publication_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT O.*
FROM Research.PublicationsOfOrganisations PO
JOIN Research.Organisations O on PO.organisation_id = O.organisation_id
WHERE PO.publication_id = @p1
;",
                &[&publication_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<Organisation>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_participants_connected_via_publications(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<AcademicParticipant, Score>> {
        let mut conn = self.get_connection().await?;
        let participant_id = participant_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT AP.*, CP.score
FROM Research.ConnectedPeopleByParticipation CP
JOIN Research.AcademicParticipants AP on AP.participant_id = CP.to_id
WHERE CP.from_id = @p1
;",
                &[&participant_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(AcademicParticipant, Score)>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_participants_connected_via_from_refs(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<AcademicParticipant, Score>> {
        let mut conn = self.get_connection().await?;
        let participant_id = participant_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT AP.*, CR.score
FROM Research.ConnectedPeopleByReferences CR
JOIN Research.AcademicParticipants AP on AP.participant_id = CR.to_id
WHERE CR.from_id = @p1
;",
                &[&participant_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(AcademicParticipant, Score)>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_participants_connected_via_to_refs(
        &self,
        participant_id: &Snowflake<AcademicParticipant>,
    ) -> Result<HashMap<AcademicParticipant, Score>> {
        let mut conn = self.get_connection().await?;
        let participant_id = participant_id.internal.to_i32();
        let rows = conn
            .query(
                "
SELECT AP.*, CR.score
FROM Research.ConnectedPeopleByReferences CR
JOIN Research.AcademicParticipants AP on AP.participant_id = CR.from_id
WHERE CR.to_id = @p1
;",
                &[&participant_id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<(AcademicParticipant, Score)>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn insert_new_participation(
        &self,
        publication_id: &Snowflake<Publication>,
        role_id: &Snowflake<PublicationRole>,
        subject_id: &Snowflake<AcademicParticipant>,
        referrer_id: &Snowflake<AcademicParticipant>,
        participation_state: &ParticipationState,
    ) -> Result<PublicationParticipation> {
        let mut conn = self.get_connection().await?;
        let publication_id = publication_id.internal.to_i32();
        let subject_id = subject_id.internal.to_i32();
        let referrer_id = referrer_id.internal.to_i32();
        let role_id = role_id.internal.to_i32();
        let row = conn
            .query(
                "
INSERT INTO Research.PublicationParticipation (publication_id, role_id, subject_id, referrer_id, participation_state)
OUTPUT inserted.*
VALUES (@p1, @p2, @p3, @p4, @p5)
;",
                &[&publication_id, &role_id, &subject_id, &referrer_id, participation_state],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<PublicationParticipation>(row)?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn update_participation_state(
        &self,
        participation_id: &Snowflake<PublicationParticipation>,
        new_state: &ParticipationState,
    ) -> Result<PublicationParticipation> {
        let mut conn = self.get_connection().await?;
        let participation_id = participation_id.internal.to_i32();
        let row = conn
            .query(
                "
UPDATE Research.PublicationParticipation
SET participation_state = @p2
OUTPUT inserted.*
WHERE participation_id = @p1
;",
                &[&participation_id, new_state],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<PublicationParticipation>(row)?;
        Ok(res)
    }
}

impl PublicationParticipationProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}

#[cfg(test)]
mod tests {
    use crate::db::from_row;

    use super::*;
    use std::borrow::Cow;

    use serde::Deserialize;
    use spectral::prelude::*;

    #[test]
    fn deserialize_row() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("participation_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(4))),
            ),
            (
                from_row::ColumnName("publication_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(5))),
            ),
            (
                from_row::ColumnName("role_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(6))),
            ),
            (
                from_row::ColumnName("subject_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(7))),
            ),
            (
                from_row::ColumnName("referrer_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(8))),
            ),
            (
                from_row::ColumnName("participation_state".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("active")))),
            ),
        ]);

        let data = PublicationParticipation::deserialize(&row);
        assert_that!(&data).is_ok_containing(&PublicationParticipation {
            participation_id: SnowflakeInternal::from_i32(4),
            publication_id: SnowflakeInternal::from_i32(5),
            role_id: SnowflakeInternal::from_i32(6),
            subject_id: SnowflakeInternal::from_i32(7),
            referrer_id: SnowflakeInternal::from_i32(8),
            participation_state: ParticipationState::Active,
        });
    }
}
