use serde::Deserialize;
use tokio_util::compat::TokioAsyncWriteCompatExt;

use super::{
    pool::{Client, DbPool},
    Result,
};

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct DbConfig {
    pub host: String,
    pub port: u16,

    pub database: String,

    pub user: String,
    pub password: String,

    pub applicationname: Option<String>,
    pub trustcertificates: bool,

    pub maxconnections: u32,
}

impl DbConfig {
    pub fn db_config(&self) -> tiberius::Config {
        let mut config = tiberius::Config::new();

        config.host(self.host.clone());
        config.port(self.port);

        config.authentication(tiberius::AuthMethod::sql_server(
            self.user.clone(),
            self.password.clone(),
        ));

        config.database(self.database.clone());

        self.applicationname
            .clone()
            .map(|name| config.application_name(name));
        if self.trustcertificates {
            config.trust_cert();
        }
        config
    }

    pub async fn build_client(&self) -> Result<Client> {
        let config = self.db_config();
        let tcp = tokio::net::TcpStream::connect(config.get_addr()).await?;
        tcp.set_nodelay(true)?;
        // Client is ready to use.
        let client = tiberius::Client::connect(config, tcp.compat_write()).await?;
        Ok(client)
    }

    pub async fn build_db_pool(&self) -> Result<DbPool> {
        let manager = bb8_tiberius::ConnectionManager::new(self.db_config());
        let pool = bb8::Pool::builder()
            .max_size(self.maxconnections)
            .build(manager)
            .await?;
        Ok(pool)
    }
}
