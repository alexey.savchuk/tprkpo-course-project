use axum::async_trait;
use chrono::NaiveDateTime;
use mockall::automock;
use serde::Deserialize;
use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    DbError, Entity, Result,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct OrganisationRole {
    #[serde(rename = "role_id")]
    pub id: SnowflakeInternal<Self>,
    #[serde(rename = "role_created_at")]
    pub created_at: NaiveDateTime,
    #[serde(rename = "role_name")]
    pub name: String,
}

impl Entity for OrganisationRole {
    const ENTITY: &'static str = "OrganisationRole";
}

impl OrganisationRole {
    pub const ADMINISTATOR: &'static str = "Administrator";
}

#[automock]
#[async_trait]
pub trait OrganisationRoleMethods {
    async fn list_all(&self) -> Result<Vec<OrganisationRole>>;
    async fn get_by_id(&self, id: &Snowflake<OrganisationRole>)
        -> Result<Option<OrganisationRole>>;
    async fn ensure(&self, id: &Snowflake<OrganisationRole>) -> Result<OrganisationRole>;
    async fn get_by_name(&self, name: &str) -> Result<Option<OrganisationRole>>;
    async fn get_admin(&self) -> Result<OrganisationRole>;
    async fn fuzzy_search(&self, fuzzy_name: &str, top_n: i64) -> Result<Vec<OrganisationRole>>;
    async fn insert_new(&self, name: &str) -> Result<OrganisationRole>;
}

#[derive(Debug, Clone)]
pub struct OrganisationRoleProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl OrganisationRoleMethods for OrganisationRoleProviderDb {
    #[instrument(skip(self))]
    async fn list_all(&self) -> Result<Vec<OrganisationRole>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "SELECT R.* FROM Research.OrganisationRoles R ORDER BY R.role_id;",
                &[],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<OrganisationRole>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn get_by_id(
        &self,
        id: &Snowflake<OrganisationRole>,
    ) -> Result<Option<OrganisationRole>> {
        let mut conn = self.get_connection().await?;
        let id = id.internal.to_i32();
        let row = conn
            .query(
                "SELECT R.* FROM Research.OrganisationRoles R WHERE R.role_id = @p1;",
                &[&id],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<OrganisationRole>(row)?))
            .transpose()
    }

    #[instrument(skip(self))]
    async fn ensure(&self, id: &Snowflake<OrganisationRole>) -> Result<OrganisationRole> {
        self.get_by_id(id).await?.ok_or(DbError::NoEntity {
            entity: OrganisationRole::ENTITY,
            id: id.external.to_hex(),
        })
    }

    #[instrument(skip(self))]
    async fn get_by_name(&self, name: &str) -> Result<Option<OrganisationRole>> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "SELECT R.* FROM Research.OrganisationRoles R WHERE R.role_name = @p1;",
                &[&name],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<OrganisationRole>(row)?))
            .transpose()
    }

    #[instrument(skip(self))]
    async fn get_admin(&self) -> Result<OrganisationRole> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "SELECT R.* FROM Research.OrganisationRoles R WHERE R.role_name = @p1;",
                &[&OrganisationRole::ADMINISTATOR],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let role = from_row::<OrganisationRole>(row)?;
        Ok(role)
    }

    #[instrument(skip(self))]
    async fn fuzzy_search(&self, fuzzy_name: &str, top_n: i64) -> Result<Vec<OrganisationRole>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT TOP (@p1) R.*,
    Research.FuzzyScore(@p2, R.role_name) as score
FROM Research.OrganisationRoles R
ORDER BY score DESC
;",
                &[&top_n, &fuzzy_name],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<OrganisationRole>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn insert_new(&self, name: &str) -> Result<OrganisationRole> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "
INSERT INTO Research.OrganisationRoles (role_name)
OUTPUT INSERTED.*
VALUES (@p1)
;",
                &[&name],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let role = from_row::<OrganisationRole>(row)?;
        Ok(role)
    }
}

impl OrganisationRoleProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}
#[cfg(test)]
mod tests {
    use std::borrow::Cow;

    use crate::{db::from_row, snowflake::SnowflakeInternal};
    use serde::Deserialize;
    use spectral::assert_that;
    use tiberius::FromSql;

    use super::OrganisationRole;

    #[test]
    fn deserialize_organisation_role() {
        let datetime = tiberius::time::DateTime::new(1, 1);
        let normal_datetime =
            chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::DateTime(Some(datetime)))
                .unwrap()
                .unwrap();

        let row = from_row::Row(vec![
            (
                from_row::ColumnName("role_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("role_created_at".into()),
                from_row::ColumnData(tiberius::ColumnData::DateTime(Some(datetime))),
            ),
            (
                from_row::ColumnName("role_name".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Name")))),
            ),
        ]);

        let data = OrganisationRole::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&OrganisationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: normal_datetime.clone(),
            name: "Name".to_string(),
        });
    }
}
