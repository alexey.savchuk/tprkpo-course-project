use axum::async_trait;
use chrono::{NaiveDate, NaiveDateTime};
use mockall::automock;
use serde::Deserialize;
use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    DbError, Entity, Result, SubjectCode,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct Publication {
    #[serde(rename = "publication_id")]
    pub id: SnowflakeInternal<Self>,
    #[serde(rename = "publication_created_at")]
    pub created_at: NaiveDateTime,
    pub code_id: SnowflakeInternal<SubjectCode>,
    #[serde(rename = "publication_title")]
    pub title: String,
    pub published_at: NaiveDate,
}

impl Entity for Publication {
    const ENTITY: &'static str = "Publication";
}

#[automock]
#[async_trait]
pub trait PublicationMethods {
    async fn list_all(&self) -> Result<Vec<(Publication, SubjectCode)>>;
    async fn ensure(&self, id: &Snowflake<Publication>) -> Result<(Publication, SubjectCode)>;
    async fn fuzzy_search_on_title(
        &self,
        fuzzy_title: &str,
        top_n: i64,
    ) -> Result<Vec<(Publication, SubjectCode)>>;
    async fn get_by_id_with_code(
        &self,
        id: &Snowflake<Publication>,
    ) -> Result<Option<(Publication, SubjectCode)>>;
    async fn insert_new(
        &self,
        title: &str,
        published_at: &NaiveDate,
        code_id: &Snowflake<SubjectCode>,
    ) -> Result<Publication>;
}

#[derive(Debug, Clone)]
pub struct PublicationProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl PublicationMethods for PublicationProviderDb {
    #[instrument(skip(self))]
    async fn list_all(&self) -> Result<Vec<(Publication, SubjectCode)>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT P.*, SC.code, SC.code_name, SC.code_created_at
FROM Research.Publications P
JOIN Research.SubjectCodes SC on SC.code_id = P.code_id
;",
                &[],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<(Publication, SubjectCode)>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn ensure(&self, id: &Snowflake<Publication>) -> Result<(Publication, SubjectCode)> {
        self.get_by_id_with_code(id)
            .await?
            .ok_or(DbError::NoEntity {
                entity: Publication::ENTITY,
                id: id.external.to_hex(),
            })
    }

    #[instrument(skip(self))]
    async fn fuzzy_search_on_title(
        &self,
        fuzzy_title: &str,
        top_n: i64,
    ) -> Result<Vec<(Publication, SubjectCode)>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT TOP (@p1) P.*, SC.code, SC.code_name, SC.code_created_at,
    Research.FuzzyScore(@p2, P.publication_title) as score
FROM Research.Publications P
JOIN Research.SubjectCodes SC on SC.code_id = P.code_id
ORDER BY score DESC
;",
                &[&top_n, &fuzzy_title],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<(Publication, SubjectCode)>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn get_by_id_with_code(
        &self,
        id: &Snowflake<Publication>,
    ) -> Result<Option<(Publication, SubjectCode)>> {
        let mut conn = self.get_connection().await?;
        let id = id.internal.to_i32();
        let row = conn
            .query(
                "
SELECT P.*, SC.code, SC.code_name, SC.code_created_at
FROM Research.Publications P
JOIN Research.SubjectCodes SC on SC.code_id = P.code_id
WHERE P.publication_id = @p1
;",
                &[&id],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<(Publication, SubjectCode)>(row)?))
            .transpose()
    }

    #[instrument(skip(self))]
    async fn insert_new(
        &self,
        title: &str,
        published_at: &NaiveDate,
        code_id: &Snowflake<SubjectCode>,
    ) -> Result<Publication> {
        let mut conn = self.get_connection().await?;
        let code_id = code_id.internal.to_i32();
        let row = conn
            .query(
                "
INSERT INTO Research.Publications (publication_title, code_id, published_at)
OUTPUT INSERTED.*
VALUES (@p1, @p2, @p3)
;",
                &[&title, &code_id, published_at],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let publication = from_row::<Publication>(row)?;
        Ok(publication)
    }
}

impl PublicationProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}
#[cfg(test)]
mod tests {
    use std::borrow::Cow;

    use crate::{db::from_row, snowflake::SnowflakeInternal};
    use serde::Deserialize;
    use spectral::assert_that;
    use tiberius::FromSql;

    use super::Publication;

    #[test]
    fn deserialize_publication() {
        let datetime = tiberius::time::DateTime::new(1, 1);
        let normal_datetime =
            chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::DateTime(Some(datetime)))
                .unwrap()
                .unwrap();

        let date = tiberius::time::Date::new(1);
        let normal_date = chrono::NaiveDate::from_sql(&tiberius::ColumnData::Date(Some(date)))
            .unwrap()
            .unwrap();

        let row = from_row::Row(vec![
            (
                from_row::ColumnName("publication_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("publication_created_at".into()),
                from_row::ColumnData(tiberius::ColumnData::DateTime(Some(datetime))),
            ),
            (
                from_row::ColumnName("code_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("publication_title".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Title")))),
            ),
            (
                from_row::ColumnName("published_at".into()),
                from_row::ColumnData(tiberius::ColumnData::Date(Some(date))),
            ),
        ]);

        let data = Publication::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&Publication {
            id: SnowflakeInternal::from_i32(1),
            created_at: normal_datetime.clone(),
            code_id: SnowflakeInternal::from_i32(1),
            title: "Title".to_string(),
            published_at: normal_date.clone(),
        });
    }
}
