use thiserror::Error;

#[derive(Debug, Error)]
pub enum DbError {
    #[error(transparent)]
    PoolBuildError(#[from] bb8_tiberius::Error),
    #[error(transparent)]
    PoolAcquireError(#[from] bb8::RunError<bb8_tiberius::Error>),
    #[error(transparent)]
    IoError(#[from] tokio::io::Error),
    #[error(transparent)]
    TiberiusError(#[from] tiberius::error::Error),
    #[error("from row: {0}")]
    FromRowError(#[from] super::from_row::RowError),
    #[error("No row was provided when expected")]
    LostRowError,
    #[error("No entity {entity} with id {id}")]
    NoEntity { entity: &'static str, id: String },
    #[error(transparent)]
    MigrationError(#[from] refinery::Error)
}

pub type Result<T> = std::result::Result<T, DbError>;
