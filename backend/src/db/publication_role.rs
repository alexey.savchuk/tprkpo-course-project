use axum::async_trait;
use chrono::NaiveDateTime;
use mockall::automock;
use serde::Deserialize;
use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    DbError, Entity, Result,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct PublicationRole {
    #[serde(rename = "role_id")]
    pub id: SnowflakeInternal<Self>,
    #[serde(rename = "role_created_at")]
    pub created_at: NaiveDateTime,
    #[serde(rename = "role_name")]
    pub name: String,
}

impl Entity for PublicationRole {
    const ENTITY: &'static str = "PublicationRole";
}

impl PublicationRole {
    pub const AUTHOR: &'static str = "Author";
}

#[automock]
#[async_trait]
pub trait PublicationRoleMethods {
    async fn list_all(&self) -> Result<Vec<PublicationRole>>;
    async fn get_by_id(&self, id: &Snowflake<PublicationRole>) -> Result<Option<PublicationRole>>;
    async fn ensure(&self, id: &Snowflake<PublicationRole>) -> Result<PublicationRole>;
    async fn get_by_name(&self, name: &str) -> Result<Option<PublicationRole>>;
    async fn get_author(&self) -> Result<PublicationRole>;
    async fn fuzzy_search(&self, fuzzy_name: &str, top_n: i64) -> Result<Vec<PublicationRole>>;
    async fn insert_new(&self, name: &str) -> Result<PublicationRole>;
}

#[derive(Debug, Clone)]
pub struct PublicationRoleProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl PublicationRoleMethods for PublicationRoleProviderDb {
    #[instrument(skip(self))]
    async fn list_all(&self) -> Result<Vec<PublicationRole>> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "SELECT R.* FROM Research.PublicationRoles R ORDER BY R.role_id;",
                &[],
            )
            .await?
            .into_first_result()
            .await?;
        row.into_iter()
            .map(|row| Ok(from_row::<PublicationRole>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn get_by_id(&self, id: &Snowflake<PublicationRole>) -> Result<Option<PublicationRole>> {
        let mut conn = self.get_connection().await?;
        let id = id.internal.to_i32();
        let row = conn
            .query(
                "SELECT R.* FROM Research.PublicationRoles R WHERE R.role_id = @p1;",
                &[&id],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<PublicationRole>(row)?))
            .transpose()
    }

    #[instrument(skip(self))]
    async fn ensure(&self, id: &Snowflake<PublicationRole>) -> Result<PublicationRole> {
        self.get_by_id(id).await?.ok_or(DbError::NoEntity {
            entity: PublicationRole::ENTITY,
            id: id.external.to_hex(),
        })
    }

    #[instrument(skip(self))]
    async fn get_by_name(&self, name: &str) -> Result<Option<PublicationRole>> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "SELECT R.* FROM Research.PublicationRoles R WHERE R.role_name = @p1;",
                &[&name],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<PublicationRole>(row)?))
            .transpose()
    }

    #[instrument(skip(self))]
    async fn get_author(&self) -> Result<PublicationRole> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "SELECT R.* FROM Research.PublicationRoles R WHERE R.role_name = @p1;",
                &[&PublicationRole::AUTHOR],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let role = from_row::<PublicationRole>(row)?;
        Ok(role)
    }

    #[instrument(skip(self))]
    async fn fuzzy_search(&self, fuzzy_name: &str, top_n: i64) -> Result<Vec<PublicationRole>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT TOP (@p1) R.*,
    Research.FuzzyScore(@p2, R.role_name) as score
FROM Research.PublicationRoles R
ORDER BY score DESC
;",
                &[&top_n, &fuzzy_name],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<PublicationRole>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn insert_new(&self, name: &str) -> Result<PublicationRole> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "
INSERT INTO Research.PublicationRoles (role_name)
OUTPUT INSERTED.*
VALUES (@p1)
;",
                &[&name],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let role = from_row::<PublicationRole>(row)?;
        Ok(role)
    }
}

impl PublicationRoleProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}
#[cfg(test)]
mod tests {
    use crate::db::from_row;

    use super::*;
    use std::borrow::Cow;
    use tiberius::FromSql;

    use serde::Deserialize;
    use spectral::prelude::*;

    #[test]
    fn deserialize_publication_role() {
        let datetime = tiberius::time::DateTime::new(1, 1);
        let normal_datetime =
            chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::DateTime(Some(datetime)))
                .unwrap()
                .unwrap();

        let row = from_row::Row(vec![
            (
                from_row::ColumnName("role_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("role_created_at".into()),
                from_row::ColumnData(tiberius::ColumnData::DateTime(Some(datetime))),
            ),
            (
                from_row::ColumnName("role_name".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Name")))),
            ),
        ]);

        let data = PublicationRole::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&PublicationRole {
            id: SnowflakeInternal::from_i32(1),
            created_at: normal_datetime.clone(),
            name: "Name".to_string(),
        });
    }
}
