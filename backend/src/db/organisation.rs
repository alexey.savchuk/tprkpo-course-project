use axum::async_trait;
use chrono::NaiveDateTime;
use mockall::automock;
use serde::Deserialize;
use tracing::instrument;

use crate::snowflake::{Snowflake, SnowflakeInternal};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    DbError, Entity, Result,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct Organisation {
    #[serde(rename = "organisation_id")]
    pub id: SnowflakeInternal<Self>,
    #[serde(rename = "organisation_created_at")]
    pub created_at: NaiveDateTime,
    #[serde(rename = "organisation_name")]
    pub name: String,
}

impl Entity for Organisation {
    const ENTITY: &'static str = "Organisation";
}

#[automock]
#[async_trait]
pub trait OrganisationMethods {
    async fn list_all(&self) -> Result<Vec<Organisation>>;
    async fn get_by_id(&self, id: &Snowflake<Organisation>) -> Result<Option<Organisation>>;
    async fn ensure(&self, id: &Snowflake<Organisation>) -> Result<Organisation>;
    async fn get_by_fuzzy_name(&self, fuzzy_name: &str, top_n: i64) -> Result<Vec<Organisation>>;
    async fn insert_new(&self, name: &str) -> Result<Organisation>;
    async fn update_name(
        &self,
        organisation_id: &Snowflake<Organisation>,
        name: &str,
    ) -> Result<Organisation>;
}

#[derive(Debug, Clone)]
pub struct OrganisationProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl OrganisationMethods for OrganisationProviderDb {
    #[instrument(skip(self))]
    async fn list_all(&self) -> Result<Vec<Organisation>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "SELECT O.* FROM Research.Organisations O ORDER BY O.organisation_created_at;",
                &[],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<Organisation>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn get_by_id(&self, id: &Snowflake<Organisation>) -> Result<Option<Organisation>> {
        let mut conn = self.get_connection().await?;
        let id = id.internal.to_i32();
        let row = conn
            .query(
                "SELECT O.* FROM Research.Organisations O WHERE O.organisation_id = @p1;",
                &[&id],
            )
            .await?
            .into_row()
            .await?;
        row.map(|row| Ok(from_row::<Organisation>(row)?))
            .transpose()
    }

    #[instrument(skip(self))]
    async fn ensure(&self, id: &Snowflake<Organisation>) -> Result<Organisation> {
        self.get_by_id(id).await?.ok_or(DbError::NoEntity {
            entity: Organisation::ENTITY,
            id: id.external.to_hex(),
        })
    }

    #[instrument(skip(self))]
    async fn get_by_fuzzy_name(&self, fuzzy_name: &str, top_n: i64) -> Result<Vec<Organisation>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT TOP (@p1) O.*,
    Research.FuzzyScore(@p2, O.organisation_name) as score
FROM Research.Organisations O
ORDER BY score DESC
;",
                &[&top_n, &fuzzy_name],
            )
            .await?
            .into_first_result()
            .await?;
        rows.into_iter()
            .map(|row| Ok(from_row::<Organisation>(row)?))
            .collect::<Result<_>>()
    }

    #[instrument(skip(self))]
    async fn insert_new(&self, name: &str) -> Result<Organisation> {
        let mut conn = self.get_connection().await?;
        let row = conn
            .query(
                "
INSERT INTO Research.Organisations (organisation_name)
OUTPUT INSERTED.*
VALUES (@p1)
;",
                &[&name],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let role = from_row::<Organisation>(row)?;
        Ok(role)
    }

    #[instrument(skip(self))]
    async fn update_name(
        &self,
        organisation_id: &Snowflake<Organisation>,
        name: &str,
    ) -> Result<Organisation> {
        let mut conn = self.get_connection().await?;
        let organisation_id = organisation_id.internal.to_i32();
        let row = conn
            .query(
                "
UPDATE Research.Organisations
SET organisation_name = ?
OUTPUT inserted.*
WHERE organisation_id = ?
;",
                &[&name, &organisation_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let role = from_row::<Organisation>(row)?;
        Ok(role)
    }
}

impl OrganisationProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}
#[cfg(test)]
mod tests {
    use std::borrow::Cow;

    use crate::{db::from_row, snowflake::SnowflakeInternal};
    use serde::Deserialize;
    use spectral::assert_that;
    use tiberius::FromSql;

    use super::Organisation;

    #[test]
    fn deserialize_organisation() {
        let datetime = tiberius::time::DateTime::new(1, 1);
        let normal_datetime =
            chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::DateTime(Some(datetime)))
                .unwrap()
                .unwrap();

        let row = from_row::Row(vec![
            (
                from_row::ColumnName("organisation_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(1))),
            ),
            (
                from_row::ColumnName("organisation_created_at".into()),
                from_row::ColumnData(tiberius::ColumnData::DateTime(Some(datetime))),
            ),
            (
                from_row::ColumnName("organisation_name".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Name")))),
            ),
        ]);

        let data = Organisation::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&Organisation {
            id: SnowflakeInternal::from_i32(1),
            created_at: normal_datetime.clone(),
            name: "Name".to_string(),
        });
    }
}
