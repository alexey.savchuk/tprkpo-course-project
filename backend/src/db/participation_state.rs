use core::fmt;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum ParticipationState {
    Pending,
    Active,
    Declined,
}

impl fmt::Display for ParticipationState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.serialize(f)
    }
}

impl ParticipationState {
    pub fn variant_name(&self) -> &'static str {
        serde_variant::to_variant_name(self).unwrap()
    }
}

impl tiberius::ToSql for ParticipationState {
    fn to_sql(&self) -> tiberius::ColumnData<'_> {
        use tiberius::IntoSql;
        self.variant_name().into_sql()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use spectral::prelude::*;

    #[test]
    fn deserialize_variant_name() {
        assert_that!(ParticipationState::Pending.variant_name()).is_equal_to("pending");
        assert_that!(ParticipationState::Active.variant_name()).is_equal_to("active");
        assert_that!(ParticipationState::Declined.variant_name()).is_equal_to("declined");
    }
}
