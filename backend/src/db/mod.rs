mod academic_participants;
mod academic_users;
mod application_participation;
mod application_role;
mod config;
mod database;
mod error;
pub mod from_row;
mod global_search;
mod migrations;
mod organisation;
mod organisation_participation;
mod organisation_role;
mod participation_state;
mod pool;
mod publication;
mod publication_participation;
mod publication_reference;
mod publication_role;
mod subject_code;

pub use config::DbConfig;
pub use database::{DatabaseMethods, DatabaseMethodsDyn, DatabaseProvider, MockDatabase};
pub use error::{DbError, Result};
pub use pool::{Client, DbPool};
pub use migrations::apply_migrations;

pub use academic_participants::AcademicParticipant;
pub use academic_users::AcademicUsers;
pub use application_participation::ApplicationParticipation;
pub use application_role::ApplicationRole;
pub use global_search::GlobalSearch;
pub use organisation::Organisation;
pub use organisation_participation::OrganisationParticipation;
pub use organisation_role::OrganisationRole;
pub use participation_state::ParticipationState;
pub use publication::Publication;
pub use publication_participation::{PublicationParticipation, Score};
pub use publication_reference::{Count, PublicationReference};
pub use publication_role::PublicationRole;
pub use subject_code::{Publications, SubjectCode};

pub trait Entity {
    const ENTITY: &'static str;
}

pub mod extract {
    pub use super::database::extract::*;
    pub use super::pool::extract::*;
}

pub mod methods {
    pub use super::academic_participants::AcademicParticipantMethods;
    pub use super::academic_users::AcademicUsersMethods;
    pub use super::application_participation::ApplicationParticipationMethods;
    pub use super::application_role::ApplicationRoleMethods;
    pub use super::global_search::GlobalSearchMethods;
    pub use super::organisation::OrganisationMethods;
    pub use super::organisation_participation::OrganisationParticipationMethods;
    pub use super::organisation_role::OrganisationRoleMethods;
    pub use super::publication::PublicationMethods;
    pub use super::publication_participation::PublicationParticipationMethods;
    pub use super::publication_reference::PublicationReferenceMethods;
    pub use super::publication_role::PublicationRoleMethods;
    pub use super::subject_code::SubjectCodeMethods;
}
