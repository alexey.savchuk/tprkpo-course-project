use std::ops::DerefMut;

use refinery::embed_migrations;

use super::{DbPool, Result};

embed_migrations!("../migrations");

pub async fn apply_migrations(db_pool: &DbPool) -> Result<refinery::Report> {
    let mut conn = db_pool.get().await?;
    let report = migrations::runner().run_async(conn.deref_mut()).await?;
    Ok(report)
}
