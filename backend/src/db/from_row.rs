use std::fmt::Display;

use itertools::{izip, Itertools};
use serde::{de, Deserialize};
use thiserror::Error;
use tiberius::FromSql;
use tracing::{debug, instrument};

#[derive(Debug, Error)]
pub enum RowError {
    #[error("Error accessing column: {0}")]
    TiberiusError(#[from] tiberius::error::Error),
    #[error("{0}")]
    SerdeCustomError(String),
}

impl de::Error for RowError {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        RowError::SerdeCustomError(msg.to_string())
    }
}

#[derive(Debug)]
pub struct ColumnData(pub tiberius::ColumnData<'static>);

impl<'de, 'a> de::Deserializer<'de> for &'a ColumnData
where
    'a: 'de,
{
    type Error = RowError;

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        match &self.0 {
            tiberius::ColumnData::U8(v) => match v {
                Some(v) => visitor.visit_u8(*v),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::I16(v) => match v {
                Some(v) => visitor.visit_i16(*v),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::I32(v) => match v {
                Some(v) => visitor.visit_i32(*v),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::I64(v) => match v {
                Some(v) => visitor.visit_i64(*v),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::F32(v) => match v {
                Some(v) => visitor.visit_f32(*v),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::F64(v) => match v {
                Some(v) => visitor.visit_f64(*v),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Bit(v) => match v {
                Some(v) => visitor.visit_bool(*v),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::String(v) => match v {
                Some(v) => visitor.visit_str(v.as_ref()),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Guid(_) => todo!(),
            tiberius::ColumnData::Binary(v) => match v {
                Some(v) => visitor.visit_borrowed_bytes(v.as_ref()),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Numeric(_) => todo!(),
            tiberius::ColumnData::Xml(_) => todo!(),
            tiberius::ColumnData::DateTime(v) => {
                let maybe_datetime =
                    chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::DateTime(*v))?;
                match maybe_datetime {
                    Some(datetime) => visitor.visit_string(format!("{:?}", datetime)),
                    None => visitor.visit_none(),
                }
            }
            tiberius::ColumnData::SmallDateTime(v) => {
                let maybe_datetime =
                    chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::SmallDateTime(*v))?;
                match maybe_datetime {
                    Some(datetime) => visitor.visit_string(format!("{:?}", datetime)),
                    None => visitor.visit_none(),
                }
            }
            tiberius::ColumnData::DateTime2(v) => {
                let maybe_datetime =
                    chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::DateTime2(*v))?;
                match maybe_datetime {
                    Some(datetime) => visitor.visit_string(format!("{:?}", datetime)),
                    None => visitor.visit_none(),
                }
            }
            tiberius::ColumnData::DateTimeOffset(v) => {
                let maybe_datetime =
                    chrono::NaiveDateTime::from_sql(&tiberius::ColumnData::DateTimeOffset(*v))?;
                match maybe_datetime {
                    Some(datetime) => visitor.visit_string(format!("{:?}", datetime)),
                    None => visitor.visit_none(),
                }
            }
            tiberius::ColumnData::Time(v) => {
                let maybe_time = chrono::NaiveTime::from_sql(&tiberius::ColumnData::Time(*v))?;
                match maybe_time {
                    Some(time) => visitor.visit_string(format!("{:?}", time)),
                    None => visitor.visit_none(),
                }
            }
            tiberius::ColumnData::Date(v) => {
                let maybe_date = chrono::NaiveDate::from_sql(&tiberius::ColumnData::Date(*v))?;
                match maybe_date {
                    Some(date) => visitor.visit_string(format!("{:?}", date)),
                    None => visitor.visit_none(),
                }
            }
        }
    }

    fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        match &self.0 {
            tiberius::ColumnData::U8(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::I16(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::I32(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::I64(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::F32(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::F64(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Bit(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::String(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Guid(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Binary(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Numeric(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Xml(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::DateTime(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::SmallDateTime(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::DateTime2(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::DateTimeOffset(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Time(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
            tiberius::ColumnData::Date(v) => match v {
                Some(_) => visitor.visit_some(self),
                None => visitor.visit_none(),
            },
        }
    }

    fn deserialize_enum<V>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_enum(self)
    }

    fn deserialize_newtype_struct<V>(
        self,
        _name: &'static str,
        visitor: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_newtype_struct(self)
    }

    serde::forward_to_deserialize_any! {
        bool u8 u16 u32 u64 i8 i16 i32 i64 f32 f64 char str string unit seq
            bytes byte_buf map unit_struct tuple_struct struct
            tuple ignored_any identifier
    }
}

impl<'de, 'a> de::VariantAccess<'de> for &'a ColumnData
where
    'a: 'de,
{
    type Error = RowError;

    fn unit_variant(self) -> Result<(), Self::Error> {
        Ok(())
    }

    fn newtype_variant_seed<T>(self, _seed: T) -> Result<T::Value, Self::Error>
    where
        T: de::DeserializeSeed<'de>,
    {
        todo!()
    }

    fn tuple_variant<V>(self, _len: usize, _visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        todo!()
    }

    fn struct_variant<V>(
        self,
        _fields: &'static [&'static str],
        _visitor: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        todo!()
    }
}

impl<'de, 'a> de::EnumAccess<'de> for &'a ColumnData
where
    'a: 'de,
{
    type Error = RowError;
    type Variant = &'a ColumnData;

    fn variant_seed<V>(self, seed: V) -> Result<(V::Value, Self::Variant), Self::Error>
    where
        V: de::DeserializeSeed<'de>,
    {
        let val = seed.deserialize(self)?;
        Ok((val, self))
    }
}

#[derive(Debug)]
pub struct ColumnName(pub String);

impl<'de, 'a> de::Deserializer<'de> for &'a ColumnName {
    type Error = RowError;

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_str(self.0.as_ref())
    }

    serde::forward_to_deserialize_any! {
        bool u8 u16 u32 u64 i8 i16 i32 i64 f32 f64 char str string unit seq
            bytes byte_buf map unit_struct tuple_struct struct newtype_struct
            tuple ignored_any identifier option enum
    }
}

#[derive(Debug)]
pub struct Row(pub Vec<(ColumnName, ColumnData)>);

impl Row {
    pub fn from_row(row: tiberius::Row) -> Self {
        let columns = row
            .columns()
            .iter()
            .map(|col| ColumnName(col.name().to_string()))
            .collect_vec();
        let data = row.into_iter().map(|data| ColumnData(data));
        let map = izip!(columns.into_iter(), data).collect();
        Row(map)
    }
}

impl<'de, 'a> de::Deserializer<'de> for &'a Row
where
    'a: 'de,
{
    type Error = RowError;

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_map(RowIter { row: &self, ix: 0 })
    }

    fn deserialize_tuple<V>(self, elems: usize, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_seq(SameRow { row: &self, elems })
    }

    serde::forward_to_deserialize_any! {
        bool u8 u16 u32 u64 i8 i16 i32 i64 f32 f64 char str string unit seq
            bytes byte_buf map unit_struct tuple_struct struct newtype_struct
            ignored_any identifier option enum
    }
}

struct SameRow<'a> {
    row: &'a Row,
    elems: usize,
}

impl<'de, 'a> de::SeqAccess<'de> for SameRow<'a>
where
    'a: 'de,
{
    type Error = RowError;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>, Self::Error>
    where
        T: de::DeserializeSeed<'de>,
    {
        if self.elems == 0 {
            Ok(None)
        } else {
            self.elems -= 1;
            seed.deserialize(self.row).map(Some)
        }
    }
}

struct RowIter<'a> {
    row: &'a Row,
    ix: usize,
}

impl<'de, 'a> de::MapAccess<'de> for RowIter<'a>
where
    'a: 'de,
{
    type Error = RowError;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>, Self::Error>
    where
        K: de::DeserializeSeed<'de>,
    {
        self.row
            .0
            .get(self.ix)
            .map(|(col, _)| seed.deserialize(col))
            .transpose()
    }

    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value, Self::Error>
    where
        V: de::DeserializeSeed<'de>,
    {
        let res = self
            .row
            .0
            .get(self.ix)
            .map(|(_, data)| seed.deserialize(data))
            .transpose()?
            .expect("value after key");
        self.ix += 1;
        Ok(res)
    }
}

#[instrument(skip(row))]
pub fn from_row<D>(row: tiberius::Row) -> Result<D, RowError>
where
    D: for<'de> de::Deserialize<'de>,
{
    let row = Row::from_row(row);
    let debug_json = serde_json::Value::deserialize(&row);
    debug!(row = ?row, deserialized_as = ?debug_json, "deserialize row");
    D::deserialize(&row)
}

#[cfg(test)]
mod tests {
    use std::borrow::Cow;

    use serde::Deserialize;
    use spectral::prelude::*;

    use super::*;

    #[test]
    fn deserialize_struct() {
        #[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
        struct Test {
            name: String,
            age: i32,
        }

        let row = Row(vec![
            (
                ColumnName("name".into()),
                ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "some name",
                )))),
            ),
            (
                ColumnName("age".into()),
                ColumnData(tiberius::ColumnData::I32(Some(30))),
            ),
        ]);

        let data = Test::deserialize(&row);
        let expected_test = Test {
            name: "some name".into(),
            age: 30,
        };

        assert_that!(&data).is_ok_containing(&expected_test);
    }

    #[test]
    fn deserialize_struct_tuple() {
        #[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
        struct Test {
            name: String,
            age: i32,
        }
        #[derive(Debug, Clone, PartialEq, Deserialize)]
        struct Score {
            score: f64,
        }

        let row = Row(vec![
            (
                ColumnName("name".into()),
                ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed(
                    "some name",
                )))),
            ),
            (
                ColumnName("age".into()),
                ColumnData(tiberius::ColumnData::I32(Some(30))),
            ),
            (
                ColumnName("score".into()),
                ColumnData(tiberius::ColumnData::F64(Some(10.0))),
            ),
        ]);

        let data = <(Test, Score)>::deserialize(&row);
        let expected_test = Test {
            name: "some name".into(),
            age: 30,
        };
        let expected_score = Score { score: 10.0 };

        assert_that!(&data)
            .is_ok()
            .map(|(test, _)| test)
            .is_equal_to(&expected_test);
        assert_that!(&data)
            .is_ok()
            .map(|(_, score)| score)
            .is_equal_to(&expected_score);
    }

    #[test]
    fn deserialize_date() {
        #[derive(Debug, Clone, PartialEq, Deserialize)]
        struct Date {
            date: chrono::NaiveDate,
        }

        let db_date = tiberius::time::Date::new(1010);
        let chrono_date = chrono::NaiveDate::from_ymd_opt(1, 1, 1).unwrap()
            + chrono::Duration::days(db_date.days().into());
        let row = Row(vec![(
            ColumnName("date".into()),
            ColumnData(tiberius::ColumnData::Date(Some(db_date))),
        )]);

        let data = Date::deserialize(&row);
        let expected_date = Date { date: chrono_date };

        assert_that!(&data).is_ok_containing(&expected_date);
    }

    #[test]
    fn deserialize_str_enum() {
        #[derive(Debug, Deserialize, PartialEq, Eq)]
        enum TestData {
            Thing,
            Other,
        }
        #[derive(Debug, Deserialize, PartialEq, Eq)]
        struct TestStruct {
            pub test: TestData,
        }

        let row = Row(vec![(
            ColumnName("test".into()),
            ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Thing")))),
        )]);

        let data = TestStruct::deserialize(&row);
        assert_that!(&data).is_ok_containing(&TestStruct {
            test: TestData::Thing,
        });

        let row = Row(vec![(
            ColumnName("test".into()),
            ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Other")))),
        )]);

        let data = TestStruct::deserialize(&row);
        assert_that!(&data).is_ok_containing(&TestStruct {
            test: TestData::Other,
        });

        let row = Row(vec![(
            ColumnName("test".into()),
            ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("None")))),
        )]);

        let data = TestStruct::deserialize(&row);
        assert_that!(&data).is_err();
    }

    #[test]
    fn deserialize_newtype_struct_value() {
        #[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
        struct TestValue(i32);
        #[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
        struct Test {
            value: TestValue,
        }

        let row = Row(vec![(
            ColumnName("value".into()),
            ColumnData(tiberius::ColumnData::I32(Some(30))),
        )]);

        let data = Test::deserialize(&row);
        let expected_test = Test {
            value: TestValue(30),
        };

        assert_that!(&data).is_ok_containing(&expected_test);
    }

    #[test]
    fn deserialize_optional_field() {
        #[derive(Debug, Deserialize, PartialEq, Eq)]
        struct TestStruct {
            pub test: Option<String>,
        }

        let row = Row(vec![(
            ColumnName("test".into()),
            ColumnData(tiberius::ColumnData::String(None)),
        )]);

        let data = TestStruct::deserialize(&row);
        assert_that!(&data).is_ok_containing(&TestStruct { test: None });

        let row = Row(vec![(
            ColumnName("test".into()),
            ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("Thing")))),
        )]);

        let data = TestStruct::deserialize(&row);
        assert_that!(&data).is_ok_containing(&TestStruct {
            test: Some("Thing".to_string()),
        });
    }

    #[test]
    fn deserialize_datetime() {
        let datetime = tiberius::time::DateTime::new(1, 1);
        let normal_datetime = chrono::NaiveDateTime::from_sql(
            &tiberius::ColumnData::DateTime(Some(datetime))
        ).unwrap().unwrap();

        #[derive(Debug, Deserialize, PartialEq, Eq)]
        struct TestStruct {
            pub datetime: chrono::NaiveDateTime,
        }

        let row = Row(vec![
            (
                ColumnName("datetime".into()),
                ColumnData(tiberius::ColumnData::DateTime(Some(datetime))),
            )
        ]);

        let data = TestStruct::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&TestStruct {
            datetime: normal_datetime
        });
    }

    #[test]
    fn deserialize_datetime2() {
        let datetime = tiberius::time::DateTime2::new(
            tiberius::time::Date::new(1),
            tiberius::time::Time::new(2, 3)
        );

        let normal_datetime = chrono::NaiveDateTime::from_sql(
            &tiberius::ColumnData::DateTime2(Some(datetime))
        ).unwrap().unwrap();

        #[derive(Debug, Deserialize, PartialEq, Eq)]
        struct TestStruct {
            pub datetime: chrono::NaiveDateTime,
        }

        let row = Row(vec![
            (
                ColumnName("datetime".into()),
                ColumnData(tiberius::ColumnData::DateTime2(Some(datetime))),
            )
        ]);

        let data = TestStruct::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&TestStruct {
            datetime: normal_datetime
        });
    }

    #[test]
    fn deserialize_time() {
        let time = tiberius::time::Time::new(1, 2);
        let normal_time = chrono::NaiveTime::from_sql(
            &tiberius::ColumnData::Time(Some(time))
        ).unwrap().unwrap();

        #[derive(Debug, Deserialize, PartialEq, Eq)]
        struct TestStruct {
            pub time: chrono::NaiveTime,
        }

        let row = Row(vec![
            (
                ColumnName("time".into()),
                ColumnData(tiberius::ColumnData::Time(Some(time))),
            )
        ]);

        let data = TestStruct::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&TestStruct {
            time: normal_time
        });
    }

    #[test]
    fn deserialize_small_datetime() {
        let datetime = tiberius::time::SmallDateTime::new(1, 1);
        let normal_datetime = chrono::NaiveDateTime::from_sql(
            &tiberius::ColumnData::SmallDateTime(Some(datetime))
        ).unwrap().unwrap();

        #[derive(Debug, Deserialize, PartialEq, Eq)]
        struct TestStruct {
            pub datetime: chrono::NaiveDateTime,
        }

        let row = Row(vec![
            (
                ColumnName("datetime".into()),
                ColumnData(tiberius::ColumnData::SmallDateTime(Some(datetime))),
            )
        ]);

        let data = TestStruct::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&TestStruct {
            datetime: normal_datetime
        });
    }

    // #[test]
    // fn deserialize_datetime_with_offset() {
    //     let datetime = tiberius::time::DateTimeOffset::new(
    //         tiberius::time::DateTime2::new(
    //             tiberius::time::Date::new(1),
    //             tiberius::time::Time::new(2, 3)
    //         ),
    //         14
    //     );
    //     let normal_datetime = chrono::DateTime::<chrono::FixedOffset>::from_sql(
    //         &tiberius::ColumnData::DateTimeOffset(Some(datetime))
    //     ).unwrap().unwrap();

    //     dbg!(datetime, normal_datetime);

    //     #[derive(Debug, Deserialize, PartialEq, Eq)]
    //     struct TestStruct {
    //         pub datetime: chrono::DateTime<chrono::FixedOffset>,
    //     }

    //     let row = Row(vec![
    //         (
    //             ColumnName("datetime".into()),
    //             ColumnData(tiberius::ColumnData::DateTimeOffset(Some(datetime))),
    //         )
    //     ]);

    //     let data = TestStruct::deserialize(&row).unwrap();
    //     assert_that!(data).is_equal_to(&TestStruct {
    //         datetime: normal_datetime
    //     });
    // }
}
