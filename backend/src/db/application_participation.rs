use axum::async_trait;
use mockall::automock;
use serde::Deserialize;
use std::collections::HashSet;

use tracing::instrument;

use crate::{
    auth::OryIdentity,
    snowflake::{Snowflake, SnowflakeInternal},
};

use super::{
    from_row::from_row,
    pool::{DbPool, PoolConnection},
    ApplicationRole, DbError, Entity, Result,
};

#[derive(Debug, Clone, Hash, PartialEq, Eq, Deserialize)]
pub struct ApplicationParticipation {
    pub user_id: String,
    pub role_id: SnowflakeInternal<ApplicationRole>,
}

impl Entity for ApplicationParticipation {
    const ENTITY: &'static str = "ApplicationParticipation";
}

#[automock]
#[async_trait]
pub trait ApplicationParticipationMethods {
    async fn get_application_roles_for_user(
        &self,
        user_id: &OryIdentity,
    ) -> Result<HashSet<ApplicationRole>>;
    async fn get_application_admins(&self) -> Result<Vec<ApplicationParticipation>>;
    async fn insert_new_participation(
        &self,
        user_id: &OryIdentity,
        role_id: &Snowflake<ApplicationRole>,
    ) -> Result<ApplicationParticipation>;
    async fn stop_participation(
        &self,
        user_id: &OryIdentity,
        role_id: &Snowflake<ApplicationRole>,
    ) -> Result<ApplicationParticipation>;
}

#[derive(Debug, Clone)]
pub struct ApplicationParticipationProviderDb {
    pub db_pool: DbPool,
}

#[async_trait]
impl ApplicationParticipationMethods for ApplicationParticipationProviderDb {
    #[instrument(skip(self))]
    async fn get_application_roles_for_user(
        &self,
        user_id: &OryIdentity,
    ) -> Result<HashSet<ApplicationRole>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT AR.*
FROM Research.ApplicationParticipations P
JOIN Research.ApplicationRoles AR on AR.role_id = P.role_id
WHERE user_id = @p1
;",
                &[&user_id.id],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<ApplicationRole>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn get_application_admins(&self) -> Result<Vec<ApplicationParticipation>> {
        let mut conn = self.get_connection().await?;
        let rows = conn
            .query(
                "
SELECT AP.*
FROM Research.ApplicationParticipations AP
JOIN Research.ApplicationRoles AR on AR.role_id = AP.role_id
WHERE AR.role_name = @p1
;",
                &[&ApplicationRole::ADMINISTATOR],
            )
            .await?
            .into_first_result()
            .await?;
        let res = rows
            .into_iter()
            .map(|row| Ok(from_row::<ApplicationParticipation>(row)?))
            .collect::<Result<_>>()?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn insert_new_participation(
        &self,
        user_id: &OryIdentity,
        role_id: &Snowflake<ApplicationRole>,
    ) -> Result<ApplicationParticipation> {
        let mut conn = self.get_connection().await?;
        let role_id = role_id.internal.to_i32();
        let row = conn
            .query(
                "
INSERT INTO Research.ApplicationParticipations(user_id, role_id)
OUTPUT inserted.*
VALUES (@p1, @p2)
;",
                &[&user_id.id, &role_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<ApplicationParticipation>(row)?;
        Ok(res)
    }

    #[instrument(skip(self))]
    async fn stop_participation(
        &self,
        user_id: &OryIdentity,
        role_id: &Snowflake<ApplicationRole>,
    ) -> Result<ApplicationParticipation> {
        let mut conn = self.get_connection().await?;
        let role_id = role_id.internal.to_i32();
        let row = conn
            .query(
                "
DELETE FROM Research.ApplicationParticipations
OUTPUT deleted.*
WHERE user_id = @p1
  AND role_id = @p2
;",
                &[&user_id.id, &role_id],
            )
            .await?
            .into_row()
            .await?
            .ok_or(DbError::LostRowError)?;
        let res = from_row::<ApplicationParticipation>(row)?;
        Ok(res)
    }
}

impl ApplicationParticipationProviderDb {
    pub fn from_db_pool(db_pool: &DbPool) -> Self {
        Self {
            db_pool: db_pool.clone(),
        }
    }

    async fn get_connection<'a>(&'a self) -> Result<PoolConnection<'a>> {
        self.db_pool.get().await.map_err(DbError::from)
    }
}
#[cfg(test)]
mod tests {
    use std::borrow::Cow;

    use crate::{db::from_row, snowflake::SnowflakeInternal};
    use serde::Deserialize;
    use spectral::assert_that;

    use super::ApplicationParticipation;

    #[test]
    fn deserialize_application_participation() {
        let row = from_row::Row(vec![
            (
                from_row::ColumnName("user_id".into()),
                from_row::ColumnData(tiberius::ColumnData::String(Some(Cow::Borrowed("id")))),
            ),
            (
                from_row::ColumnName("role_id".into()),
                from_row::ColumnData(tiberius::ColumnData::I32(Some(2))),
            ),
        ]);

        let data = ApplicationParticipation::deserialize(&row).unwrap();
        assert_that!(data).is_equal_to(&ApplicationParticipation {
            user_id: "id".to_string(),
            role_id: SnowflakeInternal::from_i32(2),
        });
    }
}
