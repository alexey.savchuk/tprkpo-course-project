use ascon_aead::{aead::Aead, Ascon128, Key as CipherKey, Nonce};
use ascon_hash::{digest::FixedOutput, AsconHash, Digest};
use axum::http;
use hex::{FromHex, ToHex};
use itertools::izip;
use moka::sync::Cache;
use serde::{Deserialize, Serialize};
// use serde_with::serde_as;
use std::{
    any::{type_name, TypeId},
    borrow::Cow,
    fmt::Debug,
    hash::Hash,
    marker::PhantomData,
    sync::Arc,
};
use thiserror::Error;
use tracing::{debug, instrument};

#[derive(Debug, Error)]
pub enum SnowflakeError {
    #[error(transparent)]
    HexError(#[from] hex::FromHexError),
    #[error(transparent)]
    SliceError(#[from] std::array::TryFromSliceError),
    #[error(transparent)]
    AsconError(#[from] ascon_aead::Error),
}

impl SnowflakeError {
    pub fn status_code(&self) -> http::StatusCode {
        match self {
            SnowflakeError::HexError(_) => http::StatusCode::BAD_REQUEST,
            SnowflakeError::SliceError(_) => http::StatusCode::INTERNAL_SERVER_ERROR,
            SnowflakeError::AsconError(_) => http::StatusCode::NOT_FOUND,
        }
    }
}

pub type Result<T> = std::result::Result<T, SnowflakeError>;

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct SnowflakeConfig {
    pub secret: String,
    #[serde(rename = "cache")]
    pub type_nonce_cache_size: u64,
}

impl SnowflakeConfig {
    fn cipher_key(&self) -> &CipherKey<Ascon128> {
        CipherKey::<Ascon128>::from_slice(self.secret.as_bytes())
    }
}

#[derive(Deserialize)]
#[serde(from = "SnowflakeInternalRepr")]
pub struct SnowflakeInternal<T> {
    id: i32,
    // #[serde(skip)]
    _d: PhantomData<T>,
}

#[derive(Debug, Deserialize)]
struct SnowflakeInternalRepr(i32);

impl<T> From<SnowflakeInternalRepr> for SnowflakeInternal<T> {
    fn from(SnowflakeInternalRepr(internal_id): SnowflakeInternalRepr) -> Self {
        SnowflakeInternal::from_i32(internal_id)
    }
}

// #[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(try_from = "SnowflakeExternalRepr", into = "SnowflakeExternalRepr")]
pub struct SnowflakeExternal<T> {
    id: [u8; 20],
    _d: PhantomData<T>,
}

#[derive(Debug, Deserialize, Serialize)]
struct SnowflakeExternalRepr<'a>(Cow<'a, str>);

impl<'a, T> TryFrom<SnowflakeExternalRepr<'a>> for SnowflakeExternal<T> {
    type Error = SnowflakeError;

    fn try_from(
        SnowflakeExternalRepr(hex_str): SnowflakeExternalRepr,
    ) -> std::result::Result<Self, Self::Error> {
        SnowflakeExternal::from_hex(hex_str.as_ref())
    }
}

impl<'a, T> Into<SnowflakeExternalRepr<'a>> for SnowflakeExternal<T> {
    fn into(self) -> SnowflakeExternalRepr<'a> {
        SnowflakeExternalRepr(self.id.encode_hex())
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Snowflake<T> {
    pub internal: SnowflakeInternal<T>,
    pub external: SnowflakeExternal<T>,
}

pub trait SnowflakeConverter {
    fn from_external<T>(&self, external: SnowflakeExternal<T>) -> Result<Snowflake<T>>
    where
        T: 'static;
    fn from_internal<T>(&self, internal: SnowflakeInternal<T>) -> Result<Snowflake<T>>
    where
        T: 'static;
}

#[derive(Clone)]
pub struct SnowflakeManager {
    nonce_cache: Cache<TypeId, Nonce<Ascon128>>,
    cipher: Arc<Ascon128>,
}

impl Debug for SnowflakeManager {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SnowflakeManager").finish()
    }
}

impl From<&SnowflakeConfig> for SnowflakeManager {
    fn from(config: &SnowflakeConfig) -> Self {
        use ascon_aead::aead::KeyInit;
        let cipher = Arc::new(Ascon128::new(config.cipher_key()));
        let nonce_cache = Cache::new(config.type_nonce_cache_size);
        SnowflakeManager {
            cipher,
            nonce_cache,
        }
    }
}

impl SnowflakeConverter for SnowflakeManager {
    #[instrument(skip(self), ret, err)]
    fn from_external<T>(&self, external: SnowflakeExternal<T>) -> Result<Snowflake<T>>
    where
        T: 'static,
    {
        use generic_array::sequence::Split;
        let cipher = &self.cipher;
        let nonce = self.nonce_cache.get_with(TypeId::of::<T>(), || {
            let mut hasher = AsconHash::new();
            let plain_nonce = type_name::<T>().as_bytes();
            hasher.update(plain_nonce);
            let big_nonce = hasher.finalize_fixed();
            let (mut fh, sh) = Split::<u8, typenum::U16>::split(big_nonce);
            assert_eq!(fh.len(), sh.len());
            izip!(fh.as_mut_slice().iter_mut(), sh.as_slice().iter())
                .for_each(|(fh, sh)| *fh ^= sh);
            fh
        });
        let ciphertext = external.id;
        let plaintext = cipher.decrypt(&nonce, ciphertext.as_ref())?;
        let internal = SnowflakeInternal {
            id: i32::from_be_bytes(plaintext.as_slice().try_into()?),
            _d: PhantomData,
        };
        Ok(Snowflake { internal, external })
    }

    #[instrument(skip(self), ret, err)]
    fn from_internal<T>(&self, internal: SnowflakeInternal<T>) -> Result<Snowflake<T>>
    where
        T: 'static,
    {
        use generic_array::sequence::Split;
        let cipher = &self.cipher;
        let nonce = self.nonce_cache.get_with(TypeId::of::<T>(), || {
            let mut hasher = AsconHash::new();
            let plain_nonce = type_name::<T>().as_bytes();
            hasher.update(plain_nonce);
            let big_nonce = hasher.finalize_fixed();
            debug!(?plain_nonce, ?big_nonce, "Type nonce");
            let (mut fh, sh) = Split::<u8, typenum::U16>::split(big_nonce);
            assert_eq!(fh.len(), sh.len());
            izip!(fh.as_mut_slice().iter_mut(), sh.as_slice().iter())
                .for_each(|(fh, sh)| *fh ^= sh);
            debug!(?fh, "Combined nonce");
            fh
        });
        let plaintext = internal.id.to_be_bytes();
        let ciphertext = cipher.encrypt(&nonce, plaintext.as_ref())?;
        debug!(?plaintext, ?ciphertext, ?nonce, "encryption");
        let external = SnowflakeExternal {
            id: ciphertext.as_slice().try_into()?,
            _d: PhantomData,
        };
        debug!(?internal, ?external, "conversion");
        Ok(Snowflake { internal, external })
    }
}

pub mod extract {
    use axum::{
        async_trait,
        extract::{FromRef, FromRequestParts},
        http::request::Parts,
    };
    #[derive(Debug)]
    pub struct SnowflakeState(pub super::SnowflakeManager);

    #[async_trait]
    impl<S> FromRequestParts<S> for SnowflakeState
    where
        super::SnowflakeManager: FromRef<S>,
        S: Send + Sync,
    {
        type Rejection = ();

        async fn from_request_parts(
            _parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let manager = super::SnowflakeManager::from_ref(state);

            Ok(SnowflakeState(manager))
        }
    }
}

impl<T> SnowflakeInternal<T> {
    pub fn from_i32(internal_id: i32) -> Self {
        Self {
            id: internal_id,
            _d: Default::default(),
        }
    }

    pub fn to_i32(self) -> i32 {
        self.id
    }

    pub fn as_i32(&self) -> &i32 {
        &self.id
    }
}

impl<T, I: AsRef<i32>> From<I> for SnowflakeInternal<T> {
    fn from(i: I) -> Self {
        Self::from_i32(*i.as_ref())
    }
}

impl<T> SnowflakeExternal<T> {
    pub fn from_hex<S: AsRef<[u8]>>(hex: S) -> Result<Self> {
        let external_id = <[u8; 20]>::from_hex(hex)?;
        Ok(Self {
            id: external_id,
            _d: Default::default(),
        })
    }

    pub fn to_hex(self) -> String {
        self.id.encode_hex()
    }
}

impl<T> Clone for SnowflakeInternal<T> {
    fn clone(&self) -> Self {
        Self {
            id: self.id.clone(),
            _d: self._d.clone(),
        }
    }
}

impl<T> Clone for SnowflakeExternal<T> {
    fn clone(&self) -> Self {
        Self {
            id: self.id.clone(),
            _d: self._d.clone(),
        }
    }
}

impl<T> Copy for SnowflakeInternal<T> {}
impl<T> Copy for SnowflakeExternal<T> {}

impl<T> Debug for SnowflakeInternal<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SnowflakeInternal")
            .field("type", &type_name::<T>())
            .field("id", &self.id)
            .finish()
    }
}

impl<T> Debug for SnowflakeExternal<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SnowflakeExternal")
            .field("type", &type_name::<T>())
            .field("id", &self.id.encode_hex::<String>())
            .finish()
    }
}

impl<T> Debug for Snowflake<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Snowflake")
            .field("type", &type_name::<T>())
            .field("internal_id", &self.internal.id)
            .field("external_id", &self.external.id.encode_hex::<String>())
            .finish()
    }
}

impl<T> PartialEq for SnowflakeInternal<T> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<T> PartialEq for SnowflakeExternal<T> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<T> Eq for SnowflakeInternal<T> {}
impl<T> Eq for SnowflakeExternal<T> {}

impl<T> Hash for SnowflakeInternal<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}
impl<T> Hash for SnowflakeExternal<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

#[cfg(test)]
pub mod test {
    use mockall::predicate;

    use super::{
        Snowflake, SnowflakeConfig, SnowflakeExternal, SnowflakeInternal, SnowflakeManager,
    };

    pub fn snowflake_manager() -> SnowflakeManager {
        SnowflakeManager::from(&SnowflakeConfig {
            secret: "some supersecret".to_string(),
            type_nonce_cache_size: 100,
        })
    }

    pub fn eq_internal<T>(expect: SnowflakeInternal<T>) -> impl mockall::Predicate<Snowflake<T>> {
        predicate::function(move |sf: &Snowflake<T>| sf.internal == expect)
    }

    pub fn eq_external<T>(expect: SnowflakeExternal<T>) -> impl mockall::Predicate<Snowflake<T>> {
        predicate::function(move |sf: &Snowflake<T>| sf.external == expect)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;
    use spectral::prelude::*;
    use tracing_test::traced_test;

    use crate::db::from_row;

    fn snowflake_internal() -> impl Strategy<Value = SnowflakeInternal<Test>> {
        (0..1000i32).prop_map(|internal_id| SnowflakeInternal::from_i32(internal_id))
    }

    fn snowflake_external() -> impl Strategy<Value = SnowflakeExternal<Test>> {
        any::<[u8; 20]>().prop_map(|bytes| SnowflakeExternal {
            id: bytes,
            _d: PhantomData,
        })
    }

    fn config() -> impl Strategy<Value = SnowflakeConfig> {
        Just(SnowflakeConfig {
            secret: "test_key_padding".to_string(),
            type_nonce_cache_size: 0,
        })
    }

    fn state(
        config: impl Strategy<Value = SnowflakeConfig>,
    ) -> impl Strategy<Value = SnowflakeManager> {
        config.prop_map(|config| SnowflakeManager::from(&config))
    }

    struct Test;

    #[test]
    fn internal_id_inner_eq() {
        let id_a = SnowflakeInternal::<Test>::from_i32(1);
        let id_b = SnowflakeInternal::<Test>::from_i32(2);
        let id_c = SnowflakeInternal::<Test>::from_i32(1);
        assert_that!(id_a).is_equal_to(id_c);
        assert_that!(id_a).is_not_equal_to(id_b);
    }

    proptest! {
        #[traced_test]
        #[test]
        fn to_external_and_back(state in state(config()), internal in snowflake_internal()) {
            let encrypted = state.from_internal(internal).unwrap().external;
            let decrypted = state.from_external(encrypted).unwrap().internal;
            assert_that!(decrypted).is_equal_to(internal);
        }

        #[test]
        fn external_to_hex_and_back(external_id in snowflake_external()) {
            let h = external_id.to_hex();
            let reverse = SnowflakeExternal::<Test>::from_hex(h);
            assert_that!(reverse).is_ok().is_equal_to(external_id);
        }
    }

    #[test]
    fn deserialize_row_with_internal_snowflake() {
        #[derive(Debug, Deserialize, PartialEq, Eq)]
        struct RowStruct {
            id: SnowflakeInternal<Test>,
        }

        let row = from_row::Row(vec![(
            from_row::ColumnName("id".into()),
            from_row::ColumnData(tiberius::ColumnData::I32(Some(30))),
        )]);

        let data = RowStruct::deserialize(&row);
        assert_that!(&data).is_ok_containing(&RowStruct {
            id: SnowflakeInternal::from_i32(30),
        });
    }
}
