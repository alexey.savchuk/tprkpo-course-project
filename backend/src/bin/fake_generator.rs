use std::{collections::HashMap, iter};

use backend::{
    db::{self, methods::*},
    snowflake::{Snowflake, SnowflakeConverter, SnowflakeManager},
    AppConfig,
};
use chrono::NaiveDate;
use clap::Parser;
use color_eyre::{
    eyre::{eyre, OptionExt},
    Result,
};
use fake::Fake;
use itertools::multiunzip;
use rand::prelude::*;
use tracing::{info, info_span, instrument, log::warn};
use tracing_subscriber::util::SubscriberInitExt;

type Database = backend::db::DatabaseProvider;

async fn generate_new_organisation(db: &Database) -> Result<db::Organisation> {
    use fake::faker::company::en::CompanyName;
    let name: String = CompanyName().fake();
    let org = db.organisation.insert_new(&name).await?;
    Ok(org)
}

#[instrument(skip(db))]
async fn generate_organisations(
    db: &Database,
    snowflake: &SnowflakeManager,
    n: usize,
) -> Result<Vec<db::Organisation>> {
    let mut res = Vec::with_capacity(n);
    for _ in iter::repeat(()).take(n) {
        let org = generate_new_organisation(db).await?;
        let org_id = snowflake.from_internal(org.id);
        info!(
            ?org_id,
            "Generated new organisation '{name}'",
            name = org.name
        );
        res.push(org);
    }
    Ok(res)
}

async fn get_weighted_organisations(
    db: &Database,
    snowflake: &SnowflakeManager,
) -> Result<HashMap<db::Organisation, f32>> {
    let organisations = db.organisation.list_all().await?;
    let mut participants = HashMap::<db::Organisation, usize>::new();
    for org in organisations.into_iter() {
        let parts = db
            .organisation_participation
            .get_active_participants_for_organisation(&snowflake.from_internal(org.id)?)
            .await?
            .len();
        participants.insert(org, parts);
    }
    let sum = participants.values().sum::<usize>() as f32;
    let weight = |participants| {
        if sum > 0.0 {
            1.0 - (participants / sum)
        } else {
            1.0
        }
    };
    let res = participants
        .into_iter()
        .map(|(org, parts)| (org, weight(parts as f32)))
        .collect();
    Ok(res)
}

async fn generate_new_participant(db: &Database) -> Result<db::AcademicParticipant> {
    use fake::faker::name::en::Name;
    let name: String = Name().fake();
    let participant = db.academic_participants.insert_new(&name).await?;
    Ok(participant)
}

async fn get_weighted_organisation_roles(
    db: &Database,
) -> Result<HashMap<db::OrganisationRole, f32>> {
    let roles = db.organisation_role.list_all().await?;
    let distr_base: f32 = 1.8;
    let res = roles
        .into_iter()
        .enumerate()
        .map(|(i, role)| (role, distr_base.powi(i as i32)))
        .collect();
    Ok(res)
}

async fn choose_organisation_referrer(
    db: &Database,
    org_id: &Snowflake<db::Organisation>,
    rng: &mut dyn rand::RngCore,
) -> Result<db::AcademicParticipant> {
    let org_participants = db
        .organisation_participation
        .get_active_participants_for_organisation(org_id)
        .await?;
    if let Some(referrer) = org_participants.keys().choose(rng) {
        Ok(referrer.clone())
    } else {
        let participants = db.academic_participants.list_all().await?;
        let referrer = participants.choose(rng).ok_or_eyre("no participants")?;
        Ok(referrer.clone())
    }
}

#[instrument(skip(rng))]
async fn generate_organisation_participation_for(
    db: &Database,
    snowflake: &SnowflakeManager,
    rng: &mut dyn rand::RngCore,
    participants: &[db::AcademicParticipant],
) -> Result<()> {
    let weighted_orgs = get_weighted_organisations(db, snowflake).await?;
    info!("Weighted orgs: {:?}", weighted_orgs);
    let (organisations, org_weights): (Vec<_>, HashMap<_, _>) =
        multiunzip(weighted_orgs.into_iter().map(|(org, weight)| {
            let id = org.id.clone();
            (org, (id, weight))
        }));
    let weighted_roles = get_weighted_organisation_roles(db).await?;
    info!("Weighted roles: {:?}", weighted_roles);
    let (roles, role_weights): (Vec<_>, HashMap<_, _>) =
        multiunzip(weighted_roles.into_iter().map(|(role, weight)| {
            let id = role.id.clone();
            (role, (id, weight))
        }));
    let org_number_distr = rand_distr::Exp::new(1.0)?;
    let role_number_distr = rand_distr::Exp::new(1.0)?;
    for subject in participants {
        let subject_id = snowflake.from_internal(subject.id)?;
        let _participant_span = info_span!(
            "Assigning participant",
            name = subject.full_name,
            ?subject_id,
        )
        .entered();
        let org_number = rng.sample::<f32, _>(org_number_distr).ceil() as usize;
        let orgs =
            organisations.choose_multiple_weighted(rng, org_number, |org| org_weights[&org.id])?;
        for org in orgs {
            let org_id = snowflake.from_internal(org.id)?;
            info!(
                n = org_number,
                ?org_id,
                "Chosen organisation '{name}'",
                name = org.name
            );
            let role_number = rng.sample::<f32, _>(role_number_distr).ceil() as usize;
            let roles =
                roles.choose_multiple_weighted(rng, role_number, |role| role_weights[&role.id])?;
            for role in roles.into_iter() {
                let referrer = choose_organisation_referrer(db, &org_id, rng).await?;
                let role_id = snowflake.from_internal(role.id)?;
                info!(
                    n = role_number,
                    ?role_id,
                    referrer_id = ?referrer.id,
                    "Chosen role '{role}'",
                    role = role.name
                );
                let started_at: NaiveDate =
                    NaiveDate::from_yo_opt((2000..=2020).fake(), (1..=365).fake())
                        .ok_or(eyre!("Constructing NaiveDate"))?;
                db.organisation_participation
                    .insert_new_participation(
                        &org_id,
                        &role_id,
                        &subject_id,
                        &snowflake.from_internal(referrer.id)?,
                        &started_at,
                        &db::ParticipationState::Active,
                    )
                    .await?;
            }
        }
    }
    Ok(())
}

#[instrument(skip(db))]
async fn generate_participants(
    db: &Database,
    snowflake: &SnowflakeManager,
    n: usize,
) -> Result<Vec<db::AcademicParticipant>> {
    let mut res = Vec::with_capacity(n);
    for _ in iter::repeat(()).take(n) {
        let participant = generate_new_participant(db).await?;
        let par_id = snowflake.from_internal(participant.id)?;
        info!(
            ?par_id,
            "Generated new participant '{name}'",
            name = participant.full_name
        );
        res.push(participant);
    }
    Ok(res)
}

async fn get_weighted_subject_codes(db: &Database) -> Result<HashMap<db::SubjectCode, usize>> {
    let scored_codes = db.subject_code.get_all_with_score().await?;
    let res = scored_codes
        .into_iter()
        .map(|(code, score)| (code, score.publications as usize + 10))
        .collect();
    Ok(res)
}

async fn generate_new_publication(
    db: &Database,
    snowflake: &SnowflakeManager,
    rng: &mut dyn rand::RngCore,
) -> Result<db::Publication> {
    use fake::faker::company::en::Bs;
    let weighted_codes = get_weighted_subject_codes(db).await?;
    let (codes, code_weights): (Vec<_>, HashMap<_, _>) =
        multiunzip(weighted_codes.into_iter().map(|(code, weight)| {
            let id = code.id.clone();
            (code, (id, weight))
        }));
    let code = codes.choose_weighted(rng, |code| code_weights[&code.id])?;
    let title: String = Bs().fake();
    let published_at: NaiveDate = NaiveDate::from_yo_opt((2000..=2020).fake(), (1..=365).fake())
        .ok_or(eyre!("Constructing NaiveDate"))?;
    let res = db
        .publication
        .insert_new(&title, &published_at, &snowflake.from_internal(code.id)?)
        .await?;
    Ok(res)
}

#[instrument(skip(rng))]
async fn generate_publications(
    db: &Database,
    snowflake: &SnowflakeManager,
    rng: &mut dyn rand::RngCore,
    n: usize,
) -> Result<Vec<db::Publication>> {
    let mut res = Vec::with_capacity(n);
    for _ in iter::repeat(()).take(n) {
        let publication = generate_new_publication(db, snowflake, rng).await?;
        let pub_id = snowflake.from_internal(publication.id)?;
        info!(
            ?pub_id,
            "Generated new publication '{title}'",
            title = publication.title
        );
        res.push(publication);
    }
    Ok(res)
}

async fn get_weighted_publication_roles(
    db: &Database,
) -> Result<HashMap<db::PublicationRole, f32>> {
    let roles = db.publication_role.list_all().await?;
    let distr_base: f32 = 1.8;
    let res = roles
        .into_iter()
        .enumerate()
        .map(|(i, role)| (role, distr_base.powi(i as i32)))
        .collect();
    Ok(res)
}

async fn choose_publication_referrer(
    db: &Database,
    pub_id: &Snowflake<db::Publication>,
    rng: &mut dyn rand::RngCore,
) -> Result<db::AcademicParticipant> {
    let org_participants = db
        .publication_participation
        .get_participants_for_publication(pub_id)
        .await?;
    if let Some(referrer) = org_participants.keys().choose(rng) {
        Ok(referrer.clone())
    } else {
        let participants = db.academic_participants.list_all().await?;
        let referrer = participants.choose(rng).ok_or_eyre("no participants")?;
        Ok(referrer.clone())
    }
}

#[instrument(skip(rng))]
async fn generate_publication_participation_for(
    db: &Database,
    snowflake: &SnowflakeManager,
    rng: &mut dyn rand::RngCore,
    publications: &[db::Publication],
) -> Result<()> {
    let participants = db.academic_participants.list_all().await?;
    let weighted_roles = get_weighted_publication_roles(db).await?;
    info!("Weighted roles: {:?}", weighted_roles);
    let (roles, role_weights): (Vec<_>, HashMap<_, _>) =
        multiunzip(weighted_roles.into_iter().map(|(role, weight)| {
            let id = role.id.clone();
            (role, (id, weight))
        }));
    let parts_number_distr = rand_distr::Exp::new(1.0 / 6.0)?;
    for publication in publications {
        let pub_id = snowflake.from_internal(publication.id)?;
        let _publication_span = info_span!(
            "Assigning publication '{title}'",
            title = publication.title,
            ?pub_id,
        )
        .entered();
        let parts_number = rng.sample::<f32, _>(parts_number_distr).ceil() as usize;
        let participants = participants.choose_multiple(rng, parts_number);
        for subject in participants {
            let subject_id = snowflake.from_internal(subject.id)?;
            let referrer = choose_publication_referrer(db, &pub_id, rng).await?;
            info!(
                n = parts_number,
                referrer_id = ?referrer.id,
                ?subject_id,
                "Chosen participant '{name}'",
                name = subject.full_name
            );
            let role = roles.choose_weighted(rng, |role| role_weights[&role.id])?;
            let role_id = snowflake.from_internal(role.id)?;
            info!(?role_id, "Chosen role '{role}'", role = role.name);
            db.publication_participation
                .insert_new_participation(
                    &pub_id,
                    &role_id,
                    &subject_id,
                    &snowflake.from_internal(referrer.id)?,
                    &db::ParticipationState::Active,
                )
                .await?;
        }
    }
    Ok(())
}

async fn get_weighted_publications(
    db: &Database,
    snowflake: &SnowflakeManager,
) -> Result<HashMap<db::Publication, usize>> {
    let publications = db.publication.list_all().await?;
    let mut res = HashMap::new();
    for (publication, _) in publications.into_iter() {
        let citation = db
            .publication_reference
            .get_citation_count_for_id(&snowflake.from_internal(publication.id)?)
            .await?;
        res.insert(publication, citation.count as usize + 10);
    }
    Ok(res)
}

#[instrument(skip(rng))]
async fn generate_publication_references_for(
    db: &Database,
    snowflake: &SnowflakeManager,
    rng: &mut dyn rand::RngCore,
    publications: &[db::Publication],
) -> Result<()> {
    let weighted_publications = get_weighted_publications(db, snowflake).await?;
    info!("Weighted publications: {:?}", weighted_publications);
    let (ref_publications, publication_weights): (Vec<_>, HashMap<_, _>) = multiunzip(
        weighted_publications
            .into_iter()
            .map(|(publication, weight)| {
                let id = publication.id.clone();
                (publication, (id, weight))
            }),
    );
    let ref_number_distr = rand_distr::Exp::new(1.0 / 10.0)?;
    for publication in publications {
        let pub_id = snowflake.from_internal(publication.id)?;
        let _publication_span = info_span!(
            "Assigning publication '{title}'",
            title = publication.title,
            ?pub_id,
        );
        let ref_number = rng.sample::<f32, _>(ref_number_distr).ceil() as usize;
        let references_to = ref_publications.choose_multiple_weighted(rng, ref_number, |publ| {
            publication_weights[&publ.id] as f64
        })?;
        for reference in references_to {
            let ref_id = snowflake.from_internal(reference.id)?;
            info!(
                n = ref_number,
                ?ref_id,
                "Chosen reference '{title}'",
                title = reference.title
            );
            if publication.id == reference.id {
                warn!("Publication and reference collided");
                continue;
            }
            db.publication_reference
                .insert_new(&pub_id, &ref_id)
                .await?;
        }
    }
    Ok(())
}

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(long)]
    organisations: usize,
    #[arg(long)]
    participants: usize,
    #[arg(long)]
    publications: usize,
}

#[tokio::main]
async fn main() -> Result<()> {
    backend::tracing_subscriber()?.init();
    let args = Args::parse();
    let config: AppConfig = AppConfig::figment().extract()?;
    let db_pool = config.db.build_db_pool().await?;
    let snowflake = SnowflakeManager::from(&config.snowflake);
    let db = Database::from_db_pool(db_pool);
    let mut rng = thread_rng();
    info_span!("Generating");

    let _new_orgs = generate_organisations(&db, &mut &snowflake, args.organisations).await?;
    let new_participants = generate_participants(&db, &snowflake, args.participants).await?;
    generate_organisation_participation_for(&db, &snowflake, &mut rng, &new_participants).await?;
    let new_publications =
        generate_publications(&db, &snowflake, &mut rng, args.publications).await?;
    generate_publication_participation_for(&db, &snowflake, &mut rng, &new_publications).await?;
    generate_publication_references_for(&db, &snowflake, &mut rng, &new_publications).await?;

    Ok(())
}
