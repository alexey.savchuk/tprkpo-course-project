use std::net::SocketAddr;

use backend::AppConfig;
use tokio::signal;
use tracing::info;
use tracing_subscriber::util::SubscriberInitExt;

#[tokio::main]
async fn main() -> color_eyre::Result<()> {
    backend::tracing_subscriber()?.init();

    let config: AppConfig = AppConfig::figment().extract()?;
    let addr = config.server.bind_addr();
    let app = backend::app(config).await?;

    info!("Serving api");

    axum::Server::bind(&addr)
        .serve(app.into_make_service_with_connect_info::<SocketAddr>())
        .with_graceful_shutdown(shutdown_signal())
        .await?;
    Ok(())
}

async fn shutdown_signal() {
    signal::unix::signal(signal::unix::SignalKind::terminate())
        .expect("failed to install signal handler")
        .recv()
        .await;

    opentelemetry::global::shutdown_tracer_provider();
}
