pub mod extract {
    use std::collections::HashSet;

    use axum::{
        async_trait,
        extract::{FromRef, FromRequestParts, Path},
        http::request::Parts,
    };

    use tracing::instrument;

    use crate::{
        api::{self, organisations::OrgId, publications::PubId, ApiError},
        auth::{api::extract::OryApiStateProvider, session::extract::Identity, AuthError},
        db::{
            self, extract::DatabaseStateProvider, AcademicParticipant, ApplicationRole,
            DatabaseMethodsDyn, OrganisationRole, PublicationRole,
        },
        snowflake::{SnowflakeConverter, SnowflakeManager},
    };

    #[derive(Debug, Clone)]
    pub struct PermissionState {
        pub application_admin_role: ApplicationRole,
        pub organisation_admin_role: OrganisationRole,
        pub publication_admin_role: PublicationRole,
    }

    #[async_trait]
    impl<S> FromRequestParts<S> for PermissionState
    where
        S: DatabaseStateProvider + Send + Sync,
    {
        type Rejection = ApiError;

        #[instrument(name = "extract_permission_state", skip_all, err)]
        async fn from_request_parts(
            _parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let db = state.provide_database();
            let permission_state = PermissionState::new(db).await?;

            Ok(permission_state)
        }
    }

    impl PermissionState {
        pub async fn new(db: DatabaseMethodsDyn) -> db::Result<Self> {
            let application_admin_role = db.application_role().get_admin().await?;
            let organisation_admin_role = db.organisation_role().get_admin().await?;
            let publication_admin_role = db.publication_role().get_author().await?;
            Ok(PermissionState {
                application_admin_role,
                organisation_admin_role,
                publication_admin_role,
            })
        }
    }

    #[derive(Debug)]
    pub struct ApplicationRoles(pub HashSet<ApplicationRole>);

    impl ApplicationRoles {
        pub fn is_application_admin(&self, state: &PermissionState) -> bool {
            self.0.contains(&state.application_admin_role)
        }
    }

    #[async_trait]
    impl<S> FromRequestParts<S> for ApplicationRoles
    where
        S: DatabaseStateProvider + OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_application_roles", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let db = state.provide_database();
            let Identity(id) = Identity::from_request_parts(parts, state).await?;

            let app_roles = db
                .application_participation()
                .get_application_roles_for_user(&id)
                .await?;

            Ok(ApplicationRoles(app_roles))
        }
    }

    #[derive(Debug)]
    pub struct ApplicationAdmin(pub ());

    #[async_trait]
    impl<S> FromRequestParts<S> for ApplicationAdmin
    where
        S: DatabaseStateProvider + OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_application_admin", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let app_roles = ApplicationRoles::from_request_parts(parts, state).await?;
            let permissions = PermissionState::from_request_parts(parts, state).await?;

            if app_roles.is_application_admin(&permissions) {
                Ok(ApplicationAdmin(()))
            } else {
                Err(ApiError::from(AuthError::InsufficientPermissions))
            }
        }
    }

    #[derive(Debug)]
    pub struct OrganisationRoles(pub HashSet<OrganisationRole>);

    impl OrganisationRoles {
        pub fn is_organisation_admin(&self, state: &PermissionState) -> bool {
            self.0.contains(&state.organisation_admin_role)
        }
    }

    #[async_trait]
    impl<S> FromRequestParts<S> for OrganisationRoles
    where
        SnowflakeManager: FromRef<S>,
        S: DatabaseStateProvider + OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_organisation_roles", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let db = state.provide_database();
            let snowflake = SnowflakeManager::from_ref(state);
            let ConnectedAcademicParticipant(connected_participant) =
                ConnectedAcademicParticipant::from_request_parts(parts, state).await?;
            let Path(OrgId { org_id }) = Path::<OrgId>::from_request_parts(parts, state).await?;

            let org_roles = db
                .organisation_participation()
                .get_roles_for(
                    &snowflake.from_external(org_id)?,
                    &snowflake.from_internal(connected_participant.id)?,
                )
                .await?;

            Ok(OrganisationRoles(org_roles))
        }
    }

    #[derive(Debug)]
    pub struct OrganisationAdmin(pub AcademicParticipant);

    #[async_trait]
    impl<S> FromRequestParts<S> for OrganisationAdmin
    where
        SnowflakeManager: FromRef<S>,
        S: DatabaseStateProvider + OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_organisation_admin", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let org_roles = OrganisationRoles::from_request_parts(parts, state).await?;
            let permissions = PermissionState::from_request_parts(parts, state).await?;
            let ConnectedAcademicParticipant(connected_participant) =
                ConnectedAcademicParticipant::from_request_parts(parts, state).await?;

            if org_roles.is_organisation_admin(&permissions) {
                Ok(OrganisationAdmin(connected_participant))
            } else {
                Err(ApiError::from(AuthError::InsufficientPermissions))
            }
        }
    }

    #[derive(Debug)]
    pub struct PublicationRoles(pub HashSet<PublicationRole>);

    impl PublicationRoles {
        pub fn is_publication_admin(&self, state: &PermissionState) -> bool {
            self.0.contains(&state.publication_admin_role)
        }
    }

    #[async_trait]
    impl<S> FromRequestParts<S> for PublicationRoles
    where
        SnowflakeManager: FromRef<S>,
        S: DatabaseStateProvider + OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_publication_roles", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let db = state.provide_database();
            let snowflake = SnowflakeManager::from_ref(state);
            let ConnectedAcademicParticipant(participant) =
                ConnectedAcademicParticipant::from_request_parts(parts, state).await?;
            let Path(PubId { pub_id }) = Path::<PubId>::from_request_parts(parts, state).await?;

            let org_roles = db
                .publication_participation()
                .get_publication_roles_for_participant(
                    &snowflake.from_internal(participant.id)?,
                    &snowflake.from_external(pub_id)?,
                )
                .await?;

            Ok(PublicationRoles(org_roles))
        }
    }

    #[derive(Debug)]
    pub struct PublicationAdmin(pub AcademicParticipant);

    #[async_trait]
    impl<S> FromRequestParts<S> for PublicationAdmin
    where
        SnowflakeManager: FromRef<S>,
        S: DatabaseStateProvider + OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_publication_admin", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let org_roles = PublicationRoles::from_request_parts(parts, state).await?;
            let permissions = PermissionState::from_request_parts(parts, state).await?;
            let ConnectedAcademicParticipant(connected_participant) =
                ConnectedAcademicParticipant::from_request_parts(parts, state).await?;

            if org_roles.is_publication_admin(&permissions) {
                Ok(PublicationAdmin(connected_participant))
            } else {
                Err(ApiError::from(AuthError::InsufficientPermissions))
            }
        }
    }

    #[derive(Debug)]
    pub struct ConnectedAcademicParticipant(pub AcademicParticipant);

    #[async_trait]
    impl<S> FromRequestParts<S> for ConnectedAcademicParticipant
    where
        S: DatabaseStateProvider + OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_connected_participant", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let db = state.provide_database();
            let Identity(id) = Identity::from_request_parts(parts, state).await?;

            let academic_participant = db
                .academic_users()
                .get_academic_participant_for_user(id.as_ref())
                .await?
                .ok_or(AuthError::NoConnectedParticipant)?;

            Ok(ConnectedAcademicParticipant(academic_participant))
        }
    }

    #[cfg(test)]
    mod tests {
        use chrono::prelude::*;
        use headers::Header;
        use mockall::predicate::{eq, function};
        use spectral::prelude::*;
        use std::sync::Arc;

        use axum::extract::FromRequestParts;
        use tracing_test::traced_test;

        use crate::{
            api::ApiError,
            auth::{
                permissions::extract::{ConnectedAcademicParticipant, PermissionState},
                test::generate_session_with_id,
                AuthError, MockOryApi, OryIdentity,
            },
            db,
            server::test::MockState,
            snowflake::SnowflakeInternal,
        };

        #[tokio::test]
        #[traced_test]
        async fn extract_permissions_ok() {
            let mut db = db::MockDatabase::new();
            let mut api_mock = MockOryApi::new();

            let app_admin = db::ApplicationRole {
                id: SnowflakeInternal::from_i32(12),
                created_at: Local::now().naive_local(),
                name: db::ApplicationRole::ADMINISTATOR.to_string(),
            };
            let app_admin_mock = app_admin.clone();
            let org_admin = db::OrganisationRole {
                id: SnowflakeInternal::from_i32(13),
                created_at: Local::now().naive_local(),
                name: db::OrganisationRole::ADMINISTATOR.to_string(),
            };
            let org_admin_mock = org_admin.clone();
            let pub_author = db::PublicationRole {
                id: SnowflakeInternal::from_i32(14),
                created_at: Local::now().naive_local(),
                name: db::PublicationRole::AUTHOR.to_string(),
            };
            let pub_author_mock = pub_author.clone();

            db.application_role
                .expect_get_admin()
                .return_once(move || Ok(app_admin_mock));
            db.organisation_role
                .expect_get_admin()
                .return_once(move || Ok(org_admin_mock));
            db.publication_role
                .expect_get_author()
                .return_once(move || Ok(pub_author_mock));

            let (mut parts, _body) = http::request::Builder::new()
                .method("GET")
                .body(())
                .unwrap()
                .into_parts();

            let state = MockState {
                database: Arc::new(db),
                ory_api: Arc::new(api_mock),
            };

            let extracted_permissions =
                PermissionState::from_request_parts(&mut parts, &state).await;
            assert_that!(extracted_permissions)
                .is_ok()
                .matches(|perms| {
                    perms.application_admin_role == app_admin
                        && perms.organisation_admin_role == org_admin
                        && perms.publication_admin_role == pub_author
                });
        }

        #[tokio::test]
        #[traced_test]
        async fn extract_connected_participant_ok() {
            let mut db = db::MockDatabase::new();
            let mut api_mock = MockOryApi::new();

            let identity_id = "identity_id";
            let session = generate_session_with_id("session_id".into(), identity_id.into());
            let session_for_mock = session.clone();
            let connected_participant = db::AcademicParticipant {
                id: SnowflakeInternal::from_i32(60),
                created_at: Local::now().naive_local(),
                full_name: "Rusty McRustface".to_string(),
            };
            let connected_participant_mock = connected_participant.clone();

            api_mock
                .expect_get_session_from_cookie()
                .return_once(move |_| Ok(session_for_mock));

            db.academic_users
                .expect_get_academic_participant_for_user()
                .with(function(move |identity: &OryIdentity| {
                    identity.id == identity_id
                }))
                .return_once(move |_| Ok(Some(connected_participant_mock)));

            let (mut parts, _body) = http::request::Builder::new()
                .method("GET")
                .header(headers::Cookie::name(), "bla bla")
                .body(())
                .unwrap()
                .into_parts();

            let state = MockState {
                database: Arc::new(db),
                ory_api: Arc::new(api_mock),
            };

            let extracted_conn_par =
                ConnectedAcademicParticipant::from_request_parts(&mut parts, &state).await;
            assert_that!(extracted_conn_par).is_ok().matches(
                |ConnectedAcademicParticipant(actual_conn_par)| {
                    actual_conn_par == &connected_participant
                },
            );
        }

        #[tokio::test]
        #[traced_test]
        async fn extract_connected_participant_err_no_participant() {
            let mut db = db::MockDatabase::new();
            let mut api_mock = MockOryApi::new();

            let identity_id = "identity_id";
            let session = generate_session_with_id("session_id".into(), identity_id.into());
            let session_for_mock = session.clone();

            api_mock
                .expect_get_session_from_cookie()
                .return_once(move |_| Ok(session_for_mock));

            db.academic_users
                .expect_get_academic_participant_for_user()
                .with(function(move |identity: &OryIdentity| {
                    identity.id == identity_id
                }))
                .return_once(move |_| Ok(None));

            let (mut parts, _body) = http::request::Builder::new()
                .method("GET")
                .header(headers::Cookie::name(), "bla bla")
                .body(())
                .unwrap()
                .into_parts();

            let state = MockState {
                database: Arc::new(db),
                ory_api: Arc::new(api_mock),
            };

            let extracted_conn_par =
                ConnectedAcademicParticipant::from_request_parts(&mut parts, &state).await;
            assert_that!(extracted_conn_par).is_err().matches(|err| {
                matches!(err, ApiError::AuthError(AuthError::NoConnectedParticipant))
            });
        }
    }
}
