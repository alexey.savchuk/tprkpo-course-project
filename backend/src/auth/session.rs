pub mod extract {
    use axum::{async_trait, extract::FromRequestParts, http::request::Parts};
    use headers::Header;
    use tracing::instrument;

    use crate::{
        api,
        auth::{api::extract::OryApiStateProvider, AuthError, OryIdentity, OrySession},
    };

    #[derive(Debug)]
    pub struct Session(pub OrySession);

    #[async_trait]
    impl<S> FromRequestParts<S> for Session
    where
        S: OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_ory_session", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let client = state.provide_ory_api();
            let cookies = parts
                .headers
                .get(headers::Cookie::name())
                .ok_or(AuthError::SessionCookieError)?;

            let session = client
                .get_session_from_cookie(cookies.to_str().map_err(AuthError::from)?)
                .await?;

            Ok(Session(session))
        }
    }

    #[derive(Debug)]
    pub struct Identity(pub Box<OryIdentity>);

    #[async_trait]
    impl<S> FromRequestParts<S> for Identity
    where
        S: OryApiStateProvider + Send + Sync,
    {
        type Rejection = api::ApiError;

        #[instrument(name = "extract_ory_identity", skip_all, err)]
        async fn from_request_parts(
            parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let Session(session) = Session::from_request_parts(parts, state).await?;

            let identity = session.identity.ok_or(AuthError::MissingIdentity)?;

            Ok(Identity(identity))
        }
    }

    #[cfg(test)]
    mod test {
        use std::sync::Arc;

        use crate::{
            api::ApiError,
            auth::{
                extract::{Identity, Session},
                test::generate_session_with_id,
                AuthError, MockOryApi,
            },
            db,
            server::test::MockState,
        };

        use axum::extract::FromRequestParts;
        use headers::Header;
        use mockall::predicate::eq;
        use spectral::prelude::*;
        use tracing_test::traced_test;

        #[tokio::test]
        #[traced_test]
        async fn extract_session_from_cookie_ok() {
            let db = db::MockDatabase::new();
            let mut api_mock = MockOryApi::new();

            let external_cookie = "Cookie inner";
            let (mut parts, _body) = http::request::Builder::new()
                .method("GET")
                .header(headers::Cookie::name(), external_cookie)
                .body(())
                .unwrap()
                .into_parts();

            let session = generate_session_with_id("session_id".into(), "identity_id".into());
            let session_for_mock = session.clone();

            api_mock
                .expect_get_session_from_cookie()
                .with(eq(external_cookie))
                .return_once(move |_| Ok(session_for_mock));

            let state = MockState {
                database: Arc::new(db),
                ory_api: Arc::new(api_mock),
            };

            let extracted_session = Session::from_request_parts(&mut parts, &state).await;
            assert_that!(extracted_session)
                .is_ok()
                .matches(|Session(ref extracted_session)| extracted_session.id == session.id)
        }

        #[tokio::test]
        #[traced_test]
        async fn extract_session_from_cookie_err_no_cookie() {
            let db = db::MockDatabase::new();
            let mut api_mock = MockOryApi::new();

            let (mut parts, _body) = http::request::Builder::new()
                .method("GET")
                .body(())
                .unwrap()
                .into_parts();

            let state = MockState {
                database: Arc::new(db),
                ory_api: Arc::new(api_mock),
            };

            let extracted_session = Session::from_request_parts(&mut parts, &state).await;
            assert_that!(extracted_session)
                .is_err()
                .matches(|err| matches!(err, ApiError::AuthError(AuthError::SessionCookieError)))
        }

        #[tokio::test]
        #[traced_test]
        async fn extract_identity_from_cookie_ok() {
            let db = db::MockDatabase::new();
            let mut api_mock = MockOryApi::new();

            let external_cookie = "Cookie inner";
            let (mut parts, _body) = http::request::Builder::new()
                .method("GET")
                .header(headers::Cookie::name(), external_cookie)
                .body(())
                .unwrap()
                .into_parts();

            let session = generate_session_with_id("session_id".into(), "identity_id".into());
            let session_for_mock = session.clone();

            api_mock
                .expect_get_session_from_cookie()
                .with(eq(external_cookie))
                .return_once(move |_| Ok(session_for_mock));

            let state = MockState {
                database: Arc::new(db),
                ory_api: Arc::new(api_mock),
            };

            let extracted_session = Identity::from_request_parts(&mut parts, &state).await;
            assert_that!(extracted_session)
                .is_ok()
                .matches(|Identity(ref extracted_identity)| {
                    extracted_identity.id == session.identity.as_ref().unwrap().id
                })
        }

        #[tokio::test]
        #[traced_test]
        async fn extract_identity_from_cookie_err_no_identity() {
            let db = db::MockDatabase::new();
            let mut api_mock = MockOryApi::new();

            let external_cookie = "Cookie inner";
            let (mut parts, _body) = http::request::Builder::new()
                .method("GET")
                .header(headers::Cookie::name(), external_cookie)
                .body(())
                .unwrap()
                .into_parts();

            let mut session = generate_session_with_id("session_id".into(), "identity_id".into());
            session.identity = None;
            let session_for_mock = session.clone();

            api_mock
                .expect_get_session_from_cookie()
                .with(eq(external_cookie))
                .return_once(move |_| Ok(session_for_mock));

            let state = MockState {
                database: Arc::new(db),
                ory_api: Arc::new(api_mock),
            };

            let extracted_identity = Identity::from_request_parts(&mut parts, &state).await;
            assert_that!(extracted_identity)
                .is_err()
                .matches(|err| matches!(err, ApiError::AuthError(AuthError::MissingIdentity)))
        }
    }
}
