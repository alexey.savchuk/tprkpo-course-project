mod api;
mod client;
mod error;
mod permissions;
mod session;
mod traits;

use serde::Deserialize;

pub use api::{MockOryApi, OryApi, OryApiDyn};
pub use client::OryClient;

pub use error::{AuthError, Result};
pub use traits::{TraitError, UserTraits};

pub type OryConfig = ory_client::apis::configuration::Configuration;
pub type OrySession = ory_client::models::Session;
pub type OryIdentity = ory_client::models::Identity;

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct AuthConfig {
    #[serde(rename = "publickratos")]
    pub public_url: String,
    #[serde(rename = "adminkratos")]
    pub admin_url: String,
}

impl AuthConfig {
    fn public_config(&self) -> OryConfig {
        OryConfig {
            base_path: self.public_url.clone(),
            ..Default::default()
        }
    }

    fn admin_config(&self) -> OryConfig {
        OryConfig {
            base_path: self.admin_url.clone(),
            ..Default::default()
        }
    }
}

pub mod extract {
    pub use super::api::extract::*;

    pub use super::permissions::extract::*;
    pub use super::session::extract::*;
}

pub mod test {
    use super::{OryIdentity, OrySession};

    pub fn generate_identity_with_id(id: String) -> OryIdentity {
        OryIdentity {
            created_at: None,
            credentials: None,
            id,
            metadata_admin: None,
            metadata_public: None,
            organization_id: None,
            recovery_addresses: None,
            schema_id: "schema".to_string(),
            schema_url: "schema".to_string(),
            state: None,
            state_changed_at: None,
            traits: None,
            updated_at: None,
            verifiable_addresses: None,
        }
    }

    pub fn generate_session_with_id(session_id: String, identity_id: String) -> OrySession {
        OrySession {
            active: None,
            authenticated_at: None,
            authentication_methods: None,
            authenticator_assurance_level: None,
            devices: None,
            expires_at: None,
            id: session_id,
            identity: Some(Box::new(generate_identity_with_id(identity_id))),
            issued_at: None,
            tokenized: None,
        }
    }
}
