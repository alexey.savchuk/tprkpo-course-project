use std::sync::Arc;

use axum::async_trait;
use mockall::automock;

use super::{OryIdentity, OrySession, Result};

#[automock]
#[async_trait]
pub trait OryApi {
    async fn get_session_from_cookie(&self, cookies: &str) -> Result<OrySession>;
    async fn get_identity_by_id(&self, id: &str) -> Result<OryIdentity>;
    async fn list_identities_for_ids<'s>(&self, ids: Vec<String>) -> Result<Vec<OryIdentity>>;
    async fn list_identities(&self) -> Result<Vec<OryIdentity>>;
}

pub type OryApiDyn = Arc<dyn OryApi + Send + Sync>;

pub mod extract {
    use std::convert::Infallible;

    use axum::{async_trait, extract::FromRequestParts, http::request::Parts};
    use tracing::instrument;

    use super::OryApiDyn;

    pub trait OryApiStateProvider {
        fn provide_ory_api(&self) -> OryApiDyn;
    }

    pub struct OryApi(pub OryApiDyn);

    #[async_trait]
    impl<S> FromRequestParts<S> for OryApi
    where
        S: OryApiStateProvider + Send + Sync,
    {
        type Rejection = Infallible;

        #[instrument(name = "extract_ory_api", level = "trace", skip_all)]
        async fn from_request_parts(
            _parts: &mut Parts,
            state: &S,
        ) -> std::result::Result<Self, Self::Rejection> {
            let api = state.provide_ory_api();
            Ok(Self(api))
        }
    }
}
