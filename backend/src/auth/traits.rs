

use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum TraitError {
    #[error("no traits were provided")]
    NoTraitsError,
    #[error("invalid json: {0}")]
    InvalidJsonError(String),
    #[error("unexpected schema: {0}")]
    UnexpectedSchema(String),
}

impl From<serde_json::Error> for TraitError {
    fn from(err: serde_json::Error) -> Self {
        match err.classify() {
            serde_json::error::Category::Io => unreachable!("should not get io error in trait"),
            serde_json::error::Category::Eof => TraitError::InvalidJsonError(format!("{}", err)),
            serde_json::error::Category::Syntax => TraitError::InvalidJsonError(format!("{}", err)),
            serde_json::error::Category::Data => TraitError::UnexpectedSchema(format!("{}", err)),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct UserTraits {
    pub email: Box<str>,
}

impl TryFrom<Option<serde_json::Value>> for UserTraits {
    type Error = TraitError;

    fn try_from(value: Option<serde_json::Value>) -> Result<Self, Self::Error> {
        let value = value.ok_or(TraitError::NoTraitsError)?;
        let traits = UserTraits::deserialize(value)?;
        Ok(traits)
    }
}

#[cfg(test)]
mod tests {
    use serde_json::json;
    use spectral::prelude::*;
    use tracing_test::traced_test;

    use crate::auth::{TraitError, UserTraits};

    #[test]
    #[traced_test]
    fn parse_user_traits_ok() {
        let email = "example@example.com";
        let raw_json = json!({ "email": email });

        let traits = UserTraits::try_from(Some(raw_json));

        assert_that!(traits).is_ok().is_equal_to(&UserTraits {
            email: email.into(),
        });
    }

    #[test]
    #[traced_test]
    fn parse_user_traits_ok_extra_keys() {
        let email = "example@example.com";
        let raw_json = json!({ "email": email, "garbage": "garbage" });

        let traits = UserTraits::try_from(Some(raw_json));

        assert_that!(traits).is_ok().is_equal_to(&UserTraits {
            email: email.into(),
        });
    }

    #[test]
    #[traced_test]
    fn parse_user_traits_err_no_key() {
        let email = "example@example.com";
        let raw_json = json!({ "not_email": email });

        let traits = UserTraits::try_from(Some(raw_json));

        assert_that!(traits)
            .is_err()
            .matches(|err| matches!(err, TraitError::UnexpectedSchema(_)));
    }
}
