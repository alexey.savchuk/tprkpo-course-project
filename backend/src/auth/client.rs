use axum::async_trait;
use futures::future::try_join_all;
use itertools::Itertools;
use tracing::{debug, instrument, warn};

use super::{api::OryApi, AuthConfig, OryConfig, OryIdentity, OrySession, Result};

#[derive(Debug, Clone)]
pub struct OryClient {
    public_config: OryConfig,
    admin_config: OryConfig,
}

impl From<&AuthConfig> for OryClient {
    fn from(config: &AuthConfig) -> Self {
        let public_config = config.public_config();
        let admin_config = config.admin_config();
        OryClient {
            admin_config,
            public_config,
        }
    }
}

#[async_trait]
impl OryApi for OryClient {
    #[instrument(skip(self))]
    async fn get_session_from_cookie(&self, cookies: &str) -> Result<OrySession> {
        type Error = ory_client::apis::Error<ory_client::apis::frontend_api::ToSessionError>;
        let session = ory_client::apis::frontend_api::to_session(
            &self.public_config,
            None,
            Some(cookies),
            None,
        )
        .await
        .map_err(Error::from)?;
        Ok(session)
    }

    #[instrument(skip(self))]
    async fn get_identity_by_id(&self, id: &str) -> Result<OryIdentity> {
        type Error = ory_client::apis::Error<ory_client::apis::identity_api::GetIdentityError>;
        let identity = ory_client::apis::identity_api::get_identity(&self.admin_config, id, None)
            .await
            .map_err(Error::from)?;
        Ok(identity)
    }

    #[instrument(skip(self, ids))]
    async fn list_identities_for_ids<'s>(&self, ids: Vec<String>) -> Result<Vec<OryIdentity>> {
        let ids = try_join_all(
            ids.into_iter()
                .map(|id| async move { self.get_identity_by_id(&id).await })
                .collect_vec(),
        )
        .await?;
        Ok(ids)
    }

    #[instrument(skip(self))]
    async fn list_identities(&self) -> Result<Vec<OryIdentity>> {
        type Error = ory_client::apis::Error<ory_client::apis::identity_api::ListIdentitiesError>;
        let mut identities = Vec::new();
        let client = &self.admin_config.client;
        let base_path = &self.admin_config.base_path;
        let mut uri = format!("/admin/identities");
        loop {
            debug!(%uri, "Request uri");
            let url = format!("{base_path}{uri}");
            let request = client.get(&url).build().map_err(Error::from)?;
            let response = client.execute(request).await.map_err(Error::from)?;
            let status = response.status();
            let link_header = response.headers().get(&http::header::LINK).cloned();
            let content = response.text().await.map_err(Error::from)?;

            if status.is_client_error() || status.is_server_error() {
                let err: Option<ory_client::apis::identity_api::ListIdentitiesError> =
                    serde_json::from_str(&content).ok();
                Err(Error::ResponseError(ory_client::apis::ResponseContent {
                    status,
                    content,
                    entity: err,
                }))?;
            } else {
                let mut batch = serde_json::from_str(&content).map_err(Error::from)?;
                debug!(next_batch = ?batch, "Next batch");
                identities.append(&mut batch);

                if let Some(link_header) = link_header {
                    let mut links = parse_link_header::parse_with_rel(link_header.to_str()?)?;
                    debug!(link_header = ?links, "Parsed link header");
                    if let Some(next_link) = links.remove("next") {
                        uri = next_link.raw_uri;
                    } else {
                        debug!("End of pagination");
                        break;
                    }
                } else {
                    warn!("Missing Link header");
                    break;
                }
            }
        }
        Ok(identities)
    }
}

pub mod extract {
    use std::sync::Arc;

    use axum::extract::FromRef;

    use crate::auth::api::{extract::OryApiStateProvider, OryApiDyn};

    impl<S> OryApiStateProvider for S
    where
        Arc<super::OryClient>: FromRef<S>,
    {
        fn provide_ory_api(&self) -> OryApiDyn {
            let client_ref = <Arc<super::OryClient>>::from_ref(self);
            client_ref
        }
    }
}
