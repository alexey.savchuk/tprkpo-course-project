use axum::http::{self, StatusCode};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum SessionCookieError {}

#[derive(Debug, Error)]
pub enum AuthError {
    #[error(transparent)]
    StrError(#[from] http::header::ToStrError),
    #[error("No session cookie")]
    SessionCookieError,
    #[error(transparent)]
    ToSessionError(#[from] ory_client::apis::Error<ory_client::apis::frontend_api::ToSessionError>),
    #[error(transparent)]
    GetIdentityError(
        #[from] ory_client::apis::Error<ory_client::apis::identity_api::GetIdentityError>,
    ),
    #[error(transparent)]
    ListIdentitiesError(
        #[from] ory_client::apis::Error<ory_client::apis::identity_api::ListIdentitiesError>,
    ),
    #[error(transparent)]
    LinkError(#[from] parse_link_header::Error),
    #[error("No identity in your session")]
    MissingIdentity,
    #[error("No connected participant")]
    NoConnectedParticipant,
    #[error("Insufficient permissions")]
    InsufficientPermissions,
}

impl AuthError {
    pub fn status_code(&self) -> StatusCode {
        match self {
            AuthError::StrError(_) => StatusCode::BAD_REQUEST,
            AuthError::SessionCookieError => StatusCode::UNAUTHORIZED,
            AuthError::MissingIdentity => StatusCode::UNAUTHORIZED,
            AuthError::InsufficientPermissions => StatusCode::FORBIDDEN,
            AuthError::NoConnectedParticipant => StatusCode::FORBIDDEN,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

pub type Result<T> = std::result::Result<T, AuthError>;

#[cfg(test)]
mod tests {
    

    use super::*;
    use spectral::prelude::*;

    // #[test]
    // fn test_status_code_str_error() {
    //     let error = AuthError::StrError();
    //     assert_that!(error.status_code()).is_equal_to(StatusCode::BAD_REQUEST);
    // }

    #[test]
    fn test_status_code_session_cookie_error() {
        let error = AuthError::SessionCookieError;
        assert_that!(error.status_code()).is_equal_to(StatusCode::UNAUTHORIZED);
    }

    #[test]
    fn test_status_code_missing_identity() {
        let error = AuthError::MissingIdentity;
        assert_that!(error.status_code()).is_equal_to(StatusCode::UNAUTHORIZED);
    }

    #[test]
    fn test_status_code_insufficient_permissions() {
        let error = AuthError::InsufficientPermissions;
        assert_that!(error.status_code()).is_equal_to(StatusCode::FORBIDDEN);
    }

    #[test]
    fn test_status_code_no_connected_participant() {
        let error = AuthError::NoConnectedParticipant;
        assert_that!(error.status_code()).is_equal_to(StatusCode::FORBIDDEN);
    }

    // #[test]
    // fn test_status_code_other() {
    //     let error = AuthError::
    //     assert_that!(error.status_code()).is_equal_to(StatusCode::UNAUTHORIZED);
    // }
}