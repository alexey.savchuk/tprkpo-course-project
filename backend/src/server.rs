use std::sync::Arc;

use axum::{extract::FromRef, Router};
use axum_tracing_opentelemetry::middleware::{OtelAxumLayer, OtelInResponseLayer};
use tower::ServiceBuilder;
use tower_http::cors::CorsLayer;

use crate::{
    api,
    auth::OryClient,
    config::AppConfig,
    db::{self},
    snowflake::SnowflakeManager,
};

#[derive(Debug, Clone, FromRef)]
pub struct AppState {
    ory_client: Arc<OryClient>,
    db_provider: Arc<db::DatabaseProvider>,
    snowflake_manager: SnowflakeManager,
}

pub async fn app(config: AppConfig) -> db::Result<Router> {
    let db_pool = config.db.build_db_pool().await?;
    db::apply_migrations(&db_pool).await?;
    let db_provider = Arc::new(db::DatabaseProvider::from_db_pool(db_pool));
    let ory_client = Arc::new(OryClient::from(&config.auth));
    let snowflake_manager = SnowflakeManager::from(&config.snowflake);

    let state = AppState {
        ory_client,
        db_provider,
        snowflake_manager,
    };
    let middleware_stack = ServiceBuilder::new()
        // start OpenTelemetry trace on incoming request
        .layer(OtelAxumLayer::default())
        // include trace context as header into the response
        .layer(OtelInResponseLayer::default())
        .layer(CorsLayer::permissive())
        .into_inner();

    let router = Router::new()
        .nest("/api/", api::routes())
        .layer(middleware_stack)
        .with_state(state);
    Ok(router)
}

pub fn tracing_subscriber() -> color_eyre::Result<impl tracing::Subscriber> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::EnvFilter;
    use tracing_tree::HierarchicalLayer;

    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    color_eyre::install()?;

    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(opentelemetry_otlp::new_exporter().tonic())
        .install_batch(opentelemetry_sdk::runtime::Tokio)?;
    let telemetry = tracing_opentelemetry::layer().with_tracer(tracer);

    let subscriber = tracing_subscriber::Registry::default()
        .with(
            EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "debug,backend=trace,otel::tracing=trace,otel=debug,tower_http=debug,axum=debug,axum::rejection=trace".into()),
        )
        .with(
            HierarchicalLayer::new(2)
                .with_targets(true)
                .with_bracketed_fields(true)
                .with_verbose_exit(true),
        )
        .with(ErrorLayer::default())
        .with(telemetry);

    Ok(subscriber)
}

#[cfg(test)]
pub mod test {

    use std::sync::Arc;

    use crate::{
        auth::{extract::OryApiStateProvider, MockOryApi, OryApiDyn},
        db::{extract::DatabaseStateProvider, DatabaseMethodsDyn, MockDatabase},
    };

    pub struct MockState {
        pub database: Arc<MockDatabase>,
        pub ory_api: Arc<MockOryApi>,
    }

    impl DatabaseStateProvider for MockState {
        fn provide_database(&self) -> DatabaseMethodsDyn {
            self.database.clone()
        }
    }

    impl OryApiStateProvider for MockState {
        fn provide_ory_api(&self) -> OryApiDyn {
            self.ory_api.clone()
        }
    }
}
