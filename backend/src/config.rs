use std::net::{IpAddr, Ipv4Addr, SocketAddr};

use figment::{
    providers::{Env, Format, Toml},
    Figment, Provider,
};
use serde::Deserialize;

use crate::{auth::AuthConfig, db::DbConfig, snowflake::SnowflakeConfig};

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct ServerConfig {
    pub port: u16,
}

impl ServerConfig {
    pub fn bind_addr(&self) -> SocketAddr {
        SocketAddr::new(IpAddr::V4(Ipv4Addr::UNSPECIFIED), self.port)
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct AppConfig {
    pub db: DbConfig,
    pub snowflake: SnowflakeConfig,
    pub auth: AuthConfig,
    pub server: ServerConfig,
}

impl AppConfig {
    pub fn figment() -> Figment {
        Figment::new()
            .join(Toml::file("config.toml"))
            .join(Env::prefixed("APP_").split("_"))
    }

    pub fn from<T: Provider>(provider: T) -> Result<Self, figment::Error> {
        Figment::from(provider).extract()
    }
}
