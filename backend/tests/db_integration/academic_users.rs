use backend::{
    auth::test::generate_identity_with_id, db::methods::*, snowflake::SnowflakeConverter,
};
use spectral::prelude::*;
use tracing_test::traced_test;

use crate::common::{get_database, snowflake_manager};

#[tokio::test]
#[traced_test]
async fn create_and_check_academic_user() {
    let db = get_database().await;
    let snowflake = snowflake_manager();

    let participant = db
        .academic_participants
        .insert_new("Test subject")
        .await
        .unwrap();
    let user_identity = generate_identity_with_id(uuid::Uuid::new_v4().to_string());
    let academic_user = db
        .academic_users
        .insert_new(
            &user_identity,
            &snowflake.from_internal(participant.id).unwrap(),
        )
        .await
        .unwrap();

    assert_that!(academic_user.participant_id).is_equal_to(participant.id);

    let back_participant = db
        .academic_users
        .get_academic_participant_for_user(&user_identity)
        .await
        .unwrap();

    assert_that!(back_participant)
        .is_some()
        .is_equal_to(participant);
}
