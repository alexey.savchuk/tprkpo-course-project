use backend::{
    db::{self, methods::*},
    snowflake::SnowflakeConverter,
};
use chrono::Local;
use spectral::prelude::*;
use tracing_test::traced_test;

use crate::common::{get_database, snowflake_manager};

#[tokio::test]
#[traced_test]
async fn create_and_accept_participation() {
    let db = get_database().await;
    let snowflake = snowflake_manager();

    let subject_code = db
        .subject_code
        .insert_new("Test code", "00.00.00")
        .await
        .unwrap();
    let publication = db
        .publication
        .insert_new(
            "Test publication",
            &Local::now().date_naive(),
            &snowflake.from_internal(subject_code.id).unwrap(),
        )
        .await
        .unwrap();
    let role = db.publication_role.insert_new("Some role").await.unwrap();
    let subject = db
        .academic_participants
        .insert_new("Test subject")
        .await
        .unwrap();
    let referrer = db
        .academic_participants
        .insert_new("Test referrer")
        .await
        .unwrap();
    let participation = db
        .publication_participation
        .insert_new_participation(
            &snowflake.from_internal(publication.id).unwrap(),
            &snowflake.from_internal(role.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer.id).unwrap(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();

    assert_that!(participation.publication_id).is_equal_to(publication.id);
    assert_that!(participation.role_id).is_equal_to(role.id);
    assert_that!(participation.subject_id).is_equal_to(subject.id);
    assert_that!(participation.referrer_id).is_equal_to(referrer.id);
    assert_that!(participation.participation_state).is_equal_to(db::ParticipationState::Pending);

    let new_participation = db
        .publication_participation
        .update_participation_state(
            &snowflake
                .from_internal(participation.participation_id)
                .unwrap(),
            &db::ParticipationState::Active,
        )
        .await
        .unwrap();

    assert_that!(new_participation).is_equal_to(db::PublicationParticipation {
        participation_state: db::ParticipationState::Active,
        ..participation
    })
}

#[tokio::test]
#[traced_test]
async fn list_pending_participations() {
    let db = get_database().await;
    let snowflake = snowflake_manager();

    let subject_code = db
        .subject_code
        .insert_new("Test code", "00.00.00")
        .await
        .unwrap();
    let role = db.publication_role.insert_new("Some role").await.unwrap();
    let referrer = db
        .academic_participants
        .insert_new("Test referrer")
        .await
        .unwrap();

    let publication_pend = db
        .publication
        .insert_new(
            "Test publication pend",
            &Local::now().date_naive(),
            &snowflake.from_internal(subject_code.id).unwrap(),
        )
        .await
        .unwrap();
    let publication_act = db
        .publication
        .insert_new(
            "Test publication act",
            &Local::now().date_naive(),
            &snowflake.from_internal(subject_code.id).unwrap(),
        )
        .await
        .unwrap();

    let subject = db
        .academic_participants
        .insert_new("Test subject")
        .await
        .unwrap();
    let participation_pend = db
        .publication_participation
        .insert_new_participation(
            &snowflake.from_internal(publication_pend.id).unwrap(),
            &snowflake.from_internal(role.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer.id).unwrap(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();
    let participation_act = db
        .publication_participation
        .insert_new_participation(
            &snowflake.from_internal(publication_act.id).unwrap(),
            &snowflake.from_internal(role.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer.id).unwrap(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();

    db.publication_participation
        .update_participation_state(
            &snowflake
                .from_internal(participation_act.participation_id)
                .unwrap(),
            &db::ParticipationState::Active,
        )
        .await
        .unwrap();

    let pending_notification = db
        .publication_participation
        .get_pending_participation_for_participant(&snowflake.from_internal(subject.id).unwrap())
        .await
        .unwrap();

    assert_that!(pending_notification).does_not_contain(&(
        participation_act,
        publication_act,
        referrer.clone(),
    ));
    assert_that!(pending_notification).contains(&(
        participation_pend,
        publication_pend,
        referrer.clone(),
    ));
}
