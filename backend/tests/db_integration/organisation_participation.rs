use backend::{
    db::{self, methods::*},
    snowflake::SnowflakeConverter,
};
use chrono::Local;
use spectral::prelude::*;
use tracing_test::traced_test;

use crate::common::{get_database, snowflake_manager};

#[tokio::test]
#[traced_test]
async fn create_and_accept_participation() {
    let db = get_database().await;
    let snowflake = snowflake_manager();

    let organisation = db
        .organisation
        .insert_new("Test organisation")
        .await
        .unwrap();
    let subject = db
        .academic_participants
        .insert_new("Test subject")
        .await
        .unwrap();
    let referrer = db
        .academic_participants
        .insert_new("Test referrer")
        .await
        .unwrap();
    let role = db.organisation_role.insert_new("Some role").await.unwrap();
    let participation = db
        .organisation_participation
        .insert_new_participation(
            &snowflake.from_internal(organisation.id).unwrap(),
            &snowflake.from_internal(role.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer.id).unwrap(),
            &Local::now().date_naive(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();

    assert_that!(participation.organisation_id).is_equal_to(organisation.id);
    assert_that!(participation.role_id).is_equal_to(role.id);
    assert_that!(participation.subject_id).is_equal_to(subject.id);
    assert_that!(participation.referrer_id).is_equal_to(referrer.id);
    assert_that!(participation.stopped_at).is_none();
    assert_that!(participation.participation_state).is_equal_to(db::ParticipationState::Pending);

    let new_participation = db
        .organisation_participation
        .update_participation_state(
            &snowflake.from_internal(participation.id).unwrap(),
            &db::ParticipationState::Active,
        )
        .await
        .unwrap();

    assert_that!(new_participation).is_equal_to(db::OrganisationParticipation {
        participation_state: db::ParticipationState::Active,
        ..participation
    })
}

#[tokio::test]
#[traced_test]
async fn list_pending_participations() {
    let db = get_database().await;
    let snowflake = snowflake_manager();

    let role_a = db
        .organisation_role
        .insert_new("Some role A")
        .await
        .unwrap();
    let role_b = db
        .organisation_role
        .insert_new("Some role B")
        .await
        .unwrap();
    let role_c = db
        .organisation_role
        .insert_new("Some role C")
        .await
        .unwrap();

    let organisation_1 = db
        .organisation
        .insert_new("Test organisation 1")
        .await
        .unwrap();
    let referrer_11 = db
        .academic_participants
        .insert_new("Test referrer 1 1")
        .await
        .unwrap();
    let referrer_12 = db
        .academic_participants
        .insert_new("Test referrer 1 2")
        .await
        .unwrap();

    let organisation_2 = db
        .organisation
        .insert_new("Test organisation 2")
        .await
        .unwrap();
    let referrer_21 = db
        .academic_participants
        .insert_new("Test referrer 2 1")
        .await
        .unwrap();
    let referrer_22 = db
        .academic_participants
        .insert_new("Test referrer 2 2")
        .await
        .unwrap();

    let subject = db
        .academic_participants
        .insert_new("Test subject")
        .await
        .unwrap();

    let participation_11a = db
        .organisation_participation
        .insert_new_participation(
            &snowflake.from_internal(organisation_1.id).unwrap(),
            &snowflake.from_internal(role_a.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer_11.id).unwrap(),
            &Local::now().date_naive(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();
    let participation_12b = db
        .organisation_participation
        .insert_new_participation(
            &snowflake.from_internal(organisation_1.id).unwrap(),
            &snowflake.from_internal(role_b.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer_12.id).unwrap(),
            &Local::now().date_naive(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();
    let participation_21a = db
        .organisation_participation
        .insert_new_participation(
            &snowflake.from_internal(organisation_2.id).unwrap(),
            &snowflake.from_internal(role_a.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer_21.id).unwrap(),
            &Local::now().date_naive(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();
    let participation_21b = db
        .organisation_participation
        .insert_new_participation(
            &snowflake.from_internal(organisation_2.id).unwrap(),
            &snowflake.from_internal(role_b.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer_21.id).unwrap(),
            &Local::now().date_naive(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();
    let participation_22c = db
        .organisation_participation
        .insert_new_participation(
            &snowflake.from_internal(organisation_2.id).unwrap(),
            &snowflake.from_internal(role_c.id).unwrap(),
            &snowflake.from_internal(subject.id).unwrap(),
            &snowflake.from_internal(referrer_22.id).unwrap(),
            &Local::now().date_naive(),
            &db::ParticipationState::Pending,
        )
        .await
        .unwrap();

    db.organisation_participation
        .update_participation_state(
            &snowflake.from_internal(participation_21b.id).unwrap(),
            &db::ParticipationState::Active,
        )
        .await
        .unwrap();

    let pending_notification = db
        .organisation_participation
        .get_pending_participation_for_user(&snowflake.from_internal(subject.id).unwrap())
        .await
        .unwrap();

    assert_that!(pending_notification).does_not_contain(&(
        participation_21b,
        organisation_2.clone(),
        referrer_21.clone(),
    ));
    assert_that!(pending_notification).contains_all_of(&[
        &(participation_11a, organisation_1.clone(), referrer_11),
        &(participation_12b, organisation_1.clone(), referrer_12),
        &(participation_21a, organisation_2.clone(), referrer_21),
        &(participation_22c, organisation_2.clone(), referrer_22),
    ]);
}
