use backend::db::DatabaseProvider;
use backend::AppConfig;

use backend::snowflake::{SnowflakeConfig, SnowflakeManager};
use ctor::ctor;
use refinery::embed_migrations;
use tokio::runtime::Runtime;
embed_migrations!("../migrations");

pub fn config() -> AppConfig {
    AppConfig::figment().extract().unwrap()
}

pub fn snowflake_manager() -> SnowflakeManager {
    SnowflakeManager::from(&SnowflakeConfig {
        secret: "some supersecret".to_string(),
        type_nonce_cache_size: 100,
    })
}

pub async fn get_database() -> DatabaseProvider {
    let pool = config().db.build_db_pool().await.unwrap();
    DatabaseProvider::from_db_pool(pool)
}

#[ctor]
fn run_migrations() {
    let rt = Runtime::new().unwrap();
    rt.block_on(async {
        let db_pool = config().db.build_db_pool().await.unwrap();
        backend::db::apply_migrations(&db_pool).await.unwrap();
    })
}
