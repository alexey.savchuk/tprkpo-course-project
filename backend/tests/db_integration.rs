mod common;
mod db_integration {
    mod academic_users;
    mod organisation_participation;
    mod publication_participation;
}
