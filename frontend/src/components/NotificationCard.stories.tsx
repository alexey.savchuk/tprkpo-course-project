import { Meta, StoryObj } from "@storybook/react";
import { NotificationCard } from "./NotificationCard";

export default {
    title: "Elements/NotificationCard",
    component: NotificationCard,
} as Meta<typeof NotificationCard>;


export const ErrorNotification: StoryObj<typeof NotificationCard> = {
    ...NotificationCard,
    args: {
        mdText: "*Error* real __bad__."
    },
};
