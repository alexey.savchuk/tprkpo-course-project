import {
  KnownParticipant,
  Participant,
  Role,
  searchParticipants,
  searchPublicationRoles,
} from "lib/api";
import { useState } from "react";
import { SearchPicker } from "./SearchPicker";

export interface KnownParticipantFormProps {
  onCommit: (known_participants: KnownParticipant) => void;
  onAbort: () => void;
}

export function KnownParticipantForm({
  onCommit,
  onAbort,
}: KnownParticipantFormProps) {
  const [role, setRole] = useState<Role | null>(null);
  const [participant, setParticipant] = useState<Participant | null>(null);
  const checkSubmit = () => {
    if (role == null || participant == null) return;
    const known_participant = {
      ...participant,
      role: role,
    } as KnownParticipant;
    onCommit(known_participant);
  };
  return (
    <div className="px-3 pt-5 pb-3 w-96">
      <div className="text-lg mb-2">Choose a known participant</div>
      <SearchPicker
        hint="Find a participant"
        searchFunc={searchParticipants}
        selectionDisplay={(participant: Participant) => participant.name}
        onChangeSelection={setParticipant}
      />
      <SearchPicker
        hint="Find a publication role"
        searchFunc={searchPublicationRoles}
        selectionDisplay={(role: Role) => role.name}
        onChangeSelection={setRole}
      />
      <div className="w-full inline-flex justify-end gap-4 mt-3">
        <div
          className="cursor-pointer border-2 border-green-600 px-3 py-2 rounded-md"
          onClick={onAbort}
        >
          Cancel
        </div>
        <div
          className="cursor-pointer border-2 border-green-600 bg-green-600 px-3 py-2 rounded-md text-white"
          onClick={checkSubmit}
        >
          Add
        </div>
      </div>
    </div>
  );
}
