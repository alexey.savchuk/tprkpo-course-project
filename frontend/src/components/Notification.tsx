import { acceptOrganisationInvitation } from "lib/api"
import { useState } from "react"

export interface NotificationProps {
    id: string,
    who: string,
    where: string,
    type: "publication" | "organisation",
    onAccept: () => Promise<void>,
    onReject: () => Promise<void>
}

export function Notification({
        id,
        who,
        where,
        type,
        onAccept,
        onReject
    }: NotificationProps
) {
    let [isDisplay, setDisplay] = useState(true)

    return <>
        {isDisplay && <div className="border-solid border-2 border-black m-2">
            <div className="p-3">
                <div className="flex flex-col items-center text-xl">
                    <div className="font-bold">{who}</div>
                    <div>
                        <span>wants to add you to the following&nbsp;</span>
                        <span>{type}</span>
                    </div>
                    <div className="font-bold">{where}</div>
                </div>
                <div className="flex justify-center gap-5">
                    <div
                        onClick={() => {
                            onAccept().then(() => setDisplay(false))
                        }}
                        className="mt-2 w-fit flex px-3 py-2 bg-green-600 rounded-md cursor-pointer"
                    >
                        <span className="text-white text-xl font-semibold">Accept</span>
                    </div>
                    <div
                        onClick={() => {
                            onReject().then(() => setDisplay(false))
                        }}
                        className="mt-2 w-fit flex px-3 py-2 bg-red-600 rounded-md cursor-pointer"
                    >
                        <span className="text-white text-xl font-semibold">Reject</span>
                    </div>
                </div>
            </div>
        </div>}
    </>
}