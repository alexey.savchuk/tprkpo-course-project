"use client";
import { debounce } from "lodash";
import { ReactNode, useCallback, useEffect, useRef, useState, useTransition } from "react";
import { useDebounceCallback } from "usehooks-ts";

export interface SearchPickerProps<T> {
  hint: string,
  searchFunc: (search: string, n: number) => Promise<Array<T>>;
  selectionDisplay: (selection: T) => string;
  selectionRender?: (selection: T) => ReactNode;
  onChangeSelection?: (selection: T | null) => void;
}

interface InactivePicker<T> {
  active: false;
  selection: T | null;
}
interface ActivePicker {
  active: true;
  input: string;
}
type PickerState<T> = InactivePicker<T> | ActivePicker;

export function SearchPicker<T>({
  hint,
  searchFunc,
  selectionDisplay,
  selectionRender,
  onChangeSelection,
}: SearchPickerProps<T>) {
  selectionRender = selectionRender ?? selectionDisplay;
  const [options, setOptions] = useState<Array<T>>([]);
  const [state, setState] = useState<PickerState<T>>({
    active: false,
    selection: null,
  });
  const [isTransitioning, startTransition] = useTransition();
  const inputElem = useRef<HTMLInputElement>(null);

  const triggerSearch = useCallback(
    (search: string) => {
      console.log("1")
      startTransition(() => {
        console.log("2")
        setOptions([]);
        searchFunc(search, 10).then((opts) => setOptions(opts));
      })
    },
    [searchFunc, setOptions]
  );
  // useEffect(() => () => triggerSearch.cancel(), [triggerSearch]);
  const onInputChangeImmediate = useCallback((
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    console.log("input change", event);
    const newInput = event.target.value ?? "";
    setState({ active: true, input: newInput });
    triggerSearch(newInput);
  }, [triggerSearch, setState]);

  const onChooseSelection = (selection: T) => {
    setOptions([]);
    setState({ active: false, selection: selection });
    onChangeSelection?.(selection);
  };

  const inputValue = state.active
    ? state.input
    : state.selection
    ? selectionDisplay(state.selection)
    : "";
  const bgcolor = state.active
    ? "bg-slate-100"
    : state.selection
    ? "bg-slate-300"
    : "bg-slate-100 ";
  const renderOptions =
    inputElem.current === document.activeElement && options.length > 0;
  return (
    <div className="relative">
      <div className="relative">
        <input
          ref={inputElem}
          type="text"
          placeholder={hint}
          value={inputValue}
          onChange={onInputChangeImmediate}
          className={`p-2 my-1 w-full bg-slate-100 rounded-md ${bgcolor}`}
        />
      </div>
      {renderOptions && (
        <div className="absolute table w-full mt-2 border-2 rounded-md z-50 shadow-lg overscroll-auto">
          <div className="table-row-group">
            {options.map((opt, i) => (
              <div
                key={i}
                onClick={() => onChooseSelection?.(opt)}
                className="table-row bg-white hover:bg-slate-50 border-b-slate-500 cursor-pointer select-none"
              >
                <div className="table-cell py-4 px-3">
                  {selectionRender?.(opt)}
                </div>
              </div>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}
