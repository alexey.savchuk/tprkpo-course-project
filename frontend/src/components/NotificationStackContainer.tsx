import { NotificationCard, NotificationCardProps } from "./NotificationCard";

export interface NotificationStackContainerProps {
  notifications: Array<string>;
  onClose: (ix: number) => void;
}

export function NotificationStackContainer({
  notifications,
  onClose,
}: NotificationStackContainerProps) {
  return (
    <div className="fixed right-0 bottom-0 mr-4 mb-4 flex flex-col-reverse items-end gap-4 z-50">
      {notifications.map((notif, i) => (
        <NotificationCard key={i} mdText={notif} onClose={() => onClose(i)} />
      ))}
    </div>
  );
}
