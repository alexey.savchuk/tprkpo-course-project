"use client";

import { SubjectCode } from "lib/api";

export interface SubjectCodeBadgeProps {
  code: SubjectCode;
}

export const SubjectCodeBadge = ({ code }: SubjectCodeBadgeProps) => (
  <div
    className="inline-flex text-xs w-fit px-2 rounded-xl bg-green-300 text-green-900 hover:underline cursor-pointer"
  >
    <div className="self-center inline-flex gap-1">
      <span>{code.code}</span>
      <span>{code.description}</span>
    </div>
  </div>
);
