"use client";

import { ParticipantPublications, Publications } from "lib/api";
import Link from "next/link";
import React from "react";
import { RoleBadge } from "./RoleBadge";
import { SubjectCodeBadge } from "./SubjectCodeBadge";

export interface ParticipantPublicationTableProps {
  publications: ParticipantPublications;
}

export const ParticipantPublicationTable = ({
  publications,
}: ParticipantPublicationTableProps) => {
  return (
    <div className="table w-full">
      <div className="table-row-group">
        {publications.map((publication) => (
          <div key={publication.id} className="table-row hover:bg-slate-50">
            <div className="table-cell py-4 border-b border-slate-500">
              <Link href={`/publications/${publication.id}`}>
                <span className="hover:underline">{publication.title}</span>
              </Link>
            </div>
            <div className="table-cell py-4 border-b border-slate-500">
              <div className="inline-flex gap-1 place-items-center">
                <SubjectCodeBadge code={publication.subject_code} />
                <div className="grow"></div>
              </div>
            </div>
            <div className="table-cell py-4 border-b border-slate-500">
              <div className="inline-flex gap-1">
                <RoleBadge name={publication.role.name} />
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
