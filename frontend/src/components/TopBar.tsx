"use client";
import { FaArrowLeft } from "react-icons/fa";
import { useRouter } from "next/navigation";
import Link from "next/link";
import { useCallback } from "react";
import { useLogout, useSessionMaybe } from "lib/SessionManager";
import { Session } from "lib/ory";

export function TopBar() {
  const router = useRouter();
  const back = useCallback(() => router.back(), [router]);
  const sessionMaybe = useSessionMaybe();
  const logout = useLogout();

  const loginRegister = (
    <div className="container mx-auto h-full flex items-stretch">
      <Link href={`/account/login`}>
        <div className="hover:bg-green-800 px-3 h-full flex">
          <span className="self-center select-none">Login</span>
        </div>
      </Link>
      <Link href={`/account/register`}>
        <div className="hover:bg-green-800 px-3 h-full flex">
          <span className="self-center select-none">Register</span>
        </div>
      </Link>
    </div>
  );
  const accountInfo = ({
    identity: {
      traits: { email },
    },
  }: Session) => (
    <div className="container mx-auto h-full flex items-stretch">
      <Link href={`/account/me`}>
        <div className="hover:bg-green-800 px-3 h-full flex">
          <span className="self-center select-none">{email}</span>
        </div>
      </Link>
      <div
        className="hover:bg-green-800 px-3 h-full flex cursor-pointer"
        onClick={() => {
          logout()
          router.push("/")
        }}
      >
        <span className="self-center select-none">Logout</span>
      </div>
    </div>
  );

  return (
    <div className="sticky h-14 bg-green-600 mb-8">
      <div className="relative contents">
        <div
          className="absolute left-0 top-0 bottom-0 flex items-stretch"
          onClick={back}
        >
          <div className="ml-3 cursor-pointer flex">
            <FaArrowLeft
              size={"1.3rem"}
              className="m-1 fill-gray-800 self-center"
            />
          </div>
        </div>
        <div className="flex h-full container mx-auto justify-between">
          <div className="h-full flex items-stretch">
            <Link href={`/`}>
              <div className="hover:bg-green-800 px-3 h-full flex">
                <span className="self-center select-none">Home</span>
              </div>
            </Link>
            <Link href={`/organisations/`}>
              <div className="hover:bg-green-800 px-3 h-full flex">
                <span className="self-center select-none">Organisations</span>
              </div>
            </Link>
            <Link href={`/publications/`}>
              <div className="hover:bg-green-800 px-3 h-full flex">
                <span className="self-center select-none">Publications</span>
              </div>
            </Link>
            <Link href={`/participants/`}>
              <div className="hover:bg-green-800 px-3 h-full flex">
                <span className="self-center select-none">Participants</span>
              </div>
            </Link>
          </div>

          <div>
            {!sessionMaybe && loginRegister}
            {sessionMaybe && accountInfo(sessionMaybe)}
          </div>
        </div>
      </div>
    </div>
  );
}
