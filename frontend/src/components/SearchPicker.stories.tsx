import {
  Organisation,
  Participant,
  Publication,
  Role,
  searchApplicationRoles,
  searchOrganisationRoles,
  searchOrganisations,
  searchParticipants,
  searchPublicationRoles,
  searchPublications,
} from "lib/api";
import type { Meta, StoryObj } from "@storybook/react";
import { SearchPicker } from "./SearchPicker";

export default {
  title: "Elements/SearchPicker",
  component: SearchPicker,
} as Meta<typeof SearchPicker>;

// export const AccountPicker: StoryObj<typeof SearchPicker<Account>> = {
//   ...SearchPicker,
//   args: {
//     hint: "Find an account",
//     searchFunc: searchAccounts,
//     selectionDisplay: (account: Account) =>
//       `${account.username} (${account.email})`,
//   },
// };

export const OrganisationPicker: StoryObj<typeof SearchPicker<Organisation>> = {
  ...SearchPicker,
  args: {
    hint: "Find an organisation",
    searchFunc: searchOrganisations,
    selectionDisplay: (organisation: Organisation) => organisation.name,
  },
};

export const PublicationPicker: StoryObj<typeof SearchPicker<Publication>> = {
  ...SearchPicker,
  args: {
    hint: "Find a publication",
    searchFunc: (s, n) => {
      console.log("search pub");
      return searchPublications(s, n);
    },
    selectionDisplay: (publication: Publication) => publication.title,
  },
};

export const ParticipantPicker: StoryObj<typeof SearchPicker<Participant>> = {
  ...SearchPicker,
  args: {
    hint: "Find a participant",
    searchFunc: searchParticipants,
    selectionDisplay: (participant: Participant) => participant.name,
  },
};

export const PublicationRolePicker: StoryObj<typeof SearchPicker<Role>> = {
  ...SearchPicker,
  args: {
    hint: "Find a publication role",
    searchFunc: searchPublicationRoles,
    selectionDisplay: (role: Role) => role.name,
  },
};

export const OrganisationRolePicker: StoryObj<typeof SearchPicker<Role>> = {
  ...SearchPicker,
  args: {
    hint: "Find an organisation role",
    searchFunc: searchOrganisationRoles,
    selectionDisplay: (role: Role) => role.name,
  },
};

export const ApplicationRolePicker: StoryObj<typeof SearchPicker<Role>> = {
  ...SearchPicker,
  args: {
    hint: "Find an application role",
    searchFunc: searchApplicationRoles,
    selectionDisplay: (role: Role) => role.name,
  },
};
