"use client";

import { Identity } from "lib/ory";
import React from "react";
import { FaUserMinus } from "react-icons/fa";

export interface AdminTableProps {
  admins: Array<Identity>;
  onDelete?: (identity: Identity) => void;
}

export const AdminTable = ({ admins, onDelete }: AdminTableProps) => {
  return (
    <div className="table w-full">
      <div className="table-row-group">
        {admins.map((identity) => (
          <div
            key={identity.traits.email}
            className="table-row hover:bg-slate-50"
          >
            <div className="relative table-cell border-b border-slate-500">
              <div className="flex mt-1" onClick={() => onDelete?.(identity)}>
                <div
                  className="p-1 border-black border-2 rounded-md flex items-center cursor-pointer"
                  role="button"
                  aria-label="Delete admin"
                >
                  <FaUserMinus className="hover:fill-red-600" />
                </div>
              </div>
            </div>
            <div className="table-cell py-4 border-b border-slate-500">
              <span className="hover:underline cursor-pointer">
                {identity.traits.email}
              </span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
