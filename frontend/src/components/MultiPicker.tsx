import { ReactNode } from "react";
import { FaPlus, FaTimes } from "react-icons/fa";

export interface MultiPickerProps<T> {
  picks: ReadonlyArray<T>;
  renderPick: (t: T) => ReactNode;
  deletePick?: (i: number) => void;
  addPick?: () => void;
}

export function MultiPicker<T>({
  picks,
  renderPick,
  deletePick,
  addPick,
}: MultiPickerProps<T>) {
  return (
    <div className="flex flex-wrap">
      {picks.map((pick, i) => (
        <div
          key={i}
          className="h-full p-2 bg-slate-200 rounded-md mr-2 flex justify-between items-center"
        >
          <div className="h-full flex items-centers pr-1">
            {renderPick(pick)}
          </div>
          <div
            className="h-full border-l border-slate-500 flex items-centers pl-1"
            onClick={() => deletePick?.(i)}
          >
            <FaTimes className="hover:fill-red-600 cursor-pointer" />
          </div>
        </div>
      ))}
      <div
        className="py-2 px-3 bg-slate-200 rounded-md flex items-center"
        onClick={() => addPick?.()}
      >
        <FaPlus className="hover:fill-sky-600 cursor-pointer" />
      </div>
    </div>
  );
}
