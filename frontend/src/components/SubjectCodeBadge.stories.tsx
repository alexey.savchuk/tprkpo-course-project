import type { Meta, StoryObj } from "@storybook/react";
import { SubjectCodeBadge } from "./SubjectCodeBadge";

export default {
  title: "Elements/SubjectCodeBadge",
  component: SubjectCodeBadge,
} as Meta<typeof SubjectCodeBadge>;

export const Example: StoryObj<typeof SubjectCodeBadge> = {
  ...SubjectCodeBadge,
  args: {
    code: {
      id: "1",
      code: "09.04.01",
      description: "Программная инженерия",
    },
  },
};
