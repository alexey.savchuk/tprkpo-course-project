import { Meta, StoryObj } from "@storybook/react";
import { NotificationStackContainer } from "./NotificationStackContainer";

export default {
  title: "Elements/NotificationStackContainer",
  component: NotificationStackContainer,
} as Meta<typeof NotificationStackContainer>;

export const Example: StoryObj<typeof NotificationStackContainer> = {
  ...NotificationStackContainer,
  args: {
    notifications: [
      "Notification 1",
      "Notification long 2",
      "Notification 3",
      "Notification longest 4 Even *more* text more more more more more",
      "Notification 5",
    ],
  },
};
