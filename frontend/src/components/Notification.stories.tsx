import { Meta, StoryObj } from "@storybook/react";
import { Notification } from "./Notification";

export default {
    title: "Elements/Notification",
    component: Notification,
} as Meta<typeof Notification>;

export const OrgNotification: StoryObj<typeof Notification> = {
    ...Notification,
    args: {
      id: "abc",
      who: "John Doe",
      type: "organisation",
      where: "UCLA",
    },
};

export const PubNotification: StoryObj<typeof Notification> = {
  ...Notification,
  args: {
    id: "abc",
    who: "John Doe",
    type: "publication",
    where: "Machine Learning & AI",
  },
};