import Markdown from "react-markdown";
import { FaRegCircleXmark } from 'react-icons/fa6'

export interface NotificationCardProps {
  mdText: string | null | undefined;
  onClose?: () => void;
}

export function NotificationCard({ mdText, onClose }: NotificationCardProps) {
  return (
    <div className="relative bg-slate-200 w-fit min-w-36 max-w-96 min-h-12 max-h-96 py-4 px-6 rounded-md">
        <FaRegCircleXmark className="absolute top-2 right-2 hover:bg-slate-400 rounded-full after:ml-2" onClick={onClose} />
        <Markdown>{mdText}</Markdown>
    </div>
  )
}
