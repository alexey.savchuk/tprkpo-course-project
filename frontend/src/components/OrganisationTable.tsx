import { Organisations } from "lib/api";
import Link from "next/link";
import React from "react";

export interface OrganisationTableProps {
  organisations: Organisations;
}

export const OrganisationTable = ({
  organisations,
}: OrganisationTableProps) => {
  return (
    <div className="table w-full">
      <div className="table-row-group">
        {organisations.map((organisation) => (
          <div key={organisation.id} className="table-row hover:bg-slate-50">
            <div className="table-cell py-4 border-b border-slate-500">
              <Link href={`/organisations/${organisation.id}`}>
                <span className="hover:underline cursor-pointer">
                  {organisation.name}
                </span>
              </Link>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
