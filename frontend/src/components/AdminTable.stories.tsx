import type { Meta, StoryObj } from "@storybook/react";
import { AdminTable } from "./AdminTable";

export default {
  title: "Elements/AdminTable",
  component: AdminTable,
} as Meta<typeof AdminTable>;

export const Example: StoryObj<typeof AdminTable> = {
  ...AdminTable,
  args: {
    admins: [
      {
        id: "id-example",
        schema_id: "schema-example",
        schema_url: "http://schema",
        traits: { email: "example@example.com" },
      },
    ],
  },
};
