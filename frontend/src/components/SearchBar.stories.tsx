import type { Meta, StoryObj } from "@storybook/react";
import { SearchBar } from "./SearchBar";

export default {
  title: "Elements/SearchBar",
  component: SearchBar,
} as Meta<typeof SearchBar>;

export const Search: StoryObj<typeof SearchBar> = {
  ...SearchBar,
  args: {
    hint: "Search here",
    isPending: false,
    options: [],
  },
};

export const Pending: StoryObj<typeof SearchBar> = {
  ...SearchBar,
  args: {
    hint: "Search here",
    isPending: true,
    options: [],
  },
};

export const Options: StoryObj<typeof SearchBar<{id: string, display: string}>> = {
  ...SearchBar,
  args: {
    hint: "Search here",
    isPending: false,
    options: [
      { id: "1", display: "Option 1" },
      { id: "2", display: "Option 2" },
    ],
    optionRender: (opt) => (<span>{opt.display}</span>)
  },
};
