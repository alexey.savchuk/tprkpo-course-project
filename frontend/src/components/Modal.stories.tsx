import { Organisation, searchOrganisations } from "lib/api";
import type { Meta, StoryObj } from "@storybook/react";
import { CreateOrgParForm } from "./CreateOrganisationParticipantForm";
import { CreatePublicationForm } from "./CreatePublicationForm";
import { EditRolesForm } from "./EditRolesForm";
import { KnownParticipantForm } from "./KnownParticipantForm";
import Modal from "./Modal";
import { NewParticipantForm } from "./NewParticipantForm";
import { SearchPicker } from "./SearchPicker";

export default {
  title: "Elements/Modal",
  component: Modal,
  argTypes: {
    children: {
      control: {
        type: 'string',
      },
      options: ["Child"],
      mapping: {
        Child: <span>Child element</span>
      }
    }
  }
} as Meta<typeof Modal>;

export const OrgPickerModal: StoryObj<typeof Modal> = {
  ...Modal,
  args: {
    children: (
      <div className="px-3 pt-5 pb-3">
        <SearchPicker
          hint="Find an organisation"
          searchFunc={searchOrganisations}
          selectionDisplay={(organisation: Organisation) => organisation.name}
        />
      </div>
    ),
  },
};

export const CreateOrgParFormModal: StoryObj<typeof Modal> = {
  ...Modal,
  args: {
    children: <CreateOrgParForm onSubmit={(par) => {}} />,
  },
};

export const EditRolesFormModal: StoryObj<typeof Modal> = {
  ...Modal,
  args: {
    children: (
      <EditRolesForm
        initRoles={[{ id: "a", name: "Role A" }]}
        onCommit={(roles) => {}}
        onAbort={() => {}}
      />
    ),
  },
};

export const KnownParticipantFormModal: StoryObj<typeof Modal> = {
  ...Modal,
  args: {
    children: (
      <KnownParticipantForm onCommit={(par) => {}} onAbort={() => {}} />
    ),
  },
};
export const NewParticipantFormModal: StoryObj<typeof Modal> = {
  ...Modal,
  args: {
    children: <NewParticipantForm onCommit={(par) => {}} onAbort={() => {}} />,
  },
};

export const CreatePublicationFormModal: StoryObj<typeof Modal> = {
  ...Modal,
  args: {
    children: (
      <CreatePublicationForm onCommit={(par) => {}} onAbort={() => {}} />
    ),
  },
};
