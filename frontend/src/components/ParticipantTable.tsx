"use client";

import {
  OrganisationParticipant,
  OrganisationParticipants,
  Participant,
} from "lib/api";
import Link from "next/link";
import React from "react";
import { FaUserMinus } from "react-icons/fa";
import { RoleBadge } from "./RoleBadge";

export interface ParticipantTableProps {
  participants: OrganisationParticipants;
  isEditable: boolean;
  onClickRole?: (par: OrganisationParticipant) => void;
  onClickDelete?: (par: OrganisationParticipant) => void;
}

export const ParticipantTable = ({
  participants,
  isEditable,
  onClickRole,
  onClickDelete,
}: ParticipantTableProps) => {
  return (
    <div className="table w-full">
      <div className="table-row-group">
        {participants.map((participant) => (
          <div key={participant.id} className="table-row hover:bg-slate-50">
            <div className="table-cell py-4 border-b border-slate-500">
              <div className="inline-flex gap-2">
                <span className="hover:underline cursor-pointer">
                  <Link href={`/participants/${participant.id}`}>
                    {participant.name}
                  </Link>
                </span>

                <div>
                  {participant.is_historical && <HistoricalBadge />}
                  {participant.is_pending && <PendingBadge />}
                  {participant.is_declined && <DeclinedBadge />}
                </div>
              </div>
            </div>
            <div className="table-cell w-fit py-4 border-b border-slate-500">
              <div className="inline-flex gap-1">
                {participant.roles.map((role) => (
                  <RoleBadge
                    key={role.id}
                    name={role.name}
                    onClick={() => onClickRole?.(participant)}
                  />
                ))}
              </div>
            </div>
            {/* <div className="table-cell py-4 border-b border-slate-500 max-w-[15em] w-1/4">
              <div className="h-2 relative rounded-lg overflow-hidden mr-3">
                <div className="absolute left-0 h-full right-0 bg-gray-400 z-10"></div>
                <div
                  className="absolute left-0 h-full bg-sky-400 z-10"
                  style={{
                    width: `${((participant.performance ?? 1.0) * 100).toFixed(
                      2
                    )}%`,
                  }}
                ></div>
              </div>
            </div> */}
            {/* перенёс эти строки кода в конец, до этого стояло в начале */}
            {isEditable && (
              <div className="relative table-cell w-fit border-b border-slate-500">
                <div
                  className="flex mt-1"
                  onClick={() => onClickDelete?.(participant)}
                >
                  <div className="p-1 border-black border-2 rounded-md flex items-center cursor-pointer">
                    <FaUserMinus className="hover:fill-red-600" />
                  </div>
                </div>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

function HistoricalBadge() {
  return (
    <div className="inline-flex text-xs w-fit px-2 rounded-xl bg-yellow-300 text-yellow-900 hover:underline cursor-pointer">
      <div className="self-center inline-flex gap-1">
        <span>Historic</span>
      </div>
    </div>
  );
}

function PendingBadge() {
  return (
    <div className="inline-flex text-xs w-fit px-2 rounded-xl bg-zinc-300 text-zinc-900 hover:underline cursor-pointer">
      <div className="self-center inline-flex gap-1">
        <span>Pending</span>
      </div>
    </div>
  );
}

function DeclinedBadge() {
  return (
    <div className="inline-flex text-xs w-fit px-2 rounded-xl bg-red-300 text-red-900 hover:underline cursor-pointer">
      <div className="self-center inline-flex gap-1">
        <span>Declined</span>
      </div>
    </div>
  );
}
