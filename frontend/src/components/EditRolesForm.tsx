import { Role, searchOrganisationRoles } from "lib/api";
import { useState } from "react";
import update from "immutability-helper";
import Modal from "./Modal";
import { MultiPicker } from "./MultiPicker";
import { SearchPicker } from "./SearchPicker";

export interface EditRolesFormProps {
  initRoles: Array<Role>;
  onCommit: (roles: Array<Role>) => void;
  onAbort: () => void;
}

export function EditRolesForm({
  initRoles,
  onCommit,
  onAbort,
}: EditRolesFormProps) {
  const [roles, setRoles] = useState<Array<Role>>(initRoles);
  const [isRoleModal, setRoleModal] = useState<boolean>(false);
  const deletePick = (i: number) => {
    setRoles((roles) => update(roles, { $splice: [[i, 1]] }));
  };
  const roleModal = (
    <Modal onClose={() => setRoleModal(false)}>
      <div className="px-3 pt-5 pb-3">
        <SearchPicker
          hint="Find an organisation role"
          searchFunc={searchOrganisationRoles}
          selectionDisplay={(role: Role) => role.name}
          onChangeSelection={(role) => {
            setRoleModal(false);
            if (role) setRoles((roles) => update(roles, { $push: [role] }));
          }}
        />
      </div>
    </Modal>
  );
  return (
    <>
      {isRoleModal && roleModal}
      <div className="px-3 pt-5 pb-3 w-96">
        <div className="text-lg mb-2">Modify roles</div>
        <MultiPicker
          picks={roles}
          renderPick={(role) => <span>{role.name}</span>}
          addPick={() => setRoleModal(true)}
          deletePick={deletePick}
        />
        <div className="w-full inline-flex justify-end gap-4 mt-3">
          <div
            className="cursor-pointer border-2 border-green-600 px-3 py-2 rounded-md"
            onClick={onAbort}
          >
            Cancel
          </div>
          <div
            className="cursor-pointer border-2 border-green-600 bg-green-600 px-3 py-2 rounded-md text-white"
            onClick={() => {
              onCommit(roles);
            }}
          >
            Modify
          </div>
        </div>
      </div>
    </>
  );
}
