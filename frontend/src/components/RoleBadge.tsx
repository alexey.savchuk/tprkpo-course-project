"use client";

export interface RoleBadgeProps {
  name: string;
  onClick?: () => void;
}

export const RoleBadge = ({ name, onClick }: RoleBadgeProps) => (
  <div
    className="inline-flex text-xs w-fit px-2 rounded-xl bg-green-300 text-green-900 hover:underline cursor-pointer"
    onClick={onClick}
  >
    <div className="self-center inline-flex gap-1">
      <span>{name}</span>
    </div>
  </div>
);
