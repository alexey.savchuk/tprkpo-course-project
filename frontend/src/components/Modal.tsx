import React from "react";

export interface ModalProps {
  label?: string;
  onClose?: () => void;
}

export default function Modal({
  label,
  children,
  onClose,
}: React.PropsWithChildren<ModalProps>) {
  return (
    <>
      <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none pointer-events-none">
        <div
          className="relative w-auto my-6 mx-auto max-w-3xl bg-white rounded-md pointer-events-auto"
          role="dialog"
          aria-label={label}
        >
          {children}
        </div>
      </div>
      <div
        className="opacity-25 fixed inset-0 z-40 bg-black"
        onClick={onClose}
      ></div>
    </>
  );
}
