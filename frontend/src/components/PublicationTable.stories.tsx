import type { Meta, StoryObj } from "@storybook/react";
import { PublicationTable } from "./PublicationTable";

export default {
  title: "Elements/PublicationTable",
  component: PublicationTable,
} as Meta<typeof PublicationTable>;

export const Example: StoryObj<typeof PublicationTable> = {
  ...PublicationTable,
  args: {
    publications: [
      {
        id: "abc",
        title: "Paper #1",
        subject_code: { id: "1", code: "09.04.01", description: "PI" },
        published_at: "2020",
      },
      {
        id: "def",
        title: "Paper #2",
        subject_code: { id: "2", code: "09.04.01", description: "PI" },
        published_at: "2020",
      },
    ],
  },
};
