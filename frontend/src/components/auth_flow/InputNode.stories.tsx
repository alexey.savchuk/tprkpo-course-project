import type { Meta, StoryObj } from "@storybook/react";
import { InputNode } from "./InputNode";

export default {
  title: "AuthFlows/InputNodes",
  component: InputNode,
} as Meta<typeof InputNode>;

import registerFresh from "./flows/register_fresh.json";

export const Email: StoryObj<typeof InputNode> = {
  ...InputNode,
  // @ts-ignore
  args: registerFresh.ui.nodes[1],
};

export const Password: StoryObj<typeof InputNode> = {
  ...InputNode,
  // @ts-ignore
  args: registerFresh.ui.nodes[2],
};

import registerWrongPassword from "./flows/register_wrong_password.json";

export const EmailFilled: StoryObj<typeof InputNode> = {
  ...InputNode,
  // @ts-ignore
  args: registerWrongPassword.ui.nodes[1],
};

export const PasswordWrong: StoryObj<typeof InputNode> = {
  ...InputNode,
  // @ts-ignore
  args: registerWrongPassword.ui.nodes[2],
};

export const Submit: StoryObj<typeof InputNode> = {
  ...InputNode,
  // @ts-ignore
  args: registerFresh.ui.nodes[3],
};

export const Hidden: StoryObj<typeof InputNode> = {
  ...InputNode,
  // @ts-ignore
  args: registerFresh.ui.nodes[0],
};