import {
  UiNode,
  UiNodeInputAttributes,
  UiNodeMeta,
  UiText,
} from "@ory/kratos-client";
import { assert } from "console";
import { useState } from "react";
import Markdown from "react-markdown";

export interface InputNodeProps extends UiNode {}

function InputText(
  attributes: UiNodeInputAttributes,
  messages: Array<UiText>,
  meta: UiNodeMeta | undefined
) {
  const [value, setValue] = useState<string>(attributes.value ?? "");
  return (
    <div>
      <input
        name={attributes.name}
        type={attributes.type}
        value={value}
        required={attributes.required}
        disabled={attributes.disabled}
        autoComplete={attributes.autocomplete}
        placeholder={meta?.label?.text}
        onChange={(event) => setValue(event.target.value ?? "")}
        className="p-2 my-1 w-full max-w-96 bg-slate-100 rounded-md"
      />
      {messages.map((message, i) => (
        <Markdown
          key={`message-${i}`}
          className="p-4 my-1 w-full max-w-96 bg-red-200 rounded-md"
        >
          {message.text}
        </Markdown>
      ))}
    </div>
  );
}

function InputButton(
  attributes: UiNodeInputAttributes,
  meta: UiNodeMeta | undefined
) {
  return (
    <button
      name={attributes.name}
      type="submit"
      value={attributes.value ?? ""}
      disabled={attributes.disabled}
      className="p-2 my-1 w-full max-w-96 bg-slate-100 rounded-md"
    >
      {meta?.label?.text}
    </button>
  );
}

export function InputNode({
  type,
  attributes,
  messages,
  meta,
}: InputNodeProps) {
  if (type != "input") {
    return <span>Unsupported input type {type}</span>;
  }
  let attr = attributes as UiNodeInputAttributes;
  switch (attr.type) {
    case "submit":
      return InputButton(attr, meta);
    default:
      return InputText(attr, messages, meta);
  }
}
