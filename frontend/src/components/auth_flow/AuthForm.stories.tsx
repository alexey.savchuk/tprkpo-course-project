import { UiContainer } from "@ory/kratos-client";
import type { Meta, StoryObj } from "@storybook/react";
import { AuthForm } from "./AuthForm";

export default {
  title: "AuthFlows/AuthForm",
  component: AuthForm,
} as Meta<typeof AuthForm>;

import loginFresh from "./flows/login_fresh.json";
export const LoginFresh: StoryObj<typeof AuthForm> = {
  ...AuthForm,
  args: {
    // @ts-ignore
    ui: loginFresh.ui,
  },
};

import loginWrongPass from "./flows/login_wrong_password.json";
export const LoginWrongPassword: StoryObj<typeof AuthForm> = {
  ...AuthForm,
  args: {
    // @ts-ignore
    ui: loginWrongPass.ui,
  },
};

import registerFresh from "./flows/register_fresh.json";
export const RegisterFresh: StoryObj<typeof AuthForm> = {
  ...AuthForm,
  args: {
    // @ts-ignore
    ui: registerFresh.ui,
  },
};

import registerWrongPassword from "./flows/register_wrong_password.json";
export const RegisterWrongPassword: StoryObj<typeof AuthForm> = {
  ...AuthForm,
  args: {
    // @ts-ignore
    ui: registerWrongPassword.ui,
  },
};

import registerEmailTaken from "./flows/register_email_taken.json";
export const RegisterEmailTaken: StoryObj<typeof AuthForm> = {
  ...AuthForm,
  args: {
    // @ts-ignore
    ui: registerEmailTaken.ui,
  },
};

import settingsFresh from "./flows/settings_fresh.json";
export const SettingsFresh: StoryObj<typeof AuthForm> = {
  ...AuthForm,
  args: {
    // @ts-ignore
    ui: settingsFresh.ui,
  },
};

import settingsWrongPassword from "./flows/settings_wrong_password.json";
export const SettingsWrongPassword: StoryObj<typeof AuthForm> = {
  ...AuthForm,
  args: {
    // @ts-ignore
    ui: settingsWrongPassword.ui,
  },
};
