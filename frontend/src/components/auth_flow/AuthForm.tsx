import { UiContainer, UiNodeInputAttributes } from "@ory/kratos-client";
import Markdown from "react-markdown";
import { InputNode } from "./InputNode";
import { set } from "lodash";
import { SyntheticEvent } from "react";

export interface AuthFormProps {
  ui: UiContainer;
  onSubmit: (data: any) => void;
}

export function AuthForm(props: AuthFormProps) {
  return (
    <div className="relative px-3 pt-5 pb-3">
      <div>
        <form
          // @ts-ignore
          onSubmit={(event: SyntheticEvent<SubmitEvent>) => {
            event.preventDefault();
            const formData = new FormData(
              event.target as HTMLFormElement,
              // @ts-ignore
              event.nativeEvent.submitter as HTMLElement,
            );
            let data = {};
            formData.forEach((value, key) => set(data, key, value));
            props.onSubmit(data);
          }}
          className="grid grid-cols-1"
        >
          {props.ui.messages?.map((message, i) => (
          <Markdown
            key={`message-${i}`}
            className="p-4 my-1 w-full max-w-96 bg-red-200 rounded-md"
          >
            {message.text}
          </Markdown>
        ))}
          {props.ui.nodes.map((node, i) => {
            if (node.type === "input")
              return (
                <InputNode
                  key={`input-${i}`}
                  {...node}
                />
              );
          })}
        </form>
      </div>
    </div>
  );
}
