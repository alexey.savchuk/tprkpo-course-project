"use client";
import {
  CreatePublicationRequest,
  KnownParticipant,
  KnownParticipants,
  NewParticipant,
  NewParticipants,
  Publication,
  Publications,
  searchPublications,
  searchSubjectCodes,
  SubjectCode,
} from "lib/api";
import update from "immutability-helper";
import DatePicker from "react-datepicker";
import { useState } from "react";
import { KnownParticipantForm } from "./KnownParticipantForm";
import Modal from "./Modal";
import { MultiPicker } from "./MultiPicker";
import { NewParticipantForm } from "./NewParticipantForm";
import { RoleBadge } from "./RoleBadge";
import { SearchPicker } from "./SearchPicker";
import { SubjectCodeBadge } from "./SubjectCodeBadge";

export interface CreateOrgParFormProps {
  onCommit: (req: CreatePublicationRequest) => void;
  onAbort: () => void;
}

export function CreatePublicationForm({
  onCommit,
  onAbort,
}: CreateOrgParFormProps) {
  const [title, setTitle] = useState<string>("");
  const [publishedAt, setPublishedAt] = useState<Date | null>(null);
  const [subjectCode, setSubjectCode] = useState<SubjectCode | null>(null);
  const [knownParts, setKnownParts] = useState<KnownParticipants>([]);
  const [newParts, setNewParts] = useState<NewParticipants>([]);
  const [references, setReferences] = useState<Publications>([]);

  const [isKnownModal, setKnownModal] = useState<boolean>(false);
  const [isNewModal, setNewModal] = useState<boolean>(false);
  const [isRefModal, setRefModal] = useState<boolean>(false);
  const [reference, setReference] = useState<Publication | null>(null);

  const submit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (title === "" || subjectCode === null || publishedAt === null) return;
    const request = {
      title: title,
      subject_code: subjectCode,
      published_at: publishedAt.getDate(),
      known_participants: knownParts,
      new_participants: newParts,
      references: references,
    } as CreatePublicationRequest;
    onCommit(request);
  };

  const changeTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newTitle = event.target.value ?? "";
    setTitle(newTitle);
  };
  const addKnown = () => {
    setKnownModal(true);
  };
  const deleteKnown = (i: number) => {
    setKnownParts((known) => update(known, { $splice: [[i, 1]] }));
  };
  const addNew = () => {
    setNewModal(true);
  };
  const deleteNew = (i: number) => {
    setNewParts((parts) => update(parts, { $splice: [[i, 1]] }));
  };
  const addRef = () => {
    setRefModal(false);
    if (reference)
      setReferences((refs) => update(refs, { $push: [reference] }));
  };
  const deleteRef = (i: number) => {
    setReferences((refs) => update(refs, { $splice: [[i, 1]] }));
  };

  const knownModal = (
    <Modal onClose={() => setKnownModal(false)}>
      <KnownParticipantForm
        onCommit={(par) => {
          setKnownParts((parts) => update(parts, { $push: [par] }));
          setKnownModal(false);
        }}
        onAbort={() => {
          setKnownModal(false);
        }}
      />
    </Modal>
  );
  const newModal = (
    <Modal onClose={() => setNewModal(false)}>
      <NewParticipantForm
        onCommit={(par) => {
          setNewParts((parts) => update(parts, { $push: [par] }));
          setNewModal(false);
        }}
        onAbort={() => {
          setNewModal(false);
        }}
      />
    </Modal>
  );
  const refModal = (
    <Modal onClose={() => setRefModal(false)}>
      <div className="px-3 pt-5 pb-3 w-96">
        <div className="text-lg mb-2">Choose referenced publication</div>
        <SearchPicker
          hint="Find a publication"
          searchFunc={searchPublications}
          selectionDisplay={(pub: Publication) => pub.title}
          onChangeSelection={setReference}
        />
        <div className="w-full inline-flex justify-end gap-4 mt-3">
          <div
            className="cursor-pointer border-2 border-green-600 px-3 py-2 rounded-md"
            onClick={() => setRefModal(false)}
          >
            Cancel
          </div>
          <div
            className="cursor-pointer border-2 border-green-600 bg-green-600 px-3 py-2 rounded-md text-white"
            onClick={addRef}
          >
            Add
          </div>
        </div>
      </div>
    </Modal>
  );
  const renderKnown = (part: KnownParticipant) => (
    <div>
      <span>{part.name}</span> <RoleBadge name={part.role.name} />
    </div>
  );
  const renderNew = (part: NewParticipant) => (
    <div>
      <span>{part.name}</span> <RoleBadge name={part.role.name} />
    </div>
  );
  const renderRef = (pub: Publication) => (
    <div>
      <span>{pub.title}</span> <SubjectCodeBadge code={pub.subject_code} />
    </div>
  );
  return (
    <div className="relative px-3 pt-5 pb-3 w-96">
      {isKnownModal && knownModal}
      {isNewModal && newModal}
      {isRefModal && refModal}
      <span className="text-xl font-bold pl-2">Create new participant</span>
      <div>
        <form onSubmit={submit}>
          <div>
            <input
              type="text"
              placeholder="Title"
              value={title}
              onChange={changeTitle}
              className="p-2 my-1 w-full bg-slate-100 rounded-md"
            />
          </div>
          <div>
            <div className="p-2 my-1 w-full bg-slate-100 rounded-md">
              <DatePicker
                selected={publishedAt}
                onChange={(date) => setPublishedAt(date)}
              />
            </div>
          </div>
          <div>
            <SearchPicker
              hint="Choose a subject code"
              searchFunc={searchSubjectCodes}
              selectionDisplay={(subject_code: SubjectCode) =>
                subject_code.code
              }
              selectionRender={(subject_code: SubjectCode) => (
                <SubjectCodeBadge code={subject_code} />
              )}
              onChangeSelection={setSubjectCode}
            />
          </div>
          <div>
            <span>Known participants:</span>
            <MultiPicker
              picks={knownParts}
              renderPick={renderKnown}
              addPick={addKnown}
              deletePick={deleteKnown}
            />
          </div>
          <div>
            <span>New participants:</span>
            <MultiPicker
              picks={newParts}
              renderPick={renderNew}
              addPick={addNew}
              deletePick={deleteNew}
            />
          </div>
          <div>
            <span>References:</span>
            <MultiPicker
              picks={references}
              renderPick={renderRef}
              addPick={() => setRefModal(true)}
              deletePick={deleteRef}
            />
          </div>
          <div className="pl-2 mt-2 w-full inline-flex justify-end">
            <input
              type="submit"
              value="Create"
              className="cursor-pointer bg-green-600 px-3 py-2 rounded-md text-white"
            />
          </div>
        </form>
      </div>
    </div>
  );
}
