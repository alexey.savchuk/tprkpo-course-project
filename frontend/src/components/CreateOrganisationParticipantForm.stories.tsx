import type { Meta, StoryObj } from "@storybook/react";
import { CreateOrgParForm } from "./CreateOrganisationParticipantForm";

export default {
  title: "Elements/CreateOrgParForm",
  component: CreateOrgParForm,
} as Meta<typeof CreateOrgParForm>;

export const Example: StoryObj<typeof CreateOrgParForm> = {
  ...CreateOrgParForm,
  args: {
  },
};
