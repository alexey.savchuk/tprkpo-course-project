import type { Meta, StoryObj } from "@storybook/react";
import { ParticipantTable } from "./ParticipantTable";

export default {
  title: "Elements/ParticipantTable",
  component: ParticipantTable,
} as Meta<typeof ParticipantTable>;

export const Example: StoryObj<typeof ParticipantTable> = {
  ...ParticipantTable,
  args: {
    participants: [
      {
        id: "abc",
        name: "John Doe",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: false,
        is_pending: false,
        is_declined: false,
      },
      {
        id: "def",
        name: "Soichiro Tanaka",
        roles: [
          { id: "2", name: "Professor" },
          { id: "3", name: "Administrator" },
        ],
        performance: 0.3,
        is_historical: false,
        is_pending: false,
        is_declined: false,
      },
    ],
  },
};

export const DifferentTypes: StoryObj<typeof ParticipantTable> = {
  ...ParticipantTable,
  args: {
    participants: [
      {
        id: "1",
        name: "Active Participant",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: false,
        is_pending: false,
        is_declined: false,
      },
      {
        id: "2",
        name: "Historic Participant",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: true,
        is_pending: false,
        is_declined: false,
      },
      {
        id: "3",
        name: "Pending Participant",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: false,
        is_pending: true,
        is_declined: false,
      },
      {
        id: "4",
        name: "Declined Participant",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: false,
        is_pending: false,
        is_declined: true,
      },
    ],
  },
};
