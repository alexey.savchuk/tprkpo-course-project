"use client";
import {
  Participant,
  Role,
  searchOrganisationRoles,
  searchParticipants,
} from "lib/api";
import update from "immutability-helper";
import { useState } from "react";
import Modal from "./Modal";
import { MultiPicker } from "./MultiPicker";
import { SearchPicker } from "./SearchPicker";

export interface CreateOrgParFormProps {
  onSubmit: (participant: Participant, roles: Array<Role>) => void;
}

export function CreateOrgParForm({ onSubmit }: CreateOrgParFormProps) {
  const [participant, setParticipant] = useState<Participant | null>();
  const [roles, setRoles] = useState<Array<Role>>([]);
  const [isRoleModal, setRoleModal] = useState<boolean>(false);
  const submit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!participant || roles.length === 0) return;
    onSubmit?.(participant, roles);
  };
  const addPick = () => {
    setRoleModal(true);
  };
  const deletePick = (i: number) => {
    setRoles((roles) => update(roles, { $splice: [[i, 1]] }));
  };

  const roleModal = (
    <Modal onClose={() => setRoleModal(false)}>
      <div className="px-3 pt-5 pb-3">
        <SearchPicker
          hint="Find an organisation role"
          searchFunc={searchOrganisationRoles}
          selectionDisplay={(role: Role) => role.name}
          onChangeSelection={(role) => {
            setRoleModal(false);
            if (role) setRoles((roles) => update(roles, { $push: [role] }));
          }}
        />
      </div>
    </Modal>
  );
  const renderRole = (role: Role) => <span>{role.name}</span>;
  return (
    <div className="relative px-3 pt-5 pb-3 w-96">
      {isRoleModal && roleModal}
      <span className="text-xl font-bold pl-2">Add a participant</span>
      <div>
        <form onSubmit={submit}>
          <div>
            <SearchPicker 
              hint="Find a participant"
              searchFunc={(q, n) => searchParticipants(q, n)}
              selectionDisplay={(participant: Participant) => participant.name}
              onChangeSelection={setParticipant}
            />
          </div>
          <div>
            <span>Roles:</span>
            <MultiPicker
              picks={roles}
              renderPick={renderRole}
              addPick={addPick}
              deletePick={deletePick}
            />
          </div>
          <div className="pl-2 mt-2 w-full inline-flex justify-end">
            <input
              type="submit"
              value="Create"
              className="cursor-pointer bg-green-600 px-3 py-2 rounded-md text-white"
            />
          </div>
        </form>
      </div>
    </div>
  );
}
