import type { Meta, StoryObj } from "@storybook/react";
import { MultiPicker } from "./MultiPicker";

export default {
  title: "Elements/MultiPicker",
  component: MultiPicker,
  argTypes: {
    addPick: { action: "addPick" },
    deletePick: { action: "deletePick" },
  },
} as Meta<typeof MultiPicker>;

export const StringPicker: StoryObj<typeof MultiPicker<string>> = {
  ...MultiPicker,
  args: {
    picks: ["Option 1", "Option 2"],
    renderPick: (s: string) => <span>{s}</span>,
  },
};
