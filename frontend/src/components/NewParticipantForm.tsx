import {
  NewParticipant,
  Role,
  searchPublicationRoles,
} from "lib/api";
import { ChangeEvent, useState } from "react";
import { SearchPicker } from "./SearchPicker";

export interface NewParticipantFormProps {
  onCommit: (known_participants: NewParticipant) => void;
  onAbort: () => void;
}

export function NewParticipantForm({
  onCommit,
  onAbort,
}: NewParticipantFormProps) {
  const [role, setRole] = useState<Role | null>(null);
  const [name, setName] = useState<string>("");
  const changeName = (event: ChangeEvent<HTMLInputElement>) => {
    const newName = event.target.value ?? "";
    setName(newName);
  };
  const checkSubmit = () => {
    if (role == null || name == "") return;
    const new_participant = {
      name: name,
      role: role,
    } as NewParticipant;
    onCommit(new_participant);
  };
  return (
    <div className="px-3 pt-5 pb-3 w-96">
      <div className="text-lg mb-2">Create a new participant</div>
      <div>
        <input
          type="text"
          placeholder="Full name"
          value={name}
          onChange={changeName}
          className="p-2 my-1 w-full bg-slate-100 rounded-md"
        />
      </div>
      <SearchPicker
        hint="Find a publication role"
        searchFunc={searchPublicationRoles}
        selectionDisplay={(role: Role) => role.name}
        onChangeSelection={setRole}
      />
      <div className="w-full inline-flex justify-end gap-4 mt-3">
        <div
          className="cursor-pointer border-2 border-green-600 px-3 py-2 rounded-md"
          onClick={onAbort}
        >
          Cancel
        </div>
        <div
          className="cursor-pointer border-2 border-green-600 bg-green-600 px-3 py-2 rounded-md text-white"
          onClick={checkSubmit}
        >
          Add
        </div>
      </div>
    </div>
  );
}
