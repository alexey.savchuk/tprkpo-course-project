import { debounce } from "lodash";
import React, { ReactNode, useCallback } from "react";
import { FaSearch, FaSpinner } from "react-icons/fa";

export interface SearchBarProps<T> {
  hint: string;
  isPending: boolean;
  options: ReadonlyArray<T>;
  optionRender: (t: T) => ReactNode;
  onInputChange?: (input: string) => void;
  onOptionSelect?: (t: T) => void;
}

export function SearchBar<T>({
  hint,
  isPending,
  options,
  optionRender,
  onInputChange,
  onOptionSelect,
}: SearchBarProps<T>) {
  const [input, setInput] = React.useState<string>("");

  const sendNewInput = useCallback((input: string) => {
    onInputChange?.(input);
  }, [onInputChange]);
  // const sendNewInput = React.useCallback(debouncedChange, [debouncedChange, onInputChange]);
  // React.useEffect(() => () => debouncedChange.cancel(), [debouncedChange, onInputChange]);
  const changeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newInput = event.target.value ?? "";
    setInput(newInput);
    sendNewInput(newInput);
  };
  return (
    <div>
      <div className="w-full inline-flex items-stretch drop-shadow-lg">
        <input
          className="grow px-3 py-4 border-black border-y border-l rounded-l-md"
          onChange={changeInput}
          type="text"
          placeholder={hint}
          value={input}
        />
        {isPending && (
          <div className="border-black border-y flex bg-white">
            <div className="self-center px-1 mr-3">
              <FaSpinner
                size={"2rem"}
                className="fill-slate-600 animate-spin"
              />
            </div>
          </div>
        )}
        <div className="bg-green-600 border-black rounded-r-md border flex cursor-pointer">
          <div className="self-center px-3">
            <FaSearch size={"2rem"} className="fill-white" />
          </div>
        </div>
      </div>
      {options.length > 0 && (
        <div className="table w-full mt-2 border-2 rounded-md shadow-lg">
          <div className="table-row-group">
            {options.map((option, i) => (
              <div
                key={i}
                onClick={() => onOptionSelect?.(option)}
                className="table-row hover:bg-slate-50 border-b-slate-500 cursor-pointer select-none"
              >
                <div className="table-cell py-4 px-3">
                  {optionRender(option)}
                </div>
              </div>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}
