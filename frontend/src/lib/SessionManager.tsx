"use client";

import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { LogoutFlow } from "@ory/kratos-client";
import { Session, kratos } from "./ory";
import axios from "axios";

interface SessionManagerContextProps {
  session?: Session;
  setSession: (session: Session) => void;
  logout: () => void;
}

const SessionContext = createContext<SessionManagerContextProps>({
  setSession: () => { throw new Error("Session Context is not initialized") },
  logout: () => { throw new Error("Session Context is not initialized") },
});

export interface SessionManagerProps {}

export const SessionManager = ({
  children,
}: React.PropsWithChildren<SessionManagerProps>) => {
  const [session, setSession] = useState<Session | undefined>();
  const [logoutFlow, setLogoutFlow] = useState<LogoutFlow | undefined>();

  const updateLogoutFlow = useCallback(() => {
    kratos.createBrowserLogoutFlow().then(({ data }) => {
      setLogoutFlow(data);
    });
  }, [setLogoutFlow]);

  const onSetSession = useCallback(
    (session: Session) => {
      console.log("set session", session);
      setSession(session);
      updateLogoutFlow();
    },
    [setSession, updateLogoutFlow]
  );

  useEffect(() => {
    if (!session) {
      const controller = new AbortController();
      kratos
        .toSession(undefined, {
          signal: controller.signal,
        })
        .then(({ data }) => {
          onSetSession(Session.parse(data));
        })
        .catch((err) => {
          console.error("Not logged in", err);
        });
      return () => controller.abort();
    }
  }, [session, onSetSession]);

  const onLogout = useCallback(() => {
    console.log("Let me out!");
    kratos
      .updateLogoutFlow({
        token: logoutFlow?.logout_token,
      })
      .then(() => {
        setSession(undefined);
        setLogoutFlow(undefined);
      })
      .catch((err) => {
        console.error("Logging out", err);
      });
  }, [logoutFlow]);

  const context = useMemo<SessionManagerContextProps>(
    () => ({
      session: session,
      setSession: onSetSession,
      logout: onLogout,
    }),
    [session, onSetSession, onLogout]
  );

  return (
    <SessionContext.Provider value={context}>
      {children}
    </SessionContext.Provider>
  );
};

export const useSessionMaybe = (): Session | undefined => {
  const { session } = useContext(SessionContext);
  return session;
};

export const useSetSession = (): ((session: Session) => void) => {
  const { setSession } = useContext(SessionContext);
  return setSession;
};

export const useLogout = (): (() => void) => {
  const { logout } = useContext(SessionContext);
  return logout;
};
