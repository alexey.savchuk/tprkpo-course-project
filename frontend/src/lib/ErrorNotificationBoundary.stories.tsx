import { Meta, StoryObj } from "@storybook/react";
import {
  AsyncErrorNotificationBoundary,
  NotificationManager,
} from "./NotificationManager";

export default {
  title: "Systems/ErrorNotificationBoundary",
  component: AsyncErrorNotificationBoundary,
  decorators: [
    (Story) => (
      <NotificationManager>
        <Story />
      </NotificationManager>
    ),
  ],
} as Meta<typeof AsyncErrorNotificationBoundary>;

function NoErrorElem() {
  return <span>No error</span>;
}

function AsyncErrorElem({ text }: { text: string }) {
  Promise.reject(new Error(text)).catch((err) => {
    console.log("test error", err);
    throw err;
  });
  return <span>Oh no!</span>;
}

export const NoError: StoryObj<typeof AsyncErrorNotificationBoundary> = {
  ...AsyncErrorNotificationBoundary,
  args: {
    children: <NoErrorElem />,
  },
};

export const AsyncError: StoryObj<typeof AsyncErrorNotificationBoundary> = {
  ...AsyncErrorNotificationBoundary,
  args: {
    children: <AsyncErrorElem text="Notification text" />,
  },
};
