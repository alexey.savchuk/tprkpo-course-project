import { Meta, StoryObj } from "@storybook/react";
import {
  NotificationManager,
  usePushNotification,
} from "./NotificationManager";
import { useEffect } from "react";

export default {
  title: "Systems/NotificationManager",
  component: NotificationManager,
} as Meta<typeof NotificationManager>;

function NoNotificationsElem() {
  return <span>No notification</span>;
}

function PushNotificationElem({ text }: { text: string }) {
  const pushNotification = usePushNotification();
  useEffect(() => {
    pushNotification(text);
  }, [text, pushNotification]);
  return <span>A notification</span>;
}

export const NoNotifications: StoryObj<typeof NotificationManager> = {
  ...NotificationManager,
  args: {
    children: <NoNotificationsElem />,
  },
};

export const Notification: StoryObj<typeof NotificationManager> = {
  ...NotificationManager,
  args: {
    children: <PushNotificationElem text="Notification text" />,
  },
};

export const MultipleNotifications: StoryObj<typeof NotificationManager> = {
  ...NotificationManager,
  args: {
    children: (
      <div>
        <PushNotificationElem text="Notification text 1" />
        <PushNotificationElem text="Notification text 2" />
        <PushNotificationElem text="Notification text 3" />
        <PushNotificationElem text="Notification text 4" />
      </div>
    ),
  },
};
