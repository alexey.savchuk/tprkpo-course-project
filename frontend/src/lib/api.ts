import axios, { AxiosRequestConfig } from "axios"
import { z } from "zod";
import { Identities, Identity } from "./ory";

const API_ROOT = (() => {
  if (typeof window === 'undefined') {
    // Server-side
    return process.env.NEXT_INTERNAL_API_URL ?? process.env.NEXT_PUBLIC_API_URL ?? 'http://localhost:8080/'
  } else {
    return process.env.NEXT_PUBLIC_API_URL ?? 'http://localhost:8080/'
  }
})()

export const api = axios.create({
  baseURL: `${API_ROOT}`,
})

export const ApiError = z.object({
  message: z.string(),
})
export type ApiError = z.infer<typeof ApiError>;

export const Role = z.object({
  id: z.string(),
  name: z.string(),
});
export type Role = z.infer<typeof Role>;
export const Roles = z.array(Role);
export type Roles = z.infer<typeof Roles>;

export const getAdmins = async (config?: AxiosRequestConfig): Promise<Identities> => {
  const resp = await api.get(`/api/admin/list`, config)
  return Identities.parseAsync(resp.data)
}

export const listAccounts = async (config?: AxiosRequestConfig): Promise<Identities> => {
  const resp = await api.get(`/api/accounts/list`, config)
  return Identities.parseAsync(resp.data)
}

export const searchAccounts = async (search: string, n: number, config?: AxiosRequestConfig): Promise<Identities> => {
  const resp = await api.get(`/api/accounts/list`, {
    params: { search, n },
    ...config
  })
  return Identities.parseAsync(resp.data)
}

export const AdminParams = z.object({
  user_id: Identity,
})
export type AdminParams = z.infer<typeof AdminParams>;

export const addAdmin = async (params: AdminParams, config?: AxiosRequestConfig<AdminParams>): Promise<void> => {
  await api.post(`/api/admin/add`, params, config);
}

export const removeAdmin = async (params: AdminParams, config?: AxiosRequestConfig<AdminParams>): Promise<void> => {
  await api.post(`/api/admin/remove`, params, config);
}

export const AccountMeta = z.object({
  connected_participant: z.string().nullable(),
  is_app_admin: z.boolean(),
});
export type AccountMeta = z.infer<typeof AccountMeta>;

export const getMyAccountMeta = async (config?: AxiosRequestConfig): Promise<AccountMeta> => {
  const resp = await api.get(`/api/accounts/my/meta`, config)
  return AccountMeta.parseAsync(resp.data)
}

export const Organisation = z.object({
  id: z.string(),
  name: z.string(),
});
export type Organisation = z.infer<typeof Organisation>;
export const Organisations = z.array(Organisation);
export type Organisations = z.infer<typeof Organisations>;

export const NewOrganisationParams = z.object({
  name: z.string(),
  admin_id: z.string(),
})
export type NewOrganisationParams = z.infer<typeof NewOrganisationParams>;

export const newOrganisation = async (params: NewOrganisationParams, config?: AxiosRequestConfig): Promise<Organisation> => {
  const resp = await api.post(`/api/organisations/create`, params, config)
  return Organisation.parseAsync(resp.data)
}

export const getOrganisation = async (id: string, config?: AxiosRequestConfig): Promise<Organisation> => {
  const resp = await api.get(`/api/organisations/${id}`, config)
  return Organisation.parseAsync(resp.data)
}

export const listOrganisations = async (config?: AxiosRequestConfig): Promise<Organisations> => {
  const resp = await api.get(`/api/organisations/all`, config)
  return Organisations.parseAsync(resp.data)
}

export const searchOrganisations = async (search: string, n: number, config?: AxiosRequestConfig): Promise<Organisations> => {
  const resp = await api.get(`/api/organisations/search`, {
    params: { search, n },
    ...config
  })
  return Organisations.parseAsync(resp.data)
}

export const getPublicationOrganisations = async (id: string, config?: AxiosRequestConfig): Promise<Organisations> => {
  const resp = await api.get(`/api/publications/${id}/organisations`, config)
  return Organisations.parseAsync(resp.data)
}

export const OrganisationMeta = z.object({
  participants: z.number(),
  publications: z.number(),
  citations: z.number(),
  editable: z.boolean(),
});
export type OrganisationMeta = z.infer<typeof OrganisationMeta>;
export const getOrganisationMeta = async (id: string, config?: AxiosRequestConfig): Promise<OrganisationMeta> => {
  const resp = await api.get(`/api/organisations/${id}/meta`, config)
  return OrganisationMeta.parseAsync(resp.data)
}

export const OrganisationParticipant = z.object({
  id: z.string(),
  name: z.string(),
  roles: Roles,
  performance: z.number().nullable(),
  is_historical: z.boolean(),
  is_pending: z.boolean(),
  is_declined: z.boolean(),
});
export type OrganisationParticipant = z.infer<typeof OrganisationParticipant>;
export const OrganisationParticipants = z.array(OrganisationParticipant);
export type OrganisationParticipants = z.infer<typeof OrganisationParticipants>;

export const OrganisationParticipantsQuery = z.object({
  include_historical: z.boolean().optional(),
  include_pending: z.boolean().optional(),
  include_declined: z.boolean().optional(),
});
export type OrganisationParticipantsQuery = z.infer<typeof OrganisationParticipantsQuery>;

export const getOrganisationParticipants = async (org_id: string, query: OrganisationParticipantsQuery, config?: AxiosRequestConfig): Promise<OrganisationParticipants> => {
  const resp = await api.get(`/api/organisations/${org_id}/participants`, {
    params: query,
    ...config
  })
  return OrganisationParticipants.parseAsync(resp.data)
}

export const SubjectCode = z.object({
  id: z.string(),
  code: z.string(),
  description: z.string(),
});
export type SubjectCode = z.infer<typeof SubjectCode>;
export const SubjectCodes = z.array(SubjectCode);
export type SubjectCodes = z.infer<typeof SubjectCodes>;

export const searchSubjectCodes = async (search: string, n: number, config?: AxiosRequestConfig): Promise<SubjectCodes> => {
  const resp = await api.get(`/api/publications/subject_codes/search`, {
    params: { search, n },
    ...config
  })
  return SubjectCodes.parseAsync(resp.data)
}

export const Publication = z.object({
  id: z.string(),
  title: z.string(),
  published_at: z.string(),
  subject_code: SubjectCode,
});
export type Publication = z.infer<typeof Publication>;
export const Publications = z.array(Publication);
export type Publications = z.infer<typeof Publications>;

export const getPublication = async (id: string, config?: AxiosRequestConfig): Promise<Publication> => {
  const resp = await api.get(`/api/publications/${id}`, config)
  return Publication.parseAsync(resp.data)
}

export const PublicationMeta = z.object({
  editable: z.boolean(),
});
export type PublicationMeta = z.infer<typeof PublicationMeta>;
export const getPublicationMeta = async (id: string, config?: AxiosRequestConfig): Promise<PublicationMeta> => {
  const resp = await api.get(`/api/publications/${id}/meta`, config)
  return PublicationMeta.parseAsync(resp.data)
}

export const listPublications = async (config?: AxiosRequestConfig): Promise<Publications> => {
  const resp = await api.get(`/api/publications/all`, config)
  return Publications.parseAsync(resp.data)
}

export const searchPublications = async (search: string, n: number, config?: AxiosRequestConfig): Promise<Publications> => {
  const resp = await api.get(`/api/publications/search`, {
    params: { search, n },
    ...config
  })
  return Publications.parseAsync(resp.data)
}

export const getOrganisationPublications = async (id: string): Promise<Publications> => {
  const resp = await api.get(`/api/organisations/${id}/publications`)
  return Publications.parseAsync(resp.data)
}

export const PublicationParticipant = z.object({
  id: z.string(),
  name: z.string(),
  role: Role,
});
export type PublicationParticipant = z.infer<typeof PublicationParticipant>;
export const PublicationParticipants = z.array(PublicationParticipant);
export type PublicationParticipants = z.infer<typeof PublicationParticipants>;
export const getPublicationParticipants = async (id: string, config?: AxiosRequestConfig): Promise<PublicationParticipants> => {
  const resp = await api.get(`/api/publications/${id}/participants`, config)
  return PublicationParticipants.parseAsync(resp.data)
}

export const PublicationReferences = z.object({
  from: z.array(Publication),
  to: z.array(Publication),
});
export type PublicationReferences = z.infer<typeof PublicationReferences>;
export const getPublicationReferences = async (id: string, config?: AxiosRequestConfig): Promise<PublicationReferences> => {
  const resp = await api.get(`/api/publications/${id}/references`, config)
  return PublicationReferences.parseAsync(resp.data)
}

export const Participant = z.object({
  id: z.string(),
  name: z.string(),
});
export type Participant = z.infer<typeof Participant>;
export const Participants = z.array(Participant);
export type Participants = z.infer<typeof Participants>;
export const getParticipant = async (id: string, config?: AxiosRequestConfig): Promise<Participant> => {
  const resp = await api.get(`/api/participants/${id}`, config)
  return Participant.parseAsync(resp.data)
}

export const listParticipants = async (config?: AxiosRequestConfig): Promise<Participants> => {
  const resp = await api.get(`/api/participants/all`, config)
  return Participants.parseAsync(resp.data)
}

export const searchParticipants = async (search: string, n: number, config?: AxiosRequestConfig): Promise<Participants> => {
  const resp = await api.get(`/api/participants/search`, {
    params: { search, n },
    ...config
  })
  return Participants.parseAsync(resp.data)
}

export const ParticipantMeta = z.object({
  citations: z.number(),
  editable: z.boolean(),
  connected: z.boolean(),
});
export type ParticipantMeta = z.infer<typeof ParticipantMeta>;
export const getParticipantMeta = async (id: string, config?: AxiosRequestConfig): Promise<ParticipantMeta> => {
  const resp = await api.get(`/api/participants/${id}/meta`, config)
  return ParticipantMeta.parseAsync(resp.data)
}

export const ParticipantOrganisation = z.object({
  id: z.string(),
  name: z.string(),
  roles: Roles,
});
export type ParticipantOrganisation = z.infer<typeof ParticipantOrganisation>;
export const ParticipantOrganisations = z.array(ParticipantOrganisation);
export type ParticipantOrganisations = z.infer<typeof ParticipantOrganisations>;
export const getParticipantOrganisations = async (id: string, config?: AxiosRequestConfig): Promise<ParticipantOrganisations> => {
  const resp = await api.get(`/api/participants/${id}/organisations`, config)
  return ParticipantOrganisations.parseAsync(resp.data)
}

export const ParticipantPublication = z.object({
  id: z.string(),
  title: z.string(),
  subject_code: SubjectCode,
  role: Role,
});
export type ParticipantPublication = z.infer<typeof ParticipantPublication>;
export const ParticipantPublications = z.array(ParticipantPublication);
export type ParticipantPublications = z.infer<typeof ParticipantPublications>;
export const getParticipantPublications = async (id: string, config?: AxiosRequestConfig): Promise<ParticipantPublications> => {
  const resp = await api.get(`/api/participants/${id}/publications`, config)
  return ParticipantPublications.parseAsync(resp.data)
}

export const ParticipantReference = z.object({
  id: z.string(),
  name: z.string(),
  score: z.number()
});
export type ParticipantReference = z.infer<typeof ParticipantReference>;
export const ParticipantReferences = z.array(ParticipantReference);
export type ParticipantReferences = z.infer<typeof ParticipantReferences>;
export const ParticipantReferenceType = z.union([
  z.literal('co_participants'),
  z.literal('referenced_to'),
  z.literal('referenced_from'),
]);
export type ParticipantReferenceType = z.infer<typeof ParticipantReferenceType>;
export const getParticipantReference = async (id: string, t: ParticipantReferenceType, config?: AxiosRequestConfig): Promise<ParticipantReferences> => {
  const resp = await api.get(`/api/participants/${id}/references`, { params: { t: t }, ...config })
  return ParticipantReferences.parseAsync(resp.data)
}

export const searchApplicationRoles = async (search: string, n: number, config?: AxiosRequestConfig): Promise<Roles> => {
  const resp = await api.get(`/api/roles/application/search`, {
    params: { search, n },
    ...config
  })
  return Roles.parseAsync(resp.data)
}

export const searchOrganisationRoles = async (search: string, n: number, config?: AxiosRequestConfig): Promise<Roles> => {
  const resp = await api.get(`/api/roles/organisation/search`, {
    params: { search, n },
    ...config
  })
  return Roles.parseAsync(resp.data)
}

export const searchPublicationRoles = async (search: string, n: number, config?: AxiosRequestConfig): Promise<Roles> => {
  const resp = await api.get(`/api/roles/publication/search`, {
    params: { search, n },
    ...config
  })
  return Roles.parseAsync(resp.data)
}

export const searchOrganisationParticipants = async (org_id: string, search: string, n: number, config?: AxiosRequestConfig): Promise<Roles> => {
  const resp = await api.get(`/api/organisations/${org_id}/participants/search`, {
    params: { search, n },
    ...config
  })
  return Participants.parseAsync(resp.data)
}

export const modifyOrganisationParticipant = async (org_id: string, par_id: string, roles: Roles, config?: AxiosRequestConfig): Promise<void> => {
  await api.put(`/api/organisations/${org_id}/participants/${par_id}`, roles, config);
}

export const deleteOrganisationParticipant = async (org_id: string, par_id: string, config?: AxiosRequestConfig): Promise<void> => {
  await api.delete(`/api/organisations/${org_id}/participants/${par_id}`, config);
}

export const KnownParticipant = z.object({
  id: z.string(),
  name: z.string(),
  role: Role,
})
export type KnownParticipant = z.infer<typeof KnownParticipant>;
export const KnownParticipants = z.array(KnownParticipant)
export type KnownParticipants = z.infer<typeof KnownParticipants>;
export const NewParticipant = z.object({
  name: z.string(),
  role: Role,
})
export type NewParticipant = z.infer<typeof NewParticipant>;
export const NewParticipants = z.array(NewParticipant)
export type NewParticipants = z.infer<typeof NewParticipants>;
export const CreatePublicationRequest = z.object({
  title: z.string(),
  published_at: z.number(),
  subject_code: SubjectCode,
  known_participants: KnownParticipants,
  new_participants: NewParticipants,
  references: z.array(Publication),
});
export type CreatePublicationRequest = z.infer<typeof CreatePublicationRequest>;

export const createPublication = async (req: CreatePublicationRequest, config?: AxiosRequestConfig): Promise<Publication> => {
  const resp = await api.post(`/api/publications/create`, req, config);
  return Publication.parseAsync(resp.data);
}

export const GlobalSearchType = z.union([
  z.literal('publication'),
  z.literal('participant'),
  z.literal('organisation'),
  z.literal('application_user'),
  z.literal('subject_code'),
]);
export type GlobalSearchType = z.infer<typeof GlobalSearchType>;
export const GlobalSearch = z.object({
  item_id: z.string(),
  item_type: GlobalSearchType,
  value: z.string(),
});
export type GlobalSearch = z.infer<typeof GlobalSearch>;
export const GlobalSearches = z.array(GlobalSearch);
export type GlobalSearches = z.infer<typeof GlobalSearches>;
export const globalSearch = async (search: string, n: number, config?: AxiosRequestConfig): Promise<GlobalSearches> => {
  const resp = await api.get(`/api/search`, {
    params: { search, n },
    ...config
  })
  return GlobalSearches.parseAsync(resp.data)
}

export const OrgInvitation = z.object({
  id: z.string(),
  organisation_name: z.string(),
  inviter_name: z.string(),
});
export type OrgInvitation = z.infer<typeof OrgInvitation>;
export const OrgInvitations = z.array(OrgInvitation);
export type OrgInvitations = z.infer<typeof OrgInvitations>;
export const getOrganisationInvitations = async (id: string, config?: AxiosRequestConfig): Promise<OrgInvitations> => {
  const resp = await api.get(`/api/participants/${id}/organisations/invitations`, config)
  return OrgInvitations.parseAsync(resp.data)
}

export const acceptOrganisationInvitation = async (par_id: string, inv_id: string, config?: AxiosRequestConfig): Promise<void> => {
  await api.post(`/api/participants/${par_id}/organisations/invitations`, {
    id: inv_id,
    status: "Accept",
  }, config)
}

export const declineOrganisationInvitation = async (par_id: string, inv_id: string, config?: AxiosRequestConfig): Promise<void> => {
  await api.post(`/api/participants/${par_id}/organisations/invitations`, {
    id: inv_id,
    status: "Decline",
  }, config)
}

export const PubInvitation = z.object({
  id: z.string(),
  publication_title: z.string(),
  inviter_name: z.string(),
});
export type PubInvitation = z.infer<typeof PubInvitation>;
export const PubInvitations = z.array(PubInvitation);
export type PubInvitations = z.infer<typeof PubInvitations>;

export const getPublicationInvitations = async (id: string, config?: AxiosRequestConfig): Promise<PubInvitations> => {
  const resp = await api.get(`/api/participants/${id}/publications/invitations`, config)
  return PubInvitations.parseAsync(resp.data)
}

export const acceptPublicationInvitation = async (par_id: string, inv_id: string, config?: AxiosRequestConfig): Promise<void> => {
  await api.post(`/api/participants/${par_id}/publications/invitations`, {
    id: inv_id,
    status: "Accept",
  }, config)
}

export const declinePublicationInvitation = async (par_id: string, inv_id: string, config?: AxiosRequestConfig): Promise<void> => {
  await api.post(`/api/participants/${par_id}/publications/invitations`, {
    id: inv_id,
    status: "Decline",
  }, config)
}

export const attachAcademicName = async (name: string, config?: AxiosRequestConfig): Promise<Participant> => {
  const resp = await api.post(`/api/accounts/my/attach_participant`, {
    "full_name": name
  }, config)
  return Participant.parseAsync(resp.data)
}