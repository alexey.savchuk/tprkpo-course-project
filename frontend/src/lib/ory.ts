import { Configuration, FrontendApi, Identity as OryIdentity, Session as OrySession } from '@ory/kratos-client'
import { z } from 'zod';

const kratosBasePath = process.env.NEXT_PUBLIC_ORY_SDK_URL || "http://localhost:4433";
export const kratos = new FrontendApi(
  new Configuration({
    basePath: kratosBasePath,
    baseOptions: {
      withCredentials: true,
    },
  })
);

const schemaForType = <T>() => <S extends z.ZodType<T, any, any>>(arg: S) => {
  return arg;
};

export const Traits = z.object({
  email: z.string(),
})

export const Identity = schemaForType<OryIdentity>()(z.object({
  id: z.string(),
  schema_id: z.string(),
  schema_url: z.string(),
  traits: Traits,
}))
export type Identity = z.infer<typeof Identity>;

export const Identities = z.array(Identity)
export type Identities = z.infer<typeof Identities>

export const Session= schemaForType<OrySession>()(z.object({
  id: z.string(),
  identity: Identity,
}))
export type Session = z.infer<typeof Session>;