import { NotificationCardProps } from "components/NotificationCard";
import { createContext, useCallback, useContext, useEffect, useState } from "react";
import update from "immutability-helper";
import { NotificationStackContainer } from "components/NotificationStackContainer";
import React from "react";

interface NotificationManagerContextProps {
  pushNotification: (mdText: string) => void;
}

const NotificationContext = createContext<NotificationManagerContextProps>({
  pushNotification: () => {
    throw new Error("Notification Context is not initialized");
  },
});

export interface NotificationManagerProps {}

export function NotificationManager({
  children,
}: React.PropsWithChildren<NotificationManagerProps>) {
  const [notifications, setNotifications] = useState<Array<string>>([]);

  const pushNotification = useCallback(
    (mdText: string) =>
      setNotifications((notifications) =>
        update(notifications, { $push: [mdText] })
      ),
    []
  );

  const onCloseNotification = useCallback(
    (i: number) =>
      setNotifications((notifications) =>
        update(notifications, { $splice: [[i, 1]] })
      ),
    []
  );

  return (
    <>
      <NotificationStackContainer
        notifications={notifications}
        onClose={onCloseNotification}
      />
      <NotificationContext.Provider
        value={{ pushNotification: pushNotification }}
      >
        {children}
      </NotificationContext.Provider>
    </>
  );
}

export function usePushNotification(): (mdText: string) => void {
  const { pushNotification } = useContext(NotificationContext);
  return pushNotification;
}

export function AsyncErrorNotificationBoundary() {
  const pushNotification = usePushNotification();

  const promiseRejectionHandler = useCallback((event: PromiseRejectionEvent) => {
    pushNotification(event.reason.toString());
  }, [pushNotification]);

  useEffect(() => {
    window.addEventListener("unhandledrejection", promiseRejectionHandler);

    return () => {
      window.removeEventListener("unhandledrejection", promiseRejectionHandler);
    };
  }, [promiseRejectionHandler]);
}
