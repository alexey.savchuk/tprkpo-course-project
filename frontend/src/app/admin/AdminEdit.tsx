"use client";
import Modal from "components/Modal";
import { SearchPicker } from "components/SearchPicker";
import {
  NewOrganisationParams,
  Participant,
  addAdmin,
  getAdmins,
  newOrganisation,
  removeAdmin,
  searchAccounts,
  searchParticipants,
} from "lib/api";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { Admin } from "./Admin";
import { Identity } from "lib/ory";
import update from "immutability-helper";

export function AdminEdit() {
  const router = useRouter();
  const [admins, setAdmins] = useState<Array<Identity>>([]);
  useEffect(() => {
    getAdmins()
      .then((admins) => setAdmins(admins))
      .catch((err) => {
        console.error(err);
        setAdmins([]);
      });
  }, [setAdmins]);

  const [isAddAdminModal, setAddAdminModal] = useState<boolean>(false);
  const onAddAdmin = (identity: Identity | null) => {
    setAddAdminModal(false);
    if (identity)
      addAdmin({ user_id: identity })
        .then(() => getAdmins())
        .then((admins) => setAdmins(admins))
        .catch((err) => console.error(err));
  };
  const addAdminModal = (
    <Modal label="Choose new admin account" onClose={() => setAddAdminModal(false)}>
      <div className="px-3 pt-5 pb-3">
        <SearchPicker
          hint="Choose an account"
          searchFunc={searchAccounts}
          selectionDisplay={({ traits: { email } }: Identity) => email}
          onChangeSelection={onAddAdmin}
        />
      </div>
    </Modal>
  );

  const [isDeleteAdminModal, setDeleteAdminModal] = useState<Identity | null>(
    null
  );
  const onDeleteAdmin = (identity: Identity | null) => {
    setDeleteAdminModal(null);
    if (identity)
      removeAdmin({ user_id: identity })
        .then(() => getAdmins())
        .then((admins) => setAdmins(admins))
        .catch((err) => console.error(err));
  };
  const deleteAdminModal = (identity: Identity) => (
    <Modal label="Confirm delete admin" onClose={() => setDeleteAdminModal(null)}>
      <div className="px-3 pt-5 pb-3">
        <span>Are you sure you want to delete {identity.traits.email}?</span>
        <div className="w-full inline-flex justify-end gap-4 mt-3">
          <div
            className="cursor-pointer border-2 border-green-600 px-3 py-2 rounded-md"
            onClick={() => setDeleteAdminModal(null)}
            role="button"
          >
            Cancel
          </div>
          <div
            className="cursor-pointer border-2 border-red-600 bg-red-600 px-3 py-2 rounded-md text-white"
            onClick={() => {
              onDeleteAdmin(identity);
            }}
            role="button"
          >
            Confirm delete
          </div>
        </div>
      </div>
    </Modal>
  );

  const [isCreateOrganisationModal, setCreateOrganisationModal] =
    useState<NewOrganisationParams | null>(null);
  const createOrganisationModal = (params: NewOrganisationParams) => (
    <Modal onClose={() => setCreateOrganisationModal(null)}>
      <div className="px-3 pt-5 pb-3">
        <input
          type="text"
          placeholder="Title"
          value={params.name}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            setCreateOrganisationModal((params) =>
              update(params, { name: { $set: event.target.value ?? "" } })
            )
          }
          className="p-2 my-1 w-full bg-slate-100 rounded-md"
        />
        <SearchPicker
          hint="Choose an account"
          searchFunc={searchParticipants}
          selectionDisplay={({ name }: Participant) => name}
          onChangeSelection={(participant) =>
            setCreateOrganisationModal((params) =>
              update(params, { admin_id: { $set: participant?.id ?? "" } })
            )
          }
        />
        <div className="pl-2 mt-2 w-full inline-flex justify-end">
          <input
            type="submit"
            value="Create"
            className="cursor-pointer bg-green-600 px-3 py-2 rounded-md text-white"
            onClick={() => {
              if (!params?.admin_id || !params?.name) return;
              newOrganisation({ ...params }).then((org) =>
                router.push(`/organisations/${org.id}`)
              );
            }}
          />
        </div>
      </div>
    </Modal>
  );

  return (
    <>
      {isAddAdminModal && addAdminModal}
      {isDeleteAdminModal && deleteAdminModal(isDeleteAdminModal)}
      {isCreateOrganisationModal &&
        createOrganisationModal(isCreateOrganisationModal)}
      <Admin
        admins={admins}
        onAddAdmin={() => setAddAdminModal(true)}
        onDeleteAdmin={setDeleteAdminModal}
        onAddOrganisation={() =>
          setCreateOrganisationModal({ name: "", admin_id: "" })
        }
      />
    </>
  );
}
