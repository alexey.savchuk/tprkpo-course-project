import { AdminTable } from "components/AdminTable";
import { Identity } from "lib/ory";

export interface AdminProps {
  admins: Array<Identity>;
  onAddAdmin?: () => void;
  onDeleteAdmin?: (identity: Identity) => void;
  onAddOrganisation?: () => void;
}

export function Admin({
  admins,
  onAddAdmin,
  onDeleteAdmin,
  onAddOrganisation,
}: AdminProps) {
  return (
    <div className="grid grid-cols-2">
      <div>
        <span className="text-lg">Admins:</span>
        <div
          className="w-fit cursor-pointer border-2 border-green-600 bg-green-600 px-3 py-2 rounded-md text-white"
          onClick={onAddAdmin}
          role="button"
        >
          Add admin
        </div>
        <AdminTable admins={admins} onDelete={onDeleteAdmin} />
      </div>
      <div>
        <span className="text-lg">Actions:</span>
        <div className="w-full inline-flex gap-4 mt-3">
          <div
            className="cursor-pointer border-2 border-green-600 bg-green-600 px-3 py-2 rounded-md text-white"
            onClick={onAddOrganisation}
            role="button"
          >
            Add organisation
          </div>
        </div>
      </div>
    </div>
  );
}
