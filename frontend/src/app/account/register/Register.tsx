"use client";

import { AuthForm } from "components/auth_flow/AuthForm";
import { useSetSession } from "lib/SessionManager";
import { Session, kratos } from "lib/ory";
import { ErrorGeneric, RegistrationFlow } from "@ory/kratos-client";
import axios, { AxiosError } from "axios";
import { useCallback, useEffect, useState } from "react";
import { ErrorBoundary } from "react-error-boundary";
import { useRouter } from "next/navigation";

interface AuthFormLoadingProps {
  flow: RegistrationFlow;
  updateFlow?: (flow: RegistrationFlow) => void;
  onFail?: (err: ErrorGeneric) => void;
  onSuccess?: (session: Session) => void;
}

const isGenericError = (
  err: RegistrationFlow | ErrorGeneric
): err is ErrorGeneric => "error" in err;

function AuthFormLoading({
  flow,
  updateFlow,
  onSuccess,
  onFail,
}: AuthFormLoadingProps) {
  const onSubmit = useCallback(
    (data: any) => {
      kratos
        .updateRegistrationFlow({ flow: flow.id, updateRegistrationFlowBody: data })
        .then(({ data }) => {
          if (data.session)
            onSuccess?.(Session.parse(data.session));
        })
        .catch((err: AxiosError<RegistrationFlow | ErrorGeneric>) => {
          console.error(err);
          const resp = err.response?.data;
          if (!resp) return;
          if (isGenericError(resp)) onFail?.(resp);
          else updateFlow?.(resp);
        });
    },
    [flow, updateFlow, onFail, onSuccess]
  );
  return <AuthForm ui={flow.ui} onSubmit={onSubmit} />;
}

export function Register() {
  const router = useRouter();
  const [flow, setFlow] = useState<RegistrationFlow | undefined>();
  const setSession = useSetSession();
  useEffect(() => {
    if (!flow) {
      const controller = new AbortController();
      kratos.createBrowserRegistrationFlow(undefined, {
        signal: controller.signal,
      }).then(({data}) => setFlow(data));
      return () => controller.abort()
    }
  }, [flow, setFlow]);
  return (
    <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none pointer-events-none">
      <div className="relative w-auto my-6 mx-auto max-w-3xl bg-white rounded-md pointer-events-auto">
        <ErrorBoundary fallback={<p>Oops!</p>}>
          {flow && <AuthFormLoading
              flow={flow}
              updateFlow={setFlow}
              onSuccess={(session) => {
                setSession(session);
                router.push("/account/me");
              }}
            />}
        </ErrorBoundary>
      </div>
    </div>
  );
}
