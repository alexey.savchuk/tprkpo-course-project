"use client";

import { AuthForm } from "components/auth_flow/AuthForm";
import { useSetSession } from "lib/SessionManager";
import { Session, kratos } from "lib/ory";
import { ErrorGeneric, LoginFlow } from "@ory/kratos-client";
import { AxiosError } from "axios";
import { useRouter, useSearchParams } from "next/navigation";
import { Suspense, useCallback, useEffect, useMemo, useState } from "react";
import { ErrorBoundary } from "react-error-boundary";

interface AuthFormLoadingProps {
  flow: LoginFlow;
  updateFlow?: (flow: LoginFlow) => void;
  onFail?: (err: ErrorGeneric) => void;
  onSuccess?: (session: Session) => void;
}

const isGenericError = (err: LoginFlow | ErrorGeneric): err is ErrorGeneric =>
  "error" in err;

function AuthFormLoading({
  flow,
  updateFlow,
  onSuccess,
  onFail,
}: AuthFormLoadingProps) {
  const onSubmit = useCallback(
    (data: any) => {
      kratos
        .updateLoginFlow({ flow: flow.id, updateLoginFlowBody: data })
        .then(({ data }) => {
          if (data.session) onSuccess?.(Session.parse(data.session));
        })
        .catch((err: AxiosError<LoginFlow | ErrorGeneric>) => {
          console.error(err);
          const resp = err.response?.data;
          if (!resp) return;
          if (isGenericError(resp)) onFail?.(resp);
          else updateFlow?.(resp);
        });
    },
    [flow, updateFlow, onFail, onSuccess]
  );
  return <AuthForm ui={flow.ui} onSubmit={onSubmit} />;
}

export function Login() {
  const router = useRouter();
  const queryParams = useSearchParams();
  const callbackUrl = useMemo(
    () => queryParams.get("callback_url") || "/account/me",
    [queryParams]
  );

  const [flow, setFlow] = useState<LoginFlow | undefined>();
  useEffect(() => {
    if (!flow) {
      const controller = new AbortController();
      kratos
        .createBrowserLoginFlow(undefined, {
          signal: controller.signal,
        })
        .then(({ data }) => setFlow(data))
        .catch((err) => {
          console.error(err);
        });
      return () => controller.abort();
    }
  }, [flow, setFlow, router, callbackUrl]);

  const setSession = useSetSession();
  return (
    <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none pointer-events-none">
      <div className="relative w-auto my-6 mx-auto max-w-3xl bg-white rounded-md pointer-events-auto">
        <ErrorBoundary fallback={<p>Oops!</p>}>
          <Suspense fallback={<p>Loading login flow</p>}>
            {flow && (
              <AuthFormLoading
                flow={flow}
                updateFlow={setFlow}
                onSuccess={(session) => {
                  setSession(session);
                  router.push(callbackUrl);
                }}
              />
            )}
          </Suspense>
        </ErrorBoundary>
      </div>
    </div>
  );
}
