"use client";
import { CreatePublicationForm } from "components/CreatePublicationForm";
import Modal from "components/Modal";
import {
  AccountMeta,
  createPublication,
  getMyAccountMeta,
  getParticipant,
  Participant,
  Organisation,
  getParticipantOrganisations,
} from "lib/api";
import { useLogout, useSessionMaybe } from "lib/SessionManager";
import { useRouter } from "next/navigation";
import { useCallback, useEffect, useState } from "react";
import { MyAccount } from "./MyAccount";

export function MyAccountFetch() {
  const router = useRouter();
  const logout = useLogout();
  const session = useSessionMaybe();
  const [meta, setMeta] = useState<AccountMeta | null>(null);
  const [participant, setParticipant] = useState<Participant | null>(null);
  const [organisations, setOrganisations] = useState<Organisation[]>([])

  useEffect(() => {
    getMyAccountMeta()
      .then((meta) => {
        console.info("Got meta", meta)
        setMeta(meta);
        if (meta.connected_participant) {
          return Promise.allSettled([
            getParticipant(meta.connected_participant),
            getParticipantOrganisations(meta.connected_participant)
          ])
        }
        return Promise.resolve(null);
      })
      .then((results) => {
        if (!results) {
          return Promise.resolve()
        }
        const [maybeParticipant, maybeOrganisations] = results
        if (maybeParticipant.status == "fulfilled") {
          setParticipant(maybeParticipant.value)
        }
        if (maybeOrganisations.status == "fulfilled") {
          setOrganisations(maybeOrganisations.value)
        }
        if (maybeParticipant.status == "rejected" || maybeOrganisations.status == "rejected") {
          return Promise.reject()
        }
      })
      .catch((err) => setMeta(null));
  }, [setMeta, setParticipant]);

  const [isCreationModal, setCreationModal] = useState<boolean>(false);
  const creationModal = (
    <Modal onClose={() => setCreationModal(false)}>
      <CreatePublicationForm
        onCommit={(req) => {
          setCreationModal(false);
          console.info("pub req", req);
          createPublication(req).then((pub) => {
            router.push(`/publications/${pub.id}`);
          });
        }}
        onAbort={() => setCreationModal(false)}
      />
    </Modal>
  );

  const gotoNotifications = useCallback(
    () => router.push(`/participants/${participant?.id}/notifications`),
    [router, participant]
  )

    return (
      <>
        {isCreationModal && creationModal}
        {session && <MyAccount
          session={session}
          participant={participant ?? undefined}
          organisations={organisations ?? undefined}
          onShowNotifications={gotoNotifications}
          onChangePassword={() => router.push('/account/settings')}
          onCreatePublication={() => setCreationModal(true)}
          onChangeParticipant={setParticipant}
          administrator={meta?.is_app_admin ?? false}
        />}
        {!session && <span> You are not logged in! scram!</span>}
      </>
    )
}
