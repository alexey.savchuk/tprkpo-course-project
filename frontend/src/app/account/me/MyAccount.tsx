"use client";

import { useCallback, useEffect, useState } from "react";
import { Organisation, Participant, attachAcademicName } from "lib/api";
import Link from "next/link";
import { FaEdit, FaPassport, FaRegAddressBook, FaRegBell } from "react-icons/fa";
import { Session } from "lib/ory";

export interface MyAccountProps {
  session: Session;
  participant?: Participant;
  organisations?: Organisation[];
  administrator: boolean;
  onShowNotifications?: () => void;
  onChangePassword?: () => void;
  onCreatePublication?: () => void;
  onChangeParticipant: (par: Participant) => void;
}

export function MyAccount({
  session,
  participant,
  organisations,
  administrator,
  onShowNotifications,
  onChangePassword,
  onCreatePublication,
  onChangeParticipant
}: MyAccountProps) {
  const [academicName, setAcademicName] = useState<string>("");

  const submit = useCallback((event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    attachAcademicName(academicName).then((par) => onChangeParticipant(par));
  }, [academicName, onChangeParticipant]);
  const changeAcademicName = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newAcademicname = event.target.value ?? "";
    setAcademicName(newAcademicname);
  };

  const PromptAcademicName = useCallback(() => <form onSubmit={submit}>
    <div>
      <input
        type="text"
        placeholder="Choose your academic name"
        value={academicName}
        onChange={changeAcademicName}
        className="p-2 my-1 w-full bg-slate-100 rounded-md"
      />
    </div>
    <div className="pl-2 mt-2 w-full inline-flex justify-end">
      <input
        type="submit"
        value="Submit"
        className="cursor-pointer bg-green-600 px-3 py-2 rounded-md text-white"
      />
    </div>
</form>, [submit, academicName]);
const PrintAcademicName = (part: Participant) => (
  <Link href={`/participants/${part.id}`}>
    <span className="text-green-800">{part.name}</span>
  </Link>
);
const participantComp = useCallback(() => {
  if (participant) return PrintAcademicName(participant);
  else return PromptAcademicName();
}, [participant, PromptAcademicName]);

  return (
    <div>
      <div className="grid grid-cols-2 mb-2">
        <span className="w-fit text-2xl font-bold">{session.identity?.traits.email}</span>
        <div className="justify-self-end justify-end flex relative right-0 w-fit ">
          <div
            onClick={onShowNotifications}
            className="p-1 border-2 rounded-md border-black cursor-pointer mr-2"
            role="button"
            aria-label="Show notifications"
          >
            <FaRegBell size={"1.3rem"} className="ml-1 mb-1" />
          </div>
          <div
            onClick={onCreatePublication}
            className="p-1 border-2 rounded-md border-black cursor-pointer mr-2"
            role="button"
            aria-label="Create publication"
          >
          <FaEdit size={"1.3rem"} className="ml-1 mb-1" />
          </div>
          <div
            onClick={onChangePassword}
            className="p-1 border-2 rounded-md border-black cursor-pointer mr-2"
            role="button"
            aria-label="Change password"
          >
          <FaPassport size={"1.3rem"} className="ml-1 mb-1" />
          </div>
        </div>
      </div>
      <div className="mt-5">
        <span className="text-lg font-semibold">My participant: </span>
        {participantComp()}
      </div>
      {organisations ?
      <>
        <span className="text-lg font-semibold">My organisations: </span>
        <ul>
          {organisations.map(org =>
            <li key={org.id}>
              <Link href={`/organisations/${org.id}`}>
                <span className="text-green-800">{org.name}</span>
              </Link>
            </li>
          )}
        </ul>
      </> : <></>}
      {administrator && (
        <Link href="/admin">
          <div className="mt-2 w-fit flex px-3 py-2 bg-green-600 rounded-md cursor-pointer">
            <span className="text-xl font-semibold">Admin</span>
          </div>
        </Link>
      )}
    </div>
  );
}
