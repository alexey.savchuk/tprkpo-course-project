import { Meta, StoryObj } from "@storybook/react";
import { MyAccount } from "./MyAccount";

export default {
    title: "Pages/MyAccount",
    component: MyAccount,
    parameters: {
      nextjs: {
        appDirectory: true,
      },
    },
} as Meta<typeof MyAccount>;

export const RegularUser: StoryObj<typeof MyAccount> = {
    ...MyAccount,
    args: {
        session: {
            id: "session-id",
            identity: {
                id: "abcd-efgh",
                schema_id: "some-id",
                schema_url: "some://url",
                traits: {
                    email: "john.doe@example.com"
                }
            }
        },
        participant: {
            id: "edf",
            name: "John Doe",
        },
        administrator: false,
    },
};

export const UserWithoutAcademicName: StoryObj<typeof MyAccount> = {
    ...MyAccount,
    args: {
        session: {
            id: "session-id",
            identity: {
                id: "abcd-efgh",
                schema_id: "some-id",
                schema_url: "some://url",
                traits: {
                    email: "john.doe@example.com"
                }
            }
        },
    },
};

export const AdminUser: StoryObj<typeof MyAccount> = {
    ...MyAccount,
    args: {
        session: {
            id: "session-id",
            identity: {
                id: "abcd-efgh",
                schema_id: "some-id",
                schema_url: "some://url",
                traits: {
                    email: "john.doe@example.com"
                }
            }
        },
        participant: {
            id: "edf",
            name: "John Doe",
        },
        administrator: true,
    },
};