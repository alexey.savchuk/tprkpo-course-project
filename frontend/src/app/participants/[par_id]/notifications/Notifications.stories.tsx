import { Meta, StoryObj } from "@storybook/react";
import { Notifications } from "./Notifications";

export default {
    title: "Pages/Notifications",
    component: Notifications,
    parameters: {
      nextjs: {
        appDirectory: true,
      },
    },
} as Meta<typeof Notifications>;

export const Example: StoryObj<typeof Notifications> = {
    ...Notifications,
    args: {
        par_id: "123",
        orgNotifications: [
            {
                id: "edf",
                inviter_name: "Ivan Ivanov",
                organisation_name: "MSU",
            },
            {
                id: "abc",
                inviter_name: "John Doe",
                organisation_name: "UCLA"
            },
        ],
        pubNotifications: [
            {
                id: "edf2",
                inviter_name: "Vasiliy Kasilov",
                publication_title: "Developing Postgres plugin",
            },
            {
                id: "abc2",
                inviter_name: "Oleg Prokofiev",
                publication_title: "Plant Disease Recognition"
            },
        ]
    },
};