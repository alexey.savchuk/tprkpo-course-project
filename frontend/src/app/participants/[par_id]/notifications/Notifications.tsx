"use client";

import { Notification, NotificationProps } from "components/Notification";
import {
  acceptOrganisationInvitation,
  acceptPublicationInvitation,
  declineOrganisationInvitation,
  declinePublicationInvitation,
  getOrganisationInvitations,
  getPublicationInvitations,
} from "lib/api";
import { useEffect, useState } from "react";

interface OrgNotification {
  id: string;
  inviter_name: string;
  organisation_name: string;
}

interface PubNotification {
  id: string;
  inviter_name: string;
  publication_title: string;
}

export interface NotificationListProps {
  par_id: string;
  orgNotifications: Array<OrgNotification>;
  pubNotifications: Array<PubNotification>;
}

export function Notifications({
  par_id,
  orgNotifications,
  pubNotifications,
}: NotificationListProps) {
  return (
    <>
      <div>
        {orgNotifications.map((n) => (
          <Notification
            key={n.id}
            id={n.id}
            who={n.inviter_name}
            type={"organisation"}
            where={n.organisation_name}
            onAccept={async () => {
              return await acceptOrganisationInvitation(par_id, n.id);
            }}
            onReject={async () => {
              return await declineOrganisationInvitation(par_id, n.id);
            }}
          />
        ))}
      </div>
      <div>
        {pubNotifications.map((n) => (
          <Notification
            key={n.id}
            id={n.id}
            who={n.inviter_name}
            type={"publication"}
            where={n.publication_title}
            onAccept={async () => {
              return await acceptPublicationInvitation(par_id, n.id);
            }}
            onReject={async () => {
              return await declinePublicationInvitation(par_id, n.id);
            }}
          />
        ))}
      </div>
    </>
  );
}

export function NotificationsLoad({ par_id }: { par_id: string }) {
  const [orgNotifications, setOrgNotifications] = useState<
    Array<OrgNotification>
  >([]);
  const [pubNotifications, setPubNotifications] = useState<
    Array<PubNotification>
  >([]);

  useEffect(() => {
    getOrganisationInvitations(par_id)
    .then((n) => setOrgNotifications(n))
    .catch((e) => console.log(e))
  }, [par_id, setOrgNotifications]);

  useEffect(() => {
    getPublicationInvitations(par_id)
    .then((n) => setPubNotifications(n))
    .catch((e) => console.log(e))
  }, [par_id, setPubNotifications]);

  return (
    <Notifications
      par_id={par_id}
      orgNotifications={orgNotifications}
      pubNotifications={pubNotifications}
    />
  );
}
