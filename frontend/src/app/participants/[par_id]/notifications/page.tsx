

import { getOrganisationInvitations, getPublicationInvitations, listParticipants } from "lib/api";
import { Notifications, NotificationsLoad } from "./Notifications";

export default async function ParticipantNotificationsPage({
  params,
}: {
  params: { par_id: string };
}) {
  return <NotificationsLoad
            par_id={params.par_id}
          />;
}
