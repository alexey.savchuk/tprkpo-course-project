import { ParticipantReferences } from "lib/api";
import Link from "next/link";

export interface ConnectedParticipantsProps {
  co_participants: ParticipantReferences;
  referenced_from: ParticipantReferences;
  referenced_to: ParticipantReferences;
}

export function ConnectedParticipants({
  co_participants,
  referenced_from,
  referenced_to,
}: ConnectedParticipantsProps) {
  const max_score = {
    co_participants: Math.max(...co_participants.map((p) => p.score)),
    referenced_from: Math.max(...referenced_from.map((p) => p.score)),
    referenced_to: Math.max(...referenced_to.map((p) => p.score)),
  };
  const co_participants_table = (
    <div className="table w-full">
      <div className="table-row-group">
        {co_participants.map((participant) => (
          <div key={participant.id} className="table-row hover:bg-slate-50">
            <div className="table-cell py-4 border-b border-slate-500">
              <span className="hover:underline cursor-pointer">
                <Link href={`/participants/${participant.id}`}>
                  {participant.name}
                </Link>
              </span>
            </div>
            <div className="table-cell py-4 border-b border-slate-500 max-w-[15em] w-1/4">
              <div className="h-2 relative rounded-lg overflow-hidden mr-3">
                <div className="absolute left-0 h-full right-0 bg-gray-400 z-10"></div>
                <div
                  className="absolute left-0 h-full bg-sky-400 z-10"
                  style={{
                    width: `${(
                      ((participant.score ?? 1.0) * 100) /
                      max_score.co_participants
                    ).toFixed(2)}%`,
                  }}
                ></div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
  const referenced_from_table = (
    <div className="table w-full">
      <div className="table-row-group">
        {referenced_from.map((participant) => (
          <div key={participant.id} className="table-row hover:bg-slate-50">
            <div className="table-cell py-4 border-b border-slate-500">
              <span className="hover:underline cursor-pointer">
                <Link href={`/participants/${participant.id}`}>
                  {participant.name}
                </Link>
              </span>
            </div>
            <div className="table-cell py-4 border-b border-slate-500 max-w-[15em] w-1/4">
              <div className="h-2 relative rounded-lg overflow-hidden mr-3">
                <div className="absolute left-0 h-full right-0 bg-gray-400 z-10"></div>
                <div
                  className="absolute left-0 h-full bg-sky-400 z-10"
                  style={{
                    width: `${(
                      ((participant.score ?? 1.0) * 100) /
                      max_score.referenced_from
                    ).toFixed(2)}%`,
                  }}
                ></div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
  const referenced_to_table = (
    <div className="table w-full">
      <div className="table-row-group">
        {referenced_to.map((participant) => (
          <div key={participant.id} className="table-row hover:bg-slate-50">
            <div className="table-cell py-4 border-b border-slate-500">
              <span className="hover:underline cursor-pointer">
                <Link href={`/participants/${participant.id}`}>
                  {participant.name}
                </Link>
              </span>
            </div>
            <div className="table-cell py-4 border-b border-slate-500 max-w-[15em] w-1/4">
              <div className="h-2 relative rounded-lg overflow-hidden mr-3">
                <div className="absolute left-0 h-full right-0 bg-gray-400 z-10"></div>
                <div
                  className="absolute left-0 h-full bg-sky-400 z-10"
                  style={{
                    width: `${(
                      ((participant.score ?? 1.0) * 100) /
                      max_score.referenced_to
                    ).toFixed(2)}%`,
                  }}
                ></div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
  return (
    <div className="grid grid-cols-3 gap-3">
      <div>
        <span className="text-lg">Co-participants:</span>
        {co_participants_table}
      </div>
      <div>
        <span className="text-lg">Referenced from:</span>
        {referenced_from_table}
      </div>
      <div>
        <span className="text-lg">Referenced to:</span>
        {referenced_to_table}
      </div>
    </div>
  );
}
