import { getParticipantReference, listParticipants } from "lib/api";
import { ConnectedParticipants } from "./ConnectedParticipants";

export default async function ParticipantsConnected({
  params,
}: {
  params: { par_id: string };
}) {
  const content = {
    co_participants: await getParticipantReference(
      params.par_id,
      "co_participants"
    ),
    referenced_from: await getParticipantReference(
      params.par_id,
      "referenced_from"
    ),
    referenced_to: await getParticipantReference(
      params.par_id,
      "referenced_to"
    ),
  };
  return <ConnectedParticipants {...content} />;
}
