import React from "react";
import { Participant } from "./Participant";
import {
  getParticipant,
  getParticipantMeta,
  getParticipantOrganisations,
  getParticipantPublications,
  listParticipants,
} from "lib/api";

export default async function ParticipantPage({
  params,
}: {
  params: { par_id: string };
}) {
  const participant = await getParticipant(params.par_id);
  const meta = await getParticipantMeta(params.par_id);
  const publications = await getParticipantPublications(params.par_id);
  const organisations = await getParticipantOrganisations(params.par_id);

  return (
    <Participant
      participant={participant}
      organisations={organisations}
      publications={publications}
      meta={meta}
    />
  );
}
