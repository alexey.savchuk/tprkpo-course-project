import { OrganisationTable } from "components/OrganisationTable";
import { ParticipantPublicationTable } from "components/ParticipantPublicationTable";
import { RoleBadge } from "components/RoleBadge";
import { SubjectCodeBadge } from "components/SubjectCodeBadge";
import {
  Participant as ParticipantApi,
  ParticipantMeta,
  ParticipantOrganisations,
  ParticipantPublications,
} from "lib/api";
import Link from "next/link";
import { FaCheck, FaEdit } from "react-icons/fa";

export interface ParticipantProps {
  participant: ParticipantApi;
  meta: ParticipantMeta;
  organisations: ParticipantOrganisations;
  publications: ParticipantPublications;
  onEdit?: () => void;
}

export const Participant = ({
  participant,
  meta,
  publications,
  organisations,
  onEdit,
}: ParticipantProps) => {
  const isEditable = meta.editable;
  return (
    <div>
      <div className="grid grid-cols-2 mb-2">
        <div className="flex items-center">
          <span className="w-fit text-2xl font-bold">{participant.name}</span>
          <div className="px-3">
            {meta.connected && <FaCheck className="fill-green-600" />}
          </div>
        </div>
        {isEditable && <div
          className="justify-self-end cursor-pointer flex relative right-0 w-fit aspect-square p-1 border-2 rounded-md border-black "
          onClick={onEdit}
        >
          <FaEdit size={"1.3rem"} className="ml-1 mb-1" />
        </div>}
      </div>
      <div className="flex justify-between">
        <div>
          <div className="inline-flex gap-4 mb-4">
            <span>Citations: {meta.citations}</span>
          </div>
          <div>
            <Link href={`/participants/${participant.id}/connected`}>
              <span className="text-xl font-semibold text-white bg-green-600 rounded-md cursor-pointer py-2 px-2">
                References...
              </span>
            </Link>
          </div>
        </div>
        <div className="ml-5">
          <span className="text-xl font-semibold">Organisations:</span>
          <div className="relative h-80 overflow-hidden">
            <OrganisationTable organisations={organisations} />
          </div>
        </div>
      </div>
      <div>
        <span className="text-xl font-semibold">Publications:</span>
        <ParticipantPublicationTable publications={publications} />
      </div>
    </div>
  );
};
