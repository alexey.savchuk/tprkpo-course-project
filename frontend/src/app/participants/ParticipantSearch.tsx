"use client";
import { SearchBar } from "components/SearchBar";
import { Participant, Participants, searchParticipants } from "lib/api";
import { useRouter } from "next/navigation";
import React from "react";

export const ParticipantSearch = () => {
  const router = useRouter();
  const [participants, setParticipants] = React.useState<Participants>([]);
  const [isSearching, startSearch] = React.useTransition();
  const onInputChange = (search: string) =>
    startSearch(() => {
      setParticipants([]);
      searchParticipants(search, 10).then((pars) => setParticipants(pars));
    });
  const onParSelect = (participant: Participant) => {
    router.push(`/participants/${participant.id}`);
  };

  return (
    <SearchBar
      hint="Search participants"
      isPending={isSearching}
      options={participants}
      optionRender={(par) => <span>{par.name}</span>}
      onInputChange={onInputChange}
      onOptionSelect={onParSelect}
    />
  );
};
