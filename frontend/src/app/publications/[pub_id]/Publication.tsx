import { OrganisationTable } from "components/OrganisationTable";
import { PublicationTable } from "components/PublicationTable";
import { RoleBadge } from "components/RoleBadge";
import { SubjectCodeBadge } from "components/SubjectCodeBadge";
import {
  Organisations,
  Publication as PublicationApi,
  PublicationParticipants,
  PublicationReferences,
} from "lib/api";
import Link from "next/link";
import { FaEdit } from "react-icons/fa";

export interface PublicationProps {
  publication: PublicationApi;
  participants: PublicationParticipants;
  organisations: Organisations;
  references: PublicationReferences;
  isEditable: boolean;
  onEdit?: () => void;
}

export const Publication = ({
  publication,
  participants,
  organisations,
  references,
  isEditable,
  onEdit,
}: PublicationProps) => {
  return (
    <div>
      <div className="grid grid-cols-2 mb-2">
        <span className="w-fit text-2xl font-bold">{publication.title}</span>
        <div
          className={
            "justify-self-end cursor-pointer flex relative right-0 w-fit aspect-square p-1 border-2 rounded-md border-black " +
            (!isEditable ? "invisible" : "")
          }
          onClick={onEdit}
        >
          <FaEdit size={"1.3rem"} className="ml-1 mb-1" />
        </div>
      </div>
      <div className="flex justify-between">
        <div>
          <div className="inline-flex gap-4 mb-4">
            <span>Published at: {publication.published_at}</span>
            <SubjectCodeBadge code={publication.subject_code} />
          </div>
          <div className="">
            <span className="text-xl font-semibold">Participants:</span>
            <div className="flex flex-wrap">
              {participants.map((part) => (
                <div key={part.id} className="inline-flex gap-1 mr-3">
                  <Link href={`/participants/${part.id}`}>
                    <span className="hover:underline">{part.name}</span>
                  </Link>
                  <RoleBadge name={part.role.name} />
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="ml-5">
          <span className="text-xl font-semibold">Organisations:</span>
          <div className="relative h-80 overflow-hidden">
            <OrganisationTable organisations={organisations} />
          </div>
        </div>
      </div>
      <div className="mt-2">
        <span className="text-xl font-semibold">References:</span>
        <div className="grid grid-cols-2 gap-5">
          <div>
            <span className="font-semibold">To publications</span>
            <PublicationTable publications={references.to} />
          </div>
          <div>
            <span className="font-semibold">From publications</span>
            <PublicationTable publications={references.from} />
          </div>
        </div>
      </div>
    </div>
  );
};
