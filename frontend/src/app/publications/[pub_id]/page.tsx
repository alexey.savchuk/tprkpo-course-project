import React from "react";
import { Publication } from "./Publication";
import {
  getPublication,
  getPublicationOrganisations,
  getPublicationParticipants,
  getPublicationReferences,
  listPublications
} from "lib/api";

export default async function PublicationPage({
  params,
}: {
  params: { pub_id: string };
}) {
  const publication = await getPublication(params.pub_id);
  const participants = await getPublicationParticipants(params.pub_id);
  const organisations = await getPublicationOrganisations(params.pub_id);
  const references = await getPublicationReferences(params.pub_id);

  return (
    <Publication
      publication={publication}
      participants={participants}
      organisations={organisations}
      references={references}
      isEditable={false}
    />
  );
}
