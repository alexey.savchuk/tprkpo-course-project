'use client';
import { SearchBar } from "components/SearchBar";
import { Publication, Publications, searchPublications } from "lib/api";
import { useRouter } from "next/navigation";
import React from "react";

export const PublicationSearch = () => {
  const router = useRouter();
  const [publications, setPublications] = React.useState<Publications>([]);
  const [isSearching, startSearch] = React.useTransition();
  const onInputChange = (search: string) =>
    startSearch(() => {
      setPublications([]);
      searchPublications(search, 10).then((pubs) => setPublications(pubs));
    });
  const onPubSelect = (pub: Publication) => {
    router.push(`/publications/${pub.id}`);
  };

  return (
    <SearchBar
      hint="Search publications"
      isPending={isSearching}
      options={publications}
      optionRender={(pub) => <span>{pub.title}</span>}
      onInputChange={onInputChange}
      onOptionSelect={onPubSelect}
    />
  );
};
