"use client";
import { SearchBar } from "components/SearchBar";
import {
  GlobalSearch as GlobalSearchResult,
  globalSearch,
  GlobalSearches,
} from "lib/api";
import { useRouter } from "next/navigation";
import React from "react";

export const GlobalSearch = () => {
  const router = useRouter();
  const [searches, setSearches] = React.useState<GlobalSearches>([]);
  const [isSearching, startSearch] = React.useTransition();
  const onInputChange = (search: string) =>
    startSearch(() => {
      setSearches([]);
      globalSearch(search, 10).then((s) => setSearches(s));
    });
  const onSelect = (search: GlobalSearchResult) => {
    switch (search.item_type) {
      case "application_user":
        return router.push(`/account/${search.item_id}`);
      case "organisation":
        return router.push(`/organisations/${search.item_id}`);
      case "publication":
        return router.push(`/publications/${search.item_id}`);
      case "participant":
        return router.push(`/participants/${search.item_id}`);
      case "subject_code":
        return router.push(`/subject_codes/${search.item_id}`);
    }
  };

  return (
    <SearchBar
      hint="Search everything"
      isPending={isSearching}
      options={searches}
      optionRender={(s) => (
        <span>
          {s.item_type}: {s.value}
        </span>
      )}
      onInputChange={onInputChange}
      onOptionSelect={onSelect}
    />
  );
};
