import { getOrganisation, getOrganisationMeta, getOrganisationPublications, listOrganisations } from "lib/api";
import React from "react";
import { OrganisationPublications } from "./OrganisationPublications";
import { PublicationTableProps } from "components/PublicationTable";

export default async function OrganisationPublicationsPage({
  params,
}: {
  params: { org_id: string };
}) {
  const organisation = await getOrganisation(params.org_id);
  const meta = await getOrganisationMeta(params.org_id);
  const publications = await getOrganisationPublications(params.org_id);
  const content: PublicationTableProps = {
    publications: publications,
  };

  return (
    <OrganisationPublications
      organisation={organisation}
      content={content}
      isEditable={meta.editable}
    />
  );
}