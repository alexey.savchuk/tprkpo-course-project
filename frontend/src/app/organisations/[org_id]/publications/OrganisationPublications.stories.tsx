import type { Meta, StoryObj } from "@storybook/react";
import { OrganisationPublications } from "./OrganisationPublications";

export default {
  title: "Pages/OrganisationPublications",
  component: OrganisationPublications,
} as Meta<typeof OrganisationPublications>;

export const Example: StoryObj<typeof OrganisationPublications> = {
  ...OrganisationPublications,
  args: {
    organisation: {
      id: "abc",
      name: "Organisation",
    },
    isEditable: true,
    content: {
      publications: [
        {
          id: "abc",
          title: "Paper #1",
          subject_code: { id: "1", code: "09.04.01", description: "PI" },
          published_at: "2020",
        },
        {
          id: "def",
          title: "Paper #2",
          subject_code: { id: "2", code: "09.04.01", description: "PI" },
          published_at: "2020",
        },
      ],
    },
  },
};
