import { FaEdit } from "react-icons/fa";

import {
  PublicationTable,
  PublicationTableProps,
} from "components/PublicationTable";
import { Organisation } from "lib/api";

export interface OrganisationPublicationsProps {
  organisation: Organisation;
  isEditable: boolean;
  content: PublicationTableProps;
  onEdit?: () => void;
}

export const OrganisationPublications = ({
  organisation,
  content,
  isEditable,
  onEdit,
}: OrganisationPublicationsProps) => {
  return (
    <div>
      <div className="grid grid-cols-2 mb-4">
        <span className="w-fit text-2xl font-bold">{organisation.name}</span>
        <div
          className={
            "justify-self-end cursor-pointer flex relative right-0 w-fit aspect-square p-1 border-2 rounded-md border-black " +
            (!isEditable ? "invisible" : "")
          }
          onClick={onEdit}
        >
          <FaEdit size={"1.3rem"} className="ml-1 mb-1" />
        </div>
      </div>
      <span className="w-fit text-xl font-semibold">Publications</span>
      <div>
        <PublicationTable {...content} />
      </div>
    </div>
  );
};
