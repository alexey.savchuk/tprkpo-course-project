'use client';
import { FaEdit } from "react-icons/fa";

import { Organisation as OrganiastionApi, OrganisationMeta } from "lib/api";
import { useRouter } from "next/navigation";

export interface OrganisationPublicationsProps {
  organisation: OrganiastionApi;
  meta: OrganisationMeta;
  onEdit?: () => void;
}

export const Organisation = ({
  organisation,
  meta,
  onEdit,
}: OrganisationPublicationsProps) => {
  const router = useRouter();
  const isEditable = meta.editable;
  const onParticipants = () => router.push(`/organisations/${organisation.id}/participants`);
  const onPublications = () => router.push(`/organisations/${organisation.id}/publications`);
  return (
    <div>
      <div className="grid grid-cols-2 mb-4">
        <span className="w-fit text-2xl font-bold">{organisation.name}</span>
        <div
          className={
            "justify-self-end cursor-pointer flex relative right-0 w-fit aspect-square p-1 border-2 rounded-md border-black " +
            (!isEditable ? "invisible" : "")
          }
          onClick={onEdit}
        >
          <FaEdit size={"1.3rem"} className="ml-1 mb-1" />
        </div>
      </div>
      <div>
        <ul>
          {(
            ["participants", "publications", "citations"] as Array<
              keyof OrganisationMeta
            >
          ).map((prop) => (
            <li key={prop} className="capitalize">
              {prop}: {meta[prop]}
            </li>
          ))}
        </ul>
      </div>
      <div className="flex gap-3 mt-4">
        <div
          className="max-w-fit bg-green-600 text-gray-100 px-3 py-2 rounded-md cursor-pointer"
          onClick={onParticipants}
        >
          <span>Participants</span>
        </div>
        <div
          className="max-w-fit bg-green-600 text-gray-100 px-3 py-2 rounded-md cursor-pointer"
          onClick={onPublications}
        >
          <span>Publications</span>
        </div>
      </div>
    </div>
  );
};
