import {
  getOrganisation,
  getOrganisationMeta,
  listOrganisations,
} from "lib/api";
import React from "react";
import { Organisation } from "./Organisation";

export default async function OrganisationPage({
  params,
}: {
  params: { org_id: string };
}) {
  const organisation = await getOrganisation(params.org_id);
  const meta = await getOrganisationMeta(params.org_id);

  return <Organisation organisation={organisation} meta={meta} />;
}