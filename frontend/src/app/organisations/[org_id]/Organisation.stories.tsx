import type { Meta, StoryObj } from "@storybook/react";
import { Organisation } from "./Organisation";

export default {
  title: "Pages/Organisation",
  component: Organisation,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
} as Meta<typeof Organisation>;

export const Example: StoryObj<typeof Organisation> = {
  ...Organisation,
  args: {
    organisation: {
      id: "abc",
      name: "Organisation",
    },
    meta: {
      participants: 100,
      publications: 50,
      citations: 75,
      editable: true,
    },
  },
};
