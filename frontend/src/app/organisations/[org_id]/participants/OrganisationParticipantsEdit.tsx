"use client";

import { CreateOrgParForm } from "components/CreateOrganisationParticipantForm";
import { EditRolesForm } from "components/EditRolesForm";
import Modal from "components/Modal";
import {
  getOrganisationMeta,
  Organisation,
  OrganisationParticipants,
  OrganisationParticipant,
  Participant,
  Role,
  deleteOrganisationParticipant,
  modifyOrganisationParticipant,
  getOrganisationParticipants,
} from "lib/api";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { OrganisationParticipantsPage } from "./OrganisationParticipantsPage";

export interface OrganisationParticipantsEditProps {
  organisation: Organisation;
  participants: OrganisationParticipants;
}

export const OrganisationParticipantsEdit = ({
  organisation,
  participants: initialParticipants,
}: OrganisationParticipantsEditProps) => {
  const router = useRouter();
  const [isEditable, setIsEditable] = useState<boolean>(false);
  const [participants, setParticipants] =
    useState<OrganisationParticipants>(initialParticipants);

  useEffect(() => setParticipants(initialParticipants), [initialParticipants]);

  const onSetFilters = (
    include_historical: boolean,
    include_pending: boolean,
    include_declined: boolean
  ) => {
    getOrganisationParticipants(organisation.id, {
      include_historical,
      include_pending,
      include_declined,
    }).then((participants) => setParticipants(participants));
  };

  useEffect(() => {
    const controller = new AbortController();
    getOrganisationMeta(organisation.id, {
      signal: controller.signal,
    })
      .then((meta) => setIsEditable(meta.editable))
      .catch((err) => {
        console.error(err);
      });
    return () => controller.abort();
  }, [organisation]);

  const [isCreateModal, setCreateModal] = useState<boolean>(false);
  const createParticipant = () => {
    setCreateModal(true);
  };
  const onCreateParticipant = (
    participant: Participant,
    roles: Array<Role>
  ) => {
    setCreateModal(false);
    modifyOrganisationParticipant(organisation.id, participant.id, roles).then(
      (par) => {
        router.refresh();
      }
    );
  };
  const createModal = (
    <Modal onClose={() => setCreateModal(false)}>
      <CreateOrgParForm onSubmit={onCreateParticipant} />
    </Modal>
  );

  const [isDeleteModal, setDeleteModal] = useState<Participant | null>(null);
  const deleteModal = (par: Participant) => (
    <Modal onClose={() => setDeleteModal(null)}>
      <div className="px-3 pt-5 pb-3 w-96">
        <div className="text-lg mb-2">Are you sure?</div>
        <div>
          Remove {par.name} from {organisation.name}
        </div>
        <div className="w-full inline-flex justify-end gap-4 mt-3">
          <div
            className="cursor-pointer border-2 border-green-600 px-3 py-2 rounded-md"
            onClick={() => setDeleteModal(null)}
          >
            Cancel
          </div>
          <div
            className="cursor-pointer border-2 border-red-600 bg-red-600 px-3 py-2 rounded-md text-white"
            onClick={() => {
              setDeleteModal(null);
              deleteOrganisationParticipant(organisation.id, par.id).then(() =>
                router.refresh()
              );
              console.info("remove part");
            }}
          >
            Remove
          </div>
        </div>
      </div>
    </Modal>
  );

  const [isModifyModal, setModifyModal] =
    useState<OrganisationParticipant | null>(null);
  const modifyModal = (par: OrganisationParticipant) => (
    <Modal onClose={() => setModifyModal(null)}>
      <EditRolesForm
        initRoles={par.roles}
        onCommit={(roles) => {
          setModifyModal(null);
          modifyOrganisationParticipant(organisation.id, par.id, roles).then(
            () => router.refresh()
          );
          console.info("set roles", roles);
        }}
        onAbort={() => setModifyModal(null)}
      />
    </Modal>
  );
  return (
    <>
      {isCreateModal && createModal}
      {isDeleteModal && deleteModal(isDeleteModal)}
      {isModifyModal && modifyModal(isModifyModal)}
      <OrganisationParticipantsPage
        organisation={organisation}
        participants={participants}
        isEditable={isEditable}
        onAddParticipant={createParticipant}
        onDeleteParticipant={setDeleteModal}
        onModifyParticipant={setModifyModal}
        onSetFilters={onSetFilters}
      />
    </>
  );
};
