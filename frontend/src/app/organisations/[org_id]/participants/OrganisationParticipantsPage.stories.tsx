import type { Meta, StoryObj } from "@storybook/react";
import { OrganisationParticipantsPage } from "./OrganisationParticipantsPage";

export default {
  title: "Pages/OrganisationParticipants",
  component: OrganisationParticipantsPage,
} as Meta<typeof OrganisationParticipantsPage>;

export const Page: StoryObj<typeof OrganisationParticipantsPage> = {
  ...OrganisationParticipantsPage,
  args: {
    organisation: {
      id: "abc",
      name: "Organisation",
    },
    isEditable: true,
    participants: [
      {
        id: "1",
        name: "Active Participant",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: false,
        is_pending: false,
        is_declined: false,
      },
      {
        id: "2",
        name: "Historic Participant",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: true,
        is_pending: false,
        is_declined: false,
      },
      {
        id: "3",
        name: "Pending Participant",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: false,
        is_pending: true,
        is_declined: false,
      },
      {
        id: "4",
        name: "Declined Participant",
        roles: [{ id: "1", name: "Bachelor" }],
        performance: 0.7,
        is_historical: false,
        is_pending: false,
        is_declined: true,
      },
    ],
  },
};
