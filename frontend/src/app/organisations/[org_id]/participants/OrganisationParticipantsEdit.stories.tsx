import type { Meta, StoryObj } from "@storybook/react";
import { OrganisationParticipantsEdit } from "./OrganisationParticipantsEdit";

export default {
  title: "Pages/OrganisationParticipants",
  component: OrganisationParticipantsEdit,
} as Meta<typeof OrganisationParticipantsEdit>;

export const Edit: StoryObj<typeof OrganisationParticipantsEdit> = {
  ...OrganisationParticipantsEdit,
  parameters: {
    nextjs: {
      router: {
        basePath: '/organisations/abc/participants',
      },
    }
  },
  args: {
    organisation: {
      id: "abc",
      name: "Organisation",
    },
  },
};
