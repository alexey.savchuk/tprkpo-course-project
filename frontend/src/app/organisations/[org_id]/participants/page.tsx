import { getOrganisation, getOrganisationParticipants } from "lib/api";
import React from "react";
import { OrganisationParticipantsEdit } from "./OrganisationParticipantsEdit";

export default async function OrganisationParticipantsPage({
  params,
}: {
  params: { org_id: string };
}) {
  const organisation = await getOrganisation(params.org_id);
  const participants = await getOrganisationParticipants(params.org_id, {});

  return (
    <OrganisationParticipantsEdit
      organisation={organisation}
      participants={participants}
    />
  );
}
