import { FaUserPlus, FaSearch } from "react-icons/fa";

import {
  Organisation,
  OrganisationParticipants,
  OrganisationParticipant,
  Participant,
} from "lib/api";
import { ParticipantTable } from "components/ParticipantTable";
import React, { useEffect, useState } from "react";

export interface OrganisationParticipantsProps {
  organisation: Organisation;
  isEditable: boolean;
  participants: OrganisationParticipants;
  onAddParticipant?: () => void;
  onFilterParticipants?: (query: string) => void;
  onDeleteParticipant?: (par: Participant) => void;
  onModifyParticipant?: (par: OrganisationParticipant) => void;
  onSetFilters?: (
    include_historical: boolean,
    include_pending: boolean,
    include_declined: boolean
  ) => void;
}

export const OrganisationParticipantsPage = ({
  organisation,
  participants,
  isEditable,
  onAddParticipant,
  onFilterParticipants,
  onDeleteParticipant,
  onModifyParticipant,
  onSetFilters,
}: OrganisationParticipantsProps) => {
  const [filterInput, setFilterInput] = useState("");
  const changeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newInput = event.target.value ?? "";
    setFilterInput(newInput);
    onFilterParticipants?.(newInput);
  };

  const [includeHistorical, setIncludeHistorical] = useState<boolean>(false);
  const [includePending, setIncludePending] = useState<boolean>(false);
  const [includeDeclined, setIncludeDeclined] = useState<boolean>(false);

  useEffect(
    () => onSetFilters?.(includeHistorical, includePending, includeDeclined),
    [includeHistorical, includePending, includeDeclined]
  );

  return (
    <div>
      <div className="grid grid-cols-2 mb-4">
        <span className="w-fit text-2xl font-bold">{organisation.name}</span>
      </div>
      <div className="inline-flex gap-3 w-max items-center">
        <span className="w-fit text-xl font-semibold">Participants</span>
        {isEditable && (
          <div
            className="p-1 border-black border-2 rounded-md flex items-center cursor-pointer"
            onClick={onAddParticipant}
          >
            <FaUserPlus className="hover:fill-sky-600 cursor-pointer" />
          </div>
        )}
        <div className="w-full inline-flex items-stretch drop-shadow-lg">
          <input
            className="grow px-2 py-1 border-black border-y border-l rounded-l-md"
            onChange={changeInput}
            type="text"
            placeholder={"Search Participants"}
            value={filterInput}
          />
          <div className="bg-green-600 border-black rounded-r-md border flex cursor-pointer">
            <div className="self-center px-3">
              <FaSearch size={"1rem"} className="fill-white" />
            </div>
          </div>
        </div>
        {isEditable && (
          <div className="flex gap-4 ml-3">
            <div className="inline-flex gap-1">
              <input
                type="checkbox"
                value={includeHistorical ? 1 : 0}
                onChange={(ev) => setIncludeHistorical((t) => !t)}
                className="px-2 py-1"
              />
              <span>Historical</span>
            </div>
            <div className="inline-flex gap-1">
              <input
                type="checkbox"
                value={includePending ? 1 : 0}
                onChange={(ev) => setIncludePending((t) => !t)}
                className="px-2 py-1"
              />
              <span>Pending</span>
            </div>
            <div className="inline-flex gap-1">
              <input
                type="checkbox"
                value={includeDeclined ? 1 : 0}
                onChange={(ev) => setIncludeDeclined((t) => !t)}
                className="px-2 py-1"
              />
              <span>Declined</span>
            </div>
          </div>
        )}
      </div>
      <div>
        <ParticipantTable
          participants={participants}
          isEditable={isEditable}
          onClickDelete={onDeleteParticipant}
          onClickRole={onModifyParticipant}
        />
      </div>
    </div>
  );
};
