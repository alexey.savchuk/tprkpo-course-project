'use client';
import { SearchBar } from "components/SearchBar";
import { Organisation, Organisations, searchOrganisations } from "lib/api";
import { useRouter } from "next/navigation";
import React from "react";

export const OrganisationSearch = () => {
  const router = useRouter();
  const [organisations, setOrganisations] = React.useState<Organisations>([]);
  const [isSearching, startSearch] = React.useTransition();
  const onInputChange = (search: string) =>
    startSearch(() => {
      setOrganisations([]);
      searchOrganisations(search, 10).then((orgs) => setOrganisations(orgs));
    });
  const onOrgSelect = (org: Organisation) => {
    router.push(`/organisations/${org.id}`);
  };

  return (
    <SearchBar
      hint="Search organisations"
      isPending={isSearching}
      options={organisations}
      optionRender={(org) => <span>{org.name}</span>}
      onInputChange={onInputChange}
      onOptionSelect={onOrgSelect}
    />
  );
};
