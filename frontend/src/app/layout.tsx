import { TopBar } from "components/TopBar";
import { SessionManager } from "lib/SessionManager";
import "./globals.css";
import "react-datepicker/dist/react-datepicker.css";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      {/*
        <head /> will contain the components returned by the nearest parent
        head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <head />
      <body>
        <SessionManager>
          <TopBar />
          <div className="container mx-auto px-4">{children}</div>
        </SessionManager>
      </body>
    </html>
  );
}
