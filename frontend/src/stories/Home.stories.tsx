import { StoryObj } from "@storybook/react";
import Home from "../app/page";

export default {
  title: "Pages/Home",
  component: Home,
};

export const HomePage: StoryObj<typeof Home> = {
  ...Home,
  parameters: {
    nextjs: {
      appDirectory: true,
    },
  },
}
