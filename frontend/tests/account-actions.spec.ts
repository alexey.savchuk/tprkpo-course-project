import { test, expect } from '@playwright/test';
import { simple_user } from './user-creds.json';
import { generateAcademicUserName } from './lib/utils';

test.use({ storageState: 'playwright/.auth/ephemeral-user.json' });

test('check account page', async ({ page }) => {
    await page.goto('/account/me');

    await expect(page.getByText('Logout')).toBeVisible();
    await expect(page.getByRole("button", { name: "Create publication" })).toBeVisible();
    await expect(page.getByRole("button", { name: "Show notifications" })).toBeVisible();
    await expect(page.getByRole("button", { name: "Change password" })).toBeVisible();
});

test('assign academic participant', async ({ page }) => {
    await page.goto('/account/me');
    let name = generateAcademicUserName();

    await page.getByPlaceholder('Choose your academic name').click();
    await page.getByPlaceholder('Choose your academic name').fill(name);
    await page.getByRole('button', { name: 'Submit' }).click();
    await expect(page.getByRole('link', { name })).toBeVisible();
})