import { test, expect } from '@playwright/test';
import { simple_user, app_admin } from './user-creds.json';
import { generateEmail, generatePassword } from './lib/utils';

export const simpleUserContext = 'playwright/.auth/simple-user.json';
test('authenticate as simple user', async ({ page }) => {
    await page.goto('/');
    await page.getByRole('link', { name: 'Login' }).click();

    await page.getByPlaceholder('ID').click();
    await page.getByPlaceholder('ID').fill(simple_user.email);
    await page.getByPlaceholder('Password').click();
    await page.getByPlaceholder('Password').fill(simple_user.password);
    await page.getByRole('button', { name: 'Sign in' }).click();

    await expect(page.getByText(simple_user.email).first()).toBeVisible();

    await page.context().storageState({ path: simpleUserContext });
});

export const ephemeralUserContext = 'playwright/.auth/ephemeral-user.json';
test('register ephemeral user', async ({ page }) => {
    await page.goto('/');
    await page.getByRole('link', { name: 'Register' }).click();

    let email = generateEmail();
    let password = generatePassword();

    await page.getByPlaceholder('E-Mail').click();
    await page.getByPlaceholder('E-Mail').fill(email);
    await page.getByPlaceholder('Password').click();
    await page.getByPlaceholder('Password').fill(password);
    await page.getByRole('button', { name: 'Sign Up' }).click();

    await expect(page.getByText(email).first()).toBeVisible();

    await page.context().storageState({ path: ephemeralUserContext });
});

export const appAdminUserContext = 'playwright/.auth/app-admin.json';
test('authenticate as application admin', async ({ page }) => {
    await page.goto('/');
    await page.getByRole('link', { name: 'Login' }).click();

    await page.getByPlaceholder('ID').click();
    await page.getByPlaceholder('ID').fill(app_admin.email);
    await page.getByPlaceholder('Password').click();
    await page.getByPlaceholder('Password').fill(app_admin.password);
    await page.getByRole('button', { name: 'Sign in' }).click();

    await expect(page.getByText(app_admin.email).first()).toBeVisible();

    await page.context().storageState({ path: appAdminUserContext });
});