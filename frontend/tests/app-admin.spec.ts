import { test, expect } from '@playwright/test';
import { simple_user, app_admin } from './user-creds.json';
import { generateAcademicUserName } from './lib/utils';

test.use({ storageState: 'playwright/.auth/app-admin.json' });

test('check admin page', async ({ page, baseURL }) => {
    await page.goto('/account/me');

    await expect(page.getByRole('link', { name: 'Admin', exact: true })).toBeVisible();
    await page.getByRole('link', { name: 'Admin', exact: true }).click();

    await expect(page.url()).toEqual(`${baseURL}/admin`);
    await expect(page.getByText(app_admin.email).first()).toBeVisible();
    await expect(page.getByRole('button', { name: 'Add admin' })).toBeVisible();
    await expect(page.getByRole('button', { name: 'Add organisation' })).toBeVisible();
});

test('add and remove other admin', async ({ page }) => {
    await page.goto('/admin');

    await page.getByRole('button', { name: 'Add admin' }).click();
    let add_admin_dialog = page.getByRole("dialog", { name: "Choose new admin account" });
    await expect(add_admin_dialog).toBeVisible()
    await add_admin_dialog.getByPlaceholder('Choose an account').click();
    await add_admin_dialog.getByPlaceholder('Choose an account').fill(simple_user.email);
    await expect(add_admin_dialog.getByText(simple_user.email)).toBeInViewport();
    await add_admin_dialog.getByText('simple-user@playwright.test').click();
    await expect(add_admin_dialog).not.toBeVisible();

    let new_admin_row = page.locator(`css=div.table-row`).filter({ has: page.getByText(simple_user.email) });
    await expect(new_admin_row).toBeVisible();

    await new_admin_row.getByRole("button", { name: 'Delete admin' }).click();
    let delete_admin_dialog = page.getByRole("dialog", { name: "Confirm delete admin" });
    await expect(delete_admin_dialog).toBeVisible();
    await delete_admin_dialog.getByRole("button", { name: 'Confirm delete' }).click();
    await expect(delete_admin_dialog).not.toBeVisible();

    await expect(new_admin_row).not.toBeVisible();
})