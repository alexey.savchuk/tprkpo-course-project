import { generate } from 'generate-password';

export function generatePassword(): string {
    return generate({ length: 8, numbers: true })
}

export function generateEmail(): string {
    let random_part = generate({ length: 5 });
    return `random-${random_part}@playwright.test`
}

export function generateAcademicUserName(): string {
    let random_part = generate({ length: 5 });
    return `Playwright user ${random_part}`
}