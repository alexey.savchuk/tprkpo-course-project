CREATE FUNCTION Research.RegExMatch(
    @pattern NVARCHAR(100),
    @match NVARCHAR(100)
) RETURNS BIT
AS EXTERNAL NAME Matchers.[matchers.Matchers].RegExMatch
;
