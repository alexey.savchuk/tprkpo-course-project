CREATE VIEW Research.ConnectedPeopleByReferences AS
    SELECT PPfrom.subject_id as from_id, PPto.subject_id as to_id, COUNT(*) AS score
    FROM PublicationReferences PR
    JOIN Research.Publications Pfrom on Pfrom.publication_id = PR.from_id
    JOIN Research.ActivePublicationParticipation PPfrom on Pfrom.publication_id = PPfrom.publication_id
    JOIN Research.Publications Pto on Pto.publication_id = PR.from_id
    JOIN Research.ActivePublicationParticipation PPto on Pto.publication_id = PPto.publication_id
    WHERE PPfrom.subject_id != PPto.subject_id
    GROUP BY PPfrom.subject_id, PPto.subject_id
;
