CREATE TABLE Research.SubjectCodes (
    code_id INT IDENTITY (0, 1) NOT NULL PRIMARY KEY,
    code_created_at DATETIME NOT NULL DEFAULT GETDATE(),

    code VARCHAR(8) NOT NULL CHECK (code LIKE '[0-9][0-9].[0-9][0-9].[0-9][0-9]'),
    code_name NVARCHAR(100) NOT NULL CHECK (TRIM(code_name) = code_name),
);
