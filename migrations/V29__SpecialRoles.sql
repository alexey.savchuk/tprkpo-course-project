INSERT INTO Research.ApplicationRoles (role_name)
VALUES ('Administrator');

INSERT INTO Research.OrganisationRoles (role_name)
VALUES ('Administrator');

INSERT INTO Research.PublicationRoles (role_name)
VALUES ('Author');