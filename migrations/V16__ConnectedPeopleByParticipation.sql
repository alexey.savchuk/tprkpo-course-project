CREATE VIEW Research.ConnectedPeopleByParticipation AS
    SELECT PPfrom.subject_id as from_id, PPto.subject_id as to_id, COUNT(*) AS score
    FROM Research.Publications P
    JOIN Research.ActivePublicationParticipation PPfrom on P.publication_id = PPfrom.publication_id
    JOIN Research.ActivePublicationParticipation PPto on P.publication_id = PPto.publication_id
    WHERE PPto.subject_id != PPfrom.subject_id
    GROUP BY PPfrom.subject_id, PPto.subject_id
;
