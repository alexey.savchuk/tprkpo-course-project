CREATE FUNCTION Research.GlobalSearch(
    @search varchar(100)
)
RETURNS TABLE
AS RETURN (
 WITH PublicationSearch(item_id, item_type, item_value, item_score) AS
     (SELECT P.publication_id                             as item_id,
             'publication'                                as item_type,
             P.publication_title                          as item_value,
             Research.FuzzyScore(@search, P.publication_title) as item_score
      FROM Research.Publications P),
 ParticipantSearch(item_id, item_type, item_value, item_score) AS
     (SELECT AP.participant_id                     as item_id,
             'participant'                         as item_type,
             AP.full_name                          as item_value,
             Research.FuzzyScore(@search, AP.full_name) as item_score
      FROM Research.AcademicParticipants AP),
 OrganisationSearch(item_id, item_type, item_value, item_score) AS
     (SELECT O.organisation_id                            as item_id,
             'organisation'                               as item_type,
             O.organisation_name                          as item_value,
             Research.FuzzyScore(@search, O.organisation_name) as item_score
      FROM Research.Organisations O),
 SubjectCodeSearch(item_id, item_type, item_value, item_score) AS
     (SELECT SC.code_id                            as item_id,
             'subject_code'                        as item_type,
             SC.code_name                          as item_value,
             Research.FuzzyScore(@search, SC.code_name) as item_score
      FROM Research.SubjectCodes SC)
SELECT *
FROM (SELECT *
      FROM PublicationSearch
      UNION
      SELECT *
      FROM ParticipantSearch
      UNION
      SELECT *
      FROM OrganisationSearch
      UNION
      SELECT *
      FROM SubjectCodeSearch
        ) as S
)
;