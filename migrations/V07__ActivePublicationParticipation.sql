CREATE VIEW Research.ActivePublicationParticipation AS
    SELECT PP.*
    FROM Research.PublicationParticipation PP
    WHERE PP.participation_state = 'active'
;
