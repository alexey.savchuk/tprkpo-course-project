CREATE VIEW Research.UserOrganisationParticipation AS
    SELECT AP.participant_id, OP.organisation_id, OP.role_id
    FROM Research.AcademicParticipants AP
    JOIN Research.OrganisationParticipation OP on AP.participant_id = OP.subject_id
;
