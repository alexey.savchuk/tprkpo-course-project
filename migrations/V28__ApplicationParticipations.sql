CREATE TABLE Research.ApplicationParticipations (
  user_id NVARCHAR(36) NOT NULL PRIMARY KEY,
  role_id INT NOT NULL FOREIGN KEY REFERENCES Research.ApplicationRoles(role_id),
);
