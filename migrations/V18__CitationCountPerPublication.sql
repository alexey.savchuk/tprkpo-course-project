CREATE VIEW Research.CitationCountPerPublication AS
    SELECT P.publication_id, SUM((case when PR.from_id is not null then 1 else 0 end)) as count
    FROM Research.Publications P
    LEFT JOIN Research.PublicationReferences PR on PR.to_id = P.publication_id
    GROUP BY P.publication_id
;
