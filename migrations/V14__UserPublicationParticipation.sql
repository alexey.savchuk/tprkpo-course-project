CREATE VIEW Research.UserPublicationParticipation AS
    SELECT AP.participant_id, PP.publication_id, PP.role_id
    FROM Research.AcademicParticipants AP
    JOIN Research.PublicationParticipation PP on AP.participant_id = PP.subject_id
;
