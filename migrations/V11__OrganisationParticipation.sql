CREATE TABLE Research.OrganisationParticipation (
    participation_id INT NOT NULL IDENTITY(0, 1) PRIMARY KEY,

    participation_started_at DATE NOT NULL DEFAULT GETDATE(),
    participation_stopped_at DATE NULL,

    organisation_id INT NOT NULL FOREIGN KEY REFERENCES Research.Organisations(organisation_id),
    role_id INT NOT NULL FOREIGN KEY REFERENCES Research.OrganisationRoles(role_id),

    subject_id INT NOT NULL FOREIGN KEY REFERENCES Research.AcademicParticipants(participant_id),
    referrer_id INT NOT NULL FOREIGN KEY REFERENCES Research.AcademicParticipants(participant_id),

    UNIQUE (organisation_id, role_id, subject_id, participation_started_at),

    participation_state NVARCHAR(10) NOT NULL CHECK
      (participation_state IN ('pending', 'active', 'declined')),
);
