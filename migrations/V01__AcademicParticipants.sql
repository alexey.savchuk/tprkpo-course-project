CREATE TABLE Research.AcademicParticipants (
    participant_id INT IDENTITY (0, 1) NOT NULL PRIMARY KEY,
    participant_created_at DATETIME NOT NULL DEFAULT GETDATE(),

    full_name NVARCHAR(100) NOT NULL CHECK (LEN(full_name) > 4 AND TRIM(full_name) = full_name),
);
