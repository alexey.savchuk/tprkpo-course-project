CREATE FUNCTION Research.InlineMax(
    @val1 int,
    @val2 int
) RETURNS INT
AS BEGIN
    IF @val1 > @val2
        RETURN @val1
    RETURN ISNULL(@val2, @val1)
END;
