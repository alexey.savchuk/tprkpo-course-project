CREATE TABLE Research.PublicationParticipation (
    participation_id INT NOT NULL IDENTITY(0, 1) PRIMARY KEY,

    publication_id INT NOT NULL FOREIGN KEY REFERENCES Research.Publications(publication_id),
    role_id INT NOT NULL FOREIGN KEY REFERENCES Research.PublicationRoles(role_id),

    subject_id INT NOT NULL FOREIGN KEY REFERENCES Research.AcademicParticipants(participant_id),
    referrer_id INT NOT NULL FOREIGN KEY REFERENCES Research.AcademicParticipants(participant_id),

    UNIQUE (publication_id, role_id, subject_id),

    participation_state NVARCHAR(10) NOT NULL CHECK
      (participation_state IN ('pending', 'active', 'declined')),
);
