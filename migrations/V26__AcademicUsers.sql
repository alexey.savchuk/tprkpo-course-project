CREATE TABLE Research.AcademicUsers (
  user_id NVARCHAR(36) NOT NULL PRIMARY KEY,
  participant_id INT NOT NULL FOREIGN KEY REFERENCES Research.AcademicParticipants(participant_id),
);
