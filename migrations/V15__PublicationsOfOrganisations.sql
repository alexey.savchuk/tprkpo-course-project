CREATE VIEW Research.PublicationsOfOrganisations AS
    SELECT DISTINCT OP.organisation_id, P.publication_id
    FROM Research.ActivePublicationParticipation PP
    JOIN Research.Publications P on P.publication_id = PP.publication_id
    JOIN Research.OrganisationParticipation OP on PP.subject_id = OP.subject_id
    WHERE (P.published_at BETWEEN OP.participation_started_at AND OP.participation_stopped_at)
       OR (P.published_at > OP.participation_started_at AND OP.participation_stopped_at IS NULL)
;
