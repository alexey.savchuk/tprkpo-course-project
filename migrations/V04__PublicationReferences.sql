CREATE TABLE Research.PublicationReferences (
    from_id INT NOT NULL FOREIGN KEY REFERENCES Research.Publications(publication_id),
    to_id INT NOT NULL FOREIGN KEY REFERENCES Research.Publications(publication_id),
    CHECK (from_id != to_id),
    PRIMARY KEY (from_id, to_id),
);
