CREATE FUNCTION Research.FuzzyScore(
    @query NVARCHAR(100),
    @reference NVARCHAR(100)
) RETURNS INT
AS EXTERNAL NAME Matchers.[matchers.Matchers].FuzzyScore
;
