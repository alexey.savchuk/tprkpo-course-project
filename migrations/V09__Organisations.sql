CREATE TABLE Research.Organisations (
    organisation_id INT IDENTITY (0, 1) NOT NULL PRIMARY KEY,
    organisation_created_at DATETIME NOT NULL DEFAULT GETDATE(),

    organisation_name NVARCHAR(100) NOT NULL CHECK (LEN(organisation_name) > 4 AND TRIM(organisation_name) = organisation_name),
);
