CREATE VIEW Research.CitationCountPerParticipant AS
    SELECT P.participant_id, SUM(COALESCE(CC.count, 0)) as count
    FROM Research.AcademicParticipants P
    LEFT JOIN Research.PublicationParticipation PP on PP.subject_id = P.participant_id
    LEFT JOIN Research.CitationCountPerPublication CC on CC.publication_id = PP.publication_id
    GROUP BY P.participant_id
;
