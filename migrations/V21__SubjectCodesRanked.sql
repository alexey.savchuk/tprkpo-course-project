CREATE VIEW Research.SubjectCodesRanked AS
    SELECT SC.code_id, SUM((case when P.code_id is not null then 1 else 0 end)) as publications
    FROM Research.Publications P
    RIGHT JOIN Research.SubjectCodes SC on SC.code_id = P.code_id
    GROUP BY SC.code_id
;
