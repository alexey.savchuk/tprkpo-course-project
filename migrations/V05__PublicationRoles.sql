CREATE TABLE Research.PublicationRoles (
    role_id INT IDENTITY (0, 1) NOT NULL PRIMARY KEY,
    role_created_at DATETIME NOT NULL DEFAULT GETDATE(),

    role_name NVARCHAR(100) NOT NULL CHECK (LEN(role_name) > 4 AND TRIM(role_name) = role_name),
);
