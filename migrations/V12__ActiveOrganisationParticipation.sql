CREATE VIEW Research.ActiveOrganisationParticipation AS
    SELECT OP.*
    FROM Research.OrganisationParticipation OP
    WHERE OP.participation_stopped_at IS NULL
      AND OP.participation_state = 'active'
;
