CREATE TABLE Research.Publications (
    publication_id INT IDENTITY (0, 1) NOT NULL PRIMARY KEY,
    publication_created_at DATETIME NOT NULL DEFAULT GETDATE(),

    code_id INT NOT NULL FOREIGN KEY REFERENCES Research.SubjectCodes(code_id),

    publication_title NVARCHAR(100) NOT NULL CHECK (LEN(publication_title) > 4 AND TRIM(publication_title) = publication_title),
    published_at DATE NOT NULL,
    CHECK (published_at <= publication_created_at),
);
