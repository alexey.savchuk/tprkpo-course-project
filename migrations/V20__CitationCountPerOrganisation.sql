CREATE VIEW Research.CitationCountPerOrganisation AS
    SELECT O.organisation_id, SUM(COALESCE(CCPP.count, 0)) as count
    FROM Research.Organisations O
    LEFT JOIN Research.PublicationsOfOrganisations POO on O.organisation_id = POO.organisation_id
    LEFT JOIN Research.CitationCountPerPublication CCPP on POO.publication_id = CCPP.publication_id
    GROUP BY O.organisation_id
;
